package com.dp1mtel.desktop;

import com.dp1mtel.desktop.controller.LoginController;
import com.dp1mtel.desktop.di.AppDIKt;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;




public class App extends Application {
    public static final String TAG = App.class.getSimpleName();
    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(LoginController.viewPath));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        AppDIKt.start();
        launch(args);
    }
}
