package com.dp1mtel.desktop.model

import com.dp1mtel.desktop.util.Point

data class Route(
    val orders: List<Order> = listOf(),
    var assignedDriver: User? = null,
    val nodes: List<Point> = listOf()
) {
    val id: Long = 0
}