package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

public class PosCombo extends RecursiveTreeObject<PosCombo>
                      implements IPosElement{
    private ProductCombo combo;
    private Double discount;
    private Integer stock;

    public PosCombo(ProductCombo combo, Integer stock,Double discount) {
        this.combo = combo;
        this.stock = stock;
        this.discount = discount;
    }

    @Override
    public Class getClassType() {
        return this.getClass();
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getDiscount() {
        return discount;
    }

    @Override
    public String getName() {
        return combo.getName();
    }

    @Override
    public String getId() {
        return combo.getId();
    }

    @Override
    public Double getPrice() {
        return Double.parseDouble(combo.getPrice());
    }

    @Override
    public Product getProduct() {
        return null;
    }

    @Override
    public ProductCombo getProductCombo() {
        return combo;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public ProductCombo getCombo() {
        return combo;
    }

    public void setCombo(ProductCombo combo) {
        this.combo = combo;
    }
}
