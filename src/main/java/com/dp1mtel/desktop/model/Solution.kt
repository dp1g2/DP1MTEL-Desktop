package com.dp1mtel.desktop.model

data class Solution(val routes: List<Route> = listOf())
