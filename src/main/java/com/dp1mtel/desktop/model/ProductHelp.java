package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.squareup.moshi.Json;
import javafx.beans.property.SimpleStringProperty;

import java.util.Objects;

public class ProductHelp  extends RecursiveTreeObject<ProductHelp> {
    @Json(name = "quantity") private SimpleStringProperty quantity = new SimpleStringProperty();
    @Json(name = "item") private Item item = new Item();

    public ProductHelp(){
    }


    public ProductHelp(String quantity, Item item){
        setQuantity(quantity);
        setItem(item);
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof ProductHelp)) {
            return false;
        }
        ProductHelp productHelp = (ProductHelp) o;
        return  item.getId().equals(productHelp.item.getId());
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getQuantity() {
        return quantity.get();
    }

    public SimpleStringProperty quantityProperty() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity.set(quantity);
    }
}
