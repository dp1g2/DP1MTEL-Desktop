package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

public class CategoryDiscount extends RecursiveTreeObject<CategoryDiscount> {

    private Long id;
    private String name;
    private Double discount;
    private String start;
    private String end;
    private Category category;
    private Boolean active;


    public CategoryDiscount(){

    }

    public CategoryDiscount(Long id, String name, Double discount, String start, String end, Category category, Boolean active) {
        this.id = id;
        this.name = name;
        this.discount = discount;
        this.start = start;
        this.end = end;
        this.category = category;
        this.active = active;
    }

    public CategoryDiscount(String name, Double discount, String start, String end, Category category, Boolean active) {

        this.name = name;
        this.discount = discount;
        this.start = start;
        this.end = end;
        this.category = category;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
