package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;

public class Employee extends RecursiveTreeObject<Employee> {
    private SimpleStringProperty dni = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty position = new SimpleStringProperty();
    private boolean select = false;
    private long id;

    public Employee(String dni, String name, String position){
        this(0,dni,name,position);
    }

    public Employee(long id, String dni, String name, String position){
        setId(id);
        setDni(dni);
        setName(name);
        setPosition(position);
    }

    public Employee(long id, String dni, String name, String position, Boolean select){
        setId(id);
        setDni(dni);
        setName(name);
        setPosition(position);
        setSelect(select);
    }

    public String getDni() {return dni.get();}

    public SimpleStringProperty dniProperty() {return dni;}

    private void setDni(String dni) {this.dni.set(dni);}

    public String getName() {return name.get(); }

    public SimpleStringProperty nameProperty() {return name;}

    private void setName(String name) { this.name.set(name); }

    public String getPosition() { return position.get(); }

    public SimpleStringProperty positionProperty() { return position; }

    private void setPosition(String position) { this.position.set(position); }

    public long getId() { return id; }

    public void setId(long id) { this.id = id;}

    public Boolean getSelect() {
        return select;
    }

    public void setSelect(Boolean select) {
        this.select = select;
    }
}
