package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductCombo extends RecursiveTreeObject<ProductCombo> {
    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();
    private SimpleStringProperty price = new SimpleStringProperty();
    private SimpleStringProperty active = new SimpleStringProperty();
    private List<ProductComboHelp> listProducts = new ArrayList<>();

    public ProductCombo(String id, String name,
                        String description, String price,
                        String active, List<ProductComboHelp> listProducts) {
        setId(id);
        setName(name);
        setDescription(description);
        setPrice(price);
        setActive(active);
        setListProducts(listProducts);
    }
    public ProductCombo(String id) {
        setId(id);
        setName("name");
        setDescription("description");
        setPrice("0.0");
        setActive("true");
        setListProducts(new ArrayList<>());
    }

    public ProductCombo() {
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getPrice() {
        return price.get();
    }

    public SimpleStringProperty priceProperty() {
        return price;
    }

    public void setPrice(String price) {
        this.price.set(price);
    }

    public String getActive() {
        return active.get();
    }

    public SimpleStringProperty activeProperty() {
        return active;
    }

    public void setActive(String active) {
        this.active.set(active);
    }

    public List<ProductComboHelp> getListProducts() {
        return listProducts;
    }

    public void setListProducts(List<ProductComboHelp> listProducts) {
        this.listProducts = listProducts;
    }
}
