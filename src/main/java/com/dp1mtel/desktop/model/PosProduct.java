package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

public class PosProduct extends RecursiveTreeObject<PosProduct>
                        implements IPosElement{

    private Product product;
    private Double discount;
    private Integer stock;

    public PosProduct() {}

    public PosProduct(Product product, Integer stock, Double discount) {
        this.product = product;
        this.stock = stock;
        this.discount = discount;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public Product getProduct() {
        return product;
    }

    @Override
    public ProductCombo getProductCombo() {
        return null;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getDiscount() {
        return discount;
    }

    @Override
    public String getName() {
        return product.getName();
    }

    @Override
    public String getId() {
        return product.getId();
    }

    @Override
    public Double getPrice() {
        return Double.parseDouble(product.getPrice());
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    @Override
    public Class getClassType() {
        return this.getClass();
    }
}
