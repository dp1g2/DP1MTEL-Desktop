package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Role extends RecursiveTreeObject<Role> {
    public static Role testRole = new Role("name", "desc", Collections.emptyList());;
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();
    private SimpleListProperty<Permission> permissions = new SimpleListProperty<>();
    private long id;

    public Role(String name, String description, Collection<Permission> permissions) {
        this(0, name, description, permissions);
    }

    public Role(long id, String name, String description, Collection<Permission> permissions) {
        setId(id);
        setName(name);
        setDescription(description);
        setPermissions(permissions);
    }

    public Role(String name) {
        setName(name);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public void setPermissions(Collection<Permission> permissions) {
        this.permissions.set(FXCollections.observableArrayList(permissions));
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleListProperty<Permission> permissionsProperty() {
        return permissions;
    }

    public List<Permission> getPermissions() {
        return permissions.get();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString(){
        return getName();
    }
}
