package com.dp1mtel.desktop.model

import java.awt.geom.Point2D
import java.util.*

data class Order(
        var id: Long = 0,
        var seller: User,
//        @Transien var customer: Customer,
        var customer: Customer,
        var status: OrderStatus,
        var store: Shop,
        var orderDetails: List<OrderDetail> = listOf(),
        val volume: Double = 0.0,
        var deliveryAddress: UserAddress? = null,
        var expectedDeliveryDate: Date? = Date(),
        var userReadyTime: Double = 0.0,
        var userDueTime: Double = 0.0,
        var userServiceTime: Double = 0.0
) {
    val coordinates = deliveryAddress?.coordinates ?: Point2D.Double(0.0, 0.0)
    var recipientName: String? = null
    var recipientDocument: String? = null
    var recipientDedication: String? = null
    var reservationCode: String? = null
    var reservationTime: Date? = null
    var cancellationReason: String? = null
    var createdAt: Date? = null

    constructor(id: Long = 0,
                seller: User,
                customer: Customer,
                recipientName: String? = null,
                recipientDocument: String? = null,
                recipientDedication: String? = null,
                status: OrderStatus,
                store: Shop,
                orderDetails: List<OrderDetail> = listOf(),
                volume: Double = 0.0,
                deliveryAddress: UserAddress? = null,
                expectedDeliveryDate: Date? = Date(),
                userReadyTime: Double = 0.0,
                userDueTime: Double = 0.0,
                userServiceTime: Double = 0.0,
                reservationCode: String? = null,
                reservationTime: Date? = null,
                cancellationReason: String? = null,
                createdAt: Date? = null): this(
            id,
            seller,
            customer,
            status,
            store,
            orderDetails,
            volume,
            deliveryAddress,
            expectedDeliveryDate,
            userReadyTime,
            userDueTime,
            userServiceTime
    ) {
        this.recipientName = recipientName
        this.recipientDocument = recipientDocument
        this.recipientDedication = recipientDedication
        this.reservationCode = reservationCode
        this.reservationTime = reservationTime
        this.cancellationReason = cancellationReason
        this.createdAt = createdAt
    }


    override fun hashCode(): Int {
        return 142 * id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Order

        if (id != other.id) return false

        return true
    }

    val total: Double
        get() {
            return orderDetails.sumByDouble { orderDetail -> orderDetail.total }
        }

    val cancelable: Boolean
        get() {
            return OrderStatusEnum.valueOf(status.slug) == OrderStatusEnum.CANCELED
        }

    val refundable: Boolean
        get() {
            return OrderStatusEnum.valueOf(status.slug) in listOf(
                    OrderStatusEnum.NON_COMPLETED,
                    OrderStatusEnum.COMPLETED
            )
        }

    val payable: Boolean
        get() {
            return OrderStatusEnum.valueOf(status.slug) == OrderStatusEnum.RESERVED
        }
}
