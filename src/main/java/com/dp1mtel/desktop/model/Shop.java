package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dp1mtel.desktop.model.Shop.Status.TRUE;
import static com.dp1mtel.desktop.model.Shop.Status.FALSE;

public class Shop extends RecursiveTreeObject<Shop> {
    public static final Map<Status, String> statusMap;
    static {
        statusMap = new HashMap<>();
        statusMap.put(TRUE, "Habilitado");
        statusMap.put(FALSE, "Deshabilitado");
    }


    private long id;

    private Point2D coordinates = new Point2D.Double(0, 0);

    private SimpleStringProperty address = new SimpleStringProperty();
    private SimpleStringProperty district = new SimpleStringProperty();
    private SimpleStringProperty nemployees = new SimpleStringProperty();
    private SimpleStringProperty numVehicles = new SimpleStringProperty();
    private SimpleStringProperty numCapacity = new SimpleStringProperty();
    private SimpleBooleanProperty status = new SimpleBooleanProperty();
    private SimpleListProperty<User> users = new SimpleListProperty<>();



    private SimpleStringProperty vehiclesCapacity = new SimpleStringProperty();

    // cambiar permission por Employee. hay que crear su modelo.
//
//    public Shop(String address,String district, Boolean status, Collection<Employee> users){
//        this(0, "", address, district,"","", status, users);
//    }
//    public Shop(String manager, String address, String district, String nemployees, String nvehicles,
//                Boolean status, Collection<Employee> users){
//        this(0, manager,address,district,nemployees,nvehicles, status, users);
//    }

    public Shop(String district) {
        setAddress("");
        setDistrict(district);
        setNemployees("0");
        setNumVehicles("0");
        setNumCapacity("0");
        setStatus(true);
    }

    public Shop(long id, String address, String district, String nemployees, String nvehicles,
                Boolean status) {
        setId(id);

        setAddress(address);
        setDistrict(district);
        setNemployees(nemployees);
        setNumVehicles(nvehicles);
        setNumCapacity("0");
        setStatus(status);
        //setUsers(users);
    }
    /*
    public Shop(String address,String district, Boolean status, Collection<User> users){
        this(0, "", address, district,"","", status, users);
    }
    */

    /*
    public Shop(String manager, String address, String district, String nemployees, String numVehicles,
                    Boolean status, Collection<User> users){
        this(0, manager,address,district,nemployees,numVehicles, status, users);
    }
    */

    /*
    public Shop(long id, String manager, String address, String district, String nemployees, String numVehicles,
                Boolean status, Collection<User> users){
        setId(id);
        setManager(manager);
        setAddress(address);
        setDistrict(district);
        setNemployees(nemployees);
        setNumVehicles(numVehicles);
        setStatus(status);
        setUsers(users);

    }
    */

    /*
    public Shop(long id, String manager, String address, String district, String nemployees, String numVehicles,

                Boolean status){
        setId(id);
        setAddress(address);
        setDistrict(district);
        setNemployees(nemployees);
        setNumVehicles(numVehicles);
        setStatus(status);
    }*/

    public Shop( long id,
                 String address,
                 String district,
                 Boolean status,
                 String nemployees,
                 String numVehicles,
                 String numCapacity,
                 Collection<User> users){
        setId(id);
        setAddress(address);
        setDistrict(district);
        setNemployees(nemployees);
        setNumVehicles(numVehicles);
        setStatus(status);

        setNumCapacity(numCapacity);
        setUsers(users);
    }

    public Shop( long id,
                 String address,
                 String district,
                 Boolean status,
                 String nemployees,
                 String numVehicles,
                 String numCapacity){
        setId(id);
        setAddress(address);
        setDistrict(district);
        setNemployees(nemployees);
        setNumVehicles(numVehicles);
        setStatus(status);
        setNumCapacity(numCapacity);
    }

    public Shop( String address,
                 String district,
                 String status,
                 String nemployees,
                 String numVehicles,
                 String numCapacity,
                 Collection<User> users){
        setAddress(address);
        setDistrict(district);
        setNemployees(nemployees);
        setNumVehicles(numVehicles);
        setStatusString(status);
        setNumCapacity(numCapacity);
        setUsers(users);
    }

    public Shop( long id,
                 String address,
                 String district,
                 String status,
                 String nemployees,
                 String numVehicles,
                 String numCapacity){
        setId(id);
        setAddress(address);
        setDistrict(district);
        setNemployees(nemployees);
        setNumVehicles(numVehicles);
        setStatusString(status);
        setNumCapacity(numCapacity);
    }

    /*
    public Shop( long id, String address, String district, String nemployees, String numVehicles,
                String status, String numCapacity, Collection<User> users){
        setId(id);
        setAddress(address);
        setDistrict(district);
        setNemployees(nemployees);
        setNumVehicles(numVehicles);
        setStatusString(status);
        setNumCapacity(numCapacity);
        setUsers(users);
    }*/

//    private void setManager(String manager){this.manager.set(manager);}
    public void setAddress(String address){this.address.set(address);}
    public void setDistrict(String district){this.district.set(district);}
    public void setStatus(Boolean status){this.status.set(status);}
    public void setNemployees(String nemployees){this.nemployees.set(nemployees);}
    public void setNumVehicles(String numVehicles){this.numVehicles.set(numVehicles);}
    public void setNumCapacity(String numCapacity){this.numCapacity.set(numCapacity);}

    public void setUsers(Collection<User> listemployee) {
        this.users.set(FXCollections.observableArrayList(listemployee));
    }


    public String getAddress() {return address.get(); }
    public String getDistrict(){return district.get();}
    public Boolean getStatus(){return status.get();}
    public String getNemployees(){return nemployees.get(); }
    public String getNumVehicles(){return numVehicles.get(); }
    public String getNumCapacity(){return numCapacity.get(); }
    public List<User> getListemployee (){return users.get();}


    public SimpleStringProperty addressProperty() {return address;}
    public SimpleStringProperty districtProperty() {return district;}
    public SimpleBooleanProperty statusProperty() {return status;}

    public SimpleStringProperty statusPropertyToString(){
        if(getStatus())
            return new SimpleStringProperty("Habilitado");
        else
            return new SimpleStringProperty("Deshabilitado");
    };

    public SimpleStringProperty nemployeesProperty() {return nemployees;}
    public SimpleStringProperty numVehiclesProperty() {return numVehicles;}
    public SimpleStringProperty numCapacityProperty() {return numCapacity;}

    public SimpleListProperty<User> listemployeeProperty(){ return users;}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setEmployee(Collection<User> employees) {
        this.users.set(FXCollections.observableArrayList(employees));
    }

    public Point2D getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Point2D coordinates) {
        this.coordinates = coordinates;
    }

    public enum Status {
        TRUE,
        FALSE,
    }

    public void setStatusString(String status){
        if (status.equals("Habilitado"))
            setStatus(true);
        else
            setStatus(false);
    }
    @Override
    public String toString(){
        return getDistrict()+" - "+getAddress();
    }
}
