package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.List;

public class MassiveStorageResponse extends RecursiveTreeObject<MassiveStorageResponse> {

    @Json(name = "correctlyInserted") private String correctlyInserted;
    @Json(name = "badlyInserted") private String badlyInserted;
    @Json(name = "permissions") private List<Integer> permissions = new ArrayList<>();

    public MassiveStorageResponse(String correctlyInserted, String badlyInserted, List<Integer> permissions) {
        this.correctlyInserted = correctlyInserted;
        this.badlyInserted = badlyInserted;
        this.permissions = permissions;
    }

    public MassiveStorageResponse() {
        this.permissions = new ArrayList<>();
    }

    public String getCorrectlyInserted() {
        return correctlyInserted;
    }

    public void setCorrectlyInserted(String correctlyInserted) {
        this.correctlyInserted = correctlyInserted;
    }

    public String getBadlyInserted() {
        return badlyInserted;
    }

    public void setBadlyInserted(String badlyInserted) {
        this.badlyInserted = badlyInserted;
    }

    public List<Integer> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Integer> permissions) {
        this.permissions = permissions;
    }
}
