package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.squareup.moshi.Json;
import javafx.beans.property.SimpleStringProperty;

public class Districts extends RecursiveTreeObject<Districts> {
    @Json(name = "id") private long id;
    @Json(name = "name")private SimpleStringProperty name = new SimpleStringProperty();

    public Districts(){
    }

    public Districts(long id , String name){
        setId(id);
        setName(name);
    }

    public long getId() {return id;}

    public void setId(long id) {this.id = id;}

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }
}
