package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import javafx.beans.property.SimpleStringProperty;

public class InventoryOperationType extends RecursiveTreeObject<InventoryOperationType> {

    /*
    * Solo los atributos que sean de tipo SimpleLoqueseaProperty se mostraran en tablas
    * Los atributos que no son de ese tipo pueden ser normales
    * */


    private Long id;
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();

    private int multiplier ;

    public enum TypeMov {
        IN,
        OUT
    }

    public InventoryOperationType(String name) {
        setId(Long.parseLong("0"));
        setDescription("");
        setName(name);
        setMultiplier(0);
    }

    public InventoryOperationType(Long id, String name, String description, int multiplier) {
       setId(id);
       setDescription(description);
       setName(name);
       setMultiplier(multiplier);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String toString(){
        return getName();
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }
}
