package com.dp1mtel.desktop.model;

public interface IPosElement {
    public Class getClassType();
    public Integer getStock();
    public Double getDiscount();
    public String getName();
    public String getId();
    public Double getPrice();
    public Product getProduct();
    public ProductCombo getProductCombo();
}
