package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;

public class Permission extends RecursiveTreeObject<Permission> {

    private long id;
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();
    private Boolean select = false;



    public Permission(long id, String name, String description, Boolean select ) {
        setId(id);
        setName(name);
        setDescription(description);
        setSelect(select);
    }


    public Permission(String name ) {
        setName(name);
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Permission)) {
            return false;
        }
        Permission permission = (Permission) o;
        return name.toString().equals(permission.getName());
    }

    private void setName(String name) {
        this.name.set(name);
    }

    private void setDescription(String description) {
        this.description.set(description);
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public String getDescription() {
        return description.get();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getSelect() {
        return select;
    }

    public void setSelect(Boolean select) {
        this.select = select;
    }
}
