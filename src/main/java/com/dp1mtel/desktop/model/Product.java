package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import io.reactivex.Observable;
import javafx.beans.property.*;

import java.util.ArrayList;
import java.util.List;

public class Product extends RecursiveTreeObject<Product> {

    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();
    private SimpleStringProperty price = new SimpleStringProperty();
    private SimpleStringProperty active = new SimpleStringProperty();

    private Category category = new Category();
    private List<ProductHelp> listItems = new ArrayList<>();
    private Boolean select = false;
    private int stock = 0;



    public Product() {}

    public Product(String id, String name, String description,String price,
                   String active,Category category,List<ProductHelp> listItems) {
        setId(id);
        setName(name);
        setDescription(description);
        setPrice(price);
        setActive(active);
        setCategory(category);
        setListItems(listItems);
    }

    public Product(String id) {
        setId(id);
        setName("");
        setDescription("");
    }

    public Product(String id,int code){
        setId(id);
        setName("");
        setDescription("");
        setActive("true");
        setPrice("0");
        setCategory(new Category(0)); // This method is only for mocking...
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public Product(String id,
                   String name,
                   String description,
                   String price,
                   String active,
                   Category productCategory,
                   List<ProductHelp> listItems,
                   int stock) {
        this(id, name, description, price, active, productCategory, listItems);
        this.stock = stock;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getPrice() {
        return price.get();
    }

    public SimpleStringProperty priceProperty() {
        return price;
    }

    public void setPrice(String price) {
        this.price.set(price);
    }

    public String getActive() {
        return active.get();
    }

    public SimpleStringProperty activeProperty() {
//        if (active.equals("true")){
//            return new SimpleStringProperty("Activo");
//        }
//        else return new SimpleStringProperty("Inactivo");
        return active;
    }

    public void setActive(String active) {
        this.active.set(active);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<ProductHelp> getListItems() {
        return listItems;
    }

    public void setListItems(List<ProductHelp> listItems) {
        this.listItems = listItems;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Boolean getSelect() {
        return select;
    }

    public void setSelect(Boolean select) {
        this.select = select;
    }
}
