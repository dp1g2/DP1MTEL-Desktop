package com.dp1mtel.desktop.model;

import com.dp1mtel.desktop.util.StringUtils;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.dp1mtel.desktop.model.User.Gender.FEMALE;
import static com.dp1mtel.desktop.model.User.Gender.MALE;
import static com.dp1mtel.desktop.model.User.Gender.UNK;

public class User extends RecursiveTreeObject<User> {
    public static final Map<Gender, String> genderMap;
    static {
        genderMap = new HashMap<>();
        genderMap.put(MALE, "Masculino");
        genderMap.put(FEMALE, "Femenino");
        genderMap.put(UNK,"?");
    }

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private SimpleStringProperty firstName = new SimpleStringProperty();
    private SimpleStringProperty lastName = new SimpleStringProperty();
    private SimpleStringProperty dni = new SimpleStringProperty();
    private SimpleStringProperty email = new SimpleStringProperty();
    private SimpleStringProperty dob = new SimpleStringProperty();
    private SimpleStringProperty gender = new SimpleStringProperty();
    private SimpleStringProperty password = new SimpleStringProperty();

    private Boolean select = false;



    private SimpleBooleanProperty active = new SimpleBooleanProperty();
    private SimpleObjectProperty<Role> role = new SimpleObjectProperty<Role>();
    private SimpleObjectProperty<Shop> shop = new SimpleObjectProperty<Shop>();
    private long id;

    public User(
                String firstName,
                String lastName,
                String dni,
                String email,
                Date dob,
                String gender,
                String password,
                Role role,
                Boolean active,
                Shop shop
    ) {
        setId(0);
        setFirstName(firstName);
        setLastName(lastName);
        setDni(dni);
        setEmail(email);
        setDob(dob);
        setGender(gender);
        setPassword(password);
        setRole(role);
        setActive(active);

        setStore(shop);
    }



    public User(long id,
                String firstName,
                String lastName,
                String dni,
                String email,
                Date dob,
                String gender,
                String password,
                Role role,
                Boolean active,
                Shop shop
    ) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setDni(dni);
        setEmail(email);
        setDob(dob);
        setGender(gender);
        setPassword(password);
        setRole(role);
        setActive(active);
        setStore(shop);

    }

    public User(long id,
                String firstName,
                String lastName,
                String dni,
                String email,
                Date dob,
                String gender,
                String password,
                Role role,
                Boolean active,
                Shop shop,
                String address) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setDni(dni);
        setEmail(email);
        setDob(dob);
        setGender(gender);
        setPassword(password);
        setRole(role);
        setActive(active);
        setStore(shop);

    }

    public User(String firstName,
                String lastName,
                String dni,
                String email,
                Date dob,
                String gender,
                String password,
                Role role,
                Boolean active,
                Shop shop,
                String address) {

        setFirstName(firstName);
        setLastName(lastName);
        setDni(dni);
        setEmail(email);
        setDob(dob);
        setGender(gender);
        setPassword(password);
        setRole(role);
        setActive(active);
        setStore(shop);

    }

    public User(String firstName,
                String lastName,
                String dni,
                String email,
                Date dob,
                String gender,
                Role role) {

        setFirstName(firstName);
        setLastName(lastName);
        setDni(dni);
        setEmail(email);
        setDob(dob);
        setGender(gender);
        setPassword("password");
        setRole(role);
        setActive(true);
    }
    public User(Shop shop,String firstName,
                String lastName,
                String dni,
                String email,
                Date dob,
                String gender,
                Role role) {

        setFirstName(firstName);
        setLastName(lastName);
        setDni(dni);
        setEmail(email);
        setDob(dob);
        setGender(gender);
        setPassword("password");
        setRole(role);
        setActive(true);
        setStore(shop);
    }


    public Shop getStore() {
        return shop.get();
    }

    public SimpleObjectProperty<Shop> storeProperty() {
        return shop;
    }

    public void setStore(Shop shop) {
        this.shop.set(shop);
    }
    public String getFirstName() {
        return firstName.get();
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }




    public Date getDob() throws ParseException {
        return dateFormat.parse(dob.get());
    }

    public void setDob(Date dob) {
        this.dob.set(dateFormat.format(dob));

    }

    public String getGender() {
        return gender.get();
    }

    public SimpleStringProperty genderProperty() {
        return gender;
    }

    public void setGender(String gender) {
        if (gender == null || !genderMap.containsValue(StringUtils.capitalize(gender))) {
            throw new IllegalArgumentException("Invalid gender");
        }
        this.gender = new SimpleStringProperty(gender);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public SimpleObjectProperty<Role> roleProperty() {
        return role;
    }

    public Role getRole() {
        return role.get();
    }

    public void setShop(Shop shop) {
        this.shop.set(shop);
    }


    public SimpleObjectProperty<Shop> shopProperty() {
        return shop;
    }

    public Shop getShop() {
        return shop.get();
    }

    public void setRole(Role role) {
        this.role.set(role);
    }


    public SimpleStringProperty fullNameProperty() {
        return new SimpleStringProperty(getFullName());
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getDni() {
        return dni.get();
    }

    public SimpleStringProperty dniProperty() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni.set(dni);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getSelect() {
        return select;
    }

    public void setSelect(Boolean select) {
        this.select = select;
    }

    public enum Gender {
        MALE,
        FEMALE,
        UNK
    }


    public boolean isActive() {
        return active.get();
    }

    public SimpleBooleanProperty activeProperty() {
        return active;
    }

    public void setActive(boolean active) {
        this.active.set(active);
    }

    public void print(){
        try{
            System.out.println("Nombre " + getFullName());
            System.out.println("DNI " + getDni());
            System.out.println("Email " + getEmail());
            System.out.println("Password" + getPassword());
            System.out.println("Fecha " + getDob().toString());
            System.out.println("Role " + getRole().getName());
            System.out.println("Sexo " + getGender());
        }catch(Exception e){
            System.err.println("Imprime error: " + e.getMessage());
        }

    }

    @Override
    public String toString() {
        return getFullName();
    }

    @Override
    public int hashCode() {
        return super.hashCode() * (int) getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User u = (User) obj;
        return id == u.id;
    }
}
