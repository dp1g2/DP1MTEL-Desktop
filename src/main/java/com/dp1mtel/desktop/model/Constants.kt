package com.dp1mtel.desktop.model

class Constants {
    companion object {
        private val _instance = Constants()
        @JvmField val IGV = _instance.IGV
        @JvmField val RUC = _instance.RUC
        @JvmField val companyName = _instance.companyName
        @JvmField val companyOwner = _instance.companyOwner
        @JvmField val companyAddress = _instance.companyAddress
    }
    var IGV = 0.18
    var RUC = "10254869567"
    var companyName = "MargaritaTEL"
    var companyOwner = "MargaritaTEL - SAC"
    var companyAddress = "Central. Tomas Valle, Cercado de Lima 15311"

    constructor()

    constructor(
            igv: Double,
            ruc: String,
            companyName: String,
            companyOwner: String,
            companyAddress: String
    ): this() {
        this.IGV = igv
        this.RUC = ruc
        this.companyName = companyName
        this.companyOwner = companyOwner
        this.companyAddress = companyAddress
    }
}