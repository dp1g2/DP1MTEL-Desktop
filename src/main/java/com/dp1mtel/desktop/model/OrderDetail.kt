package com.dp1mtel.desktop.model

class OrderDetail {
    var id: Long = 0
    var product: Product? = null
    var combo: ProductCombo? = null
    var quantity: Int = 0
    var unitPrice: Double = 0.0
    // Assuming discount
    var discount: Double = 0.0
    val total: Double
        get() = quantity * unitPrice * (1 - discount)
}
