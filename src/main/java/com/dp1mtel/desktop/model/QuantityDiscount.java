package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import java.util.HashMap;
import java.util.Map;

public class QuantityDiscount  extends RecursiveTreeObject<QuantityDiscount> {


    private Long id;
    private String name;
    private Double discount;
    private ProductCombo productCombo;
    private String startDate;
    private String endDate;



    public QuantityDiscount() {

    }

    public QuantityDiscount(Long id, String name, Double discount, ProductCombo productCombo, String startDate, String endDate) {
        this.id = id;
        this.name = name;
        this.discount = discount;
        this.productCombo = productCombo;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public QuantityDiscount(Long id, String name,  ProductCombo productCombo, String startDate, String endDate) {
        this.id = id;
        this.name = name;
        this.productCombo = productCombo;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public ProductCombo getProductCombo() {
        return productCombo;
    }

    public void setProductCombo(ProductCombo productCombo) {
        this.productCombo = productCombo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}