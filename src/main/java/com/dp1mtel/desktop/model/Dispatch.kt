package com.dp1mtel.desktop.model

data class Dispatch(val routes: List<Route>)