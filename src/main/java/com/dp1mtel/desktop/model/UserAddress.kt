package com.dp1mtel.desktop.model

import java.awt.geom.Point2D

data class UserAddress(
        val address: String,
        val name: String? = null,
        val coordinates: Point2D = Point2D.Double(0.0, 0.0)
) {
    var id: Long = 0

    override fun toString(): String {
        return when (name) {
            null, "" -> address
            else -> "$name - $address"
        }
    }
}