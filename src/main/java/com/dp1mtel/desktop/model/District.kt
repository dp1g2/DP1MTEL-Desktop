package com.dp1mtel.desktop.model

data class District(
        val id: Long = 0,
        val name: String
)