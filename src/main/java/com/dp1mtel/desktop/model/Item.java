package com.dp1mtel.desktop.model;

import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;

public class Item extends RecursiveTreeObject<Item> {

    private SimpleStringProperty id =  new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();
    private SimpleBooleanProperty expires = new SimpleBooleanProperty();
    private SimpleDoubleProperty volume = new SimpleDoubleProperty();
    private SimpleStringProperty active = new SimpleStringProperty();


    //Auxiliares
    private int quantity;
    private boolean select;
    private String motive;
    private String lot;
    private SimpleStringProperty expirationDate = new SimpleStringProperty();
    private LocalDate localDate;


    public Item(String id, String name, String description, Boolean expires, Double volume) {
        setId(id);
        setDescription(description);
        setName(name);
        setExpires(expires);
        setVolume(volume);
    }

    public Item(String id, String name, String description, String active, Boolean expires, Double volume) {
        setId(id);
        setDescription(description);
        setName(name);
        setActive(active);
        setExpires(expires);
        setVolume(volume);
    }

    public Item(String id, String name, String description, Boolean expires) {
        setId(id);
        setDescription(description);
        setName(name);
        setExpires(expires);
    }

    public Item(String id, String name, String description, String active,Boolean expires) {
        setId(id);
        setDescription(description);
        setName(name);
        setActive(active);
        setExpires(expires);
    }

    public Item(String id, String name, String description) {
        setId(id);
        setDescription(description);
        setName(name);
        setActive("true");
    }

    public Item(String name, String description) {
        setDescription(description);
        setName(name);
    }

    public Item(String id) {
        setId(id);
        setName("");
        setActive("true");
        setDescription("");
    }

    public Item() {
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }


    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }


    public boolean isExpires() {
        return expires.get();
    }

    public SimpleBooleanProperty expiresProperty() {
        return expires;
    }

    public void setExpires(boolean expires) {
        this.expires.set(expires);
    }

    public double getVolume() {
        return volume.get();
    }

    public SimpleDoubleProperty volumeProperty() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume.set(volume);
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getMotive() {
        return motive;
    }

    public void setMotive(String motive) {
        this.motive = motive;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getExpirationDate() {
        return DateExtensions.toDate(localDate);
    }

    public SimpleStringProperty expirationDateProperty() {
        return expirationDate;
    }

    public void setExpirationDate(Date date) {
        localDate = DateExtensions.toLocalDate(date);
        this.expirationDate.set(localDate.toString());
    }

    public String getActive() {
        return active.get();
    }

    public SimpleStringProperty activeProperty() {
        return active;
    }

    public void setActive(String active) {
        this.active.set(active);
    }
}
