package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.squareup.moshi.Json;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductComboHelp extends RecursiveTreeObject<ProductComboHelp> {

    @Json(name = "quantity") private SimpleStringProperty quantity = new SimpleStringProperty();
    @Json(name = "product") private Product product = new Product();

    public ProductComboHelp(){
    }

    public ProductComboHelp(String quantity, Product product) {
        setQuantity(quantity);
        setProduct(product);
    }


    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof ProductComboHelp)) {
            return false;
        }
        ProductComboHelp productComboHelp = (ProductComboHelp) o;
        return  product.getId().equals(productComboHelp.getProduct().getId());
    }


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getQuantity() {
        return quantity.get();
    }

    public SimpleStringProperty quantityProperty() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity.set(quantity);
    }
}
