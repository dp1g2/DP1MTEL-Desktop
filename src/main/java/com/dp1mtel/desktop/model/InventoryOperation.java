package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dp1mtel.desktop.model.InventoryOperation.TypeProd.ITEM;
import static com.dp1mtel.desktop.model.InventoryOperation.TypeProd.PRODUCTO;


public class InventoryOperation extends RecursiveTreeObject<InventoryOperation> {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static final Map<TypeProd, String> typeProdMap;
    static {
        typeProdMap = new HashMap<>();
        typeProdMap.put(ITEM, "Item");
        typeProdMap.put(PRODUCTO, "Producto");
    }

    public enum TypeProd {
        ITEM,
        PRODUCTO

    }

    private Long id;
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();
    private transient SimpleStringProperty date = new SimpleStringProperty();
    private SimpleIntegerProperty quantity = new SimpleIntegerProperty();
    private SimpleObjectProperty<Item>  item = new SimpleObjectProperty<Item>();
    private SimpleObjectProperty<Lot> lot = new SimpleObjectProperty<Lot>();
    private SimpleObjectProperty<InventoryOperationType> typeOperation = new SimpleObjectProperty<InventoryOperationType>();
    private SimpleObjectProperty<User> user = new SimpleObjectProperty<User>();
    private SimpleObjectProperty<Shop> store = new SimpleObjectProperty<Shop>();


    private int reduceQuantity;

    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public InventoryOperation(){

    }

//    //Para el post tipo salida
//
//    public InventoryOperation(
//            String description,
//            int quantity,
//            Item item,
//            Shop store,
//            User user,
//            Lot lot,
//            InventoryOperationType typeOperation) {
//
//
//        setId(id);
//        setDescription(description);
//        setQuantity(quantity);
//        setItem(item);
//        setUser(user);
//        setLot(lot);
//        setStore(store);
//        setTypeOperation(typeOperation);
//
//    }
//
//    //Para el post tipo entrada
//    public InventoryOperation(
//                              String description,
//                              int quantity,
//                              Item item,
//                              Shop store,
//                              User user,
//                              InventoryOperationType typeOperation) {
//
//
//        setDescription(description);
//        setQuantity(quantity);
//        setItem(item);
//        setUser(user);
//        setStore(store);
//        setTypeOperation(typeOperation);
//
//    }

    // para el get
    public InventoryOperation(Long id,
                              String description,
                              Date date,
                              int quantity,
                              Item item,
                              Shop store,
                              User user,
                              Lot lot,
                              InventoryOperationType typeOperation) {

        setId(id);
        setDescription(description);
        setQuantity(quantity);
        setItem(item);
        setLot(lot);
        setDate(date);
        setUser(user);
        setStore(store);
        setTypeOperation(typeOperation);

    }
//
//    public InventoryOperation( String description,
//                               Date date,
//                               int quantity,
//                               Item item,
//                               Shop store,
//                               User user,
//                               InventoryOperationType inventoryOperationType){
//
//        setDescription(description);
//        setDate(date);
//        setQuantity(quantity);
//        setItem(item);
//        setStore(store);
//        setUser(user);
//        setTypeOperation(inventoryOperationType);
//
//    }

    public Lot getLot() {
        return lot.get();
    }

    public SimpleObjectProperty<Lot> lotProperty() {
        return lot;
    }

    public void setLot(Lot lot) {
        this.lot.set(lot);
    }

    public User getUser() {
        return user.get();
    }

    public SimpleObjectProperty<User> userProperty() {
        return user;
    }

    public void setUser(User user) {
        this.user.set(user);
    }

//    public SimpleStringProperty dateProperty() {
//        return date;
//    }


    public Date getDate() throws ParseException {
        return dateFormat.parse(date.get());
    }

    public void setDate(Date date) {
        this.date.set(dateFormat.format(date));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Shop getStore() {
        return store.get();
    }

    public SimpleObjectProperty<Shop> storeProperty() {
        return store;
    }

    public void setStore(Shop store) {
        this.store.set(store);
    }

    public int getQuantity() {
        return quantity.get();
    }

    public SimpleIntegerProperty quantityProperty() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity.set(quantity);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public Item getItem() {
        return item.get();
    }

    public SimpleObjectProperty<Item> itemProperty() {
        return item;
    }

    public void setItem(Item item) {
        this.item.set(item);
    }

    public InventoryOperationType getTypeOperation() {
        return typeOperation.get();
    }

    public SimpleObjectProperty<InventoryOperationType> typeOperationProperty() {
        return typeOperation;
    }

    public void setTypeOperation(InventoryOperationType typeOperation) {
        this.typeOperation.set(typeOperation);
    }



    public static boolean containsItem(final List<InventoryOperation> list, final String item_id){
//        return list.stream().filter(o -> o.getItem().getId().equals(item_id)).findFirst().isPresent();
        if(list==null) return false;
        return list.stream().anyMatch(inventoryOperation -> inventoryOperation.getItem().getId().equals(item_id));
    }

    public int getReduceQuantity() {
        return reduceQuantity;
    }

    public void setReduceQuantity(int reduceQuantity) {
        this.reduceQuantity = reduceQuantity;
    }

}
