package com.dp1mtel.desktop.model;

import com.dp1mtel.desktop.util.DecimalFormatter;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dp1mtel.desktop.model.Bill.DocumentType.BOLETA;
import static com.dp1mtel.desktop.model.Bill.DocumentType.FACTURA;
import static com.dp1mtel.desktop.model.Bill.DocumentType.NOTA_CREDITO;

;


public class Bill {
    private Long    id;
    private Double tax;
    private String  type;
    private Order   order;
    private Double  subtotal;
    private String  date;
    private String  serie;
    private String  recipientRuc;
    private String  recipientName;
    private String  recipientAddress;
    private String  recipientBussiness;

    public String getRecipientBussiness() {
        return recipientBussiness;
    }

    public void setRecipientBussiness(String recipientBussiness) {
        this.recipientBussiness = recipientBussiness;
    }

    //esto mandara el back
    private Long codeFactura;
    private Long codeNotaCredito;
    private Long codeBoleta;

    private Long code;


    public static final Map<DocumentType, String> docTypeMap;
    static {
        docTypeMap = new HashMap<>();
        docTypeMap.put(FACTURA, "Factura");
        docTypeMap.put(BOLETA, "Boleta");
        docTypeMap.put(NOTA_CREDITO, "Nota de Credito");
    }



    public Bill(Long codeBoleta, Long codeFactura, Long codeNotaCredito){

        setCodeBoleta(codeBoleta);
        setCodeFactura(codeFactura);
        setCodeNotaCredito(codeNotaCredito);
    }

    public Bill(String type, Long codeBoleta, Long codeFactura, Long codeNotaCredito){
        if(type==DocumentType.FACTURA.toString()){
            code = codeFactura;
            serie = "F001";

        }

        if(type==DocumentType.BOLETA.toString()){
            code = codeBoleta;
            serie = "BD001";

        }


        if(type==DocumentType.NOTA_CREDITO.toString()){
            code = codeNotaCredito;
            serie = "NC001";

        }
        setType(type);

    }




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public Long getCode() {

        //llamar a la bd

        return code;
    }


    public String getRecipientRuc() {
        return recipientRuc;
    }

    public void setRecipientRuc(String recipientRuc) {
        this.recipientRuc = recipientRuc;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    public Long getCodeFactura() {
        return codeFactura;
    }

    public void setCodeFactura(Long codeFactura) {
        this.codeFactura = codeFactura;
    }

    public Long getCodeNotaCredito() {
        return codeNotaCredito;
    }

    public void setCodeNotaCredito(Long codeNotaCredito) {
        this.codeNotaCredito = codeNotaCredito;
    }

    public Long getCodeBoleta() {
        return codeBoleta;
    }

    public void setCodeBoleta(Long codeBoleta) {
        this.codeBoleta = codeBoleta;
    }


    public static enum DocumentType {
        BOLETA,
        FACTURA,
        NOTA_CREDITO
    }

    public void generateBillPDF(){

        if(type==null){
            System.out.println("Falta asignar el tipo");
            return;
        }


        if(order == null || code == null){
            System.out.println("Falta asignar la orden o el codigo al documento");
            return;
        }

        try{
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
            File fileDirectory = directoryChooser.showDialog(null);

            OutputStream file = new FileOutputStream(new File(fileDirectory.getPath() + File.separator + getType() +
                    getCode() +  ".pdf"));


            Document document = new Document();
            PdfWriter.getInstance(document, file);

            Font tableBold = new Font(Font.FontFamily.HELVETICA,10, Font.BOLD);
            Font fontBold = new Font(Font.FontFamily.HELVETICA,12, Font.BOLD);
            Font fontTypeDoc = new Font(Font.FontFamily.HELVETICA,15, Font.BOLD);
            Font ruc = new Font(Font.FontFamily.HELVETICA,20,Font.BOLD);
            Font normal = new Font(Font.FontFamily.HELVETICA,10, Font.NORMAL);
            Font client = new Font(Font.FontFamily.HELVETICA,10, Font.ITALIC);

            document.open();

            Chunk glue = new Chunk(new VerticalPositionMark());

            //Datos Empresa
            Image brand = Image.getInstance(getClass().getResource("/com/dp1mtel/desktop/pictures/margaritaTelLogo.png"));
            brand.scaleAbsolute(170,50);

            PdfPTable headTable = new PdfPTable(2);

            headTable.setWidthPercentage(100);


            PdfPCell logo = new PdfPCell();

            logo.addElement(brand);

            logo.setBorder(Rectangle.NO_BORDER);
            Paragraph ownerData = new Paragraph("De: "+ Constants.companyOwner +"\n"+
                    "Direccion: " + Constants.companyAddress,fontBold);
//            ownerData.setAlignment(Element.ALIGN_CENTER);

            logo.addElement(ownerData);
            headTable.addCell(logo);

            Paragraph auxParagraph;
            PdfPCell docInfo = new PdfPCell();
            docInfo.setBorder(Rectangle.NO_BORDER);


            auxParagraph = new Paragraph("R.U.C N° " + Constants.RUC,ruc );
            auxParagraph.setAlignment(Element.ALIGN_CENTER);
            docInfo.addElement(auxParagraph);
            docInfo.setHorizontalAlignment(Element.ALIGN_CENTER);

            if(getType().equals(DocumentType.BOLETA.toString())) {
                auxParagraph = new Paragraph( getType()+ " DE VENTA",fontTypeDoc);
            } else if (getType().equals(DocumentType.FACTURA.toString())) {
                auxParagraph= new Paragraph( getType(),fontTypeDoc);
            } else {
                auxParagraph = new Paragraph("Nota de Crédito", fontTypeDoc);
            }


            auxParagraph.setAlignment(Element.ALIGN_CENTER);
            docInfo.addElement( auxParagraph);


//            String typeDoc;
//            if (getType() == DocumentType.BOLETA.toString()) {
//                typeDoc = "BD";
//            } else if (getType() == DocumentType.FACTURA.toString()) {
//                typeDoc = "F";
//            } else {
//                typeDoc = "NC";
//            }


            String numberDoc = String.format("%07d", code);
            //7 digitos


            Paragraph numDoc = new Paragraph( serie  + " - " + numberDoc );
            numDoc.setAlignment(Element.ALIGN_CENTER);
            docInfo.addElement(numDoc);
            headTable.addCell(docInfo);
            document.add(headTable);


            // Datos cliente
            document.add(new Paragraph(" "));
            document.add(new LineSeparator());

            Phrase auxPhrase;

            if (!getType().equals(DocumentType.FACTURA.toString()) ) {
                auxPhrase = new Phrase("Cliente: ");
                auxPhrase.add(new Phrase( getOrder().getCustomer().getFullName(), client));
                auxParagraph = new Paragraph();
                auxParagraph.add(auxPhrase);
                document.add(auxParagraph);

                auxPhrase = new Phrase("Documento: ");
                auxPhrase.add(new Phrase( getOrder().getCustomer().getDocument(), client));
                auxParagraph = new Paragraph();
                auxParagraph.add(auxPhrase);
                document.add(auxParagraph);


            } else {
                auxPhrase = new Phrase("Empresa: ");
                auxPhrase.add(new Phrase( recipientBussiness, client));
                auxParagraph = new Paragraph();
                auxParagraph.add(auxPhrase);
                document.add(auxParagraph);

                auxPhrase = new Phrase("Dirección: ");
                auxPhrase.add(new Phrase(recipientAddress, client));
                auxParagraph = new Paragraph();
                auxParagraph.add(auxPhrase);
                document.add(auxParagraph);

                auxPhrase = new Phrase("RUC: ");
                auxPhrase.add(new Phrase(recipientRuc, client));
                auxParagraph = new Paragraph();
                auxParagraph.add(auxPhrase);
                document.add(auxParagraph);

            }


            auxPhrase = new Phrase("Fecha de Emisión: ");
            auxPhrase.add(new Phrase( getDate(), client));
            auxParagraph = new Paragraph();
            auxParagraph.add(auxPhrase);
            document.add(auxParagraph);


            document.add(new Paragraph(" "));
            document.add(new LineSeparator());
            document.add(new Paragraph(" "));


            // Datos Orden
            PdfPTable reportTable = new PdfPTable(7);
            reportTable.setWidthPercentage(90);
            PdfPCell table_cell;

            reportTable.setHorizontalAlignment(Element.ALIGN_CENTER);

            table_cell=new PdfPCell(new Phrase("Código",    fontBold));
            reportTable.addCell(table_cell);
            table_cell=new PdfPCell(new Phrase("Producto",  fontBold));
            reportTable.addCell(table_cell);
            table_cell=new PdfPCell(new Phrase("P. Unit. S./",  fontBold));
            reportTable.addCell(table_cell);
            table_cell=new PdfPCell(new Phrase("% Desc",    fontBold));
            reportTable.addCell(table_cell);
            table_cell=new PdfPCell(new Phrase("P. Final S./" ,  fontBold));
            reportTable.addCell(table_cell);
            table_cell=new PdfPCell(new Phrase("Cantidad",  fontBold));
            reportTable.addCell(table_cell);
            table_cell=new PdfPCell(new Phrase("Total",     fontBold));
            reportTable.addCell(table_cell);

            List<OrderDetail> detailList  = order.getOrderDetails();

            for(int i=0;i<detailList.size();i++) {


                if(detailList.get(i).getProduct()!= null){

                    table_cell= new PdfPCell(new Phrase(detailList.get(i).getProduct().getId(),normal));
                    reportTable.addCell(table_cell);


                    table_cell= new PdfPCell(new Phrase(detailList.get(i).getProduct().getName(),normal));
                    reportTable.addCell(table_cell);

                    Double price = Double.parseDouble(detailList.get(i).getProduct().getPrice()) ;
                    table_cell= new PdfPCell(new Phrase( DecimalFormatter.formats(price) ,normal));
                    reportTable.addCell(table_cell);

                    table_cell= new PdfPCell(new Phrase( DecimalFormatter.formats(detailList.get(i).getDiscount()*100) , normal));
                    reportTable.addCell(table_cell);


                    double priceDisc =  Double.parseDouble(detailList.get(i).getProduct().getPrice()) *(1- detailList.get(i).getDiscount());
                    table_cell= new PdfPCell(new Phrase(DecimalFormatter.formats(priceDisc)  , normal));
                    reportTable.addCell(table_cell);


                }else{

                    table_cell= new PdfPCell(new Phrase(detailList.get(i).getCombo().getId(),normal));
                    reportTable.addCell(table_cell);

                    table_cell= new PdfPCell(new Phrase(detailList.get(i).getCombo().getName(),normal));
                    reportTable.addCell(table_cell);

                    Double price = Double.parseDouble(detailList.get(i).getCombo().getPrice()) ;
                    table_cell= new PdfPCell(new Phrase(DecimalFormatter.formats(price),normal));
                    reportTable.addCell(table_cell);

                    table_cell= new PdfPCell(new Phrase( DecimalFormatter.formats(detailList.get(i).getDiscount()*100) , normal));
                    reportTable.addCell(table_cell);


                    double priceDisc =  Double.parseDouble(detailList.get(i).getCombo().getPrice())* (1-detailList.get(i).getDiscount()) ;


                    table_cell= new PdfPCell(new Phrase( DecimalFormatter.formats(priceDisc), normal));
                    reportTable.addCell(table_cell);

                }

                table_cell= new PdfPCell(new Phrase( String.valueOf(detailList.get(i).getQuantity()) , normal));
                reportTable.addCell(table_cell);
                table_cell= new PdfPCell(new Phrase(DecimalFormatter.formats(detailList.get(i).getTotal()) ,normal));
                reportTable.addCell(table_cell);

//                totalDiscount+= detailList.get(i).getDiscount();


            }

            document.add(reportTable);
            document.add(new Paragraph(" "));
            document.add(new LineSeparator());

            Paragraph subtotal=new Paragraph("Op. Gravada: "+ DecimalFormatter.formats(getSubtotal()) ,   normal);
            subtotal.setAlignment(Element.ALIGN_RIGHT);
            document.add(subtotal);
            Double impuestos = getTax();
            Double impuestostrunc = BigDecimal.valueOf(impuestos)
                    .setScale(2, RoundingMode.HALF_UP)
                    .doubleValue();

            Paragraph impuestosx=new Paragraph("IGV: "+ impuestostrunc.toString(),normal);
            impuestosx.setAlignment(Element.ALIGN_RIGHT);
            document.add(impuestosx);


//            Paragraph descuentos=new Paragraph("Descuentos: "+  DecimalFormatter.formats(totalDiscount) ,normal);
//            descuentos.setAlignment(Element.ALIGN_RIGHT);
//            document.add(descuentos);

            double totalD = impuestos + getSubtotal();

            Paragraph total=new Paragraph("Importe Total (S/.) : "+ DecimalFormatter.formats(totalD) , fontBold);
            total.setAlignment(Element.ALIGN_RIGHT);
            document.add(total);

            document.close();
            file.close();


        }catch (Exception ex){

            System.out.println("error al crear documento de pago");
            System.out.println(ex.getMessage());
        }

    }
}
