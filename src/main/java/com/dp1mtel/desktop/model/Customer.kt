package com.dp1mtel.desktop.model

data class Customer
(
        val id: Long = 0,
        var firstName: String,
        var lastName: String,
        var phone: String,
        var document: String,
        var addresses: MutableList<UserAddress> = mutableListOf()
) {
    val fullName: String
        get() = "$firstName $lastName"

    override fun toString(): String {
        return "$document, $fullName"
    }
}