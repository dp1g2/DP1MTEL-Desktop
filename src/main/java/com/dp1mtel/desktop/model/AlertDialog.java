package com.dp1mtel.desktop.model;


import com.dp1mtel.desktop.App;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Dialog;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AlertDialog  extends Dialog {

    private JFXDialogLayout content = new JFXDialogLayout();

    public AlertDialog() {
    }

    public JFXDialogLayout getContent() {
        return content;
    }

    public void setContent(JFXDialogLayout content) {
        this.content = content;
    }

    public static void showMessage(StackPane container, String title, String message){
        showMessage(container, title, message, () -> {});
    }

    public static void showMessage(StackPane container, String title, String message, Runnable onClose){

        try{
            JFXDialogLayout content = new JFXDialogLayout();

            Text titleHead = new Text(title);
            titleHead.setStyle("-fx-font-size: 20");
            titleHead.setText(title);

            content.setHeading(titleHead);

            Text bodyMessage = new Text(message);
            bodyMessage.wrappingWidthProperty().set(500);
            bodyMessage.setStyle("-fx-font-size: 25");
            content.setBody(bodyMessage);

            JFXDialog dialog = new JFXDialog(container,content,JFXDialog.DialogTransition.CENTER);
            JFXButton aceptar =  new JFXButton("Aceptar");
            aceptar.setButtonType(JFXButton.ButtonType.RAISED);
            aceptar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                    onClose.run();
                }
            });
            content.setActions(aceptar);
            dialog.show();
        }catch(Exception ex){
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public static void showConfirmationDialog(StackPane container,
                                       String title,
                                       String message,
                                       Runnable onConfirm,
                                       Runnable onCancel) {
        try {
            JFXDialogLayout content = new JFXDialogLayout();
            Text titleHead = new Text(title);
            titleHead.setStyle("-fx-font-size: 20");
            titleHead.setText(title);

            content.setHeading(titleHead);

            Text bodyMessage = new Text(message);
            bodyMessage.wrappingWidthProperty().set(500);
            bodyMessage.setStyle("-fx-font-size: 25");
            content.setBody(bodyMessage);

            JFXDialog dialog = new JFXDialog(container,content,JFXDialog.DialogTransition.CENTER);
            JFXButton aceptar =  new JFXButton("Aceptar");
            aceptar.setButtonType(JFXButton.ButtonType.RAISED);
            aceptar.getStyleClass().add("btn-success");
            aceptar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                    onConfirm.run();
                }
            });
            JFXButton cancelar = new JFXButton("Cancelar");
            cancelar.setButtonType(JFXButton.ButtonType.RAISED);
            cancelar.getStyleClass().add("btn-default");
            cancelar.setOnAction((e) -> {
                dialog.close();
                onCancel.run();
            });
            content.setActions(cancelar, aceptar);
            dialog.show();
        }catch(Exception ex){
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
