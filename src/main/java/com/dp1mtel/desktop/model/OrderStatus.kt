package com.dp1mtel.desktop.model

enum class OrderStatusEnum {RESERVED,PAID,CANCELED,SENT,NON_COMPLETED,COMPLETED,RETURNED}

data class OrderStatus(
        val id: Long,
        val name: String,
        val description: String
) {
    var slug: String = ""

    constructor(id: Long, name: String, description: String, slug: String)
            : this(id, name, description) {
        this.slug = slug
    }
}

