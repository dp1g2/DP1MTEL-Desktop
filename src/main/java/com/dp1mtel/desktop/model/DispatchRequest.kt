package com.dp1mtel.desktop.model

data class DispatchRequest(
        val orderList: List<Order>,
        val availableVehicles: Int,
        val storeId: Long
)
