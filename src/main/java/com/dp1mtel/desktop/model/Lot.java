package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.text.ParseException;
import java.util.Date;

public class Lot extends RecursiveTreeObject<Lot> {

    private Long id;
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty quantity = new SimpleStringProperty();
    private SimpleStringProperty expirationDate = new SimpleStringProperty();

    private Item item;



    public Lot(String quantity, Date expirationDate) {
        setId(new Long(0));
        setQuantity(quantity);
        setExpirationDate(expirationDate);
    }

    public Lot(Long id, String quantity){
        setId(id);
        setQuantity(quantity);
    }

    public Lot(Long id, String quantity, Date expirationDate) {
        setId(id);
        setQuantity(quantity);
        if(expirationDate==null){
            setExpirationDate(new Date());
        }else setExpirationDate(expirationDate);
    }

    public Lot (Long id){
        setId(id);
    }

    public Lot() {

    }

    public Lot(Date expirationDate){

        setExpirationDate(expirationDate);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }


    public String getQuantity() {
        return quantity.get();
    }

    public SimpleStringProperty quantityProperty() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity.set(quantity);
    }

    public SimpleStringProperty expirationDateProperty() {
        return expirationDate;
    }


    public void setExpirationDate(Date expirationDate) {
        this.expirationDate.set(User.dateFormat.format(expirationDate));
    }

    public Date getExpirationDate() throws ParseException {
        return User.dateFormat.parse(expirationDate.get());
    }

    public void print (){
        System.out.println("Lote: ID " + getId() + " Cantidad: "+ getQuantity());
    }

    public Item getItem() {
        return item;
    }


    public void setItem(Item item) {
        this.item = item;
    }


    @Override
    public String toString(){
        if(getItem()!=null) return "Lote "+ getId()+ " - Item: "+ getItem().getName();
        else{
            System.out.println("EL lote esta sin item");
            return "Lote "+ getId();
        }
    }
}
