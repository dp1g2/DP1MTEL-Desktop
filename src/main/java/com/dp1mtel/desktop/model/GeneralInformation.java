package com.dp1mtel.desktop.model;

public class GeneralInformation {

    private Long id;
    private Double igv;
    private String ruc;
    private String name;
    private String owner;
    private String address;

    public GeneralInformation(Long id, Double igv, String ruc, String name, String owner, String address) {
        this.id = id;
        this.igv = igv;
        this.ruc = ruc;
        this.name = name;
        this.owner = owner;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getIgv() {
        return igv;
    }

    public void setIgv(Double igv) {
        this.igv = igv;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
