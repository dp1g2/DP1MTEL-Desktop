package com.dp1mtel.desktop.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;

import java.util.Objects;

public class Category extends RecursiveTreeObject<Category>  {
    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();
    private SimpleStringProperty active = new SimpleStringProperty();

    @Override
    public String toString() {
        return name.get();
    }

    public Category(String name, String description) {
        setName(name);
        setDescription(description);
    }

    public Category(String id, String name, String description) {
        setId(id);
        setName(name);
        setDescription(description);
        setActive("true");
    }

    public Category(String id, String name, String description, String active) {
        setId(id);
        setName(name);
        setDescription(description);
        setActive(active);
    }

    public Category(String id) {
        setId(id);
        setName("");
        setActive("true");
        setDescription("");
    }

    public Category() {
        setName("");
        setDescription("");
        setActive("true");
    }

    public Category(int cod){
        //This is only mocking for passing the combo product
        setId("ITE0001");
        setActive("true");
        setName("");
        setDescription("");
    }
    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getActive() {
        return active.get();
    }

    public SimpleStringProperty activeProperty() {
        return active;
    }

    public void setActive(String active) {
        this.active.set(active);
    }
}
