package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.reports.ItemInventory
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ItemInventoryService {


        @GET("/api/stores/{id}/pos/items")
        fun getItemInventory(@Path("id")id:Long): Observable<List<ItemInventory>>
}