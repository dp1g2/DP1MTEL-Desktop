package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface ShopsService {
    @GET("/api/stores")
    fun getShops(): Observable<List<Shop>>

    @POST("/api/stores")
    fun saveShop(@Body shop: Shop): Observable<Shop>

    @GET("/api/stores/{id}")
    fun getShopById(@Path("id") id : Long): Observable<Shop>

    @DELETE("/api/stores/{id}")
    fun deleteShop(@Path("id") id: Long): Observable<Response<Any>>

    @GET("/api/stores/{id}/users")
    fun getEmployees(@Path("id") id :Long): Observable<List<User>>

    @GET("/api/districts")
    fun getDistricts(): Observable<List<Districts>>

    @PUT("/api/stores/{id}/users")
    fun saveEmployees(@Path("id") id :Long, @Body users:  Array<User>): Observable<List<User>>

    @PUT("/api/stores/{id}")
    fun updateShopById(@Path("id") id: Long, @Body shop: Shop): Observable<Shop>
}