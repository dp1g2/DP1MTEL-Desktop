package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Role
import com.dp1mtel.desktop.model.User
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface RolesService {
    @GET("/api/roles")
    fun getRoles(): Observable<List<Role>>

    @POST("/api/roles")
    fun saveRole(@Body role: Role): Observable<Role>

    @GET("/api/roles/{id}")
    fun getRoleById(@Path("id") id: Long): Observable<Role>

    @DELETE("/api/roles/{id}")
    fun deleteRole(@Path("id") id: Long): Observable<Response<Any>>

    @POST("/api/roles/bulk")
    fun saveMassiveRoles(@Body listRoles: List< @JvmSuppressWildcards Role>): Observable<MassiveStorageResponse>

    @PUT("/api/roles/{id}")
    fun updateRoleById(@Path("id") id:Long, @Body role:Role): Observable<Role>
}
