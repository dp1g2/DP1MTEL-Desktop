package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Product
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ProductsService {
    @GET("/api/products")
    fun getProducts(): Observable<List<Product>>

    @POST("/api/products")
    fun saveProduct(@Body product: Product): Observable<Response<Product>>

    @Multipart
    @POST("/api/products/uploadImage")
    fun uploadImage(@Part image: MultipartBody.Part,@Part("id") id: RequestBody): Observable<Response<Void>>

    @POST("/api/products/bulk")
    fun saveMassiveProducts(@Body listProducts: List<@JvmSuppressWildcards Product>): Observable<MassiveStorageResponse>

    @GET("/api/products/{id}")
    fun getProductById(@Path("id") id: String): Observable<Product>

    @DELETE("/api/products/{id}")
    fun deleteProduct(@Path("id") id: String): Observable<Response<Void>>

    @PUT("/api/products/{id}")
    fun updateProductById(@Path("id") id: String, @Body product: Product): Observable<Product>
}