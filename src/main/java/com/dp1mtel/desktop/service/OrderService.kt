package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Order
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface OrderService {
    @POST("/api/orders")
    fun createOrder(@Body order: Order): Observable<Response<Order>>

    @GET("/api/orders")
    fun getOrders(): Observable<List<Order>>

    @POST("/api/orders/bulk")
    fun saveMassiveOrders(@Body listOrders: List< @JvmSuppressWildcards Order>): Observable<MassiveStorageResponse>

    @GET("/api/orders")
    fun getOrdersByStore(@Query("store") storeId: Long): Observable<List<Order>>

    @POST("/api/orders/reserve")
    fun reserveOrder(@Body order: Order): Observable<Response<Order>>

    @POST("/api/orders/{id}/refund")
    fun refundOrder(@Path("id") orderId: Long, @Body order: Order): Observable<Response<Order>>

    @POST("/api/orders/{id}/pay")
    fun payOrder(@Body order: Order,
                 @Query("reservation_code") reservationCode: String): Observable<Response<Order>>

    @PUT("/api/orders/{id}")
    fun updateOrder(@Path ("id") id:Long, @Body order: Order):Observable<Response<Order>>
}