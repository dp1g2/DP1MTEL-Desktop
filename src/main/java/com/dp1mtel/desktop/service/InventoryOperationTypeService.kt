package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.InventoryOperationType
import io.reactivex.Observable
import retrofit2.http.*

interface InventoryOperationTypeService {
    @GET("/api/inventory-operation-types")
    fun getInventoryOperationTypes(): Observable<List<InventoryOperationType>>

    @POST("/api/inventory-operation-types")
    fun saveInventoryOperationType(@Body inventoryOperationType: InventoryOperationType): Observable<InventoryOperationType>

    @GET("/api/inventory-operation-types/{id}")
    fun getInventoryOperationTypeById(@Path("id") id: Long): Observable<InventoryOperationType>

    @DELETE("/api/inventory-operation-types/{id}")
    fun deleteInventoryOperationType(@Path("id") id: Long): Observable<Any>

    @PUT("/api/inventory-operation-types/{id}")
    fun updateInventoryOperationTypeById(@Path("id") id: Long, @Body category: InventoryOperationType): Observable<InventoryOperationType>
}