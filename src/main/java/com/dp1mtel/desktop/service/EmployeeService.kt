package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Employee
import io.reactivex.Observable
import retrofit2.http.*

interface EmployeeService {
    @GET("/api/employees")
    fun getEmployee(): Observable<List<Employee>>

    @POST("/api/employees")
    fun saveEmployee(@Body employee: Employee): Observable<Employee>

    @GET("/api/employees/{id}")
    fun getEmployeeById(@Path("id") id: Long): Observable<Employee>

    @DELETE("/api/employees/{id}")
    fun deleteEmployee(@Path("id") id: Long): Observable<Any>
}