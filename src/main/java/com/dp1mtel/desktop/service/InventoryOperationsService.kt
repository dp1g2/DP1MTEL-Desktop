package com.dp1mtel.desktop.service


import com.dp1mtel.desktop.model.InventoryOperation
import com.dp1mtel.desktop.model.MassiveStorageResponse
import io.reactivex.Observable
import retrofit2.http.*

interface InventoryOperationsService {
    @GET("/api/inventory-operations")
    fun getInventoryOperations(@Query ("store-id") id:Long): Observable<List<InventoryOperation>>


    @POST("/api/inventory-operations")
    fun saveInventoryOperation(@Body inventoryOperation: InventoryOperation): Observable<InventoryOperation>

    @POST("/api/inventory-operations/bulk")
    fun saveMassiveOperations(@Body listOperations: List< @JvmSuppressWildcards InventoryOperation>): Observable<MassiveStorageResponse>

    @GET("/api/inventory-operations/{id}")
    fun getInventoryOperationById(@Path("id") id: Long): Observable<InventoryOperation>

    @DELETE("/api/inventory-operations/{id}")
    fun deleteInventoryOperation(@Path("id") id: Long): Observable<Any>

    @PUT("/api/inventory-operations/{id}")
    fun updateInventoryOperationById(@Path("id") id: Long, @Body inventoryOperation: InventoryOperation): Observable<InventoryOperation>
}