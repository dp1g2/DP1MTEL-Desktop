package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.model.MassiveStorageResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface ItemsService {

    @GET("/api/items")
    fun getItems(): Observable<List<Item>>

    @GET("/api/items")
    fun getItemsByName(@Query("name") name:String): Observable<List<Item>>

    @POST("/api/items")
    fun saveItem(@Body item: Item): Observable<Response<Item>>

    @POST("/api/items/bulk")
    fun saveMassiveItems(@Body listItems: List< @JvmSuppressWildcards Item>): Observable<MassiveStorageResponse>

    @GET("/api/items/{id}")
    fun getItemById(@Path("id") id: String): Observable<Item>

    @DELETE("/api/items/{id}")
    fun deleteItem(@Path("id") id: String): Observable<Response<Void>>

    @PATCH("/api/items/{id}")
    fun disableItem(@Path("id") id: String, @Body item: Item): Observable<Response<Void>>

    @PUT("/api/items/{id}")
    fun updateItemById(@Path("id") id: String, @Body item: Item): Observable<Item>

}