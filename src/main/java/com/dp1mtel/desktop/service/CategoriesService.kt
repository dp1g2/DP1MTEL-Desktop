package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.model.MassiveStorageResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface CategoriesService {
    @GET("/api/product-categories")
    fun getCategories(): Observable<List<Category>>

    @GET("/api/product-categories")
    fun getCategoryByName(@Query("name") name: String): Observable<List<Category>>

    @POST("/api/product-categories")
    fun saveCategory(@Body category: Category): Observable<Response<Category>>

    @POST("/api/product-categories/bulk")
    fun saveMassiveCategories(@Body listCategories: List< @JvmSuppressWildcards Category>): Observable<MassiveStorageResponse>

    @GET("/api/product-categories/{id}")
    fun getCategoryById(@Path("id") id: String): Observable<Category>

    @DELETE("/api/product-categories/{id}")
    fun deleteCategory(@Path("id") id: String): Observable<Response<Void>>

    @PATCH("/api/product-categories/{id}")
    fun disableItem(@Path("id") id: String, @Body category: Category): Observable<Response<Void>>

    @PUT("/api/product-categories/{id}")
    fun updateCategoryById(@Path("id") id: String, @Body category: Category): Observable<Category>
}