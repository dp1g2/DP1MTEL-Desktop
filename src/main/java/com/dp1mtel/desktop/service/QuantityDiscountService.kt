package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.QuantityDiscount
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface QuantityDiscountService {

    @GET("/api/quantity-discounts")
    fun getQuantityDiscounts(): Observable<List<QuantityDiscount>>

    @POST("/api/quantity-discounts")
    fun saveQuantityDiscount(@Body quantityDiscount: QuantityDiscount): Observable<Response<QuantityDiscount>>

    @DELETE("/api/quantity-discounts/{id}")
    fun deleteQuantityDiscount(@Path ("id") id :Long) : Observable<Response<Void>>


}