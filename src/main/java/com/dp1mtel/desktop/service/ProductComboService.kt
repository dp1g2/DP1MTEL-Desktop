package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Product
import com.dp1mtel.desktop.model.ProductCombo
import retrofit2.http.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

interface ProductComboService {
    @GET("/api/combos")
    fun getProductCombo(): Observable<List<ProductCombo>>

    @POST("/api/combos")
    fun saveProductCombo(@Body productCombo: ProductCombo): Observable<Response<ProductCombo>>

    @Multipart
    @POST("/api/combos/uploadImage")
    fun uploadImage(@Part image: MultipartBody.Part, @Part("id") id: RequestBody): Observable<Response<Void>>

    @POST("/api/combos/bulk")
    fun saveMassiveCombos(@Body listCombos: List<@JvmSuppressWildcards ProductCombo>): Observable<MassiveStorageResponse>

    @GET("/api/combos/{id}")
    fun getProductComboById(@Path("id") id: String): Observable<ProductCombo>

    @DELETE("/api/combos/{id}")
    fun deleteProductCombo(@Path("id") id: String): Observable<Response<Void>>

    @PUT("/api/combos/{id}")
    fun updateProductComboById(@Path("id") id: String, @Body productCombo: ProductCombo): Observable<ProductCombo>
}