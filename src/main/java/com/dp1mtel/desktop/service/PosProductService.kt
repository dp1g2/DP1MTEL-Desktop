package com.dp1mtel.desktop.service;

import com.dp1mtel.desktop.model.PosProduct;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface PosProductService {
    @GET("/api/stores/{id}/pos/products")
    fun getPosProducts(@Path("id") id : Long): Observable<List<PosProduct>>
}
