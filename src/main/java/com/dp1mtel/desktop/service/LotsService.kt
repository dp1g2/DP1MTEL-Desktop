package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Lot
import io.reactivex.Observable
import retrofit2.http.GET


interface LotsService {

    @GET("/api/lots")
    fun getLots(): Observable<List<Lot>>
}