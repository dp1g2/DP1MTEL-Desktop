package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Dispatch
import com.dp1mtel.desktop.model.DispatchRequest
import com.dp1mtel.desktop.model.Route
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import java.awt.geom.Point2D

interface DispatchService {
    @GET("api/distribution/dispatch/demo")
    fun dispatchDemo(): Observable<List<Route>>

    @POST("api/distribution/dispatch")
    fun dispatch(@Body orders: DispatchRequest): Observable<List<Route>>

    @POST("api/distribution")
    fun saveDispatch(@Body dispatch: Dispatch): Observable<Dispatch>

    @GET("api/distribution/geocode")
    fun geocode(@Query("address") address: String,
                @Query("limit") limit: Int = 5): Observable<List<Point2D>>

    @GET("api/distribution")
    fun getDispatchedRoutes(@Query("storeId") storeId: Long): Observable<List<Route>>
}