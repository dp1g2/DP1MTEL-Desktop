package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.OrderStatus
import io.reactivex.Observable
import retrofit2.http.GET

interface OrderStatusService {

    @GET("/api/order-status")
    fun getAllOrderStatus(): Observable<List<OrderStatus>>
}