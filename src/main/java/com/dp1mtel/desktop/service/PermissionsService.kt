package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Permission
import retrofit2.http.GET
import io.reactivex.Observable

interface PermissionsService {
    @GET("/api/permissions")
    fun getPermissions(): Observable<List<Permission>>
}