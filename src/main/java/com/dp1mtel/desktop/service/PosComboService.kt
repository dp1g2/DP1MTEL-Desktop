package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.PosCombo
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface PosComboService {
    @GET("/api/stores/{id}/pos/combos")
    fun getPosCombos(@Path("id") id:Long): Observable<List<PosCombo>>
}