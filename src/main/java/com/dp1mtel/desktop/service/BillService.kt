package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Bill
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Query

interface BillService {

    @POST("/api/bill")
    fun createBill(@Query("type") type:String): Observable<Response<Bill>>

}