package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.User
import io.reactivex.Observable
import io.reactivex.internal.operators.observable.ObservableError
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

import java.util.ArrayList
import java.util.Date

// TODO: Update to use data provided by server
interface UsersService {
    @GET("/api/users")
    fun getUsers(): Observable<List<User>>

    @POST("/api/users")
    fun saveUser(@Body user: User): Observable<User>

    @DELETE("/api/users/{id}")
    fun deleteUser(@Path("id") id: Long): Observable<Any>

    @POST("/api/login")
    fun loginUser(@Query("user") user : String, @Query("password")password:String): Observable<Response<User>>

    @POST("/api/users/bulk")
    fun saveMassiveUsers(@Body listUsers: List< @JvmSuppressWildcards User>): Observable<MassiveStorageResponse>

    @PUT("/api/users/{id}")
    fun updateUserById(@Path("id") id: Long, @Body user:User): Observable<User>
}
