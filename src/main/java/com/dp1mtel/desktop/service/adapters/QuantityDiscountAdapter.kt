package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.ProductCombo
import com.dp1mtel.desktop.model.QuantityDiscount
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class QuantityDiscountAdapter {

    @ToJson
    fun toJson(quantityDiscount: QuantityDiscount): QuantityDiscountJson{
        return QuantityDiscountJson(
                quantityDiscount.id,
                quantityDiscount.name,
                //quantityDiscount.discount,
                quantityDiscount.startDate,
                quantityDiscount.endDate,
                quantityDiscount.productCombo

        )
    }

    @FromJson
    fun fromJson(quantityDiscountJson: QuantityDiscountJson) :QuantityDiscount{
        return QuantityDiscount(
                quantityDiscountJson.id,
                quantityDiscountJson.name,
                //quantityDiscountJson.discount,
                quantityDiscountJson.combo,
                quantityDiscountJson.startDate,
                quantityDiscountJson.endDate
                )
    }






    data class QuantityDiscountJson (
            val id : Long?,
            val name : String,
            //val discount : Double,
            val startDate : String,
            val endDate : String,
            val combo : ProductCombo

    )

}