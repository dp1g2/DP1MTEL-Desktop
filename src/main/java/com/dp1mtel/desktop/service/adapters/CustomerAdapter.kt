package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Customer
import com.dp1mtel.desktop.model.UserAddress
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class CustomerAdapter {
    @ToJson fun toJson(customer: Customer): CustomerJson {
        return CustomerJson(
                customer.id,
                customer.firstName,
                customer.lastName,
                customer.phone,
                customer.document,
                customer.addresses
        )
    }

    @FromJson fun fromJson(customerJson: CustomerJson): Customer {
        return Customer(
                customerJson.id,
                customerJson.firstName,
                customerJson.lastName,
                customerJson.phone,
                customerJson.document,
                customerJson.addresses.toMutableList()
        )
    }

    data class CustomerJson(
            val id: Long,
            val firstName: String,
            var lastName: String,
            var phone: String,
            var document: String,
            var addresses: List<UserAddress> = listOf()
    )
}