package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.PosCombo
import com.dp1mtel.desktop.model.ProductCombo
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class PosComboAdapter {

    @ToJson
    fun toJson(posCombo: PosCombo):PosComboJson {
        return PosComboJson(
            posCombo.combo,
            posCombo.stock,
            posCombo.discount
        )
    }

    @FromJson
    fun fromJson(posComboJson: PosComboJson): PosCombo {
        val posCombo = PosCombo(
                posComboJson.combo,
                posComboJson.stock,
                posComboJson.discount
        )
        return posCombo
    }

    data class PosComboJson(val combo: ProductCombo,
                            val stock: Int,
                            val discount: Double)
}