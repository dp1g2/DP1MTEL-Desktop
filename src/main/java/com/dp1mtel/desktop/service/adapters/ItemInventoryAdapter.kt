package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.reports.ItemInventory
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class ItemInventoryAdapter {
    data class ItemInventoryJson(val item: Item, val stock:Int );
    @ToJson
    fun toJson(itemInventory: ItemInventory): ItemInventoryJson {
        return ItemInventoryJson(
                itemInventory.item,
                itemInventory.stock
        )
    }

    @FromJson
    fun fromJson(itemInventoryJson: ItemInventoryJson): ItemInventory {
        val itemInventory = ItemInventory(
                itemInventoryJson.item,
                itemInventoryJson.stock
        )

        return itemInventory

    }
}