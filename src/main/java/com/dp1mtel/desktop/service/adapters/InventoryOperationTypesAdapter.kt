package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.InventoryOperationType
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class InventoryOperationTypesAdapter {
    @ToJson
    fun toJson(inventoryOperationType: InventoryOperationType): InventoryOperationTypeJson {
        return InventoryOperationTypeJson(inventoryOperationType.id, inventoryOperationType.name, inventoryOperationType.description, inventoryOperationType.multiplier)
    }

    @FromJson
    fun fromJson(inventoryOperationTypeJson: InventoryOperationTypeJson): InventoryOperationType {
        return InventoryOperationType(inventoryOperationTypeJson.id, inventoryOperationTypeJson.name, inventoryOperationTypeJson.description, inventoryOperationTypeJson.multiplier)
    }

    data class InventoryOperationTypeJson(
                            val id: Long,
                            val name: String,
                            val description: String,
                            val multiplier: Int)
}