package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Lot
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*

class LotAdapter{

    @ToJson
    fun toJson(lot: Lot): LotJson {
        return LotJson(lot.id,  lot.quantity, lot.expirationDate)
    }

    @FromJson
    fun fromJson(lotJson: LotJson): Lot {
        return Lot(lotJson.id, lotJson.quantity, lotJson.expirationDate)
    }


    data class LotJson(val id: Long?,
                        val quantity: String?,
                        val expirationDate : Date? = Date()

    )
}