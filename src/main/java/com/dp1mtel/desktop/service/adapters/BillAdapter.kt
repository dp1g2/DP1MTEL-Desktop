package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Bill
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class BillAdapter {


    @FromJson fun fromJson(billJson: BillJson):Bill{
        return Bill(
                billJson.codeBoleta,
                billJson.codeFactura,
                billJson.codeNotaCredito)
    }


    data class BillJson(
            var codeBoleta: Long,
            var codeFactura: Long,
            var codeNotaCredito: Long
    )

}