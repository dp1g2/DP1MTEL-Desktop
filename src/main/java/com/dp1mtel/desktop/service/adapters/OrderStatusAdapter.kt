package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.OrderStatus
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class OrderStatusAdapter {
    @ToJson fun toJson(status: OrderStatus): OrderStatusJson {
        return OrderStatusJson(status.id, status.name, status.description, status.slug)
    }

    @FromJson fun fromJson(statusJson: OrderStatusJson): OrderStatus {
        return OrderStatus(statusJson.id, statusJson.name, statusJson.description, statusJson.slug)
    }

    data class OrderStatusJson(
            val id: Long,
            val name: String,
            val description: String,
            val slug: String
    )
}