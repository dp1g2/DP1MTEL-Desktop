package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Role
import com.dp1mtel.desktop.model.Shop
import com.dp1mtel.desktop.model.User
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*

class UserAdapter {
    @ToJson fun userToJson(user: User): UserJson {

        return UserJson(
                user.id,
                user.dni,
                user.firstName,
                user.lastName,
                user.email,
                user.dob,
                user.gender,
                user.password,
                user.role,
                user.store,
                user.isActive
        )
    }

    @FromJson fun userFromJson(userJson: UserJson): User {
        return User(
                userJson.id,
                userJson.firstName,
                userJson.lastName,
                userJson.dni,
                userJson.email,
                userJson.birthday,
                userJson.gender,
                userJson.password,
                userJson.role,
                userJson.active,
                userJson.store
        )
    }

    class UserJson(val id: Long = 0,
                   val dni: String,
                   val firstName: String,
                   val lastName: String,
                   val email: String,
                   val birthday: Date,
                   val gender: String,
                   val password: String? = "",
                   val role: Role,
                   val store: Shop?,
                   val active:Boolean? = true)
}