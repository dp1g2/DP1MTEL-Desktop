package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Role
import com.dp1mtel.desktop.model.Permission
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

//TODO: Update to match value returned by server
class RoleAdapter {
    @ToJson fun toJson(role: Role): RoleJson {
        return RoleJson(role.id, role.name, role.description, role.permissions)
    }


    @FromJson fun fromJson(roleJson: RoleJson): Role {
        return Role(roleJson.id, roleJson.name, roleJson.description, roleJson.permissions)
    }

    data class RoleJson(val id: Long,
                        val name: String,
                        val description: String,
                        val permissions: List<Permission> = listOf())
}