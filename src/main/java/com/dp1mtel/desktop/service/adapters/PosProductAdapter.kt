package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.PosProduct
import com.dp1mtel.desktop.model.Product
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class PosProductAdapter {

    @ToJson
    fun toJson(posProduct: PosProduct): PosProductJson {
        return PosProductJson(
                posProduct.product,
                posProduct.stock,
                posProduct.discount
        )
    }

    @FromJson
    fun fromJson(posProductJson: PosProductJson): PosProduct {
        val posProduct = PosProduct(
                posProductJson.product,
                posProductJson.stock,
                posProductJson.discount
        )

        return posProduct

    }

    data class PosProductJson(val product: Product,
                              val stock: Int,
                              val discount: Double)
}