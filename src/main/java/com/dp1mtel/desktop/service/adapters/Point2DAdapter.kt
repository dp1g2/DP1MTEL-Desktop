package com.dp1mtel.desktop.service.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.awt.geom.Point2D

class Point2DAdapter {
    @ToJson fun toJson(point: Point2D): PointJson {
        return PointJson(point.x, point.y)
    }

    @FromJson fun fromJson(point: PointJson): Point2D {
        return Point2D.Double(point.x, point.y)
    }

    data class PointJson(val x: Double, val y: Double)
}