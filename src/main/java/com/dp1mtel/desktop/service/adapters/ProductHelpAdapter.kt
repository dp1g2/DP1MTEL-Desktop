package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.model.ProductHelp
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class ProductHelpAdapter {
    @ToJson
    fun toJson(productHelp: ProductHelp): ProductHelpJson {
        return ProductHelpJson(productHelp.quantity.toInt(),productHelp.item)
    }

    @FromJson
    fun fromJson(productHelpJson: ProductHelpJson): ProductHelp {
        return ProductHelp(productHelpJson.quantity.toString(), productHelpJson.item)
    }

    data class ProductHelpJson(val quantity: Int, val item : Item)
}