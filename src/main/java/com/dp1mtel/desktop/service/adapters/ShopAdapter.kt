package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Employee
import com.dp1mtel.desktop.model.Shop
import com.dp1mtel.desktop.model.User
import java.awt.geom.Point2D
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class ShopAdapter{
    @ToJson fun toJson(shop: Shop): ShopJson {
//        return ShopJson(shop.id, shop.address,
//
//                        shop.district, shop.status,
//                        shop.nemployees, shop.numVehicles,
//                        shop.numVehicles)
        return ShopJson(
                shop.id,
                shop.address,
                shop.district,
                shop.status,
                shop.nemployees,
                shop.numVehicles,
                shop.numCapacity,
                shop.coordinates)
    }

    @FromJson fun fromJson(shopJson:ShopJson): Shop {
        return Shop(
                shopJson.id,
                shopJson.address,
                shopJson.district,
                shopJson.active,
                shopJson.numEmployees,
                shopJson.numVehicles,
                shopJson.vehiclesCapacity).apply {
            coordinates = shopJson.coordinates
        }
    }

    data class ShopJson (val id : Long,
                         val address : String,
                         val district : String,
                         val active: Boolean,
                         val numEmployees: String? = 0.toString(),
                         val numVehicles: String,
                         val vehiclesCapacity: String,
                         val coordinates: Point2D? = Point2D.Double(0.0, 0.0))
}