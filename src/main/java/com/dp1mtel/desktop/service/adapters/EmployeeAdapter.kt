package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Employee
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class EmployeeAdapter {
    @ToJson fun toJson(employee: Employee): EmployeeJson {
        return EmployeeJson(employee.id,employee.dni,employee.name,employee.position)
    }


    @FromJson fun fromJson(employeeJson: EmployeeJson): Employee{
        return Employee(employeeJson.id, employeeJson.dni, employeeJson.name, employeeJson.position)
    }

    data class EmployeeJson(val id: Long,
                        val dni: String,
                        val name: String,
                        val position: String)
}
