package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.District
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class DistrictAdapter {
    @ToJson fun toJson(district: District): DistrictJson {
        return DistrictJson(district.id, district.name)
    }

    @FromJson fun fromJson(districtJson: DistrictJson): District {
        return District(districtJson.id, districtJson.name)
    }

    data class DistrictJson(
            val id: Long,
            val name: String
    )
}