package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.model.Product
import com.dp1mtel.desktop.model.ProductHelp
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class ProductAdapter {

    @ToJson
    fun toJson(product: Product): ProductJson {
        return ProductJson(
                product.id,
                product.name,
                product.description,
                product.price.toDouble(),
                product.active.toBoolean(),
                product.stock,
                product.category,
                product.listItems)
    }

    @FromJson
    fun fromJson(productJson: ProductJson): Product {
        //Need to add productCategory and items...
        val prod = Product(
                productJson.id,
                productJson.name,
                productJson.description,
                productJson.price.toString(),
                productJson.active.toString(),
                productJson.productCategory,
                productJson.productItems)
        return prod
    }
    data class ProductJson(val id: String,
                           val name: String,
                           val description: String,
                           val price: Double,
                           val active: Boolean,
                           val stock: Int,
                           val productCategory: Category,
                           val productItems: List<ProductHelp> = listOf())
                           
}
