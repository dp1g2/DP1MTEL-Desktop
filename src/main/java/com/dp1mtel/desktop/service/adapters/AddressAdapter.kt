package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.UserAddress
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class AddressAdapter {
    @ToJson fun toJson(address: UserAddress): AddressJson {
        return AddressJson(
                address.id,
                address.address,
                address.name
        )
    }

    @FromJson fun fromJson(addressJson: AddressJson): UserAddress {
        return UserAddress(
                addressJson.address,
                addressJson.name
        ).apply {
            id = addressJson.id
        }
    }

    data class AddressJson(
            val id: Long,
            val address: String,
            val name: String? = null
    )
}