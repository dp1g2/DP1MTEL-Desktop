package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.*
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.awt.geom.Point2D
import java.util.*

class OrderAdapter {
    @ToJson fun toJson(order: Order): OrderJson {
        return OrderJson(
                order.id,
                order.seller,
                order.customer,
                order.recipientName,
                order.recipientDocument,
                order.recipientDedication,
                order.status,
                order.orderDetails,
                order.volume,
                order.deliveryAddress,
                order.expectedDeliveryDate,
                order.userReadyTime,
                order.userDueTime,
                order.userServiceTime,
                order.store,
                order.reservationCode,
                order.reservationTime,
                order.cancellationReason,
                order.createdAt
        )
    }

    @FromJson fun fromJson(orderJson: OrderJson): Order {
        return Order(
                orderJson.id,
                orderJson.seller,
                orderJson.customer,
                orderJson.recipientName,
                orderJson.recipientDocument,
                orderJson.recipientDedication,
                orderJson.orderStatus,
                orderJson.store,
                orderJson.orderDetails,
                orderJson.volume,
                orderJson.deliveryAddress,
                orderJson.expectedDeliveryDate,
                orderJson.userReadyTime,
                orderJson.userDueTime,
                orderJson.userServiceTime,
                orderJson.reservationCode,
                orderJson.reservationTime,
                orderJson.cancellationReason,
                orderJson.createdAt
        )
    }

    data class OrderJson(
            var id: Long,
            var seller: User,
            var customer: Customer,
            var recipientName: String? = null,
            var recipientDocument: String? = null,
            var recipientDedication: String? = null,
            var orderStatus: OrderStatus,
            var orderDetails: List<OrderDetail> = listOf(),
            val volume: Double,
            val deliveryAddress: UserAddress? = null,
            val expectedDeliveryDate: Date? = Date(),
            var userReadyTime: Double,
            var userDueTime: Double,
            var userServiceTime: Double,
            var store: Shop,
            val reservationCode: String? = null,
            val reservationTime: Date? = null,
            val cancellationReason: String? = null,
            val createdAt: Date? = null
    )
}