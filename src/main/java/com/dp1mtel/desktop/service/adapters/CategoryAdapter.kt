package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.Product
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class CategoryAdapter {
    @ToJson
    fun toJson(category: Category): CategoryJson {
        return CategoryJson(category.id, category.name, category.description,category.active.toBoolean())
    }

    @FromJson
    fun fromJson(categoryJson: CategoryJson): Category {
        return Category(categoryJson.id, categoryJson.name, categoryJson.description,categoryJson.active.toString())
    }

    data class CategoryJson(val id: String,
                            val name: String,
                            val description: String,
                            val active: Boolean)
}