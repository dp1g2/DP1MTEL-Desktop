package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Product
import com.dp1mtel.desktop.model.ProductCombo
import com.dp1mtel.desktop.model.ProductComboHelp
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson


//TODO: Update to match value returned by server
class ProductComboAdapter{
    @ToJson fun toJson(productCombo: ProductCombo): ProductComboJson {
        return ProductComboJson (productCombo.id,productCombo.name,productCombo.description,
                productCombo.price.toDouble(),productCombo.active.toBoolean(),
                productCombo.listProducts)
    }


    @FromJson fun fromJson(productComboJson: ProductComboJson): ProductCombo{
        return ProductCombo(productComboJson.id,productComboJson.name,
                productComboJson.description,productComboJson.price.toString(),
                productComboJson.active.toString(),productComboJson.productCombos)
    }

    data class ProductComboJson (
                                 val id:String,
                                 val name: String,
                                 val description: String,
                                 val price: Double,
                                 val active: Boolean,
                                 val productCombos: List<ProductComboHelp> = listOf())
}