package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.CategoryDiscount
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class CategoryDiscountAdapter {

    @ToJson
    fun toJson(categoryDiscount: CategoryDiscount) : CategoryDiscountJson{
        return CategoryDiscountJson(
                categoryDiscount.id,
                categoryDiscount.name,
                categoryDiscount.discount,
                categoryDiscount.start,
                categoryDiscount.end,
                categoryDiscount.category,
                categoryDiscount.active)
    }

    @FromJson
    fun fromJson(categoryDiscountJson : CategoryDiscountJson) : CategoryDiscount {
        return CategoryDiscount(
                categoryDiscountJson.id,
                categoryDiscountJson.name,
                categoryDiscountJson.discount,
                categoryDiscountJson.startDate,
                categoryDiscountJson.endDate,
                categoryDiscountJson.productCategory,
                categoryDiscountJson.active)
    }


    data class CategoryDiscountJson (
            val id: Long,
            val name : String,
            val discount : Double,
            val startDate : String,
            val endDate : String,
            val productCategory : Category,
            val active : Boolean
            )
}