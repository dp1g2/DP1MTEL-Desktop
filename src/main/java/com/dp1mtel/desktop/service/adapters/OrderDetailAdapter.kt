package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.OrderDetail
import com.dp1mtel.desktop.model.Product
import com.dp1mtel.desktop.model.ProductCombo
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class OrderDetailAdapter {
    @ToJson fun toJson(detail: OrderDetail): OrderDetailJson {
        return OrderDetailJson(
                detail.id,
                detail.product,
                detail.combo,
                detail.quantity,
                detail.unitPrice,
                detail.discount)
    }

    @FromJson fun fromJson(detail: OrderDetailJson): OrderDetail {
        return OrderDetail().apply {
            id = detail.id
            product = detail.product
            combo = detail.combo
            quantity = detail.quantity
            unitPrice = detail.price
            discount = detail.discount
        }
    }

    data class OrderDetailJson(
            val id: Long,
            val product: Product? = null,
            val combo: ProductCombo? = null,
            val quantity: Int,
            val price: Double,
            val discount: Double
    )
}