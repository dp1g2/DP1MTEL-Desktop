package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Permission
import com.dp1mtel.desktop.model.Product
import com.dp1mtel.desktop.model.ProductComboHelp
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

//TODO: Update to match value returned by server
class ProductComboHelpAdapter {
    @ToJson fun toJson(productComboHelp: ProductComboHelp): ProductComboHelpJson {
        return ProductComboHelpJson(productComboHelp.quantity.toInt(),productComboHelp.product)
    }

    @FromJson fun fromJson(productComboHelpJson: ProductComboHelpJson): ProductComboHelp{
        return ProductComboHelp(productComboHelpJson.quantity.toString(), productComboHelpJson.product)
    }

    data class ProductComboHelpJson(val quantity: Int,
                                    val product: Product)
}