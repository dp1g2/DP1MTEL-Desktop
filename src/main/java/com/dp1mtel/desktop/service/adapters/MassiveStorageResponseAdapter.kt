package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.ProductComboHelp
import com.dp1mtel.desktop.model.Shop
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class MassiveStorageResponseAdapter {
    @ToJson fun toJson(massive: MassiveStorageResponse): MassiveStorageResponseAdapter.MassiveStorageResponseJson {
        return MassiveStorageResponseAdapter.MassiveStorageResponseJson(massive.correctlyInserted,massive.badlyInserted, massive.permissions)
    }

    @FromJson
    fun fromJson(massive: MassiveStorageResponseJson): MassiveStorageResponse {
        return MassiveStorageResponse(massive.correctlyInserted, massive.badlyInserted,massive.permissions)
    }

    data class MassiveStorageResponseJson
                            (val correctlyInserted: String,
                            val badlyInserted: String,
                            val permissions: List<Int> = arrayListOf())
}