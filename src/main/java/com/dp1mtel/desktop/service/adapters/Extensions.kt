@file:JvmName("AdapterExtensions")

package com.dp1mtel.desktop.service.adapters

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

inline fun <reified T> Moshi.listAdapter(): JsonAdapter<List<T>> {
    val type = Types.newParameterizedType(List::class.java, T::class.java)
    return this.adapter<List<T>>(type)
}

inline fun <reified T> Moshi.addAdapter() {
    this.listAdapter<T>()
    this.adapter(T::class.java)
}