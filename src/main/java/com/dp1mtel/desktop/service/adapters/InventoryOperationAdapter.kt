package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.*
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*


class InventoryOperationAdapter {
    @ToJson
    fun toJson(inventoryOperation: InventoryOperation): InventoryOperationJson {
        return InventoryOperationJson(
                inventoryOperation.id,
//                inventoryOperation.name,
                inventoryOperation.description,
                inventoryOperation.date,
                inventoryOperation.quantity,
                inventoryOperation.item,
                inventoryOperation.store,
                inventoryOperation.user,
                inventoryOperation.lot,
                inventoryOperation.typeOperation)
    }


    @FromJson
    fun fromJson(inventoryOperationJson: InventoryOperationJson): InventoryOperation {
        return InventoryOperation(
                inventoryOperationJson.id,
//                inventoryOperationJson.name,
                inventoryOperationJson.description,
                inventoryOperationJson.createdAt,
                inventoryOperationJson.quantity,
                inventoryOperationJson.item,
                inventoryOperationJson.store,
                inventoryOperationJson.user,
                inventoryOperationJson.lot,
                inventoryOperationJson.inventoryOperationType)
    }

    //Todo: Preguntar sobre el int o debe ser string

    data class InventoryOperationJson(
            val id: Long?,
//                            val name: String?,
            val description: String,
            val createdAt: Date?,
            val quantity: Int,
            val item: Item,
            val store: Shop,
            val user: User,
            val lot: Lot,
            val inventoryOperationType: InventoryOperationType)

}