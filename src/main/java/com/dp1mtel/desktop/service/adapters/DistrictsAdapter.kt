package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Districts
import com.dp1mtel.desktop.model.Role
import com.dp1mtel.desktop.model.Permission
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

//TODO: Update to match value returned by server
class DistrictsAdapter {
    @ToJson fun toJson(district: Districts): DistrictsJson {
        return DistrictsJson(district.id, district.name)
    }

    @FromJson fun fromJson(districtJson: DistrictsJson): Districts {
        return Districts(districtJson.id, districtJson.name)
    }

    data class DistrictsJson(val id: Long,
                             val name: String)
}