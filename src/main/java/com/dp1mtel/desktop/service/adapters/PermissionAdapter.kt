package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Permission
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

//TODO: Update to match value returned by server
class PermissionAdapter {
    @ToJson fun toJson(permission: Permission): PermissionJson {
        return PermissionJson(
                permission.id,
                permission.name,
                permission.description)
    }

    @FromJson fun fromJson(permissionJson: PermissionJson): Permission {
        return Permission(
                          permissionJson.id,
                          permissionJson.name,
                          permissionJson.description,
                        false
                )
    }

    data class PermissionJson(
                        val id: Long,
                        val name: String,
                        val description: String )
}