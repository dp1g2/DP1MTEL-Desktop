package com.dp1mtel.desktop.service.adapters

import com.dp1mtel.desktop.model.Item
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class ItemAdapter {
    @ToJson
    fun toJson(item: Item): ItemJson {
        return ItemJson(
                item.id,
                item.name,
                item.description,
                item.active.toBoolean(),
                item.isExpires,
                item.volume
        )
    }

    @FromJson
    fun fromJson(itemJson: ItemJson): Item {
        return Item(
                itemJson.id,
                itemJson.name,
                itemJson.description,
                itemJson.active.toString(),
                itemJson.expires,
                itemJson.volume
        )
    }

    data class ItemJson(val id: String,
                        val name: String,
                        val description: String,
                        val active: Boolean,
                        val expires: Boolean,
                        val volume: Double)
}