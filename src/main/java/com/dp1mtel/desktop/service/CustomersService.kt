package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.Customer
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.User
import com.dp1mtel.desktop.model.UserAddress
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface CustomersService {
    @GET("/api/customers")
    fun getCustomers(): Observable<List<Customer>>

    @GET("/api/customers/{id}")
    fun getCustomerById(@Path("id") id: Long): Observable<Customer>

    @POST("/api/customers")
    fun saveCustomer(@Body customer: Customer): Observable<Customer>

    @POST("/api/customers/bulk")
    fun saveMassiveCustomers(@Body listCustomers: List< @JvmSuppressWildcards Customer>): Observable<MassiveStorageResponse>

    @GET("/api/customers/{id}/addresses")
    fun getCustomerAddresses(@Path("id") id: Long): Observable<List<UserAddress>>

    @POST("/api/customers/{id}/addresses")
    fun saveCustomerAddress(
            @Path("id") customerId: Long,
            @Body address: UserAddress
    ): Observable<UserAddress>
}