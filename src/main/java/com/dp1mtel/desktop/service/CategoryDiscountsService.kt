package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.CategoryDiscount
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface CategoryDiscountsService {

    @GET("/api/product-category-discounts")
    fun getCategoryDiscounts(): Observable<List<CategoryDiscount>>

    @POST("/api/product-category-discounts")
    fun saveCategoryDiscounts(@Body categoryDiscount: CategoryDiscount): Observable<Response<CategoryDiscount>>

    @PUT("/api/product-category-discounts/{id}")
    fun updateCategoryDiscounts(@Path("id") id:Long, @Body categoryDiscount: CategoryDiscount): Observable<Response<CategoryDiscount>>

    @DELETE("/api/product-category-discounts/{id}")
    fun deleteCategoryDiscount(@Path("id") id: Long): Observable<Response<Void>>
}