package com.dp1mtel.desktop.service

import com.dp1mtel.desktop.model.District
import io.reactivex.Observable
import retrofit2.http.GET

interface DistrictService {
    @GET("/api/districts")
    fun getAllDistricts(): Observable<List<District>>
}