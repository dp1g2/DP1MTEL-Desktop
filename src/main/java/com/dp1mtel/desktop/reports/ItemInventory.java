package com.dp1mtel.desktop.reports;

import com.dp1mtel.desktop.model.InventoryOperation;
import com.dp1mtel.desktop.model.Item;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javafx.stage.Stage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ItemInventory {
    private Item item;
    private int stock;

    public void setItem(Item item) {
        this.item = item;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Item getItem() {
        return item;
    }

    public int getStock() {
        return stock;
    }
    public ItemInventory(Item item, int stock){
        setItem(item);
        setStock(stock);
    }


}
