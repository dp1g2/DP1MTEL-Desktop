package com.dp1mtel.desktop.controller.stores;

import com.dp1mtel.desktop.controller.BaseController;

public class StoresController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/stores/index.fxml";

    public void goStores(){
        setContentPane(StoresController.viewPath);
    }
}
