package com.dp1mtel.desktop.controller;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.di.UsersHolder;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class LoginController implements Initializable {
    public static final String viewPath = "/com/dp1mtel/desktop/view/login.fxml";
    public static final String windowTitle = "Login";
    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;

    private UsersHolder usersHolder = new UsersHolder();

    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXPasswordField txtContrasena;
    @FXML
    private JFXButton btnIniciarSesion;
    @FXML
    private Label txtMensaje;
    @FXML
    private Label txtValidacion1;
    @FXML
    private Label txtValidacion2;


    public void login(Event event) throws IOException {
        System.out.println(txtUsuario.getText());
        if ( txtUsuario.getText().equals("") || txtContrasena.getText().equals("")){
            txtValidacion1.setText("*");
            txtValidacion2.setText("*");
            txtMensaje.setText("Los campos * son obligatorios");
        } else {
            usersHolder.getUsersRepository().loginUser(txtUsuario.getText(), txtContrasena.getText())
                    .subscribeOn(Schedulers.io())
                    .subscribe((_response_user) -> {
                        Platform.runLater(() -> {
                            if (_response_user.code() != 400) {
                                if (_response_user.body() != null) {
                                    try {
                                        System.out.println("El usuario.. " + txtUsuario.getText() + " se ha logeado correctamente");
                                        usersHolder.getUsersRepository().setCurrentUser(_response_user.body());
                                        if (_response_user.body().getShop()==null){
                                            txtMensaje.setText("Asegúrese que su superior le haya asignado a una tienda");
                                        }
                                        else{
                                            loadWindow(AppController.viewPath, AppController.windowTitle);
                                            String logMensaje="El usuario "+ txtUsuario.getText()+ " ingresó al sistema";
                                            LOGGERAUDIT.info(logMensaje);
                                        }


                                    } catch (IOException e) {
                                        System.out.println(e.getMessage());
                                    }
                                }
                            } else {
                                txtValidacion1.setText("");
                                txtValidacion2.setText("");
                                txtMensaje.setText("El usuario o la constraseña son incorrectos.");
                            }
                        });
                    },(ex) -> {
                        Platform.runLater(() -> System.err.println(ex.getMessage()));
                    });
        }
    }

    private void loadWindow(String viewPath, String windowTitle) throws IOException {
        txtUsuario.getScene().getWindow().hide();
        Parent newRoot =FXMLLoader.load(getClass().getResource(viewPath));
        Stage stage = new Stage();
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(newRoot));
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        btnIniciarSesion.setDefaultButton(true);
    }

}
