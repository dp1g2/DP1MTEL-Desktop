package com.dp1mtel.desktop.controller.storage.items;

import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Item;
import com.dp1mtel.desktop.model.User;
import com.jfoenix.controls.JFXButton;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class ItemCreateController extends BaseItemCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/items/create.fxml";
    public static final String windowTitle = "Crear un nuevo ítem";
    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @FXML
    private JFXButton btnCancel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnCancel;
    }

    @Override
    protected void saveItem() {
        try {
            Item item = new Item(auxCod.getText() + txtCod.getText(),txtName.getText(), txtDescription.getText(),"true",
                    expiresField.isSelected(),Double.parseDouble(spaceField.getText()));
            itemsHolder.getItemsRepository().saveItem(item)
                    .subscribeOn(Schedulers.io())
                    .subscribe((_response_item) -> {
                                Platform.runLater(() -> {
                                    if (_response_item.code() != 500) {
                                        if (_response_item.body() != null) {
                                            System.err.println(_response_item.body().getName());
                                            User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                            String logMensaje="CREACIÓN - Item '"+item.getId()+" "+item.getName()+"' - Usuario "+ usuario.getFullName();
                                            LOGGERAUDIT.info(logMensaje);
                                            setupFieldValidation();
                                            goBack();
                                        }
                                        else {
                                            //Show error message that the field is already used;
                                            AlertDialog.showMessage(stackPane,"Error al guardar","Dicho item ya existe");
                                        }
                                    }
                                });
                            },
                            (e) -> {
                                Platform.runLater(() -> System.err.println(e.getMessage()));
                            });
        } catch (Exception e) {
            System.out.println("NO GUARDO NADA");
            System.out.println(e);
        }
    }
}
