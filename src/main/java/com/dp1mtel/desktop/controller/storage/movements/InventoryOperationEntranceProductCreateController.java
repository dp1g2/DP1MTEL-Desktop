package com.dp1mtel.desktop.controller.storage.movements;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.di.ProductsHolder;
import com.dp1mtel.desktop.model.InventoryOperation;
import com.dp1mtel.desktop.model.Product;
import com.dp1mtel.desktop.model.User;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class InventoryOperationEntranceProductCreateController extends BaseInventoryOperationEntranceProductCreateController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/movements/registerProductEntranceMovement.fxml";


    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;

    private int contador = 0;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        loadModalDataTable();
    }


    public void saveMovement() {

        for (InventoryOperation op : operations) {

            op.setTypeOperation(MoveComboField.getValue());

        }
        inventoryOperationsHolder.getInventoryOperationsRepository().saveMassiveOperations(operations)
                .subscribeOn(Schedulers.io())
                .subscribe((_op) -> {
                    Platform.runLater(() -> {
                        if (Integer.parseInt(_op.getCorrectlyInserted()) == operations.size()) {

                            String logMensaje = "CREAR - Movimiento en almacén de entrada - Cantidad: " + operations.size();
                            LOGGERAUDIT.info(logMensaje);
                            goBack();
                        }
                    });
                }, (e) -> {
                    e.printStackTrace();
                });


    }




    public void onSelectElementClicked(MouseEvent mouseEvent) {

        // Mostrar el modal
        System.out.print("La lista de items esta vacia ? : ");
        System.out.println(productList.isEmpty());

        try{
            JFXDialogLayout content = new JFXDialogLayout();
            Pane header = FXMLLoader.load(getClass().getResource(SearchProductController.headViewPath));
            content.setHeading(header);
            Pane pane= FXMLLoader.load(getClass().getResource(SearchProductController.bodyViewPath));
            content.setBody(pane);

            content.getStylesheets().add("com/dp1mtel/desktop/styles/styles.css");

            JFXDialog dialog = new JFXDialog(dialogContainer,content,JFXDialog.DialogTransition.TOP);
            JFXButton aceptar =  new JFXButton("Aceptar");
            JFXButton cancelar =  new JFXButton("Cancelar");
            aceptar.getStyleClass().add("btn-success");
            cancelar.getStyleClass().add("btn-danger");


            aceptar.setOnAction(param ->{
                dialog.close();

                //Todo: Filtrar los items por el campo "Select" de cada item (solo true)
                List<Product> filteredList=productList.stream().filter((ob) -> {
                    return ob.getSelect() == true;
                }).collect(Collectors.toList());

                for (Product p: productList
                     ) {
                    p.setSelect(false);
                }

                setupInventoryOperationList(filteredList);
                setupItemTable();

                if(inventoryOperationList.size()>0) saveBtn.setDisable(false); else saveBtn.setDisable(true);
            });


            cancelar.setOnAction(param ->{
                dialog.close();
            });

            content.setActions(cancelar, aceptar);

            dialog.show();
        }catch(Exception ex){
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }

    }



    //cargar al inicio de la vista
    private void loadModalDataTable(){
        productsHolder.getProductsRepository().getProducts()
                .subscribe(_products ->{
                    productList= FXCollections.observableArrayList(_products);
                });
    }
}
