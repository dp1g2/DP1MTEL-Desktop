package com.dp1mtel.desktop.controller.storage.movements;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.InventoryOperationsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.InventoryOperation;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.text.ParseException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class InventoryOperationIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/movements/movements.fxml";
    public static final String windowTitle = "Inventario de Productos";
    @FXML private StackPane dialogContainer;

    @FXML private JFXButton btnNewInvMovement;

    @FXML private JFXTreeTableColumn<InventoryOperation, String>  dateColumn;
    @FXML private JFXTreeTableColumn<InventoryOperation, String>  typeColumn;
    @FXML private JFXTreeTableColumn<InventoryOperation, String>  productColumn;
    @FXML private JFXTreeTableColumn<InventoryOperation, String>  loteColumn;
    @FXML private JFXTreeTableColumn<InventoryOperation, String>  quanColumn;
    @FXML private JFXTreeTableColumn<InventoryOperation, String>  descColumn;
    @FXML private JFXTreeTableColumn<InventoryOperation, String>  indexColumn;


    @FXML private JFXTreeTableView<InventoryOperation> dataTable;
    @FXML private JFXTextField searchMovement;

    ObservableList<InventoryOperation> operationList;
    InventoryOperationsHolder inventoryOperationsHolder = new InventoryOperationsHolder();
    UsersHolder usersHolder = new UsersHolder();

    public static String selectedElement;
    public static String selectedType;


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {


        System.out.println("en initialize");
        loadIOperations();
    }

    private void loadIOperations() {
        inventoryOperationsHolder.getInventoryOperationsRepository().getInventoryOperations(usersHolder.getUsersRepository().getCurrentUser().getShop().getId())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (_operations) -> Platform.runLater(() -> setupInventoryOperationTable(_operations)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));

    }

    private void setupInventoryOperationTable(List<InventoryOperation> inventoryOperations) {
        operationList = FXCollections.observableArrayList(inventoryOperations);

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

            dateColumn.setCellValueFactory((param) -> {
                if (dateColumn.validateValue(param)) {

                    try{
                        return  new SimpleStringProperty(User.dateFormat.format(param.getValue().getValue().getDate()));

                    }catch (ParseException ex){

                        return new SimpleStringProperty();
                    }


                } else {
                    return dateColumn.getComputedValue(param);
                }
            });


        typeColumn.setCellValueFactory((param)->{
            if (typeColumn.validateValue(param)) {
                return param.getValue().getValue().getTypeOperation().nameProperty();
            } else {
                return typeColumn.getComputedValue(param);
            }
        });

        productColumn.setCellValueFactory((param)->{
            if (productColumn.validateValue(param)) {
                return param.getValue().getValue().itemProperty().getValue().nameProperty();
            } else {
                return productColumn.getComputedValue(param);
            }
        });

        loteColumn.setCellValueFactory((param)->{
            if (loteColumn.validateValue(param)) {
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getLot().getId()));
            } else {
                return loteColumn.getComputedValue(param);
            }
        });

        quanColumn.setCellValueFactory((param)->{
            if (quanColumn.validateValue(param)) {
                return new SimpleStringProperty(param.getValue().getValue().quantityProperty().getValue().toString());
            } else {
                return quanColumn.getComputedValue(param);
            }
        });

        descColumn.setCellValueFactory((param)->{
            if (descColumn.validateValue(param)) {
                return param.getValue().getValue().descriptionProperty();
            } else {
                return descColumn.getComputedValue(param);
            }
        });



        final TreeItem<InventoryOperation> root = new RecursiveTreeItem<>(operationList, RecursiveTreeObject::getChildren);
        dataTable.setRoot(root);
        dataTable.setShowRoot(false);
        dataTable.getColumns().setAll(indexColumn, dateColumn,typeColumn, loteColumn,productColumn,quanColumn );
        dataTable.setId("my-table");

        searchMovement.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                dataTable.setPredicate(new Predicate<TreeItem<InventoryOperation>>() {
                    @Override
                    public boolean test(TreeItem<InventoryOperation> inventoryOperationTreeItem) {
                        Boolean flag = inventoryOperationTreeItem.getValue().typeOperationProperty().getName().toLowerCase().contains(newValue.toLowerCase())||
                                inventoryOperationTreeItem.getValue().getItem().getName().toLowerCase().contains(newValue.toLowerCase())||
                                inventoryOperationTreeItem.getValue().getDescription().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });

    }



    public void onCreateInvOperationClicked(MouseEvent mouseEvent) {

        try{
            JFXDialogLayout content = new JFXDialogLayout();
            Text title = new Text("Nuevo Movimiento");
            title.setStyle("-fx-font-size: 25");
            content.setHeading(title);

            Pane pane = FXMLLoader.load(getClass().getResource(SelectMovementController.viewPath));
            content.setBody(pane);

            content.getStylesheets().add("com/dp1mtel/desktop/styles/styles.css");
            JFXButton aceptar = new JFXButton("Aceptar");
            JFXButton cancel = new JFXButton("Cancelar");
            aceptar.getStyleClass().add("btn-success");
            cancel.getStyleClass().add("btn-danger");

            JFXDialog dialog = new JFXDialog(dialogContainer,content,JFXDialog.DialogTransition.TOP);

            aceptar.setOnAction(event -> {
                dialog.close();

                if(selectedType == "IN" && selectedElement=="Item"){
                    setContentPane(InventoryOperationEntranceItemCreateController.viewPath);
                }else if(selectedType == "IN" && selectedElement=="Producto"){
                    setContentPane(InventoryOperationEntranceProductCreateController.viewPath);
                }else if(selectedType == "OUT" && selectedElement=="Item") {
                    InventoryOperationDepartureItemController.indexOperations = operationList;
                    setContentPane(InventoryOperationDepartureItemController.viewPath);
                }

            });
            cancel.setOnAction(event -> {
                dialog.close();
            });

            content.setActions(aceptar,cancel);
            dialog.show();

        }catch(Exception ex){
            System.out.println("Entre aqui");
            System.out.println(ex.getMessage());
        }

    }

    @Override protected Node getPaneReferenceComponent() {
        return btnNewInvMovement;
    }


}

