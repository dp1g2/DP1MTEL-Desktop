package com.dp1mtel.desktop.controller.storage.products;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.controller.management.upload.supplies.SupplyUploadController;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductCreateController extends BaseProductCreateController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/products/create.fxml";
    public static final String windowTitle = "Crear producto";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT = LogManager.getLogger("FileAuditAppender");
    private UsersHolder usersHolder = new UsersHolder();

    public static Item itemSelected;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        loadModalListItems();
    }

    private void loadModalListItems() {
        itemsHolder.getItemsRepository().getItems()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (_items) -> Platform.runLater(() -> modalList = FXCollections.observableArrayList(_items)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnCancel;
    }

    @Override
    protected void saveProduct() {
        try {
            Product product = new Product(
                    auxCod.getText() + txtCod.getText(),
                    txtName.getText()
                    , txtDescription.getText()
                    , txtBasePrice.getText()
                    , "true"
                    , new Category(txtCategory.getSelectionModel().getSelectedItem().getId()),
                    itemsQuantityList);

            productsHolder.getProductsRepository().saveProduct(product)
                    .subscribeOn(Schedulers.io())
                    .subscribe((_response_product) -> {
                                Platform.runLater(() -> {
                                    if (_response_product.code() != 500) {
                                        if (_response_product.body() != null) {
                                            User usuario = usersHolder.getUsersRepository().getCurrentUser();
                                            String logMensaje = "CREACIÓN - Producto de venta '" + product.getId() + " " + product.getName() + "' - Usuario " + usuario.getFullName();
                                            LOGGERAUDIT.info(logMensaje);
                                            setupFieldValidation();
                                            goBack();
                                        } else {
                                            //Show error message that the field is already used;
                                            AlertDialog.showMessage(stackPane, "Error al guardar", "Dicho producto ya existe");
                                        }
                                    }
                                });
                            },
                            (e) -> {
                                Platform.runLater(() -> System.err.println(e.getMessage()));
                            });

            //Save image if chosen
            if (!txtPathImage.getText().isEmpty()) {
                RequestBody idPart = RequestBody.create(MultipartBody.FORM,auxCod.getText() + txtCod.getText());

                RequestBody filePart = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part sentFile = MultipartBody.Part.createFormData("image", file.getName(), filePart);
                productsHolder.getProductsRepository().uploadImage(sentFile,idPart)
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                (_res) -> Platform.runLater(() -> {
                                    if (_res.code() == 200) {
                                        System.out.println("Image uploaded successfully");
                                    } else if (_res.code() == 400) {
                                        System.err.println("There was an error 400");
                                    } else {
                                        System.err.println("There was an error something else");
                                    }
                                }),
                                (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage())));
            }

        } catch (Exception e) {
            System.out.println("Couldnt save product correctly...");
            e.printStackTrace();
        }
    }

    public void onAddNewItemClicked(MouseEvent mouseEvent) {

        try {
            JFXDialogLayout content = new JFXDialogLayout();
            //Pane header = FXMLLoader.load(getClass().getResource(ProductSearchItemController.viewHeaderPath));
            //content.setHeading(header);
            Pane pane = FXMLLoader.load(getClass().getResource(ProductSearchItemController.viewPath));

            content.setBody(pane);

            JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);

            JFXButton add = new JFXButton("Agregar");
            JFXButton cancel = new JFXButton("Cancelar");

            add.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                    ProductHelp ph = new ProductHelp("0", itemSelected);
                    setupItemsTable(ph);
                }
            });

            cancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                }
            });

            content.setActions(cancel, add);
            dialog.show();

        } catch (Exception ex) {
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }
    }


}



