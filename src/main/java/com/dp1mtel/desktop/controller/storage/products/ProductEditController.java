package com.dp1mtel.desktop.controller.storage.products;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.model.Item;
import com.dp1mtel.desktop.model.Product;
import com.dp1mtel.desktop.model.ProductHelp;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductEditController extends BaseProductCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/products/edit.fxml";
    public static final String windowTitle = "Editar producto";

    public static Item itemSelected;
    public static Product modelComboBox = null;

    @Override
    public void initModel(Product model) {
        modelComboBox = model;
        this.productModel = model;
        txtCod.setEditable(false);
        txtCod.setText(productModel.getId().substring(3,productModel.getId().length()));
        txtName.setText(productModel.getName());
        txtDescription.setText(productModel.getDescription());
        txtBasePrice.setText(productModel.getPrice());
        final Double[] totalSum = {0.0};
        model.getListItems().forEach((productHelp -> {
            totalSum[0] += Double.parseDouble(productHelp.getQuantity())*productHelp.getItem().getVolume();
        }));
        txtUnitCharge.setText(totalSum[0].toString());
        setupItemsTable();
        loadModalListItems();
    }

    private void loadModalListItems() {
        itemsHolder.getItemsRepository().getItems()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (_items) -> Platform.runLater(() -> modalList = FXCollections.observableArrayList(_items)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    protected void setupItemsTable() {
        itemsQuantityList = FXCollections.observableArrayList(productModel.getListItems());

        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().getItem().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().getItem().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descriptionColumn.setCellValueFactory((param) -> {
            if (descriptionColumn.validateValue(param)) {
                return param.getValue().getValue().getItem().descriptionProperty();
            } else {
                return descriptionColumn.getComputedValue(param);
            }
        });

        quantityColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());

        quantityColumn.setCellValueFactory((param) -> {
            if (descriptionColumn.validateValue(param)) {
                return param.getValue().getValue().quantityProperty();
            } else {
                return descriptionColumn.getComputedValue(param);
            }
        });

        quantityColumn.setOnEditCommit(new EventHandler<TreeTableColumn.CellEditEvent<ProductHelp, String>>() {
            @Override
            public void handle(TreeTableColumn.CellEditEvent<ProductHelp, String> event) {
                TreeItem<ProductHelp> item = event.getRowValue();
                ProductHelp it = item.getValue();
                it.setQuantity(event.getNewValue());
                final Double[] totalSum = {0.0};
                itemsQuantityList.forEach((productHelp -> {
                    totalSum[0] += Double.parseDouble(productHelp.getQuantity())*productHelp.getItem().getVolume();
                }));
                txtUnitCharge.setText(totalSum[0].toString());
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<ProductHelp, Void>() {
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(deleteBtn);

            {

                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                deleteBtn.setOnMouseClicked((e) -> {
                    itemsQuantityList.remove(itemsTable.getTreeItem(getIndex()).getValue());
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<ProductHelp> root = new RecursiveTreeItem<>(itemsQuantityList, RecursiveTreeObject::getChildren);
        itemsTable.setRoot(root);
        itemsTable.setShowRoot(false);
        itemsTable.getColumns().setAll(indexColumn, nameColumn, descriptionColumn, quantityColumn, actionsColumn);
    }

    public void onAddNewItemClicked(MouseEvent mouseEvent) {

        try {
            JFXDialogLayout content = new JFXDialogLayout();
            //Pane header = FXMLLoader.load(getClass().getResource(ProductSearchItemController.viewHeaderPath));
            //content.setHeading(header);
            Pane pane = FXMLLoader.load(getClass().getResource(ProductSearchItemController.viewPath));

            content.setBody(pane);

            JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);

            JFXButton add = new JFXButton("Agregar");
            JFXButton cancel = new JFXButton("Cancelar");

            add.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                    ProductHelp ph = new ProductHelp("0", itemSelected);
                    setupItemsTable(ph);
                }
            });

            cancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                }
            });

            content.setActions(cancel, add);
            dialog.show();

        } catch (Exception ex) {
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}


