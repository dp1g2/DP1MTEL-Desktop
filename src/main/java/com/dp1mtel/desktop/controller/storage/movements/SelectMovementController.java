package com.dp1mtel.desktop.controller.storage.movements;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.model.InventoryOperation;
import com.dp1mtel.desktop.model.InventoryOperationType;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXRadioButton;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

import static com.dp1mtel.desktop.model.InventoryOperation.typeProdMap;

public class SelectMovementController extends BaseController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/movements/selectMovement.fxml";

    @FXML private JFXComboBox<String> elementField;
    @FXML private JFXRadioButton inField;
    @FXML private JFXRadioButton outField;

    @FXML private ToggleGroup toogle;



    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);



        elementField.setItems(FXCollections.observableArrayList(typeProdMap.values()));

        inField.setUserData(InventoryOperationType.TypeMov.IN);
        outField.setUserData(InventoryOperationType.TypeMov.OUT);

        toogle.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {

            if(toogle.getSelectedToggle()!= null){
                InventoryOperationIndexController.selectedType = newValue.getUserData().toString();


                if(newValue.getUserData().toString().equals("OUT")){
                    elementField.getItems().remove(typeProdMap.get(InventoryOperation.TypeProd.PRODUCTO));
                }else{

                    if(elementField.getItems().size()==1)
                        elementField.getItems().add(typeProdMap.get(InventoryOperation.TypeProd.PRODUCTO));
                }


            }
        });

        inField.setSelected(true);


    }

    @Override
    protected Node getPaneReferenceComponent() {
        return inField;
    }


    public void onElementSelected(ActionEvent actionEvent) {


        InventoryOperationIndexController.selectedElement = elementField.getValue();
    }


}
