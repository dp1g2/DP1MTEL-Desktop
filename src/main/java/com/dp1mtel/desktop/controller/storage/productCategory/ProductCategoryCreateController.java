package com.dp1mtel.desktop.controller.storage.productCategory;

import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Category;
import com.dp1mtel.desktop.model.User;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.scene.Node;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class ProductCategoryCreateController extends BaseCategoryCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/productCategories/create.fxml";
    public static final String windowTitle = "Crear categoría de productos";
    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnCancel;
    }

    @Override
    protected void saveCategory() {
        Category category = new Category(auxCod.getText() + txtCod.getText(), txtName.getText(), txtDescription.getText(),"true");
        categoriesHolder.getCategoriesRepository().saveCategory(category)
                .subscribeOn(Schedulers.io())
                .subscribe((_response_category) -> {
                            Platform.runLater(() -> {
                                if (_response_category.code() != 500) {
                                    if (_response_category.body() != null) {
                                        System.err.println(_response_category.body().getName());
                                        setupFieldValidation();
                                        goBack();
                                    }
                                    else {
                                        //Show error message that the field is already used;
                                        AlertDialog.showMessage(stackPane,"Error al guardar","Dicha categoria ya existe");
                                    }
                                }
                            });
                        },
                        (e) -> {
                            Platform.runLater(() -> System.err.println(e.getMessage()));
                        });
        User usuario= usersHolder.getUsersRepository().getCurrentUser();
        String logMensaje="CREACIÓN - Categoría '"+category.getId()+category.getName()+"' - Usuario "+ usuario.getFullName();
        LOGGERAUDIT.info(logMensaje);
    }
}
