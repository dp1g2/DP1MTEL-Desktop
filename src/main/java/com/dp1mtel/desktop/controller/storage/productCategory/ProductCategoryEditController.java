package com.dp1mtel.desktop.controller.storage.productCategory;

import com.dp1mtel.desktop.model.Category;

public class ProductCategoryEditController extends BaseCategoryCreateController{
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/productCategories/edit.fxml";
    public static final String windowTitle = "Editar Familia de Producto";

    @Override
    public void initModel(Category model) {
        this.categoryModel = model;
        txtCod.setText(categoryModel.getId().substring(3,categoryModel.getId().length()));
        txtName.setText(categoryModel.getName());
        txtDescription.setText(categoryModel.getDescription());
    }
}
