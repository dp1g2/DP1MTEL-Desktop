package com.dp1mtel.desktop.controller.storage.productCombo;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductComboCreateController extends BaseProductComboCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/productCombo/create.fxml";
    public static final String windowTitle = "Crear combo de productos";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT = LogManager.getLogger("FileAuditAppender");
    private UsersHolder usersHolder = new UsersHolder();

    public static Product productSelected;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        loadModalListProducts();
    }

    private void loadModalListProducts() {
        productsHolder.getProductsRepository().getProducts()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (_products) -> Platform.runLater(() -> modalList = FXCollections.observableArrayList(_products)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnCancel;
    }

    @Override
    protected void saveProductCombo() {
        try {
            ProductCombo productCombo = new ProductCombo(
                    auxCod.getText() + txtCod.getText(),
                    txtName.getText(),
                    txtDescription.getText(),
                    txtRealPrice.getText(),
                    "true",
                    productsQuantityList);

            productComboHolder.getProductComboRepository().saveProductCombo(productCombo)
                    .subscribeOn(Schedulers.io())
                    .subscribe((_response_productCombo) -> {
                                Platform.runLater(() -> {
                                    if (_response_productCombo.code() != 500) {
                                        if (_response_productCombo.body() != null) {
                                            User usuario = usersHolder.getUsersRepository().getCurrentUser();
                                            String logMensaje = "CREACIÓN - Combo de productos '" + productCombo.getId() + " " + productCombo.getName() + "' - Usuario " + usuario.getFullName();
                                            LOGGERAUDIT.info(logMensaje);
                                            setupFieldValidation();
                                            goBack();
                                        } else {
                                            //Show error message that the field is already used;
                                            AlertDialog.showMessage(stackPane,"Error al guardar","Dicho combo ya existe");
                                        }
                                    }
                                });
                            },
                            (e) -> {
                                Platform.runLater(() -> System.err.println(e.getMessage()));
                            });
            if (!txtPathImage.getText().isEmpty()) {
                RequestBody idPart = RequestBody.create(MultipartBody.FORM,auxCod.getText() + txtCod.getText());

                RequestBody filePart = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part sentFile = MultipartBody.Part.createFormData("image", file.getName(), filePart);
                productComboHolder.getProductComboRepository().uploadImage(sentFile,idPart)
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                (_res) -> Platform.runLater(() -> {
                                    if (_res.code() == 200) {
                                        System.out.println("Image uploaded successfully");
                                    } else if (_res.code() == 400) {
                                        System.err.println("There was an error 400");
                                    } else {
                                        System.err.println("There was an error something else");
                                    }
                                }),
                                (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage())));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onAddNewProductClicked(MouseEvent mouseEvent) {

        try {
            JFXDialogLayout content = new JFXDialogLayout();
            //Pane header = FXMLLoader.load(getClass().getResource(ProductSearchItemController.viewHeaderPath));
            //content.setHeading(header);
            Pane pane = FXMLLoader.load(getClass().getResource(ProductComboSearchProductController.viewPath));
            content.setBody(pane);
            JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);

            JFXButton add = new JFXButton("Agregar");
            JFXButton cancel = new JFXButton("Cancelar");

            add.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    dialog.close();
                    System.out.print("Nombre item: " + productSelected.getName());
                    ProductComboHelp ph = new ProductComboHelp("0", productSelected);
                    setupProductsTable(ph);
                }
            });

            cancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                }
            });

            content.setActions(cancel, add);

            dialog.show();
        } catch (Exception ex) {
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }
    }


}
