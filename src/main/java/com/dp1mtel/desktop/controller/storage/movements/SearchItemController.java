package com.dp1mtel.desktop.controller.storage.movements;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.model.Item;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;


public class SearchItemController extends BaseController {


    public static final String headViewPath = "/com/dp1mtel/desktop/view/storage/movements/selectItemHeader.fxml";
    public static final String bodyViewPath = "/com/dp1mtel/desktop/view/storage/movements/selectItemBody.fxml";

    @FXML private JFXTreeTableView<Item> dataTable;
    @FXML private JFXTreeTableColumn<Item,String> indexColumn;
    @FXML private JFXTreeTableColumn<Item,String> nameColumn;
    @FXML private JFXTreeTableColumn<Item,String> codeColumn;
    @FXML private JFXTreeTableColumn<Item,CheckBox> checkColumn;




    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);

        //Todo: Al iniciarse la vista debe cargarse la tabla.
        //Uso la lista de items que cargue en la vista de crear movimiento de iventario.
        setupTable();


    }


    private void setupTable(){



        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        nameColumn.setCellValueFactory((param) -> {
            if(nameColumn.validateValue(param)){
                return param.getValue().getValue().nameProperty();
            }else{
                return nameColumn.getComputedValue(param);
            }

        });

        codeColumn.setCellValueFactory((param)->{
            if(codeColumn.validateValue(param)){
                return  param.getValue().getValue().idProperty();
            }else{
                return codeColumn.getComputedValue(param);
            }
        });

        checkColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Item, CheckBox>,
                ObservableValue<CheckBox>>() {

            @Override
            public ObservableValue<CheckBox> call (
                    TreeTableColumn.CellDataFeatures<Item,CheckBox> arg0){

                Item item = arg0.getValue().getValue();
                JFXCheckBox checkBox = new JFXCheckBox();
                checkBox.selectedProperty().setValue(item.isSelect());

                checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable,
                                        Boolean oldValue, Boolean newValue) {

                        item.setSelect(newValue);
                        //validateFields();
                    }
                });

                return new SimpleObjectProperty<CheckBox>(checkBox);
            }

        });

        final TreeItem<Item> root = new RecursiveTreeItem<>(BaseInventoryOperationEntranceItemCreateController.itemList, RecursiveTreeObject::getChildren);
        dataTable.setRoot(root);
        dataTable.setShowRoot(false);
        dataTable.getColumns().setAll(indexColumn ,codeColumn, nameColumn, checkColumn );

    }

//    @Override protected Node getPaneReferenceComponent() {
//        return dataTable;
//    }

}
