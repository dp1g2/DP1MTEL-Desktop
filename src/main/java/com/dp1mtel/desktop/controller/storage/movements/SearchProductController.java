package com.dp1mtel.desktop.controller.storage.movements;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.model.Product;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class SearchProductController extends BaseController {

    public static final String headViewPath = "/com/dp1mtel/desktop/view/storage/movements/selectProductHeader.fxml";
    public static final String bodyViewPath = "/com/dp1mtel/desktop/view/storage/movements/selectProductBody.fxml";

    @FXML private JFXTreeTableView<Product> dataTable;
    @FXML private JFXTreeTableColumn<Product,String> indexColumn;
    @FXML private JFXTreeTableColumn<Product,String> nameColumn;
    @FXML private JFXTreeTableColumn<Product,String> codeColumn;
    @FXML private JFXTreeTableColumn<Product,CheckBox> checkColumn;


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupTable();
    }

    private void setupTable(){


        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        nameColumn.setCellValueFactory((param) -> {
            if(nameColumn.validateValue(param)){
                return param.getValue().getValue().nameProperty();
            }else{
                return nameColumn.getComputedValue(param);
            }

        });

        codeColumn.setCellValueFactory((param)->{
            if(codeColumn.validateValue(param)){
                return  param.getValue().getValue().idProperty();
            }else{
                return codeColumn.getComputedValue(param);
            }
        });

        checkColumn.setCellValueFactory(param -> {
            Product product = param.getValue().getValue();
            JFXCheckBox checkBox = new JFXCheckBox();
            checkBox.selectedProperty().setValue(product.getSelect());

            checkBox.selectedProperty().addListener((observable, oldValue,newValue) ->{
                product.setSelect(newValue);

            });


            return new SimpleObjectProperty<CheckBox>(checkBox);
        });



        final TreeItem<Product> root = new RecursiveTreeItem<>(BaseInventoryOperationEntranceProductCreateController.productList, RecursiveTreeObject::getChildren);
        dataTable.setRoot(root);
        dataTable.setShowRoot(false);
        dataTable.getColumns().setAll(indexColumn ,codeColumn, nameColumn, checkColumn );

    }


}
