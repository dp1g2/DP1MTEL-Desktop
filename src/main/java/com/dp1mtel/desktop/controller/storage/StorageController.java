package com.dp1mtel.desktop.controller.storage;

import com.dp1mtel.desktop.controller.storage.items.ItemController;
import com.dp1mtel.desktop.controller.storage.movements.InventoryOperationIndexController;
import com.dp1mtel.desktop.controller.storage.productCombo.ProductComboController;
import com.dp1mtel.desktop.controller.storage.productCategory.ProductCategoryIndexController;
import com.dp1mtel.desktop.controller.storage.products.ProductController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import com.dp1mtel.desktop.controller.BaseController;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class StorageController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/index.fxml";

    @FXML
    private JFXButton btnProducts;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url,resourceBundle);
    }
    @Override
    protected Node getPaneReferenceComponent() {
        return btnProducts;
    }

    public void onProductFamiliesClicked(MouseEvent mouseEvent) {
        setContentPane(ProductCategoryIndexController.viewPath);
    }
    public void onItemsClicked(MouseEvent mouseEvent) {
        setContentPane(ItemController.viewPath);
    }
    public void onProductsClicked(MouseEvent mouseEvent) {
        setContentPane(ProductController.viewPath);
    }
    public void onComboProductsClicked(MouseEvent mouseEvent) {setContentPane(ProductComboController.viewPath);}
    public void onMovementsClicked(MouseEvent mouseEvent) {
        setContentPane(InventoryOperationIndexController.viewPath);
    }

}
