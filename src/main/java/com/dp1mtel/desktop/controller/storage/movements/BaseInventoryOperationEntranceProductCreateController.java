package com.dp1mtel.desktop.controller.storage.movements;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.InventoryOperationTypesHolder;
import com.dp1mtel.desktop.di.InventoryOperationsHolder;
import com.dp1mtel.desktop.di.ProductsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.dp1mtel.desktop.util.validation.DateValidator;
import com.jfoenix.controls.*;
import com.jfoenix.controls.cells.editors.TextFieldEditorBuilder;
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTreeTableCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

import javax.xml.soap.Text;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;


public class BaseInventoryOperationEntranceProductCreateController extends GenericBaseController<InventoryOperation> {

    public static ObservableList<Product> productList;
    public JFXComboBox<InventoryOperationType> MoveComboField;


    public JFXButton cancelBtn;
    public JFXButton saveBtn;
    public JFXTreeTableView<InventoryOperation> operationsTable;
    public JFXTreeTableColumn<InventoryOperation, String> indexColumn;
    public JFXTreeTableColumn<InventoryOperation, String> codeColumn;
    public JFXTreeTableColumn<InventoryOperation, String> nameColumn;
    public JFXTreeTableColumn<InventoryOperation, String> cantColumn;
    public JFXTreeTableColumn<InventoryOperation, String> descColumn;
    //    public JFXTreeTableColumn<InventoryOperation, String> lotColumn;
    public JFXTreeTableColumn<InventoryOperation, JFXDatePicker> expColumn;
    public JFXTreeTableColumn<InventoryOperation, Void> actionsColumn;
    public JFXTreeTableColumn<InventoryOperation, Boolean> expiresColumn;
    public StackPane dialogContainer;

    public InventoryOperationsHolder inventoryOperationsHolder = new InventoryOperationsHolder();
    public InventoryOperationTypesHolder inventoryOperationTypesHolder = new InventoryOperationTypesHolder();

    protected List<InventoryOperation> inventoryOperationList = new ArrayList<>();
    ;

    //IMPORTANTE PARA EL MODAL
    protected ProductsHolder productsHolder = new ProductsHolder();
    protected UsersHolder usersHolder = new UsersHolder();

    protected User currentUser;
    protected Shop currentShop;

    protected ObservableList<InventoryOperation> operations;

    protected String description = "Ingrese descripcion";


    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        loadComboBoxInventoryOperationType(); // lleno la filteredList del combo box
        saveBtn.setDisable(true);

        currentUser = usersHolder.getUsersRepository().getCurrentUser();
        currentShop = currentUser.getShop();
    }

    private void loadComboBoxInventoryOperationType() {

        inventoryOperationTypesHolder.getInventoryOperationTypeRepository().getInventoryOperationTypes()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (types) -> Platform.runLater(() -> setupMovementsOnComboBox(types)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }


    //FILTRA POR RADIO SELECCIONADO
    private void setupMovementsOnComboBox(List<InventoryOperationType> operations) {


        System.out.println("Entre a setupMovementsOnComboBox");


        List<InventoryOperationType> filteredList = operations.stream().filter((ob) -> {
            return ob.getMultiplier() == 1 && !ob.getName().equals("Devolucion") &&   !ob.getName().equals( "Devolución");
        }).collect(Collectors.toList());

        MoveComboField.setItems(FXCollections.observableArrayList(filteredList));


    }

    protected void setupItemTable() {


        operations = FXCollections.observableArrayList(inventoryOperationList);

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().getItem().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }

        });

        codeColumn.setCellValueFactory((param) -> {
            if (codeColumn.validateValue(param)) {
                return param.getValue().getValue().getItem().idProperty();
            } else {
                return codeColumn.getComputedValue(param);
            }
        });

        cantColumn.setCellFactory(param -> new GenericEditableTreeTableCell( new TextFieldEditorBuilder()) );

        cantColumn.setOnEditCommit((param) -> {
            TreeItem<InventoryOperation> treeItem = param.getRowValue();
            InventoryOperation op = treeItem.getValue();

            try {

                int aux = Integer.parseInt(param.getNewValue());
                if(aux<0 || aux == 0) {
                    AlertDialog.showMessage(dialogContainer,"Error","Cantidad no puede ser negativa");
                    op.setQuantity(0);
                }else{
                    op.setQuantity(aux);
                }

            } catch (Exception ex) {
                AlertDialog.showMessage(dialogContainer, "Error", "Cantidad inválida");
                op.setQuantity(0);
            }
        });


        cantColumn.setCellValueFactory((param) -> {
            if (cantColumn.validateValue(param)) {
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getQuantity()));
            } else {
                return cantColumn.getComputedValue(param);
            }
        });



        descColumn.setCellFactory(param -> new GenericEditableTreeTableCell( new TextFieldEditorBuilder()) );

        descColumn.setCellValueFactory((param) -> {
            if (descColumn.validateValue(param)) {
                return new SimpleStringProperty(param.getValue().getValue().getDescription());
            } else {
                return descColumn.getComputedValue(param);
            }
        });

        descColumn.setOnEditCommit(param ->{
            TreeItem<InventoryOperation> opTree = param.getRowValue();
                InventoryOperation op = opTree.getValue();
                op.setDescription(param.getNewValue());
        });



        expColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InventoryOperation, JFXDatePicker>,
                ObservableValue<JFXDatePicker>>() {

            @Override
            public ObservableValue<JFXDatePicker> call(
                    TreeTableColumn.CellDataFeatures<InventoryOperation, JFXDatePicker> arg0) {

                InventoryOperation op = arg0.getValue().getValue();

                JFXDatePicker picker = new JFXDatePicker();
                JFXDatePicker minDate = new JFXDatePicker();

                minDate.setValue(DateExtensions.toLocalDate(new Date())); // minimo la fecha de hoy
                final Callback<DatePicker, DateCell> dayCellFactory;

                dayCellFactory = (final DatePicker datePicker) -> new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item.isBefore(minDate.getValue())) {
                            setDisable(true);
                            setVisible(false);
                            setStyle("-fx-background-color: #ffc0cb;");
                        } else {
                            setVisible(true);
                            setDisable(false);
                        }
                    }

                };
                picker.setDayCellFactory(dayCellFactory);

//                picker.setEditable(false);
                picker.setPromptText("Ingrese Fecha");

                picker.valueProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {

                        if(op.getLot()==null) op.setLot(new Lot(DateExtensions.toDate(newValue)));
                        else op.getLot().setExpirationDate(DateExtensions.toDate(newValue));

                    }
                });

                picker.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {

                    try {
                        LocalDate nowDate = DateExtensions.toLocalDate(new Date());
                        int nowDateLength = User.dateFormat.format(new Date()).length();

                        if (newValue.length() == nowDateLength) {
                            LocalDate newDate = DateExtensions.toLocalDate(User.dateFormat.parse(newValue));

                            if (newDate.isBefore(nowDate)) {
                                //picker.getEditor().textProperty().setValue("");
                                AlertDialog.showMessage(dialogContainer, "Cuidado", "La fecha que ingreso es menor a la actual");
                            }else{

                                //correcto
                                if (op.getLot() == null) op.setLot(new Lot(User.dateFormat.parse(newValue)));
                                else op.getLot().setExpirationDate(User.dateFormat.parse(newValue));
                            }
                        }


                    } catch (ParseException ex) {
                        System.out.println(ex.getLocalizedMessage());
                        AlertDialog.showMessage(dialogContainer, "Error", "Formato Incorrecto de Fecha (dd/mm/yyyy)");

                    }
                });

                if(!op.getItem().isExpires()){
                    picker.setDisable(true);
                }

                return new SimpleObjectProperty<JFXDatePicker>(picker);
            }

        });


        expiresColumn.setCellFactory(CheckBoxTreeTableCell.forTreeTableColumn(expiresColumn));
        expiresColumn.setEditable(false);

        expiresColumn.setCellValueFactory((param) -> {
            if (expiresColumn.validateValue(param)) {
                return new SimpleBooleanProperty(param.getValue().getValue().getItem().isExpires());
            } else {
                return expiresColumn.getComputedValue(param);
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<InventoryOperation, Void>() {
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                deleteBtn.setOnMouseClicked((event) -> {
                    System.out.println("Trash can");
                    inventoryOperationList.remove(operationsTable.getTreeItem(getIndex()).getValue());
                    operations.remove(operationsTable.getTreeItem(getIndex()).getValue());
                    operationsTable.refresh();


                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<InventoryOperation> root = new RecursiveTreeItem<>(operations, RecursiveTreeObject::getChildren);
        operationsTable.setRoot(root);
        operationsTable.setShowRoot(false);
        operationsTable.getColumns().setAll(indexColumn, codeColumn, nameColumn, cantColumn, expiresColumn, expColumn, actionsColumn);

        operationsTable.setEditable(true);
        operationsTable.getSelectionModel().cellSelectionEnabledProperty().set(true);


        nameColumn.setStyle("-fx-alignment: CENTER");

    }


    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();
    }

    public void onSaveClicked(MouseEvent mouseEvent) {

        //Verificar los campos segun el tipo de movimiento

        if (validateFields()) {
            saveMovement();
        } else {
//            AlertDialog.showMessage(dialogContainer, "Error", "Aun tiene campos por rellenar");

            System.out.println("DEBE LLENAR TODOS LOS CAMPOS EDITABLES");
        }

    }

    private boolean validateFields() {

        // validar que haya escogido un tipo de movimento
        if (MoveComboField.getValue() == null) {
            AlertDialog.showMessage(dialogContainer, "Error", "Debe escoger un Movimiento");
            return false;
        }

        try {

            for (InventoryOperation i : inventoryOperationList) {

                if(i.getQuantity() == 0){
                    AlertDialog.showMessage(dialogContainer,"Error", "Debe llenar: Cantidad");
                    return false;
                }
                if(i.getLot()==null){
                    AlertDialog.showMessage(dialogContainer,"Error","Debe llenar: Fecha Expiracion");
                    System.out.println("lote nulo");
                    return false;
                }

                if(i.getQuantity()<0){
                    AlertDialog.showMessage(dialogContainer,"Error","Cantidad no puede ser negativo");
                    return false;
                }

            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return true;
    }

    protected void setupInventoryOperationList(List<Product> filteredList) {

        if (inventoryOperationList != null) {
            inventoryOperationList = new ArrayList<>(); // vacio la lista ps
        }
        for (Product p : filteredList) {

            List<ProductHelp> helpList = p.getListItems();

            for (ProductHelp ph : helpList) {

                Item i = ph.getItem();
                if (!InventoryOperation.containsItem(inventoryOperationList, i.getId())) {

                    InventoryOperation op = new InventoryOperation();

                    op.setStore(currentShop);
                    op.setUser(currentUser);
                    op.setItem(i);
                    op.setDate(new Date());
                    op.setDescription(description);

                    if(!op.getItem().isExpires()){
                        op.setLot( new Lot(new Long(0),"0", new Date()));
                    }


                    inventoryOperationList.add(op);

                }

            }
        }
    }


    public void saveMovement() {

    }


    public void goBack() {
        setContentPane(InventoryOperationIndexController.viewPath);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return saveBtn;
    }

}

