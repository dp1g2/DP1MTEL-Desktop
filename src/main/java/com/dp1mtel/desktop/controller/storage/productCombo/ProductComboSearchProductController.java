package com.dp1mtel.desktop.controller.storage.productCombo;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.model.Item;
import com.dp1mtel.desktop.model.Product;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableRow;
import javafx.scene.input.MouseButton;

import java.net.URL;
import java.util.ResourceBundle;

public class ProductComboSearchProductController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/productCombo/searchProduct.fxml";
    //public static final String viewHeaderPath = "/com/dp1mtel/desktop/view/storage/products/searchHeader.fxml";

    @FXML
    private JFXTreeTableView<Product> productsTable;
    @FXML
    private JFXTreeTableColumn<Product, String> indexColumn;
    @FXML
    private JFXTreeTableColumn<Product, String> nameColumn;
    @FXML
    private JFXTreeTableColumn<Product, String> descripColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setupProductsTable();
    }

    private void setupProductsTable() {

        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descripColumn.setCellValueFactory((param) -> {
            if (descripColumn.validateValue(param)) {
                return param.getValue().getValue().descriptionProperty();
            } else {
                return descripColumn.getComputedValue(param);
            }
        });

        final TreeItem<Product> root = new RecursiveTreeItem<>(BaseProductComboCreateController.modalList, RecursiveTreeObject::getChildren);
        productsTable.setRoot(root);
        productsTable.setShowRoot(false);
        productsTable.getColumns().setAll(indexColumn, nameColumn, descripColumn);


        productsTable.setRowFactory(tv -> {
            TreeTableRow<Product> row = new TreeTableRow<Product>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY) {
                    ProductComboCreateController.productSelected = row.getItem();
                    ProductComboEditController.productSelected = row.getItem();
                }
            });
            return row;
        });
    }


    @Override
    protected Node getPaneReferenceComponent() {
        return productsTable;
    }
}
