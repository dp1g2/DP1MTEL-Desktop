package com.dp1mtel.desktop.controller.storage.items;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.ItemsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Item;
import com.dp1mtel.desktop.model.User;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class ItemController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/items/index.fxml";
    public static final String windowTitle = "Items";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    private final String deleteTitle = "Operación realizada";
    private final String deleteMessage = "La categoría ha sido eliminada correctamente";

    @FXML
    private JFXButton btnNewItem;
    @FXML
    private JFXTreeTableView<Item> itemsTable;
    @FXML
    private JFXTreeTableColumn<Item, String> indexColumn;
    @FXML
    private JFXTreeTableColumn<Item, String> nameColumn;
    @FXML
    private JFXTreeTableColumn<Item, String> descripColumn;
    @FXML
    private JFXTreeTableColumn<Item, Void> actionsColumn;
    @FXML
    private StackPane stackPane;
    private ItemsHolder itemsHolder = new ItemsHolder();
    @FXML
    private JFXTextField searchItem;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        loadItems();
    }

    private void loadItems() {
        itemsHolder.getItemsRepository().getItems()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (items) -> Platform.runLater(() -> setupItemsTable(items)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));

    }

    private void setupItemsTable(List<Item> items) {
        ObservableList<Item> itemList = FXCollections.observableArrayList(items);
        //TreeTableColumnExtensions.makeIndexColumn(indexColumn);
        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });


        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descripColumn.setCellValueFactory((param) -> {
            if (descripColumn.validateValue(param)) {
                return param.getValue().getValue().descriptionProperty();
            } else {
                return descripColumn.getComputedValue(param);
            }
        });


        actionsColumn.setCellFactory((col) -> new TreeTableCell<Item, Void>() {

            private final MaterialDesignIconView editBtn = new MaterialDesignIconView(MaterialDesignIcon.PENCIL);
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);

            private final HBox pane = new HBox(editBtn, deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                editBtn.setOnMouseClicked((e) -> {
                    Item item = itemsTable.getTreeItem(getIndex()).getValue();
                    setContentPane(ItemEditController.viewPath, ItemEditController.windowTitle, item);
                });

                //BEGIN DELETE BUTTON
                deleteBtn.setOnMouseClicked((e) -> {
                    JFXDialogLayout content = new JFXDialogLayout();
                    content.setHeading(new Text("Confirmación"));
                    content.setBody(new Text("Está seguro que desea eliminar el ítem?"));
                    JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);
                    JFXButton add = new JFXButton("Aceptar");
                    JFXButton cancel = new JFXButton("Cancelar");
                    add.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            dialog.close();
                            //BEGIN OPERATION
                            Item item = itemsTable.getTreeItem(getIndex()).getValue();

                            itemsHolder.getItemsRepository().disableItem(item)
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(
                                            (_res) -> Platform.runLater(() -> {
                                                if (_res.code() == 200) {
                                                    loadItems();
                                                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                                    String logMensaje="ELIMINAR - Item '"+item.getId()+" "+item.getName()+"' - Usuario "+ usuario.getFullName();
                                                    LOGGERAUDIT.info(logMensaje);
                                                    AlertDialog.showMessage(stackPane,
                                                            "Operación exitosa", "Se ha borrado correctamente el ítem");
                                                } else if (_res.code() == 400) {
                                                    AlertDialog.showMessage(stackPane,
                                                            "Error al eliminar", "Verifique que el ítem no está en uso");
                                                } else {
                                                    AlertDialog.showMessage(stackPane,
                                                            "Error al eliminar",  "Hubo un error. Contacte al administrador");
                                                }
                                            }),
                                            (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage())));
                            //END OPERATION
                        }
                    });
                    cancel.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            dialog.close();
                        }
                    });
                    content.setActions(cancel, add);
                    dialog.show();
                });
                //END DELETE BUTTON
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<Item> root = new RecursiveTreeItem<>(itemList, RecursiveTreeObject::getChildren);
        itemsTable.setRoot(root);
        itemsTable.setShowRoot(false);
        itemsTable.getColumns().setAll(indexColumn, nameColumn, descripColumn, actionsColumn);

        searchItem.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                itemsTable.setPredicate(new Predicate<TreeItem<Item>>() {
                    @Override
                    public boolean test(TreeItem<Item> itemTreeItem) {
                        Boolean flag = itemTreeItem.getValue().getId().toLowerCase().contains(newValue.toLowerCase())||
                                itemTreeItem.getValue().getName().toLowerCase().contains(newValue.toLowerCase())||
                                itemTreeItem.getValue().getDescription().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnNewItem;
    }

    public void onCreateItemClicked(MouseEvent mouseEvent) {
        setContentPane(ItemCreateController.viewPath);
    }

    public void confirmationMessage(){

            JFXDialogLayout content = new JFXDialogLayout();
            content.setHeading(new Text("Confirmación"));
            content.setBody(new Text("Está seguro que desea eliminar el ítem?"));
            JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);
            JFXButton add = new JFXButton("Aceptar");
            JFXButton cancel = new JFXButton("Cancelar");
            add.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                }
            });
            cancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                }
            });
            content.setActions(cancel, add);
            dialog.show();
    }
}
