package com.dp1mtel.desktop.controller.storage.movements;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.InventoryOperationTypesHolder;
import com.dp1mtel.desktop.di.LotsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class BaseInventoryOperationDepartureItemCreateController extends GenericBaseController<InventoryOperation> {


    public JFXButton addLotBtn;
    public JFXComboBox<Lot> lotField;
    public JFXComboBox<InventoryOperationType> moveField;
    public JFXButton cancelBtn;
    public JFXButton saveBtn;
    public JFXTreeTableView<InventoryOperation>  operationsTable;
    public JFXTreeTableColumn<InventoryOperation,String> indexColumn;
    public JFXTreeTableColumn<InventoryOperation,String> codeColumn;
    public JFXTreeTableColumn<InventoryOperation,String> nameColumn;
    public JFXTreeTableColumn<InventoryOperation,String> lotColumn;
    public JFXTreeTableColumn<InventoryOperation,String> cantColumn;
    public  JFXTreeTableColumn<InventoryOperation,String> realCantColumn;
    public JFXTreeTableColumn<InventoryOperation,String> descColumn;
    public JFXTreeTableColumn<InventoryOperation,Void> actionsColumn;
    public StackPane dialogContainer;

    protected User currentUser;
    protected Shop currentShop;
    private UsersHolder usersHolder = new UsersHolder();


    protected String description = "Ingrese Descripcion";
    public static List<InventoryOperation> indexOperations;
    protected ObservableList<InventoryOperation> operations = FXCollections.observableArrayList();
    private InventoryOperationTypesHolder inventoryOperationTypesHolder = new InventoryOperationTypesHolder();


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupLotComboBox();

        inventoryOperationTypesHolder.getInventoryOperationTypeRepository().getInventoryOperationTypes()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (types) -> Platform.runLater(() -> setupMoveComboBox(types)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));

        setupOperationsTable();

        currentUser = usersHolder.getUsersRepository().getCurrentUser();
        currentShop = currentUser.getShop();
    }

    private void setupMoveComboBox(List<InventoryOperationType> moveTypes) {

        List<InventoryOperationType> filteredList = moveTypes.stream().filter((ob) -> {
            return ob.getMultiplier() == -1;
        }).filter((par)->{
            return !par.getName().equals("Venta");
        }).collect(Collectors.toList());

        moveField.setItems( FXCollections.observableArrayList(filteredList) );
    }

    private void setupOperationsTable() {

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        operationsTable.setEditable(true);
        operationsTable.getSelectionModel().cellSelectionEnabledProperty().set(true);


        nameColumn.setStyle("-fx-alignment: CENTER");

        nameColumn.setCellValueFactory((param) -> {
            if(nameColumn.validateValue(param)){
                return param.getValue().getValue().getItem().nameProperty();
            }else{
                return nameColumn.getComputedValue(param);
            }

        });


        codeColumn.setCellValueFactory((param)->{
            if(codeColumn.validateValue(param)){
                return  param.getValue().getValue().getItem().idProperty();
            }else{
                return codeColumn.getComputedValue(param);
            }
        });

        cantColumn.setCellFactory(TextFieldTreeTableCell.<InventoryOperation>forTreeTableColumn());

        cantColumn.setOnEditCommit(new EventHandler<TreeTableColumn.CellEditEvent<InventoryOperation, String>>() {
            @Override
            public void handle(TreeTableColumn.CellEditEvent<InventoryOperation, String> event) {

                TreeItem<InventoryOperation> opTree = event.getRowValue();
                InventoryOperation op = opTree.getValue();
                try{
                    int aux = Integer.parseInt(event.getNewValue());
                    if(aux<=0) {
                        AlertDialog.showMessage(dialogContainer,"Error","Cantidad no puede ser negativa");
                        op.setReduceQuantity(0);
                    }else{
                        op.setReduceQuantity(aux);
                    }

                }catch (Exception ex){
                    AlertDialog.showMessage(dialogContainer,"Error","Cantidad inválida");
                    op.setReduceQuantity(0);
                }
            }
        });




        cantColumn.setCellValueFactory((param)->{
            if(cantColumn.validateValue(param)){
                return new SimpleStringProperty( String.valueOf(param.getValue().getValue().getReduceQuantity())) ;
            }else{
                return cantColumn.getComputedValue(param);
            }
        });


        realCantColumn.setCellValueFactory((param)->{
            if(realCantColumn.validateValue(param)){
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getLot().getQuantity()));
            }else{
                return realCantColumn.getComputedValue(param);
            }
        });


        lotColumn.setCellValueFactory((param)->{
            if(lotColumn.validateValue(param)){
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getLot().getId()));
            }else{
                return lotColumn.getComputedValue(param);
            }
        });


        descColumn.setCellFactory(TextFieldTreeTableCell.<InventoryOperation>forTreeTableColumn());

        descColumn.setCellValueFactory((param)->{
            if(descColumn.validateValue(param)){
                return new SimpleStringProperty(param.getValue().getValue().getDescription());
            }else{
                return descColumn.getComputedValue(param);
            }
        });


        descColumn.setOnEditCommit(new EventHandler<TreeTableColumn.CellEditEvent<InventoryOperation, String>>() {
            @Override
            public void handle(TreeTableColumn.CellEditEvent<InventoryOperation, String> event) {
                TreeItem<InventoryOperation> opTree = event.getRowValue();
                InventoryOperation op = opTree.getValue();
                op.setDescription(event.getNewValue());
            }
        });


        actionsColumn.setCellFactory((col) -> new TreeTableCell<InventoryOperation, Void>() {
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                deleteBtn.setOnMouseClicked((event) -> {
                    //probar que sea el correcto
                    System.out.println("Trash can");
                    operations.remove(operationsTable.getTreeItem(getIndex()).getValue());



                });
            }
            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });
        final TreeItem<InventoryOperation> root = new RecursiveTreeItem<>(operations, RecursiveTreeObject::getChildren);
        operationsTable.setRoot(root);
        operationsTable.setShowRoot(false);
        operationsTable.getColumns().setAll(indexColumn ,codeColumn, nameColumn, realCantColumn, lotColumn,cantColumn,actionsColumn );
    }

    private void setupLotComboBox() {
        List<Lot> listLot = new ArrayList<>();


        LotsHolder lotsHolder = new LotsHolder();

        lotsHolder.getLotsRepository().getLots()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (lots) -> Platform.runLater(() -> {
                            if(lots.size()==0){
                                AlertDialog.showMessage(dialogContainer,"Informacion","Por favor, primero realice movimientos de entrada" );
                                saveBtn.setDisable(true);
                                addLotBtn.setDisable(true);
                            }else{

                                List<InventoryOperation> entranceMovements = indexOperations.stream().filter((obj)->{
                                    return obj.getTypeOperation().getMultiplier()==1 && Integer.parseInt(obj.getLot().getQuantity())!=0;
                                }).filter((op)->{
                                    return !op.getTypeOperation().getName().equals("Devolucion");
                                }).collect(Collectors.toList());

                                System.out.println( "ENTRANCE: " + entranceMovements.size());


                                for (InventoryOperation op: entranceMovements) {
                                    for(Lot lot: lots){
                                        if(op.getLot().getId().equals(lot.getId()) ){
                                            lot.setItem(op.getItem());
                                            break;
                                        }
                                    }
                                }




                                lots.sort((o1, o2) -> {
                                    return o1.getId()<o2.getId() ? -1 : 1;
                                });


                                List<Lot>tmp = lots.stream().filter((obj)->{
                                    return obj.getItem()!=null;
                                }).collect(Collectors.toList());

                                System.out.println("TMP: " + tmp.size());

                                lotField.setItems(FXCollections.observableArrayList(tmp));
                            }
                        }),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));



    }


    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();
    }

    public void onSaveClicked(MouseEvent mouseEvent) {

        if(validateFields()){
            saveMovement();
        }else{

            System.out.println("DEBE LLENAR TODOS LOS CAMPOS EDITABLES");
        }
    }

    private boolean validateFields() {

        if(moveField.getValue()==null){
            AlertDialog.showMessage(dialogContainer,"Error","Debe elegir un movimiento");
            return false;
        }

        for(InventoryOperation op: operations){
            if(op.getReduceQuantity()==0){
                AlertDialog.showMessage(dialogContainer,"Error", "Debe llenar Cantidad y Descripcion");
                return false;
            }

            if(op.getReduceQuantity()<0){
                AlertDialog.showMessage(dialogContainer,"Error","Cantidad no puede ser negativo");
                return false;
            }

            if (op.getReduceQuantity() > 0 && ( Integer.parseInt(op.getLot().getQuantity())  < op.getReduceQuantity())) {

                AlertDialog.showMessage(dialogContainer,"Error","Revisar cantidades a descontar");
                return false;
            }
        }

        return true;
    }

    public void saveMovement(){

    }

    public void goBack() {
        setContentPane(InventoryOperationIndexController.viewPath);
    }


    @Override protected Node getPaneReferenceComponent() {
        return saveBtn;
    }

}
