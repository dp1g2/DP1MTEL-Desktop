package com.dp1mtel.desktop.controller.storage.productCombo;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.model.Product;
import com.dp1mtel.desktop.model.ProductCombo;
import com.dp1mtel.desktop.model.ProductComboHelp;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductComboEditController extends BaseProductComboCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/productCombo/edit.fxml";
    public static final String windowTitle = "Editar combo de productos";

    public static Product productSelected;

    @Override
    public void initModel(ProductCombo productComboModel) {
        this.productComboModel = productComboModel;
        txtCod.setEditable(false);
        txtCod.setText(productComboModel.getId().substring(3,productComboModel.getId().length()));
        txtName.setText(productComboModel.getName());
        txtDescription.setText(productComboModel.getDescription());
        txtBasePrice.setText(productComboModel.getPrice());
        final Double[] totalSum = {0.0};
        productComboModel.getListProducts().forEach((productComboHelp -> {
            totalSum[0] += Double.parseDouble(productComboHelp.getQuantity())*Double.parseDouble(productComboHelp.getProduct().getPrice());
        }));
        txtBasePrice.setText(totalSum[0].toString());
        setupProductsTable();
        loadModalListProducts();
    }

    private void loadModalListProducts() {
        productsHolder.getProductsRepository().getProducts()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (_products) -> Platform.runLater(() -> modalList = FXCollections.observableArrayList(_products)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    protected void setupProductsTable(){

        productsQuantityList = FXCollections.observableArrayList(productComboModel.getListProducts());

        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().getProduct().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().getProduct().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        categoryColumn.setCellValueFactory((param) -> {
            if (categoryColumn.validateValue(param)) {
                return param.getValue().getValue().getProduct().getCategory().nameProperty();
            } else {
                return categoryColumn.getComputedValue(param);
            }
        });

        quantityColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());

        quantityColumn.setCellValueFactory((param) -> {
            if (quantityColumn.validateValue(param)) {
                return param.getValue().getValue().quantityProperty();
            } else {
                return quantityColumn.getComputedValue(param);
            }
        });

        quantityColumn.setOnEditCommit(new EventHandler<TreeTableColumn.CellEditEvent<ProductComboHelp, String>>() {
            @Override
            public void handle(TreeTableColumn.CellEditEvent<ProductComboHelp, String> event) {
                TreeItem<ProductComboHelp> item = event.getRowValue();
                ProductComboHelp it = item.getValue();
                it.setQuantity(event.getNewValue());
                final Double[] totalSum = {0.0};
                productsQuantityList.forEach((productComboHelp -> {
                    totalSum[0] += Double.parseDouble(productComboHelp.getQuantity())*Double.parseDouble(productComboHelp.getProduct().getPrice());
                }));
                txtBasePrice.setText(totalSum[0].toString());
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<ProductComboHelp, Void>() {
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                deleteBtn.setOnMouseClicked((e) -> {
                    productsQuantityList.remove(productsTable.getTreeItem(getIndex()).getValue());
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<ProductComboHelp> root = new RecursiveTreeItem<>(productsQuantityList, RecursiveTreeObject::getChildren);
        productsTable.setRoot(root);
        productsTable.setShowRoot(false);
        productsTable.getColumns().setAll(indexColumn, nameColumn, categoryColumn, quantityColumn, actionsColumn);
    }

    public void onAddNewProductClicked(MouseEvent mouseEvent) {

        try {
            JFXDialogLayout content = new JFXDialogLayout();
            //Pane header = FXMLLoader.load(getClass().getResource(ProductSearchItemController.viewHeaderPath));
            //content.setHeading(header);
            Pane pane = FXMLLoader.load(getClass().getResource(ProductComboSearchProductController.viewPath));
            content.setBody(pane);
            JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);

            JFXButton add = new JFXButton("Agregar");
            JFXButton cancel = new JFXButton("Cancelar");

            add.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    dialog.close();
                    System.out.print("Nombre item: " + productSelected.getName());
                    ProductComboHelp ph = new ProductComboHelp("0", productSelected);
                    setupProductsTable(ph);
                }
            });

            cancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                }
            });

            content.setActions(cancel, add);

            dialog.show();
        } catch (Exception ex) {
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }
    }

}
