package com.dp1mtel.desktop.controller.storage.movements;

import com.dp1mtel.desktop.di.InventoryOperationsHolder;

import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.InventoryOperation;

import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class InventoryOperationDepartureItemController extends BaseInventoryOperationDepartureItemCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/movements/registerItemDepartureMovement.fxml";


    private static final org.apache.logging.log4j.Logger LOGGERAUDIT = LogManager.getLogger("FileAuditAppender");
    private InventoryOperationsHolder inventoryOperationsHolder = new InventoryOperationsHolder();

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);

    }

    public void onAddLotClicked(MouseEvent mouseEvent) {

        // crear un movimiento de inventario y agregarlo a operations
        // pero debo verificar que el lote no este en la lista



        if(lotField.getValue()!=null){
            List<InventoryOperation> tmp = operations.stream().filter((obj)->{

                return obj.getLot().getId()==lotField.getValue().getId();
            }).collect(Collectors.toList());

            if(tmp.size()>0) return;

            InventoryOperation op = new InventoryOperation();
            op.setLot(lotField.getValue());
            System.out.println(op.getLot());
            op.setItem(op.getLot().getItem());
            op.setDate(new Date());
            op.setDescription(description);
            op.setUser(currentUser);
            op.setStore(currentShop);

            if(!op.getItem().isExpires()){
                System.out.println("Add new date to lot");
                op.getLot().setExpirationDate(new Date());
            }
            //operations es la lista que contiene los movimientos de inventario
            operations.add(op);
//            operationsTable.refresh();

        }else{
            AlertDialog.showMessage(dialogContainer,"Error", "Debe escoger un Lote");
        }
    }

    public void saveMovement(){

        for (InventoryOperation op : operations) {

//            if(op.getLot()==null){
//                System.out.println("Lote nulo");
//            }else{
//
//                try{
//                    System.out.println(op.getLot().getExpirationDate());
//
//                }catch (Exception ex){
//                    ex.printStackTrace();
//                }
//            }

            op.setQuantity(op.getReduceQuantity());
            op.setTypeOperation(moveField.getValue());
            System.out.println(op.getTypeOperation().getName() + " - " + op.getTypeOperation().getMultiplier());

        }

        System.out.println("llamada al repository");

        inventoryOperationsHolder.getInventoryOperationsRepository().saveMassiveOperations(operations)
                    .subscribeOn(Schedulers.io())
                    .subscribe((_op) -> {
                        Platform.runLater(() -> {
                            if(Integer.parseInt(_op.getCorrectlyInserted() ) == operations.size()){

                                String logMensaje = "CREAR - Movimiento en almacén de salida - Cantidad: " + operations.size()  +  " Usuario " + currentUser.getFullName();
                                LOGGERAUDIT.info(logMensaje);
                                goBack();

                            }else{
                                System.out.println("algo salio mal");
                            }

                        });

                    }, (e) -> {
                        System.out.println("en el catch");
                        e.printStackTrace();
                    });


//            inventoryOperationsHolder.getInventoryOperationsRepository().saveInventoryOperation(op)
//                    .subscribeOn(Schedulers.io())
//                    .subscribe((_op) -> {
//                        Platform.runLater(() -> {
//                            String logMensaje = "CREAR - Movimiento en almacén de salida '" + _op.getId() + " " + _op.getItem().getId() + "-" + _op.getTypeOperation() + " " + _op.getItem().getName() + "' - Usuario " + currentUser.getFullName();
//                            LOGGERAUDIT.info(logMensaje);
//                            if (contador == operations.size() - 1) goBack();
//                            contador += 1;
//                        });
//
//                    }, (e) -> {
//                        e.printStackTrace();
//                    });
//


    }


}
