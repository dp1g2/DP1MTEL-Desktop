package com.dp1mtel.desktop.controller.storage.productCombo;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.ProductComboHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Category;
import com.dp1mtel.desktop.model.ProductCombo;
import com.dp1mtel.desktop.model.User;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.collections.ObservableList;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class ProductComboController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/productCombo/index.fxml";
    public static final String windowTitle = "Combo de productos";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @FXML private JFXButton btnNewCombo;
    @FXML private JFXTreeTableView<ProductCombo> productComboTable;
    @FXML private JFXTreeTableColumn<ProductCombo, String> indexColumn;
    @FXML private JFXTreeTableColumn<ProductCombo, String> nameColumn;
    @FXML private JFXTreeTableColumn<ProductCombo, String> descriptionColumn;
    @FXML private JFXTreeTableColumn<ProductCombo, String> priceColumn;
    @FXML private JFXTreeTableColumn<ProductCombo, String> stateColumn;
    @FXML private JFXTreeTableColumn<ProductCombo, Void> actionsColumn;
    @FXML private StackPane stackPane;
    @FXML private JFXTextField searchCombo;

    private ProductComboHolder productComboHolder = new ProductComboHolder();

    @Override
    protected Node getPaneReferenceComponent() {
        return btnNewCombo;
    }

    public void onCreateComboClicked(MouseEvent mouseEvent) {
        setContentPane(ProductComboCreateController.viewPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        super.initialize(url,resourceBundle);
        loadProductCombo();
    }

    private void loadProductCombo(){
        productComboHolder.getProductComboRepository().getProductCombo()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (productCombos) -> Platform.runLater(() -> setupProductComboTable(productCombos)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    private void setupProductComboTable(List<ProductCombo> productCombo){
        ObservableList<ProductCombo> productComboList = FXCollections.observableArrayList(productCombo);

        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });


        nameColumn.setCellValueFactory((param)->{
            if (nameColumn.validateValue(param)){
                return param.getValue().getValue().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descriptionColumn.setCellValueFactory((param)->{
            if (descriptionColumn.validateValue(param)){
                return param.getValue().getValue().descriptionProperty();
            } else {
                return descriptionColumn.getComputedValue(param);
            }
        });

        priceColumn.setCellValueFactory((param)->{
            if (priceColumn.validateValue(param)){
                return param.getValue().getValue().priceProperty();
            } else {
                return priceColumn.getComputedValue(param);
            }
        });

        stateColumn.setCellValueFactory((param)->{
            if (stateColumn.validateValue(param)){
                return new SimpleStringProperty(param.getValue().getValue().activeProperty().getValue().equals("true")? "Activo":"Inactivo");
            } else {
                return stateColumn.getComputedValue(param);
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<ProductCombo, Void>() {
            private final MaterialDesignIconView editBtn = new MaterialDesignIconView(MaterialDesignIcon.PENCIL);
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(editBtn, deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                editBtn.setOnMouseClicked((e) -> {
                    ProductCombo productCombo = productComboTable.getTreeItem(getIndex()).getValue();
                    System.out.println("EL NOMBRE ES "  + productCombo.getName());
                    System.out.println("LA DESCRIPCION ES  " + productCombo.getDescription());
                    setContentPane(ProductComboEditController.viewPath, ProductComboEditController.windowTitle, productCombo);
                });

                deleteBtn.setOnMouseClicked((e) -> {
                    JFXDialogLayout content = new JFXDialogLayout();
                    content.setHeading(new Text("Confirmación"));
                    content.setBody(new Text("Está seguro que desea eliminar el combo?"));
                    JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);
                    JFXButton add = new JFXButton("Aceptar");
                    JFXButton cancel = new JFXButton("Cancelar");
                    add.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            dialog.close();
                            //BEGIN OPERATION
                            ProductCombo productCombo = productComboTable.getTreeItem(getIndex()).getValue();
                            productComboHolder.getProductComboRepository().deleteProductCombo(productCombo.getId())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(
                                            (_res) -> Platform.runLater(() -> {
                                                if (_res.code() == 200) {
                                                    loadProductCombo();

                                                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                                    String logMensaje="ELIMINAR - Combo de productos '"+productCombo.getId()+" "+productCombo.getName()+"' - Usuario "+ usuario.getFullName();
                                                    LOGGERAUDIT.info(logMensaje);

                                                    AlertDialog.showMessage(stackPane,
                                                            "Operación exitosa", "Se ha borrado correctamente el combo");
                                                } else if (_res.code() == 409) {
                                                    AlertDialog.showMessage(stackPane,
                                                            "Error al eliminar", "Verifique que el combo no está en uso");
                                                } else {
                                                    AlertDialog.showMessage(stackPane,
                                                            "Error al eliminar", "Hubo un error. Contacte al administrador");
                                                }
                                            }),
                                            (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage()))
                                    );
                            //END OPERATION
                        }
                    });
                    cancel.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            dialog.close();
                        }
                    });
                    content.setActions(cancel, add);
                    dialog.show();
                });

            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });


        final TreeItem<ProductCombo> root = new RecursiveTreeItem<>(productComboList, RecursiveTreeObject::getChildren);
        productComboTable.setRoot(root);
        productComboTable.setShowRoot(false);
        productComboTable.getColumns().setAll(indexColumn, nameColumn, descriptionColumn, priceColumn, stateColumn,actionsColumn);

        searchCombo.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                productComboTable.setPredicate(new Predicate<TreeItem<ProductCombo>>() {
                    @Override
                    public boolean test(TreeItem<ProductCombo> productComboTreeItem) {
                        Boolean flag = productComboTreeItem.getValue().getId().toLowerCase().contains(newValue.toLowerCase())||
                                productComboTreeItem.getValue().getName().toLowerCase().contains(newValue.toLowerCase())||
                                productComboTreeItem.getValue().getDescription().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });
    }

}
