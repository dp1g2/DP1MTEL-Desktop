package com.dp1mtel.desktop.controller.storage.products;

import com.dp1mtel.desktop.controller.BaseController;

import com.dp1mtel.desktop.di.ProductsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Product;
import com.dp1mtel.desktop.model.ProductCombo;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class ProductController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/products/index.fxml";
    public static final String windowTitle = "Productos";

    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @FXML
    private JFXButton btnNewProduct;
    @FXML
    private JFXTreeTableView<Product> productsTable;
    @FXML
    private JFXTreeTableColumn<Product, String> indexColumn;
    @FXML
    private JFXTreeTableColumn<Product, String> nameColumn;
    @FXML
    private JFXTreeTableColumn<Product, String> descripColumn;
    @FXML
    private JFXTreeTableColumn<Product, String> priceColumn;
    @FXML
    private JFXTreeTableColumn<Product, String> categoryColumn;
    @FXML
    private JFXTreeTableColumn<Product, String> stateColumn;
    @FXML
    private JFXTreeTableColumn<Product, Void> actionsColumn;
    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTextField searchProduct;

    private ProductsHolder productsHolder = new ProductsHolder();

    @Override
    protected Node getPaneReferenceComponent() {
        return btnNewProduct;
    }

    public void onCreateProductClicked(MouseEvent mouseEvent) {
        setContentPane(ProductCreateController.viewPath);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        loadProducts();
    }

    private void loadProducts() {
        productsHolder.getProductsRepository().getProducts()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (products) -> Platform.runLater(() -> setupProductsTable(products)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    private void setupProductsTable(List<Product> products) {
        ObservableList<Product> productList = FXCollections.observableArrayList(products);
        //TreeTableColumnExtensions.makeIndexColumn(indexColumn);
        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descripColumn.setCellValueFactory((param) -> {
            if (descripColumn.validateValue(param)) {
                return param.getValue().getValue().descriptionProperty();
            } else {
                return descripColumn.getComputedValue(param);
            }
        });

        priceColumn.setCellValueFactory((param) -> {
            if (priceColumn.validateValue(param)) {
                return param.getValue().getValue().priceProperty();
            } else {
                return priceColumn.getComputedValue(param);
            }
        });

        categoryColumn.setCellValueFactory((param) -> {
            if (categoryColumn.validateValue(param)) {
                return new SimpleStringProperty(param.getValue().getValue().getCategory().getName());
            } else {
                return categoryColumn.getComputedValue(param);
            }
        });

        stateColumn.setCellValueFactory((param) -> {
            if (stateColumn.validateValue(param)) {
                return new SimpleStringProperty(param.getValue().getValue().activeProperty().getValue().equals("true")? "Activo":"Inactivo");
            } else {
                return stateColumn.getComputedValue(param);
            }
        });


        actionsColumn.setCellFactory((col) -> new TreeTableCell<Product, Void>() {
            private final MaterialDesignIconView editBtn = new MaterialDesignIconView(MaterialDesignIcon.PENCIL);
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(editBtn, deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                editBtn.setOnMouseClicked((e) -> {
                    Product product = productsTable.getTreeItem(getIndex()).getValue();
                    setContentPane(ProductEditController.viewPath, ProductEditController.windowTitle, product);
                });

                deleteBtn.setOnMouseClicked((e) -> {
                    JFXDialogLayout content = new JFXDialogLayout();
                    content.setHeading(new Text("Confirmación"));
                    content.setBody(new Text("Está seguro que desea eliminar el producto?"));
                    JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);
                    JFXButton add = new JFXButton("Aceptar");
                    JFXButton cancel = new JFXButton("Cancelar");
                    add.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            dialog.close();
                            //BEGIN OPERATION
                            Product product = productsTable.getTreeItem(getIndex()).getValue();
                            productsHolder.getProductsRepository().deleteProduct(product.getId())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(
                                            (_res) -> Platform.runLater(() -> {
                                                if (_res.code() == 200) {
                                                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                                    String logMensaje="ELIMINAR - Producto '"+product.getId()+" "+product.getName()+"' - Usuario "+ usuario.getFullName();
                                                    LOGGERAUDIT.info(logMensaje);
                                                    loadProducts();
                                                    AlertDialog.showMessage(stackPane,
                                                            "Operación exitosa", "Se ha borrado correctamente el producto");
                                                } else if (_res.code() == 409) {
                                                    AlertDialog.showMessage(stackPane,
                                                            "Error al eliminar", "Verifique que el producto no está en uso");
                                                } else {
                                                    AlertDialog.showMessage(stackPane,
                                                            "Error al eliminar", "Hubo un error. Contacte al administrador");
                                                }
                                            }),
                                            (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage()))
                                    );
                            //END OPERATION
                        }
                    });
                    cancel.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            dialog.close();
                        }
                    });
                    content.setActions(cancel, add);
                    dialog.show();
                });

            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<Product> root = new RecursiveTreeItem<>(productList, RecursiveTreeObject::getChildren);
        productsTable.setRoot(root);
        productsTable.setShowRoot(false);
        productsTable.getColumns().setAll(indexColumn, nameColumn, descripColumn, priceColumn, categoryColumn,
                stateColumn, actionsColumn);

        searchProduct.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                productsTable.setPredicate(new Predicate<TreeItem<Product>>() {
                    @Override
                    public boolean test(TreeItem<Product> productTreeItem) {
                        Boolean flag = productTreeItem.getValue().getId().contains(newValue)||
                                productTreeItem.getValue().getName().toLowerCase().contains(newValue.toLowerCase())||
                                productTreeItem.getValue().getDescription().toLowerCase().contains(newValue.toLowerCase())||
                                productTreeItem.getValue().getCategory().getName().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });
    }
}
