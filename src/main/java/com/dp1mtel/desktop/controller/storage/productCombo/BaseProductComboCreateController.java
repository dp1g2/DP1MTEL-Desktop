package com.dp1mtel.desktop.controller.storage.productCombo;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.ProductComboHolder;
import com.dp1mtel.desktop.di.ProductsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.validation.DigitsValidator;
import com.dp1mtel.desktop.util.validation.MyDoubleValidator;
import com.dp1mtel.desktop.util.validation.OnlyString20Validator;
import com.dp1mtel.desktop.util.validation.String100Validator;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.validation.RequiredFieldValidator;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class BaseProductComboCreateController extends GenericBaseController<ProductCombo> {

    public JFXButton btnNewProductCombo;
    public JFXButton btnCancel;

    public JFXTextField auxCod;
    public JFXTextField txtCod;
    public JFXTextField txtName;
    public JFXTextArea txtDescription;
    public JFXTextField txtRealPrice;
    public JFXTextField txtBasePrice;
    public StackPane stackPane;
    public JFXButton btnAddNewProduct;

    //For image upload
    public JFXButton btnSelectImage;
    public JFXTextField txtPathImage;
    public FileChooser fileChooser = new FileChooser();
    public File file;
    public String line = "";


    public JFXTreeTableView<ProductComboHelp> productsTable;

    public JFXTreeTableColumn<ProductComboHelp, String> indexColumn;
    public JFXTreeTableColumn<ProductComboHelp, String> nameColumn;
    public JFXTreeTableColumn<ProductComboHelp, String> categoryColumn;
    public JFXTreeTableColumn<ProductComboHelp, String> quantityColumn;
    public JFXTreeTableColumn<ProductComboHelp, Void> actionsColumn;

    protected ProductCombo productComboModel;

    protected ProductComboHolder productComboHolder = new ProductComboHolder();
    protected ProductsHolder productsHolder = new ProductsHolder();
    protected ObservableList<ProductComboHelp> productsQuantityList;
    public static ObservableList<Product> modalList;

    private DigitsValidator digitsValidator;
    private RequiredFieldValidator requiredFieldValidator;
    private OnlyString20Validator onlyString20Validator;
    private String100Validator string100Validator ;
    private MyDoubleValidator myDoubleValidator;

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFieldValidation();
        productsTable.setEditable(true);
    }

    protected void setupFieldValidation() {

        digitsValidator = new DigitsValidator();
        requiredFieldValidator = new RequiredFieldValidator();
        onlyString20Validator = new OnlyString20Validator();
        string100Validator = new String100Validator();
        myDoubleValidator = new MyDoubleValidator();

        digitsValidator.setMessage("Debe contener exactamente 4 dígitos");
        requiredFieldValidator.setMessage("Este campo es obligatorio");
        onlyString20Validator.setMessage("Debe ser una cadena de máximo 20 caracteres");
        string100Validator.setMessage("Debe contener como máximo 100 caracteres");
        myDoubleValidator.setMessage("Debe ser un valor decimal");

        txtCod.setValidators(requiredFieldValidator,digitsValidator);
        txtCod.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtCod.validate();});

        txtName.setValidators(requiredFieldValidator, onlyString20Validator);
        txtName.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtName.validate();});

        txtDescription.setValidators(requiredFieldValidator,string100Validator);
        txtDescription.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtDescription.validate();});

        txtRealPrice.setValidators(requiredFieldValidator, myDoubleValidator);
        txtRealPrice.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtRealPrice.validate();});

    }

    protected void setupProductsTable(ProductComboHelp newProductSelected) {
        if (productsQuantityList == null) {
            productsQuantityList = FXCollections.observableArrayList(newProductSelected);
        } else {
            if (!productsQuantityList.contains(newProductSelected)){
                productsQuantityList.add(newProductSelected);
            }
        }

        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().getProduct().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().getProduct().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        categoryColumn.setCellValueFactory((param) -> {
            if (categoryColumn.validateValue(param)) {
                return param.getValue().getValue().getProduct().getCategory().nameProperty();
            } else {
                return categoryColumn.getComputedValue(param);
            }
        });

        quantityColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());

        quantityColumn.setCellValueFactory((param) -> {
            if (quantityColumn.validateValue(param)) {
                return param.getValue().getValue().quantityProperty();
            } else {
                return quantityColumn.getComputedValue(param);
            }
        });

        quantityColumn.setOnEditCommit(new EventHandler<TreeTableColumn.CellEditEvent<ProductComboHelp, String>>() {
            @Override
            public void handle(TreeTableColumn.CellEditEvent<ProductComboHelp, String> event) {
                TreeItem<ProductComboHelp> item = event.getRowValue();
                ProductComboHelp it = item.getValue();
                it.setQuantity(event.getNewValue());
                final Double[] totalSum = {0.0};
                productsQuantityList.forEach((productComboHelp -> {
                    totalSum[0] += Double.parseDouble(productComboHelp.getQuantity())*Double.parseDouble(productComboHelp.getProduct().getPrice());
                }));
                txtBasePrice.setText(totalSum[0].toString());
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<ProductComboHelp, Void>() {
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                deleteBtn.setOnMouseClicked((e) -> {
                    productsQuantityList.remove(productsTable.getTreeItem(getIndex()).getValue());
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<ProductComboHelp> root = new RecursiveTreeItem<>(productsQuantityList, RecursiveTreeObject::getChildren);
        productsTable.setRoot(root);
        productsTable.setShowRoot(false);
        productsTable.getColumns().setAll(indexColumn, nameColumn, categoryColumn, quantityColumn, actionsColumn);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnCancel;
    }

    public void onSaveClicked(MouseEvent mouseEvent) {
        if(validateFields()){

            saveProductCombo();
        }
        else {
            AlertDialog.showMessage(stackPane, "Error al guardar",
                    "Por favor, corregir los errores mostrados.");
        }
    }

    public boolean validateFields(){
        if (productsQuantityList==null || productsQuantityList.isEmpty() || emptyQuantities()){
            AlertDialog.showMessage(stackPane, "Error al guardar",
                    "Por favor, introducir al menos un producto con su cantidad");
            return false;
        }
        if(txtCod.validate() && txtName.validate() && txtDescription.validate() && txtBasePrice.validate()) return true;
        else return false;
    }

    private boolean emptyQuantities() {
        for (int i = 0; i< productsQuantityList.size();i++){
            if (Integer.parseInt(productsQuantityList.get(i).getQuantity()) == 0) {
                return true;
            }
        }
        return false;
    }

    protected void saveProductCombo() {
        productComboModel.setId(auxCod.getText() + txtCod.getText());
        productComboModel.setName(txtName.getText());
        productComboModel.setDescription(txtDescription.getText());
        productComboModel.setActive("true");
        productComboModel.setPrice(txtRealPrice.getText());
        productComboModel.setListProducts(productsQuantityList);
        productComboHolder.getProductComboRepository().updateProductComboById(productComboModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_product) -> {

                    Platform.runLater(() -> {
                        User usuario= usersHolder.getUsersRepository().getCurrentUser();
                        String logMensaje="EDICIÓN - Combo de productos '"+productComboModel.getId()+" "+productComboModel.getName()+"' - Usuario "+ usuario.getFullName();
                        LOGGERAUDIT.info(logMensaje);
                        System.err.println(_product.getName());
                        goBack();
                    });
                }, (e) -> {
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });

        if (!txtPathImage.getText().isEmpty()) {
            RequestBody idPart = RequestBody.create(MultipartBody.FORM,auxCod.getText() + txtCod.getText());

            RequestBody filePart = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part sentFile = MultipartBody.Part.createFormData("image", file.getName(), filePart);
            productComboHolder.getProductComboRepository().uploadImage(sentFile,idPart)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            (_res) -> Platform.runLater(() -> {
                                if (_res.code() == 200) {
                                    System.out.println("Image uploaded successfully");
                                } else if (_res.code() == 400) {
                                    System.err.println("There was an error 400");
                                } else {
                                    System.err.println("There was an error something else");
                                }
                            }),
                            (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage())));
        }
    }

    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();
    }

    public void onSelectImageClicked(MouseEvent mouseEvent) {
        initiliazeFileReader();
    }

    public void initiliazeFileReader() {
        fileChooser.setTitle("Seleccione la imagen");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        file = fileChooser.showOpenDialog(null);
        String fileName = file.getAbsolutePath();
        //Verify that the file has a .csv extension
        if (verifyFileExtension(fileName)) txtPathImage.setText(fileName);

    }

    public Boolean verifyFileExtension(String fileName) {
        String[] validExtensions = {".JPEG", ".JPG", ".GIF", ".PNG"};
        for (int i = 0; i < validExtensions.length; i++) {
            if (fileName.substring(fileName.length() - 5).toUpperCase().contains(validExtensions[i])) {
                return true;
            }
        }
        AlertDialog.showMessage(stackPane, "Error", "El archivo debe tener un formato de image válido");
        return false;
    }

    protected void goBack() {
        setContentPane(ProductComboController.viewPath);
    }
}
