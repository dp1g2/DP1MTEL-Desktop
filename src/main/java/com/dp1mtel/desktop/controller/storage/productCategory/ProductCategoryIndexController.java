package com.dp1mtel.desktop.controller.storage.productCategory;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.CategoriesHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Category;
import com.dp1mtel.desktop.model.Item;
import com.dp1mtel.desktop.model.User;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class ProductCategoryIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/productCategories/index.fxml";
    public static final String windowTitle = "Productos";

    private final String deleteTitle = "Operación realizada";
    private final String deleteMessage = "La categoría ha sido eliminada correctamente";

    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @FXML private JFXButton btnNewCategory;
    @FXML private JFXTreeTableView<Category> categoryTable;
    @FXML private JFXTreeTableColumn<Category, String> indexColumn;
    @FXML private JFXTreeTableColumn<Category, String> nameColumn;
    @FXML private JFXTreeTableColumn<Category, String> descripColumn;
    @FXML private JFXTreeTableColumn<Category, Void> actionsColumn;
    @FXML private StackPane stackPane;
    @FXML private JFXTextField searchCategoria;

    private CategoriesHolder categoriesHolder = new CategoriesHolder();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url,resourceBundle);
        loadCategories();
    }

    private void loadCategories() {
        categoriesHolder.getCategoriesRepository().getCategories()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (categories) -> Platform.runLater(() -> setupCategoryTable(categories)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnNewCategory;
    }

    public void onCreateProductCategory(MouseEvent mouseEvent) {
        setContentPane(ProductCategoryCreateController.viewPath);
    }

    private void setupCategoryTable(List<Category> categories) {
        System.out.println("Las categorias son...");
        categories.forEach((category -> {
            System.out.println(category.getName() + "  ");
        }));

        ObservableList<Category> categoryList = FXCollections.observableArrayList(categories);


        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descripColumn.setCellValueFactory((param) -> {
            if (descripColumn.validateValue(param)) {
                return param.getValue().getValue().descriptionProperty();
            } else {
                return descripColumn.getComputedValue(param);
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<Category, Void>() {
            private final MaterialDesignIconView editBtn = new MaterialDesignIconView(MaterialDesignIcon.PENCIL);
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(editBtn, deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                editBtn.setOnMouseClicked((e) -> {
                    Category category = categoryTable.getTreeItem(getIndex()).getValue();
                    setContentPane(ProductCategoryEditController.viewPath, ProductCategoryEditController.windowTitle, category);
                });

                deleteBtn.setOnMouseClicked((e) -> {
                    JFXDialogLayout content = new JFXDialogLayout();
                    content.setHeading(new Text("Confirmación"));
                    content.setBody(new Text("Está seguro que desea eliminar la categoría?"));
                    JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);
                    JFXButton add = new JFXButton("Aceptar");
                    JFXButton cancel = new JFXButton("Cancelar");
                    add.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            dialog.close();
                            //BEGIN OPERATION
                            Category category = categoryTable.getTreeItem(getIndex()).getValue();
                            categoriesHolder.getCategoriesRepository().disableCategory(category)
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(
                                            (_res) -> Platform.runLater(() -> {
                                                if (_res.code() == 200) {
                                                    loadCategories();
                                                    AlertDialog.showMessage(stackPane,
                                                            "Operación exitosa", "Se ha borrado correctamente la categoría");
                                                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                                    String logMensaje="ELIMINAR - Categoría '"+category.getId()+" "+category.getName()+"' - Usuario "+ usuario.getFullName();
                                                    LOGGERAUDIT.info(logMensaje);
                                                } else if (_res.code() == 400) {
                                                    AlertDialog.showMessage(stackPane,
                                                            "Error al eliminar", "Verifique que la categoría no está en uso");
                                                } else {
                                                    AlertDialog.showMessage(stackPane,
                                                            "Error al eliminar", "Hubo un error. Contacte al administrador");
                                                }
                                            }),
                                            (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage()))
                                    );
                            //END OPERATION
                        }
                    });
                    cancel.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            dialog.close();
                        }
                    });
                    content.setActions(cancel, add);
                    dialog.show();
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<Category> root = new RecursiveTreeItem<>(categoryList, RecursiveTreeObject::getChildren);
        categoryTable.setRoot(root);
        categoryTable.setShowRoot(false);
        categoryTable.getColumns().setAll(indexColumn, nameColumn, descripColumn, actionsColumn);

        searchCategoria.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                categoryTable.setPredicate(new Predicate<TreeItem<Category>>() {
                    @Override
                    public boolean test(TreeItem<Category> categoryTreeItem) {
                        Boolean flag = categoryTreeItem.getValue().getId().toLowerCase().contains(newValue.toLowerCase())||
                                categoryTreeItem.getValue().getName().toLowerCase().contains(newValue.toLowerCase())||
                                categoryTreeItem.getValue().getDescription().toLowerCase().contains(newValue.toLowerCase());
                        return  flag;
                    }
                });
            }
        });
    }
}
