package com.dp1mtel.desktop.controller.storage.items;

import com.dp1mtel.desktop.model.Item;

public class ItemEditController  extends BaseItemCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/items/edit.fxml";
    public static final String windowTitle = "Editar ítem";

    @Override
    public void initModel(Item model) {
        this.itemModel = model;
        txtCod.setText(itemModel.getId().substring(3,itemModel.getId().length()));
        txtName.setText(itemModel.getName());
        txtDescription.setText(itemModel.getDescription());
        expiresField.setSelected(itemModel.isExpires());
        spaceField.setText(String.valueOf(itemModel.getVolume()));
    }

}
