package com.dp1mtel.desktop.controller.storage.products;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.ItemsHolder;
import com.dp1mtel.desktop.model.Item;
import com.dp1mtel.desktop.model.ProductHelp;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.TreeTableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ProductSearchItemController extends BaseController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/storage/products/searchItem.fxml";
    public static final String viewHeaderPath = "/com/dp1mtel/desktop/view/storage/products/searchHeader.fxml";

    @FXML
    private JFXTreeTableView<Item> itemsTable;
    @FXML
    private JFXTreeTableColumn<Item, String> indexColumn;
    @FXML
    private JFXTreeTableColumn<Item, String> nameColumn;
    @FXML
    private JFXTreeTableColumn<Item, String> descripColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        setupItemsTable();
    }

    private void setupItemsTable() {

        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descripColumn.setCellValueFactory((param) -> {
            if (descripColumn.validateValue(param)) {
                return param.getValue().getValue().descriptionProperty();
            } else {
                return descripColumn.getComputedValue(param);
            }
        });

        final TreeItem<Item> root = new RecursiveTreeItem<>(BaseProductCreateController.modalList, RecursiveTreeObject::getChildren);
        itemsTable.setRoot(root);
        itemsTable.setShowRoot(false);
        itemsTable.getColumns().setAll(indexColumn, nameColumn, descripColumn);


        itemsTable.setRowFactory(tv -> {
            TreeTableRow<Item> row = new TreeTableRow<Item>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY) {
                    ProductCreateController.itemSelected = row.getItem();
                    ProductEditController.itemSelected = row.getItem();
                }
            });
            return row;
        });
    }


    @Override
    protected Node getPaneReferenceComponent() {
        return itemsTable;
    }

}
