package com.dp1mtel.desktop.controller.storage.items;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.ItemsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Item;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.validation.DigitsValidator;
import com.dp1mtel.desktop.util.validation.OnlyString20Validator;
import com.dp1mtel.desktop.util.validation.String100Validator;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.DoubleValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class BaseItemCreateController extends GenericBaseController<Item> {

    public JFXButton btnSave;
    public JFXButton btnCancel;
    public JFXTextField auxCod;
    public JFXTextField txtCod;
    public JFXTextField txtName;
    public JFXTextArea txtDescription;
    public JFXCheckBox expiresField;
    public JFXTextField spaceField;
    public StackPane stackPane;

    private DigitsValidator digitsValidator;
    private RequiredFieldValidator requiredFieldValidator;
    private OnlyString20Validator onlyString20Validator;
    private String100Validator string100Validator ;
    private DoubleValidator doubleValidator;

    protected Item itemModel;

    protected ItemsHolder itemsHolder = new ItemsHolder();

    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFieldValidation();
    }

    protected void setupFieldValidation() {

        digitsValidator = new DigitsValidator();
        requiredFieldValidator = new RequiredFieldValidator();
        onlyString20Validator = new OnlyString20Validator();
        string100Validator = new String100Validator();
        doubleValidator = new DoubleValidator();

        digitsValidator.setMessage("Debe contener exactamente 4 dígitos");
        requiredFieldValidator.setMessage("Este campo es obligatorio");
        onlyString20Validator.setMessage("Debe ser una cadena de máximo 20 caracteres");
        string100Validator.setMessage("Debe contener como máximo 100 caracteres");
        doubleValidator.setMessage("Debe ser un valor decimal");

        txtCod.setValidators(requiredFieldValidator,digitsValidator);
        txtCod.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtCod.validate();});

        txtName.setValidators(requiredFieldValidator, onlyString20Validator);
        txtName.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtName.validate();});

        txtDescription.setValidators(requiredFieldValidator,string100Validator);
        txtDescription.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtDescription.validate();});

        spaceField.setValidators(requiredFieldValidator,doubleValidator);
        spaceField.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) spaceField.validate();});

    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnSave;
    }

    public void onSaveClicked(MouseEvent mouseEvent) {
        if(validateFields()) saveItem();
        else {
            AlertDialog.showMessage(stackPane, "Error al guardar",
                    "Por favor, corregir los errores mostrados.");
        }
    }

    public boolean validateFields(){
        if(spaceField.validate() && txtCod.validate() && txtName.validate() && txtDescription.validate()) return true;
        else return false;
    }

    protected void saveItem() {

        itemModel.setId(auxCod.getText() + txtCod.getText());
        itemModel.setName(txtName.getText());
        itemModel.setDescription(txtDescription.getText());
        itemModel.setVolume( Double.parseDouble(spaceField.getText()));
        itemModel.setExpires(expiresField.isSelected());

        itemsHolder.getItemsRepository().updateItemById(itemModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_item) -> {
                    Platform.runLater(() -> {
                        User usuario= usersHolder.getUsersRepository().getCurrentUser();
                        String logMensaje="EDICIÓN - Item '"+itemModel.getId()+" "+itemModel.getName()+"' - Usuario "+ usuario.getFullName();
                        LOGGERAUDIT.info(logMensaje);
                        System.err.println(_item.getName());
                        goBack();
                    });
                }, (e) -> {
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });
    }

    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();
    }

    protected void goBack() {
        setContentPane(ItemController.viewPath);
    }

}
