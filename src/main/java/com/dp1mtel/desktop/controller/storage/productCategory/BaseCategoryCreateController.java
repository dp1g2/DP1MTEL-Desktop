package com.dp1mtel.desktop.controller.storage.productCategory;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.CategoriesHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Category;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.validation.DigitsValidator;
import com.dp1mtel.desktop.util.validation.OnlyString20Validator;
import com.dp1mtel.desktop.util.validation.String100Validator;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class BaseCategoryCreateController extends GenericBaseController<Category> {
    public JFXButton btnSave;
    public JFXButton btnCancel;

    public JFXTextField auxCod;
    public JFXTextField txtCod;
    public JFXTextField txtName;
    public JFXTextArea txtDescription;
    public StackPane stackPane;

    protected Category categoryModel;
    protected CategoriesHolder categoriesHolder = new CategoriesHolder();

    private DigitsValidator digitsValidator;
    private RequiredFieldValidator requiredFieldValidator;
    private OnlyString20Validator onlyString20Validator;
    private String100Validator string100Validator ;

    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();



    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFieldValidation();
    }

    protected void setupFieldValidation() {

        digitsValidator = new DigitsValidator();
        requiredFieldValidator = new RequiredFieldValidator();
        onlyString20Validator = new OnlyString20Validator();
        string100Validator = new String100Validator();

        digitsValidator.setMessage("Debe contener exactamente 4 dígitos");
        requiredFieldValidator.setMessage("Este campo es obligatorio");
        onlyString20Validator.setMessage("Debe ser una cadena de máximo 20 caracteres");
        string100Validator.setMessage("Debe contener como máximo 100 caracteres");

        txtCod.setValidators(requiredFieldValidator,digitsValidator);
        txtCod.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtCod.validate();});

        txtName.setValidators(requiredFieldValidator, onlyString20Validator);
        txtName.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtName.validate();});

        txtDescription.setValidators(requiredFieldValidator,string100Validator);
        txtDescription.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtDescription.validate();});

    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnSave;
    }

    public void onSaveClicked(MouseEvent mouseEvent) {
        if(validateFields()) {
            saveCategory();
        }
        else {
            AlertDialog.showMessage(stackPane, "Error al guardar",
                    "Por favor, corregir los errores mostrados.");
        }
    }

    public boolean validateFields(){
        if(txtCod.validate() && txtName.validate() && txtDescription.validate()) return true;
        else return false;
    }

    protected void saveCategory() {

        categoryModel.setId(auxCod.getText() + txtCod.getText());
        categoryModel.setName(txtName.getText());
        categoryModel.setDescription(txtDescription.getText());
        categoriesHolder.getCategoriesRepository().updateCategoryById(categoryModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_category) -> {
                    Platform.runLater(() -> {
                        System.err.println(_category.getName());
                        goBack();
                    });
                }, (e) -> {
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });
        User usuario= usersHolder.getUsersRepository().getCurrentUser();
        String logMensaje="EDICIÓN - Categoría '"+categoryModel.getId()+" "+categoryModel.getName()+"' - Usuario "+ usuario.getFullName();
        LOGGERAUDIT.info(logMensaje);
    }

    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();
    }

    protected void goBack() {
        setContentPane(ProductCategoryIndexController.viewPath);
    }
}
