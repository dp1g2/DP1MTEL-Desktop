package com.dp1mtel.desktop.controller.storage.products;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.CategoriesHolder;
import com.dp1mtel.desktop.di.ItemsHolder;
import com.dp1mtel.desktop.di.ProductsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.validation.DigitsValidator;
import com.dp1mtel.desktop.util.validation.MyDoubleValidator;
import com.dp1mtel.desktop.util.validation.OnlyString20Validator;
import com.dp1mtel.desktop.util.validation.String100Validator;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.validation.RequiredFieldValidator;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BaseProductCreateController extends GenericBaseController<Product> {

    public JFXButton btnNewProduct;
    public JFXButton btnCancel;

    public JFXTextField auxCod;
    public JFXTextField txtCod;
    public JFXTextField txtName;
    public JFXTextArea txtDescription;
    public JFXTextField txtBasePrice;
    public JFXTextField txtUnitCharge;
    public JFXComboBox<Category> txtCategory;
    public StackPane stackPane;
    public JFXButton btnAddNewItem;

    //For image upload
    public JFXButton btnSelectImage;
    public JFXTextField txtPathImage;
    public FileChooser fileChooser = new FileChooser();
    public File file;
    public String line = "";

    public JFXTreeTableView<ProductHelp> itemsTable;

    public JFXTreeTableColumn<ProductHelp, String> indexColumn;
    public JFXTreeTableColumn<ProductHelp, String> nameColumn;
    public JFXTreeTableColumn<ProductHelp, String> descriptionColumn;
    public JFXTreeTableColumn<ProductHelp, String> quantityColumn;
    public JFXTreeTableColumn<ProductHelp, Void> actionsColumn;

    protected Product productModel;

    protected ProductsHolder productsHolder = new ProductsHolder();
    protected CategoriesHolder categoriesHolder = new CategoriesHolder();
    protected ItemsHolder itemsHolder = new ItemsHolder();
    protected ObservableList<ProductHelp> itemsQuantityList;

    private DigitsValidator digitsValidator;
    private RequiredFieldValidator requiredFieldValidator;
    private OnlyString20Validator onlyString20Validator;
    private String100Validator string100Validator ;
    private MyDoubleValidator myDoubleValidator;

    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    public static ObservableList<Item> modalList;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFieldValidation();
        loadComboBoxCategories();
        itemsTable.setEditable(true);
    }

    private void loadComboBoxCategories() {
        categoriesHolder.getCategoriesRepository().getCategories()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (categories) -> Platform.runLater(() -> setupCategoriesComboBox(categories)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    protected void setupFieldValidation() {

        digitsValidator = new DigitsValidator();
        requiredFieldValidator = new RequiredFieldValidator();
        onlyString20Validator = new OnlyString20Validator();
        string100Validator = new String100Validator();
        myDoubleValidator = new MyDoubleValidator();

        digitsValidator.setMessage("Debe contener exactamente 4 dígitos");
        requiredFieldValidator.setMessage("Este campo es obligatorio");
        onlyString20Validator.setMessage("Debe ser una cadena de máximo 20 caracteres");
        string100Validator.setMessage("Debe contener como máximo 100 caracteres");
        myDoubleValidator.setMessage("Debe ser un valor decimal");

        txtCod.setValidators(requiredFieldValidator,digitsValidator);
        txtCod.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtCod.validate();});

        txtName.setValidators(requiredFieldValidator, onlyString20Validator);
        txtName.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtName.validate();});

        txtDescription.setValidators(requiredFieldValidator,string100Validator);
        txtDescription.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtDescription.validate();});

        txtBasePrice.setValidators(requiredFieldValidator, myDoubleValidator);
        txtBasePrice.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) txtBasePrice.validate();});

    }

    private void setupCategoriesComboBox(List<Category> categories) {
        for (int i = 0; i < categories.size(); i++) txtCategory.getItems().add(categories.get(i));
        if (ProductEditController.modelComboBox !=null){
            for (int i=0;i<txtCategory.getItems().size();i++){
                if (txtCategory.getItems().get(i).getName().equals(ProductEditController.modelComboBox.getCategory().getName())){
                    txtCategory.getSelectionModel().select(i);
                    break;
                }
            }
            ProductEditController.modelComboBox=null;
        }
    }

    protected void setupItemsTable(ProductHelp newItemSelected) {
        if (itemsQuantityList == null) {
            itemsQuantityList = FXCollections.observableArrayList(newItemSelected);
        } else {
            if (!itemsQuantityList.contains(newItemSelected)){
                itemsQuantityList.add(newItemSelected);
            }
        }
        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return param.getValue().getValue().getItem().idProperty();
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().getItem().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descriptionColumn.setCellValueFactory((param) -> {
            if (descriptionColumn.validateValue(param)) {
                return param.getValue().getValue().getItem().descriptionProperty();
            } else {
                return descriptionColumn.getComputedValue(param);
            }
        });

        quantityColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());

        quantityColumn.setCellValueFactory((param) -> {
            if (descriptionColumn.validateValue(param)) {
                return param.getValue().getValue().quantityProperty();
            } else {
                return descriptionColumn.getComputedValue(param);
            }
        });

        quantityColumn.setOnEditCommit(new EventHandler<TreeTableColumn.CellEditEvent<ProductHelp, String>>() {
            @Override
            public void handle(TreeTableColumn.CellEditEvent<ProductHelp, String> event) {
                TreeItem<ProductHelp> item = event.getRowValue();
                ProductHelp it = item.getValue();
                it.setQuantity(event.getNewValue());
                final Double[] totalSum = {0.0};
                itemsQuantityList.forEach((productHelp -> {
                    totalSum[0] += Double.parseDouble(productHelp.getQuantity())*productHelp.getItem().getVolume();
                }));
                txtUnitCharge.setText(totalSum[0].toString());
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<ProductHelp, Void>() {
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                deleteBtn.setOnMouseClicked((e) -> {
                    itemsQuantityList.remove(itemsTable.getTreeItem(getIndex()).getValue());
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<ProductHelp> root = new RecursiveTreeItem<>(itemsQuantityList, RecursiveTreeObject::getChildren);
        itemsTable.setRoot(root);
        itemsTable.setShowRoot(false);
        itemsTable.getColumns().setAll(indexColumn, nameColumn, descriptionColumn, quantityColumn, actionsColumn);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnCancel;
    }

    public void onSaveClicked(MouseEvent mouseEvent) {
        if(validateFields()) saveProduct();
        else {
            AlertDialog.showMessage(stackPane, "Error al guardar",
                    "Por favor, corregir los errores mostrados.");
        }
    }

    private boolean validateFields() {
        if (itemsQuantityList==null || itemsQuantityList.isEmpty() || emptyQuantities()){
            AlertDialog.showMessage(stackPane, "Error al guardar",
                    "Por favor, introducir al menos un ítem con su cantidad");
            return false;
        }
        if(txtCod.validate() && txtName.validate() && txtDescription.validate() && txtBasePrice.validate()) return true;
        else return false;
    }

    private boolean emptyQuantities() {
        for (int i = 0; i< itemsQuantityList.size();i++){
            if (Integer.parseInt(itemsQuantityList.get(i).getQuantity()) == 0) {
                return true;
            }
        }
        return false;
    }

    protected void saveProduct() {
        productModel.setId(auxCod.getText() + txtCod.getText());
        productModel.setName(txtName.getText());
        productModel.setDescription(txtDescription.getText());
        productModel.setPrice(txtBasePrice.getText());
        productModel.setCategory(new Category(txtCategory.getSelectionModel().getSelectedItem().getId()));
        productModel.setListItems(itemsQuantityList);

        productsHolder.getProductsRepository().updateProductById(productModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_product) -> {
                    Platform.runLater(() -> {
                        User usuario= usersHolder.getUsersRepository().getCurrentUser();
                        String logMensaje="EDICIÓN - Producto '"+productModel.getId()+" "+productModel.getName()+"' - Usuario "+ usuario.getFullName();
                        LOGGERAUDIT.info(logMensaje);
                        System.err.println(_product.getName());
                        goBack();
                    });
                }, (e) -> {
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });

        //Save image if chosen
        if (!txtPathImage.getText().isEmpty()) {
            RequestBody idPart = RequestBody.create(MultipartBody.FORM,auxCod.getText() + txtCod.getText());

            RequestBody filePart = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part sentFile = MultipartBody.Part.createFormData("image", file.getName(), filePart);
            productsHolder.getProductsRepository().uploadImage(sentFile,idPart)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            (_res) -> Platform.runLater(() -> {
                                if (_res.code() == 200) {
                                    System.out.println("Image uploaded successfully");
                                } else if (_res.code() == 400) {
                                    System.err.println("There was an error 400");
                                } else {
                                    System.err.println("There was an error something else");
                                }
                            }),
                            (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage())));
        }

    }

    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();
    }

    public void onSelectImageClicked(MouseEvent mouseEvent) {
        initiliazeFileReader();
    }

    public void initiliazeFileReader() {
        fileChooser.setTitle("Seleccione la imagen");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        file = fileChooser.showOpenDialog(null);
        String fileName = file.getAbsolutePath();
        //Verify that the file has a .csv extension
        if (verifyFileExtension(fileName)) txtPathImage.setText(fileName);

    }

    public Boolean verifyFileExtension(String fileName) {
        String[] validExtensions = {".JPEG", ".JPG", ".GIF", ".PNG"};
        for (int i = 0; i < validExtensions.length; i++) {
            if (fileName.substring(fileName.length() - 5).toUpperCase().contains(validExtensions[i])) {
                return true;
            }
        }
        AlertDialog.showMessage(stackPane, "Error", "El archivo debe tener un formato de image válido");
        return false;
    }

    protected void goBack() {
        setContentPane(ProductController.viewPath);
    }
}
