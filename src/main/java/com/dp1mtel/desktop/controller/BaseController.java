package com.dp1mtel.desktop.controller;

import com.dp1mtel.desktop.App;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class BaseController implements Initializable {
    protected static Runnable postContentPaneUpdate = () -> {};
    protected static Consumer<FXMLLoader> onLayoutLoad = (l) -> {};

    private Pane mainContentPane;

    protected Double prefHeight = 510.0;

    protected Double prefWidth = 1000.0;


    public void initialize(URL location, ResourceBundle resourceBundle) {}

    /**
     * Returns the main content pane, that holds each module screen for the system
     *
     * @return Main content pane
     */
    public Pane getMainContentPane() {
        Node refComponent = getPaneReferenceComponent();
        if (mainContentPane == null && refComponent != null) {
            mainContentPane = (Pane) refComponent.getScene().lookup("#mainContentPane");
        }
        return mainContentPane;
    }

    /**
     * Returns view component to use as reference to get the main content pane
     *
     * @return reference component
     */
    protected Node getPaneReferenceComponent() {
        return null;
    }

//    public Stage getPrimaryStage() {
//        System.out.println(getMainContentPane() == null);
//        System.out.println(getMainContentPane().getScene() == null);
//        return (Stage) getMainContentPane().getScene().getWindow();
//    }

    /**
     * Sets the current main pane content
     *
     * @param viewPath Path to view to load
     * @param windowTitle Title to add to window
     */
    public void setContentPane(String viewPath, String windowTitle) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(viewPath));
            Pane pane = loader.load();
            onLayoutLoad.accept(loader);
            getMainContentPane().getChildren().setAll(pane);


//            if (!(windowTitle == null || windowTitle.isEmpty())) {
//                getPrimaryStage().setTitle(windowTitle);
//            }
            postContentPaneUpdate.run();
        } catch (Exception ex) {
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Sets the current main pane content
     *
     * @param viewPath Path to view to load
     */
    public void setContentPane(String viewPath) {
        setContentPane(viewPath, "");
    }

    public <T> void setContentPane(String viewPath, String windowTitle, T model) {
        onLayoutLoad = (loader) -> {
            try {
                ((GenericBaseController<T>) loader.getController()).initModel(model);
            } catch (ClassCastException ex) {
                Logger.getLogger(App.TAG).log(Level.WARNING, "Controller not generic");
            }
        };
        setContentPane(viewPath, windowTitle);
    }

    public <T> void setContentPane(String viewPath, T model) {
        setContentPane(viewPath, "", model);
    }
}
