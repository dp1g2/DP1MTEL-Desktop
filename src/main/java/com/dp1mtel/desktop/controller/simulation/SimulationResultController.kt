package com.dp1mtel.desktop.controller.simulation

import com.dp1mtel.desktop.controller.GenericBaseController
import com.dp1mtel.desktop.controller.distribution.TrackOrderController
import com.dp1mtel.desktop.controller.sales.orderStatus.OrderStatusIndexController
import com.dp1mtel.desktop.model.*
import com.dp1mtel.desktop.repository.IDispatchRepository
import com.dp1mtel.desktop.repository.IUsersRepository
import com.dp1mtel.desktop.util.extensions.subscribeFx
import com.dp1mtel.desktop.util.toCoordinate
import com.dp1mtel.desktop.util.view.RouteAssignmentRow
import com.jfoenix.controls.JFXButton
import com.sothawo.mapjfx.*
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import tornadofx.action
import tornadofx.observableList
import tornadofx.onChange
import java.net.URL
import java.util.*

class SimulationResultController :
        GenericBaseController<SimulationResultController.Model>(),
        KoinComponent {
    companion object {
        @JvmField val viewPath = "/com/dp1mtel/desktop/view/simulation/results.fxml"
    }

    @FXML private lateinit var progressContainer: HBox
    @FXML private lateinit var resultsLabel: Label
    @FXML private lateinit var resultsContainer: GridPane
    @FXML private lateinit var routeAssignmentContainer: VBox
    @FXML private lateinit var resultsMap: MapView
    @FXML private lateinit var btnSave: JFXButton
    @FXML private lateinit var btnCancel: JFXButton
    @FXML private lateinit var actionButtons: HBox
    @FXML private lateinit var stackPane: StackPane

    private val pucpLocation = Coordinate(-12.0652157, -77.0817718)

    private val userRepository: IUsersRepository by inject()
    private val dispatchRepository: IDispatchRepository by inject()

    private var currentUser = userRepository.currentUser!!
    private val customerMarkers = mutableListOf<Marker>()
    private var tracks = observableList<CoordinateLine>()

    private lateinit var model: SimulationResultController.Model
    private lateinit var dispatchRequest: DispatchRequest

    private val availableDrivers = observableList<User>()
    private var routes: List<Route> = listOf()

    /**
     * Model
     */
    data class Model(
            val orders: List<Order>,
            val vehicleNumber: Int,
            val vehicleCapacity: Int,
            val allowAssignment: Boolean = true
    )

    /**
     * Initialization
     */
    override fun initialize(location: URL?, resourceBundle: ResourceBundle?) {
        super.initialize(location, resourceBundle)
        initMap()
        initView()
    }

    private fun initView() {
        userRepository.getUsers().subscribeFx(
                { users ->
                    availableDrivers.setAll(users.filter { it.role.name == "Conductor" })
                },
                { e -> println(e.message) }
        )
    }

    private fun saveDispatch() {
        val dispatch = Dispatch(routes)
        dispatchRepository.saveDispatch(dispatch).subscribeFx(
                {
                    AlertDialog.showMessage(stackPane, "Éxito", "Se ha registrado el despacho correctamente")
                    setContentPane(OrderStatusIndexController.viewPath)
                },
                { e -> println(e.message) }
        )
    }

    private fun validateFields(): Boolean {
        if (routes.isEmpty()) {
            // TODO: alert. Should not arrive here, something happened
            return false
        }
        val assignedDrivers = mutableListOf<Long>()
        routes.forEach {
            if (it.assignedDriver == null) {
                // TODO: alert
                AlertDialog.showMessage(stackPane, "Cuidado!", "No se asignó conductor a alguna ruta")
                return false
            }
            if (it.assignedDriver?.id in assignedDrivers) {
                // TODO: alert
                AlertDialog.showMessage(stackPane, "Cuidado!", "Ese conductor ya ha sido asignado a una ruta")
                return false
            }
            assignedDrivers.add(it.assignedDriver!!.id)
        }
        return true
    }

    fun goBack() {
        setContentPane(DispatchController.viewPath, DispatchController.Model(model.allowAssignment))
    }

    private fun initMap() {
        resultsMap.initialize()
        tracks.onChange {
            if (it.next()) {
                if (it.wasRemoved()) {
                    it.removed.forEach { resultsMap.removeCoordinateLine(it) }
                } else if (it.wasAdded()) {
                    it.addedSubList.forEach { resultsMap.addCoordinateLine(it) }
                }
            }
        }
        resultsMap.initializedProperty().addListener { it, oldVal, newVal ->
            if (newVal) {
                resultsMap.center = currentUser.store.coordinates.toCoordinate()
                resultsMap.zoom = 14.0
            }
        }
    }

    override fun initModel(model: Model?) {
        this.model = model!!
        actionButtons.isVisible = this.model.allowAssignment
        btnCancel.action { goBack() }
        btnSave.action {
            if (validateFields()) {
                saveDispatch()
            }
        }
        runAlgorithm()
    }

    override fun getPaneReferenceComponent(): Node {
        return progressContainer
    }

    /**
     * Algorithm processing
     */
    fun runAlgorithmDemo() {
        dispatchRepository.dispatchDemo().subscribeFx(
                { routes ->
                    handleAlgorithmResults(routes)
                },
                { e -> println(e.message) }
        )
    }

    fun runAlgorithm() {
        this.dispatchRequest = DispatchRequest(model.orders.take(25), model.vehicleNumber, currentUser.store.id)
        dispatchRepository.dispatch(dispatchRequest).subscribeFx(
                { routes ->
                    handleAlgorithmResults(routes)
                },
                { e -> println(e.message) }
        )
    }

    private fun handleAlgorithmResults(routes: List<Route>) {
        val solution = transformRoutes(routes)
        this.routes = routes
        progressContainer.isVisible = false
        if (!solution.isPresent || solution.get().routes.isEmpty()) {
            resultsLabel.text = "No se encontró solución"
            resultsLabel.isVisible = true
        } else {
            resultsContainer.isVisible = true
            setupRoutes(solution.get())
        }
    }

    private fun transformRoutes(routes: List<Route>): Optional<Solution> {
        return when (routes.isEmpty()) {
            true -> Optional.empty()
            false -> Optional.of(Solution(routes))
        }
    }

    private fun setupRoutes(solution: Solution) {
        val customerCoordinates = solution.routes
                .map { route ->
                    route.orders
                }.map { orders ->
                    orders.map { order ->
                         order.coordinates.toCoordinate()
                    }
                }
        val routeCoordinates = solution.routes.map { it.nodes
                .sortedBy { it.id }
                .map { it.toCoordinate() }
        }
        setMapMarkers(customerCoordinates)
        addMapTracks(routeCoordinates, solution.routes)
    }

    private fun addMapTracks(solutionCoordinates: List<List<Coordinate>>, routes: List<Route>) {
        tracks.clear()
        solutionCoordinates.forEachIndexed { index, route ->
            val track = convertRouteToTracks(route)
            tracks.add(track)
            val row = loadRouteAssignmentRow(track.color, index)
            row.route = routes[index]
            routeAssignmentContainer.children.add(row)
        }
    }

    private fun loadRouteAssignmentRow(color: Color, index: Int): RouteAssignmentRow {
        val row = FXMLLoader.load<RouteAssignmentRow>(javaClass.getResource(
                "/com/dp1mtel/desktop/view/simulation/route_assignment_row.fxml"
        ))
        row.routeColor = color
        row.routeName = "Ruta ${('A'.toInt() + index).toChar()}"
        row.routeDriverSelectElement.apply {
            isVisible = model.allowAssignment
            items = availableDrivers
            valueProperty().addListener { observable, oldValue, newValue ->
                row.route.assignedDriver = newValue
            }
        }
        return row
    }

    private fun convertRouteToTracks(route: List<Coordinate>): CoordinateLine {
        val trackColor = Color.color(Math.random(), Math.random(), Math.random()).darker()
        val track = CoordinateLine(route).apply {
            color = trackColor
            visible = true
        }
        return track
    }

    private val customerLabels = mutableListOf<MapLabel>()

    private fun setMapMarkers(coordinates: List<List<Coordinate>>) {
        customerMarkers.forEach { marker -> resultsMap.removeMarker(marker) }
        customerLabels.forEach { label -> resultsMap.removeLabel(label) }
        customerLabels.clear()
        customerMarkers.apply {
            clear()
            add(Marker.createProvided(Marker.Provided.BLUE).apply {
                position = currentUser.store.coordinates.toCoordinate()
                visible = true
                resultsMap.addMarker(this)
            })
            addAll(
                    coordinates.map { routeCustomers ->
                        routeCustomers.mapIndexed { index, coord ->
                            val marker = Marker.createProvided(Marker.Provided.RED).apply {
                                position = coord
                                visible = true
                            }
                            val label = MapLabel((index + 1).toString()).setPosition(coord).setVisible(true)
                            customerLabels.add(label)
                            resultsMap.addLabel(label)
                            resultsMap.addMarker(marker)
                            marker
                        }
                    }.flatten()
            )
        }
    }
}
