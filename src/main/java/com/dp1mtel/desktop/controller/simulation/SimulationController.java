package com.dp1mtel.desktop.controller.simulation;

import com.dp1mtel.desktop.controller.BaseController;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;

import java.net.URL;
import java.util.ResourceBundle;

public class SimulationController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/simulation/index.fxml";

    @FXML private JFXButton btnStartSimulation;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);


    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnStartSimulation;
    }

    public void onSimularClicked(ActionEvent actionEvent) {
        setContentPane(DispatchController.viewPath);
    }
}
