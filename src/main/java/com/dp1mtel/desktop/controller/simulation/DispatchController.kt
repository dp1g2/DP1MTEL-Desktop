package com.dp1mtel.desktop.controller.simulation

import com.dp1mtel.desktop.controller.GenericBaseController
import com.dp1mtel.desktop.model.*
import com.dp1mtel.desktop.repository.IOrderRepository
import com.dp1mtel.desktop.repository.IUsersRepository
import com.dp1mtel.desktop.util.extensions.*
import com.jfoenix.controls.*
import com.jfoenix.controls.cells.editors.IntegerTextFieldEditorBuilder
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.scene.Node
import javafx.scene.control.CheckBox
import javafx.scene.control.Label
import javafx.scene.layout.StackPane
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import tornadofx.observable
import java.net.URL
import java.util.*
import tornadofx.getValue
import tornadofx.setValue
import java.time.LocalDate
import javafx.beans.value.ObservableValue
import javafx.scene.control.TreeItem
import java.util.function.Predicate


class DispatchController :
        GenericBaseController<DispatchController.Model>(),
        KoinComponent {
    companion object {
        @JvmField
        val viewPath = "/com/dp1mtel/desktop/view/simulation/configDispatch.fxml"
    }

    @FXML
    private lateinit var btnGenerateRoutes: JFXButton
    @FXML
    private lateinit var orderTable: JFXTreeTableView<OrderTreeObject>
    @FXML
    private lateinit var vehicleNumberSelection: JFXComboBox<Int>
    @FXML
    private lateinit var vehicleCapacity: Label
    @FXML
    private lateinit var btnCancel: JFXButton
    @FXML
    private lateinit var indexCol: JFXTreeTableColumn<OrderTreeObject, Int>
    @FXML
    private lateinit var customerCol: JFXTreeTableColumn<OrderTreeObject, String>
    @FXML
    private lateinit var orderVolumeCol: JFXTreeTableColumn<OrderTreeObject, Double>
    @FXML
    private lateinit var dateLimitCol: JFXTreeTableColumn<OrderTreeObject, String>
    @FXML
    private lateinit var selectedCol: JFXTreeTableColumn<OrderTreeObject, JFXCheckBox>
    @FXML
    private lateinit var stackPane: StackPane
    @FXML
    private lateinit var dateDispatch: JFXDatePicker

    private val userRepository: IUsersRepository by inject()
    private val orderRepository: IOrderRepository by inject()
    private val currentUser = userRepository.currentUser!!
    private val selectedOrders = mutableSetOf<Order>()
    private var model = Model()

    override fun initialize(url: URL?, resourceBundle: ResourceBundle?) {
        super.initialize(url, resourceBundle)
        initView()
    }

    private fun initView() {
        btnGenerateRoutes.setOnAction { onGenerateRoutesClicked() }
        btnCancel.setOnAction { onCancelClicked() }

        dateDispatch.valueProperty().addListener { observable, oldValue, newValue ->
            if (newValue == null) {
                orderTable.setPredicate { true }
                return@addListener
            }
            orderTable.setPredicate {
                val comparisonDate = (it.value.dateLimit?.clone() as Date?)?.apply {
                    hours = 0
                    minutes = 0
                    seconds = 0
                }?.equals(newValue.toDate()) == true
                comparisonDate
            }
        }

        vehicleCapacity.text = currentUser.store.numCapacity.toString()
        vehicleNumberSelection.items.addAll(1..currentUser.store.numVehicles.toInt())
        orderRepository.getOrdersByStore(currentUser.store.id).subscribeFx(
                { orders ->
                    setupOrderTable(orders)
                },
                { e -> println(e) }
        )
    }

    private fun setupOrderTable(orders: List<Order>) {
        val orderList = orders
                .filter { OrderStatusEnum.valueOf(it.status.slug) == OrderStatusEnum.PAID }
                .filter { it.deliveryAddress != null }
                .map { OrderTreeObject(it) }
                .observable()

        indexCol.makeIndexColumn()
        customerCol.setCellValueFactory { param -> param.value.value.customerNameProperty }
        orderVolumeCol.setCellValueFactory { param -> param.value.value.volumeProperty }
        dateLimitCol.setCellValueFactory { param -> SimpleStringProperty(User.dateFormat.format( param.value.value.dateLimitProperty.get()) )  }

        selectedCol.setCellValueFactory { param ->
            val order = param.value.value
            order.selectedProperty.addListener { obs, oldVal, newVal ->
                if (newVal) {
                    selectedOrders.add(order.order)
                } else {
                    selectedOrders.remove(order.order)
                }
            }
            val checkbox = JFXCheckBox().apply {
                selectedProperty().bindBidirectional(order.selectedProperty)
                order.selectedProperty.set(true);
            }
            SimpleObjectProperty(checkbox)
        }

        val root = RecursiveTreeItem(orderList) { param -> param.children }
        orderTable.root = root
        orderTable.isShowRoot = false
        orderTable.isEditable = true
        orderTable.columns.setAll(indexCol, customerCol, orderVolumeCol, dateLimitCol, selectedCol)
    }

    override fun getPaneReferenceComponent(): Node? {
        return btnGenerateRoutes
    }

    fun onCancelClicked() {
        setContentPane(SimulationController.viewPath)
    }

    fun onGenerateRoutesClicked() {
        if (!validateFields()) {
            return
        }
        val model = createSimulationModel()
        setContentPane(SimulationResultController.viewPath, model)
    }

    private fun createSimulationModel(): SimulationResultController.Model {
        return SimulationResultController.Model(
                selectedOrders.toList(),
                vehicleNumberSelection.value,
                currentUser.store.numCapacity.toInt(),
                model.allowAssignment
        )
    }

    private fun validateFields(): Boolean {
        if (dateDispatch.value == null) {
            AlertDialog.showMessage(stackPane, "Error", "Debe selecciona una fecha de pedidos")
            //Clean selected products

            return false
        }
        if (vehicleNumberSelection.value == null) {
            AlertDialog.showMessage(stackPane, "Error", "Debe seleccionar un máximo de vehículos para el pedido")
            return false
        }
        if (selectedOrders.isEmpty()) {
            AlertDialog.showMessage(stackPane, "Error", "Debe seleccionar al menos un pedido")
            return false
        }
        return true
    }

    data class Model(val allowAssignment: Boolean = false)

    override fun initModel(model: Model?) {
        this.model = model!!
    }

    class OrderTreeObject(val order: Order) : RecursiveTreeObject<OrderTreeObject>() {
        val customerNameProperty = SimpleStringProperty(order.customer.fullName)
        val customerName by customerNameProperty
        val volumeProperty = SimpleObjectProperty(order.volume)
        val volume by volumeProperty
        val dateLimitProperty = SimpleObjectProperty(order.expectedDeliveryDate?.apply {
            localTime = order.userReadyTime.toLocalTime()
        })
        val dateLimit by dateLimitProperty
        val selectedProperty = SimpleBooleanProperty(false)
        val selected by selectedProperty
    }
}
