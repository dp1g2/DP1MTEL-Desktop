package com.dp1mtel.desktop.controller.sales.orderStatus;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.model.Order;
import com.dp1mtel.desktop.model.OrderStatus;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import jdk.nashorn.internal.codegen.CompilerConstants;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.EventListener;
import java.util.ResourceBundle;

public class CancelOrderController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/sales/orderStatus/cancel_order.fxml";
    private static final String windowTitle = "cancel order";

    public StackPane container;
    public JFXTextField cancellationReason;
    public JFXTextField Cliente;
    public JFXButton btnCredit;
    public JFXButton btnCancel;
    public JFXButton btnConfirm;
    public Label customerInfo;

    public Model model;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        super.initialize(url, resourceBundle);
        btnCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                model.callback.onCancelClicked();
            }
        });
        btnConfirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                OrderStatus orderStatus = new OrderStatus(0,"Cancelado","Sin descripción","CANCELED");
                model.getOrder().setStatus(orderStatus);
                model.callback.onConfirmClicked();
            }
        });
    }

    public void initModel(Model model){
        this.model = model;
        customerInfo.setText(model.order.getCustomer().getFullName());
    }

    protected Node getPaneReferenceComponent() {return btnConfirm;}

    public static final class Model{
        private Order order;
        private Callback callback;

        public Model(Order order, Callback callback){
            setOrder(order);
            setCallback(callback);
        }

        public Model(Order order){
            setOrder(order);
        }

        public Model(){
        }

        public void setOrder(Order order){this.order = order;}
        public Order getOrder(){return order;}

        public void setCallback (Callback callback){this.callback = callback;}
        public Callback getCallback(){return callback;}

    }

    public interface Callback{
        public void onCancelClicked();
        public void onConfirmClicked();
    }
}
