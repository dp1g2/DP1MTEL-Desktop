package com.dp1mtel.desktop.controller.sales.orderStatus;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.sales.AddCustomerAddressController;
import com.dp1mtel.desktop.controller.sales.SalesPOSController;
import com.dp1mtel.desktop.di.OrdersHolder;
import com.dp1mtel.desktop.di.PosCombosHolder;
import com.dp1mtel.desktop.di.PosProductsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class OrderStatusEditController
        extends BaseController
        implements AddCustomerAddressController.AddAddressCallback,
                    CancelOrderController.Callback,
                    RefundOrderDialogController.RefundCallback,
                    PayOrderDialogController.PayOrderCallback {
    public static final String viewPath = "/com/dp1mtel/desktop/view/sales/orderStatus/edit.fxml";
    public static final String windowTitle = "Editar";
    public OrderStatusIndexController.OrderRow orderRowModel;
    private User currentUser;
    @FXML
    public StackPane dialogContainer;
    @FXML
    public JFXTextField txtCliente;
    @FXML
    public JFXTextField txtVendedor;
    @FXML
    public JFXTextField txtEstado;
    @FXML
    public JFXTextField txtTienda;

    @FXML
    public JFXComboBox<UserAddress> cmbDireccion;
    @FXML
    public JFXTextField txtVolumen;

    @FXML
    public JFXDatePicker txtFecha2;
    @FXML
    public JFXTimePicker txtHora1_2;
    @FXML
    public JFXTimePicker txtHora2_2;

    @FXML
    public JFXTreeTableView<SalesPOSController.CartItem> orderTable;

    @FXML
    public JFXTreeTableColumn<SalesPOSController.CartItem, String> codProdColumn;
    @FXML
    public JFXTreeTableColumn<SalesPOSController.CartItem, String> productoColumn;
    @FXML
    public JFXTreeTableColumn<SalesPOSController.CartItem, String> cantidadColumn;
    @FXML
    public JFXTreeTableColumn<SalesPOSController.CartItem, String> pUnitColumn;
    @FXML
    public JFXTreeTableColumn<SalesPOSController.CartItem, String> dsctoColumn;
    @FXML
    public JFXTreeTableColumn<SalesPOSController.CartItem, String> stockColumn;
    @FXML
    public JFXTreeTableColumn<SalesPOSController.CartItem, String> totalColumn;

    public ObservableList<OrderDetailRow> orderDetailRowsLists;
    public ObservableList<OrderDetailRow> ordersDetailList2;
    public ObservableList<SalesPOSController.CartItem> cartItemList;
    public JFXButton btnPay;
    public JFXButton btnCancelOrder;
    public JFXButton btnBack;
    public JFXButton btnRefundOrder;
    public JFXButton btnAddAddress;

    public StackPane stackPane;
    protected OrdersHolder ordersHolder = new OrdersHolder();
    protected PosProductsHolder posProductsHolder = new PosProductsHolder();
    protected PosCombosHolder posCombosHolder = new PosCombosHolder();
    protected UsersHolder usersHolder = new UsersHolder();

    @FXML
    public JFXButton btnSave;

    public List<OrderDetailRow> orderDetailsList;

    public static OrderStatusIndexController.OrderRow orderRowSelected;
    public static CancelOrderController.Model modelStatic;

    private Order order;
    private JFXDialog dialog;
    private Order orderAux;
    private JFXDialog refundDialog;
    private JFXDialog paymentDialog;

    protected Node getPaneReferenceComponent() {
        return btnSave;
    }

    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        currentUser = usersHolder.getUsersRepository().getCurrentUser();
        initModel(orderRowSelected);
        setupFieldValidation();
        setupCurrentAddress(orderRowSelected.getDeliveryAddress());
        btnPay.setOnAction((e) -> onPayOrder());
        btnCancelOrder.setOnAction((e) -> onCancelOrder());
        btnRefundOrder.setOnAction((e) -> onRefundOrder());
        btnAddAddress.setOnAction((e) -> displayAddressCreateDialog());
    }

    private void setupCurrentAddress(UserAddress deliveryAddress) {
        for (int i=0;i<cmbDireccion.getItems().size();i++){
            if (cmbDireccion.getItems().get(i).getAddress().equals(deliveryAddress.getAddress())){
                cmbDireccion.getSelectionModel().select(i);
                break;
            }
        }
    }

    private void displayAddressCreateDialog() {
        try {
            JFXDialogLayout content = new JFXDialogLayout();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/dp1mtel/desktop/view/sales/add_customer_address.fxml"));
            content = fxmlLoader.load();
            content.setPrefWidth(683.00);
            dialog = new JFXDialog(dialogContainer, content, JFXDialog.DialogTransition.CENTER);
            ((AddCustomerAddressController) fxmlLoader.getController()).
                    initModel(new AddCustomerAddressController.Model(order.getCustomer(), this));
            dialog.show();

        } catch (IOException e) {
            System.err.println("There is an error");
            e.printStackTrace();
        }
    }

    private void onRefundOrder() {
        displayRefundOrderDialog();
    }

    private void displayRefundOrderDialog() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/dp1mtel/desktop/view/sales/orderStatus/refund_order_dialog.fxml"));
            JFXDialogLayout content = fxmlLoader.load();
//            content.setPrefWidth(683.00);
            refundDialog = new JFXDialog(dialogContainer, content, JFXDialog.DialogTransition.CENTER);
            ((RefundOrderDialogController) fxmlLoader.getController()).
                    initModel(new RefundOrderDialogController.Model(order, this));
            refundDialog.show();

        } catch (IOException e) {
            System.err.println("There is an error");
            e.printStackTrace();
        }
    }

    private void onCancelOrder() {
        System.out.println("On cancel order");
        try {
            JFXDialogLayout content = new JFXDialogLayout();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/dp1mtel/desktop/view/sales/orderStatus/cancel_order.fxml"));
            content = fxmlLoader.load();
            content.setPrefWidth(683.00);
            orderAux = order;
            dialog = new JFXDialog(dialogContainer, content, JFXDialog.DialogTransition.CENTER);
            ((CancelOrderController)fxmlLoader.getController()).initModel(new CancelOrderController.Model(orderAux,this));
            dialog.show();

        }catch (Exception e){

        };
    }

    private void onPayOrder() {
        displayPayOrderDialog();
    }

    private void displayPayOrderDialog() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/dp1mtel/desktop/view/sales/orderStatus/pay_order_dialog.fxml"));
            JFXDialogLayout content = fxmlLoader.load();
            paymentDialog = new JFXDialog(dialogContainer, content, JFXDialog.DialogTransition.CENTER);
            ((PayOrderDialogController) fxmlLoader.getController()).
                    initModel(new PayOrderDialogController.Model(order, this));
            paymentDialog.show();
        } catch (IOException e) {
            System.err.println("There is an error");
            e.printStackTrace();
        }
    }


    public void initModel(OrderStatusIndexController.OrderRow model) {
        this.orderRowModel = model;
        this.order = orderRowModel.getOrder();
        btnPay.setVisible(this.order.getPayable());

        if(order.getStatus().getName().equals("Reservado")){
            btnRefundOrder.setVisible(false);
            btnCancelOrder.setLayoutX(533);
        }
        if(order.getStatus().getName().equals("Completado")){
            btnCancelOrder.setVisible(false);
            btnRefundOrder.setLayoutX(688);
        }

        cmbDireccion.setItems(FXCollections.observableArrayList(this.order.getCustomer().getAddresses()));
        txtCliente.setText(orderRowModel.getCustomer());
        txtVendedor.setText(orderRowModel.getSeller());
        txtEstado.setText(orderRowModel.getState());
        txtTienda.setText(orderRowModel.getShop().getDistrict() + "/" + orderRowModel.getShop().getId());

        txtVolumen.setText(Double.toString(orderRowModel.getVolume()));

        try {
            txtFecha2.setValue(DateExtensions.toLocalDate(orderRowModel.getDate()));
        } catch (Exception e) {
            Logger.getLogger(App.TAG).log(Level.WARNING, "Invalid date of order for orderRowModel: " + orderRowModel.getDeliveryAddress());
            txtFecha2.setValue(DateExtensions.toLocalDate(new Date()));
        }


        txtHora1_2.setValue(LocalTime.parse(convertTimetoString(orderRowModel.getUserReadyTime())));
        txtHora2_2.setValue(LocalTime.parse(convertTimetoString(orderRowModel.getUserDueTime())));
        setupTableOrderDetails(orderRowModel.getOrderDetailList());
    }

    public String convertTimetoString(double number) {
        int hour = (int) number;
        int min = (int) (number - hour) * 60;
        String s_hour = (hour < 10) ? "0" + String.valueOf(hour) : String.valueOf(hour);
        String s_min = (min < 10) ? "0" + String.valueOf(min) : String.valueOf(min);
        return s_hour + ":" + s_min;
    }

    public void setupFieldValidation() {
        RequiredFieldValidator requiredValidator = new RequiredFieldValidator();
        requiredValidator.setMessage("Este campo es obligatorio");
        // Dirección de envío


        // Volumen
        /*txtVolumen.setValidators(requiredValidator);
        txtVolumen.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) txtVolumen.validate();
        });
        txtVolumen.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue){
                if(!newValue.matches("\\d*")){
                    txtVolumen.setText(newValue.replaceAll("[^\\d]",""));
                }
            }
        });*/
        // Fecha
        /*txtFecha.setValidators(requiredValidator);
        txtFecha.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) txtFecha.validate();
        });
        // Hora inicio
        /*txtHora1.setValidators(requiredValidator);
        txtHora1.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) txtHora1.validate();
        });
        // Hora fin
        /*txtHora2.setValidators(requiredValidator);
        txtHora2.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) txtHora2.validate();
        });*/
    }

    public void setupTableOrderDetails(List<OrderDetail> orderDetailList) {

        // Getting all the avaliable products in the store
        Long storeId = currentUser.getShop().getId();
        posProductsHolder.getPosProRepository().getPosProducts(storeId)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (posProducts) -> posCombosHolder.getPosComboRepository().getPosCombos(storeId)
                                .subscribeOn(Schedulers.io())
                                .subscribe(
                                        (posCombos) -> Platform.runLater(() ->
                                                setupTableNotReplicatingProducts(orderDetailList, posProducts, posCombos)),
                                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage()))));

    }

    private void setupTableNotReplicatingProducts(List<OrderDetail> userOrderDetailList,
                                                  List<PosProduct> posProducts, List<PosCombo> posCombos) {

        //Lo que inicialmente ha pedido el usuario, lo hago orderDetailRow
        orderDetailsList = userOrderDetailList.stream()
                .map((orderDetail) -> new OrderDetailRow(orderDetail)).collect(Collectors.toList());

        //Obtengo los productos y combos de base de datos, con sus respectivos stock (clase CartItem)
        List<IPosElement> sellingProducts = new ArrayList<>(posProducts);
        sellingProducts.addAll(posCombos);

        List<SalesPOSController.CartItem> listCart = sellingProducts.stream()
                .map(p -> new SalesPOSController.CartItem(p)).collect(Collectors.toList());

        //Lo que en realidad estoy haciendo aqui es, convirtiendo todo a cart item, por lo que se quedaria sin codigo
        userOrderDetailList.forEach((userProduct) -> {
            listCart.forEach((cartItem -> {
                //If the user product is a product indeed
                if (userProduct.getProduct() != null) {
                    //Check if the current cartItem is a product too
                    if (cartItem.getPosElement().getProduct() != null) {
                        if (cartItem.getPosElement().getProduct().getId().equals(userProduct.getProduct().getId())) {
                            cartItem.setQuantity(cartItem.getQuantity() + userProduct.getQuantity());
                        }
                    }
                }
                if (userProduct.getCombo() != null) {
                    //Check if the current cartItem is a combo too
                    if (cartItem.getPosElement().getProductCombo() != null) {
                        if (cartItem.getPosElement().getProductCombo().getId().equals(userProduct.getCombo().getId())) {
                            cartItem.setQuantity(cartItem.getQuantity() + userProduct.getQuantity());
                        }
                    }
                }
            }));
        });

        cartItemList = FXCollections.observableArrayList(listCart);

        codProdColumn.setCellValueFactory((param) -> {
            if (codProdColumn.validateValue(param)) {
                return new SimpleStringProperty(param.getValue().getValue().getCode());
            }
            return codProdColumn.getComputedValue(param);
        });

        productoColumn.setCellValueFactory((param) -> {
            if (productoColumn.validateValue(param)) {
                return param.getValue().getValue().getNameProperty();
            }
            return productoColumn.getComputedValue(param);
        });

        stockColumn.setCellValueFactory((param) -> {
            if (stockColumn.validateValue(param)) {
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getStock()));
            }
            return stockColumn.getComputedValue(param);
        });

        cantidadColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());

        cantidadColumn.setCellValueFactory((param) -> {
            if (cantidadColumn.validateValue(param)) {
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getQuantity()));
            } else {
                return cantidadColumn.getComputedValue(param);
            }
        });

        cantidadColumn.setOnEditCommit(new EventHandler<TreeTableColumn.CellEditEvent<SalesPOSController.CartItem, String>>() {
            @Override
            public void handle(TreeTableColumn.CellEditEvent<SalesPOSController.CartItem, String> event) {
                TreeItem<SalesPOSController.CartItem> item = event.getRowValue();
                SalesPOSController.CartItem object = item.getValue();

                if (event.getOldValue() != event.getNewValue() && (Integer.parseInt(event.getNewValue()) - Integer.parseInt(event.getOldValue()) > object.getStock() || Integer.parseInt(event.getNewValue()) < 0)) {
                    AlertDialog.showMessage(dialogContainer, "Error", "Ingrese una cantidad válida");
                    object.setQuantity(Integer.parseInt(event.getOldValue()));
                } else {
                    object.setQuantity(Integer.parseInt(event.getNewValue()));
                    object.setFinalPrice(object.getQuantity() * (1 - object.getDiscount() / 100) * object.getPrice());
                }
                orderTable.refresh();
            }
        });


        pUnitColumn.setCellValueFactory((param) -> {
            if (pUnitColumn.validateValue(param)) {
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getPrice()));
            } else {
                return pUnitColumn.getComputedValue(param);
            }
        });

        dsctoColumn.setCellValueFactory((param) -> {
            if (dsctoColumn.validateValue(param)) {
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getDiscount()));
            } else {
                return dsctoColumn.getComputedValue(param);
            }
        });

        totalColumn.setCellValueFactory((param) -> {
            if (totalColumn.validateValue(param)) {
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getFinalPrice()));
            } else {
                return totalColumn.getComputedValue(param);
            }
        });

        final TreeItem<SalesPOSController.CartItem> root = new RecursiveTreeItem<>(cartItemList, RecursiveTreeObject::getChildren);
        orderTable.setRoot(root);
        orderTable.setShowRoot(false);
        orderTable.getColumns().setAll(codProdColumn, productoColumn, stockColumn, cantidadColumn, pUnitColumn, dsctoColumn, totalColumn);
    }


    public void onSaveMouseClicked(MouseEvent mouseEvent) {
        saveOrderDetails();
    }

    public void onGoBackClicked(MouseEvent mouseEvent) {
        goBack();
    }

    public void goBack() {
        setContentPane(OrderStatusIndexController.viewPath);
    }

    public void saveOrderDetails() {

        List<OrderDetail> newOrderDetailList = new ArrayList<>();

        cartItemList.forEach((cartItem -> {
            if (cartItem.getQuantity()!=0){
                newOrderDetailList.add(cartItem.toOrderDetail());
            }
        }));

//        newOrderDetailList.forEach((orderDetail -> {
//            orderDetail.setId(0);
//        }));
        //Busco el id del order detail anterior.

        for (int i= 0;i<newOrderDetailList.size();i++){
            for (int j = 0 ; j<orderDetailsList.size();j++){
                if (newOrderDetailList.get(i).getProduct()!= null &&
                        (newOrderDetailList.get(i).getProduct().getId().equals(orderDetailsList.get(j).getProduct().getId()))){
                    newOrderDetailList.get(i).setId(orderDetailsList.get(j).getId());
                    break;
                }
                if (newOrderDetailList.get(i).getCombo()!= null &&
                        (newOrderDetailList.get(i).getCombo().getId().equals(orderDetailsList.get(j).getProductCombo().getId()))){
                    newOrderDetailList.get(i).setId(orderDetailsList.get(j).getId());
                    break;
                }
            }
        }

        for (int i=0;i<newOrderDetailList.size();i++){
            System.out.println("EL ID ES " + newOrderDetailList.get(i).getId());
            System.out.println("LA CANTIDAD ES "  + newOrderDetailList.get(i).getQuantity());
        }

        orderRowModel.order.setOrderDetails(newOrderDetailList);
        orderRowModel.setDeliveryAddress(cmbDireccion.getSelectionModel().getSelectedItem());
        orderRowModel.setVolume(Double.parseDouble(txtVolumen.getText()));
        orderRowModel.order.setExpectedDeliveryDate(DateExtensions.toDate(txtFecha2.getValue()));
        orderRowModel.order.setUserReadyTime(convertTimetoDouble(txtHora1_2.getValue()));
        orderRowModel.order.setUserDueTime(convertTimetoDouble(txtHora2_2.getValue()));

        ordersHolder.getOrderRepository().updateOrder(orderRowModel.order)
                .subscribeOn(Schedulers.io())
                .subscribe((_order) -> {
                    Platform.runLater(() -> {
                        goBack();
                    });
                }, (e) -> {
                    e.printStackTrace();
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });
    }

    public double convertTimetoDouble(LocalTime time) {
        double integer = time.getHour();
        double decimal = time.getMinute() / 60;
        return integer + decimal;
    }

    @Override
    public void onAddressSave(@NotNull UserAddress address) {
        if (cmbDireccion.getItems() == null) {
            cmbDireccion.setItems(FXCollections.observableArrayList(address));
        } else {
            cmbDireccion.getItems().add(address);
        }
        dialog.close();
    }

    @Override
    public void onAddAddressCancel() {
        dialog.close();
    }

    @Override
    public void onCancelClicked() {
        dialog.close();
    }

    @Override
    public void onConfirmClicked() {
        txtEstado.setText(orderAux.getStatus().getName());
        dialog.close();
    }

    public void onConfirmRefundOrder(Order order) {
        System.out.println("onConfirmRefund");
        ordersHolder.getOrderRepository().refundOrder(order.getId(), order).subscribeOn(Schedulers.io())
                .subscribe(
                        (res) -> Platform.runLater(() -> {
                            if (res == null) {
                                return;
                            }
                            System.out.println(res.code());
                            if (!res.isSuccessful() || res.body() == null) {
                                switch (res.code()) {
                                    case 400: {
                                        AlertDialog.showMessage(dialogContainer, "Error", "El codigo de reserva es incorrecto");
                                        break;
                                    }
                                    case 401: {
                                        AlertDialog.showMessage(dialogContainer, "Error", "Debe llenar el codigo de reserva");
                                        break;
                                    }
                                    default: {
                                        AlertDialog.showMessage(dialogContainer, "Error", "Ocurrio un error al procesar su solicitud");
                                    }
                                }
                                return;
                            }
                            AlertDialog.showMessage(dialogContainer, "Correcto", "Se actualizo el pedido con exito");
                            refundDialog.close();
                            initModel(new OrderStatusIndexController.OrderRow(res.body()));
                        }),
                        (e) -> Platform.runLater(() -> {
                            AlertDialog.showMessage(dialogContainer, "Error", "Ocurrio un error al procesar su solicitud. " + e.getLocalizedMessage());
                            refundDialog.close();
                        })
                );
    }

    @Override
    public void onRefundCancel() {
        refundDialog.close();
    }

    @Override
    public void onConfirmPayment(Order order, String reservationCode) {
        System.out.println("onConfirmPayment");
        ordersHolder.getOrderRepository().payOrder(order, reservationCode).subscribeOn(Schedulers.io())
                .subscribe(
                        (res) -> Platform.runLater(() -> {
                            System.out.println(res.code());
                            if (!res.isSuccessful() || res.body() == null) {
                                switch (res.code()) {
                                    case 400: {
                                        AlertDialog.showMessage(dialogContainer, "Error", "El codigo de reserva es incorrecto");
                                        break;
                                    }
                                    case 401: {
                                        AlertDialog.showMessage(dialogContainer, "Error", "Debe llenar el codigo de reserva");
                                        break;
                                    }
                                    default: {
                                        AlertDialog.showMessage(dialogContainer, "Error", "Ocurrio un error al procesar su solicitud");
                                    }
                                }
                                return;
                            }
                            AlertDialog.showMessage(dialogContainer, "Correcto", "Se actualizo el pedido con exito");
                            paymentDialog.close();
                            initModel(new OrderStatusIndexController.OrderRow(res.body()));
                        }),
                        (e) -> Platform.runLater(() -> {
                            AlertDialog.showMessage(dialogContainer, "Error", "Ocurrio un error al procesar su solicitud. " + e.getLocalizedMessage());
                            paymentDialog.close();
                        })
                );
    }

    @Override
    public void onPaymentCancel() {
        paymentDialog.close();
    }

    public class OrderDetailRow extends RecursiveTreeObject<OrderDetailRow> {
        //private final OrderDetail detail;
        private long id;
        private Product product = new Product();
        private ProductCombo productCombo = new ProductCombo();
        private int quantity;
        private double unitPrice;
        private double discount;
        private double total;

        public OrderDetailRow(OrderDetail orderDetail) {
            setId(orderDetail.getId());
            setProduct(orderDetail.getProduct());
            setProductCombo(orderDetail.getCombo());
            setQuantity(orderDetail.getQuantity());
            setUnitPrice(orderDetail.getUnitPrice());
            setDiscount(orderDetail.getDiscount());
            setTotal(orderDetail.getTotal());
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public ProductCombo getProductCombo() {
            return productCombo;
        }

        public void setProductCombo(ProductCombo productCombo) {
            this.productCombo = productCombo;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public double getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(double unitPrice) {
            this.unitPrice = unitPrice;
        }

        public double getDiscount() {
            return discount;
        }

        public void setDiscount(double discount) {
            this.discount = discount;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(double total) {
            this.total = total;
        }
    }
}