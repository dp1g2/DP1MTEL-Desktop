package com.dp1mtel.desktop.controller.sales;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.sales.orderStatus.OrderStatusIndexController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class SalesController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/sales/index.fxml";

    @FXML
    private JFXButton btnPOS;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnPOS;
    }

    public void onEstadoPedidoClicked(MouseEvent mouseEvent) {
        setContentPane(OrderStatusIndexController.viewPath);
    }

    public void onPOSClicked(MouseEvent mouseEvent) {
        setContentPane(SalesPOSController.viewPath);
    }

    public void onRefundClicked(MouseEvent mouseEvent) {
        setContentPane(RefundController.viewPath);
    }
}

