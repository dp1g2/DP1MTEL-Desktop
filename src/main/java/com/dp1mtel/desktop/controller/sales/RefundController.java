package com.dp1mtel.desktop.controller.sales;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.OrdersHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.dp1mtel.desktop.util.extensions.ObservableExtensionsKt;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RefundController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/sales/sales_refund.fxml";

    public StackPane dialogContainer;
    public JFXTextField textSearch;
    public JFXDatePicker dateSearch;
    public JFXButton btnRefund;
    public JFXTreeTableView<OrderRow> orderTable;
    public JFXTreeTableColumn<OrderRow, Customer> customerCol;
    public JFXTreeTableColumn<OrderRow, User> sellerCol;
    public JFXTreeTableColumn<OrderRow, Date> dateCol;
    public JFXTreeTableColumn<OrderRow, String> reasonCol;
    public JFXTreeTableColumn actionsCol;

    private OrdersHolder ordersHolder = new OrdersHolder();
    private UsersHolder usersHolder = new UsersHolder();
    private Shop currentStore = usersHolder.getUsersRepository().getCurrentUser().getStore();

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        iniView();
    }

    private void iniView() {
        ordersHolder.getOrderRepository().getOrdersByStore(currentStore.getId())
                .subscribeOn(Schedulers.io())
                .subscribe((orders) -> {
                    Platform.runLater(() -> setupOrderTable(orders));
                }, (e) -> {
                    Platform.runLater(() -> AlertDialog.showMessage(
                            dialogContainer,
                            "Error!",
                            "Ocurrió un error cargando los pedidos"));
                });
        textSearch.textProperty().addListener((obs, oldValue, newValue) -> {
            Predicate<TreeItem<OrderRow>> predicate;
            String search = newValue;
            if (search == null || search.isEmpty()) {
                predicate = (item) -> true;
            } else {
                predicate = (item) -> {
                    Order order = item.getValue().order;
                    return order.getCustomer().getFullName().toLowerCase().contains(search)
                            || order.getCustomer().getDocument().contains(search)
                            || order.getSeller().getFullName().toLowerCase().contains(search);
                };
            }
            orderTable.setPredicate(predicate);
        });
        dateSearch.valueProperty().addListener((obs, oldValue, newValue) -> {
            Predicate<TreeItem<OrderRow>> predicate;
            if (newValue == null) {
                predicate = (item) -> true;
            } else {
                Date search = DateExtensions.toDate(newValue);
                predicate = (item) -> {
                    Date date = item.getValue().date.get();
                    return (date.getYear() == search.getYear()
                            && date.getMonth() == search.getMonth()
                            && date.getDate() == search.getDate());
                };
            }
            orderTable.setPredicate(predicate);
        });
    }

    private void setupOrderTable(List<Order> orders) {
        List<OrderRow> filteredOrders = orders.stream().filter((order) -> {
            return order.getStatus().getSlug().equals(OrderStatusEnum.RETURNED.toString());
        }).map(OrderRow::new).collect(Collectors.toList());
        ObservableList<OrderRow> orderList = FXCollections.observableList(filteredOrders);

        customerCol.setCellValueFactory(param -> param.getValue().getValue().customer);
        sellerCol.setCellValueFactory(param -> param.getValue().getValue().seller);
        dateCol.setCellValueFactory(param -> param.getValue().getValue().date);
        reasonCol.setCellValueFactory(param -> param.getValue().getValue().reason);

        RecursiveTreeItem<OrderRow> root = new RecursiveTreeItem<>(orderList, RecursiveTreeObject::getChildren);
        orderTable.setRoot(root);
        orderTable.setShowRoot(false);
        orderTable.setEditable(false);
        orderTable.getColumns().setAll(customerCol, sellerCol, dateCol, reasonCol, actionsCol);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnRefund;
    }

    public static class OrderRow extends RecursiveTreeObject<OrderRow> {
        private final Order order;
        private final SimpleObjectProperty<Customer> customer;
        private final SimpleObjectProperty<User> seller;
        private final SimpleObjectProperty<Date> date;
        private final SimpleStringProperty reason;

        public OrderRow(Order order) {
            this.order = order;
            System.out.println(order.getCreatedAt());
            this.customer = new SimpleObjectProperty<>(order.getCustomer());
            this.seller = new SimpleObjectProperty<>(order.getSeller());
            this.date = new SimpleObjectProperty<>(order.getCreatedAt());
            this.reason = new SimpleStringProperty(order.getCancellationReason());
        }
    }
}
