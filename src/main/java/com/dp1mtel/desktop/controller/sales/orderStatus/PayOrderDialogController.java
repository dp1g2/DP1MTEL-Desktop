package com.dp1mtel.desktop.controller.sales.orderStatus;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.BillsHolder;
import com.dp1mtel.desktop.di.OrdersHolder;
import com.dp1mtel.desktop.model.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import retrofit2.Response;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class PayOrderDialogController extends GenericBaseController<PayOrderDialogController.Model> {
    public StackPane container;
    public JFXTextField txtReservationCode;
    public Label txtCustomerInfo;
    public Label txtOrderTotal;
    public JFXButton btnSave;
    public JFXButton btnBack;
    public JFXRadioButton radioBoleta;
    public JFXRadioButton radioFactura;
    private Model model;
    private OrdersHolder ordersHolder = new OrdersHolder();
    private BillsHolder billsHolder = new BillsHolder();
    private Bill documentPayment;
    private String docType;

    public interface PayOrderCallback {
        void onConfirmPayment(Order order, String reservationCode);
        void onPaymentCancel();
    }

    public static class Model {
        private final Order order;
        private final PayOrderCallback callback;

        public Model(Order order, PayOrderCallback callback) {
            this.order = order;
            this.callback = callback;
        }

        public Order getOrder() {
            return order;
        }

        public PayOrderCallback getCallback() {
            return callback;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        btnBack.setOnAction((e) -> model.callback.onPaymentCancel());
        btnSave.setOnAction((e) -> {
            if (!validateFields()) {
                return;
            }
            saveOrder();
        });
    }

    private void saveOrder() {
        AlertDialog.showMessage(container, "Confirmar", "Seleccione donde guardar el documento de pago", () -> {
            generateBill();
            model.order.setStatus(new OrderStatus(0L, "", "", OrderStatusEnum.PAID.toString()));
            model.getCallback().onConfirmPayment(model.order, txtReservationCode.getText());
        });
    }

    private void generateBill() {

        if (radioBoleta.isSelected()) {
            docType = Bill.DocumentType.BOLETA.toString();
        } else {
            docType = Bill.DocumentType.FACTURA.toString();
        }


        billsHolder.getBillRepository().createBill(docType)
                .subscribe(this::fillPaymentDocument);

        documentPayment.generateBillPDF();
    }

    private void fillPaymentDocument(Response<Bill> billResponse) {
        if (billResponse.code() != 500) {
            if (billResponse.body() != null) {
                documentPayment = new Bill(docType,
                        billResponse.body().getCodeBoleta(),
                        billResponse.body().getCodeFactura(),
                        billResponse.body().getCodeNotaCredito());

                documentPayment.setTax(model.getOrder().getTotal()*Constants.IGV);
                documentPayment.setOrder(model.order);
                documentPayment.setSubtotal(model.getOrder().getTotal());
                documentPayment.setDate(User.dateFormat.format(new Date()));

            } else {
                System.out.println("hubo un problema con el back");
            }
        }
    }

    private boolean validateFields() {
        if (txtReservationCode.getText() == null || txtReservationCode.getText().isEmpty()) {
            AlertDialog.showMessage(container, "Error", "Debe llenar el codigo de reserva");
            return false;
        }
        if (!radioBoleta.isSelected() && !radioFactura.isSelected()) {
            AlertDialog.showMessage(container, "Error", "Debe seleccionar el documento de pago");
            return false;
        }
        return true;
    }

    @Override
    public void initModel(Model model) {
        this.model = model;
        txtCustomerInfo.setText(model.order.getCustomer().toString());
        txtOrderTotal.setText(String.format("%.2f", model.order.getTotal()));
    }
}
