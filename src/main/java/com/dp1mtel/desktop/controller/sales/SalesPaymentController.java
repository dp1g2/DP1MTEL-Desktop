package com.dp1mtel.desktop.controller.sales;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.BillsHolder;
import com.dp1mtel.desktop.di.OrdersHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.DecimalFormatter;
import com.dp1mtel.desktop.util.validation.CreditCardNumberValidator;
import com.dp1mtel.desktop.util.validation.IdValidator;
import com.dp1mtel.desktop.util.validation.NameValidator;
import com.dp1mtel.desktop.util.validation.RUCValidator;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import retrofit2.Response;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class SalesPaymentController extends GenericBaseController<SalesPaymentController.Model> {
    public static final String viewPath = "/com/dp1mtel/desktop/view/sales/sales_paymentMethod.fxml";
    public static final String windowTitle = "Información de pago";
    @FXML private JFXRadioButton rbttn_Efectivo;
    @FXML private JFXRadioButton rbttn_Tarjeta;
    @FXML private JFXTextField lbl_NombreTitular;
    @FXML private JFXTextField lbl_NumTarjeta;
    @FXML private JFXTextField lbl_EntidadFin;

    @FXML private JFXRadioButton rbttn_Boleta;
    @FXML private JFXRadioButton rbttn_Factura;
    @FXML private JFXTextField lbl_NomEmpresa;
    @FXML private JFXTextField lbl_Ruc;
    @FXML private JFXTextField lbl_Direccion;

    @FXML private Label amountField;
    @FXML private Label lbl_client;
    @FXML private StackPane dialogContainer;
    private double subTotal;
    private Model model;
    private OrdersHolder ordersHolder = new OrdersHolder();

    private BillsHolder billsHolder = new BillsHolder();

    private Bill documentPayment;

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @Override
    public void initModel(Model model) {
        this.model = model;

        amountField.setText(DecimalFormatter.formats(model.order.getTotal()));
        lbl_client.setText(model.order.getCustomer().getFullName());
    }

    public static class Model {
        public Order order;
        public Model(Order order) { this.order = order;}
    }
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url,resourceBundle);

        lbl_NombreTitular.disableProperty().bind(rbttn_Tarjeta.selectedProperty().not());
        lbl_EntidadFin.disableProperty().bind(rbttn_Tarjeta.selectedProperty().not());
        lbl_NumTarjeta.disableProperty().bind(rbttn_Tarjeta.selectedProperty().not());

        lbl_NomEmpresa.disableProperty().bind(rbttn_Factura.selectedProperty().not());
        lbl_Ruc.disableProperty().bind(rbttn_Factura.selectedProperty().not());
        lbl_Direccion.disableProperty().bind(rbttn_Factura.selectedProperty().not());


        RequiredFieldValidator requiredFieldValidator = new RequiredFieldValidator();
        requiredFieldValidator.setMessage("Campo obligatorio");
        NameValidator nameValidator = new NameValidator();
        nameValidator.setMessage("Valor inválido");

        CreditCardNumberValidator creditCardNumberValidator = new CreditCardNumberValidator();
        creditCardNumberValidator.setMessage("Valor inválido");

        RUCValidator rucValidator = new RUCValidator();
        rucValidator.setMessage("Valor inválido");

        IdValidator idValidator = new IdValidator();
        idValidator.setMessage("Valor inválido");

        lbl_NomEmpresa.setValidators(requiredFieldValidator);
        lbl_Ruc.setValidators(requiredFieldValidator,rucValidator);
        lbl_Direccion.setValidators(requiredFieldValidator);

        lbl_NombreTitular.setValidators(requiredFieldValidator, nameValidator);
        lbl_EntidadFin.setValidators(requiredFieldValidator);
        lbl_NumTarjeta.setValidators(requiredFieldValidator,creditCardNumberValidator);



        lbl_Ruc.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) lbl_Ruc.validate();
        });

        lbl_Direccion.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) lbl_Direccion.validate();
        });

        lbl_NomEmpresa.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) lbl_NomEmpresa.validate();
        });

        lbl_NombreTitular.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) lbl_NombreTitular.validate();
        });

        lbl_NumTarjeta.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) lbl_NumTarjeta.validate();
        });

        lbl_EntidadFin.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) lbl_EntidadFin.validate();
        });




    }

    @Override
    protected Node getPaneReferenceComponent() {
        return lbl_NombreTitular;
    }

    private Boolean validateFactura() {

        if(lbl_Ruc.validate() && lbl_NomEmpresa.validate() && lbl_Direccion.validate())
            return true;

       return false;

    }

    private boolean validateTarjeta() {

        if(lbl_EntidadFin.validate() && lbl_NumTarjeta.validate() && lbl_NombreTitular.validate())
            return true;

        return false;
    }


    public void onTerminarMouseClicked(MouseEvent mouseEvent) {

        try {
            OrderStatus orderStatus;
            if (model.order.getDeliveryAddress() == null) {
                // Order is picked up in-store
                orderStatus = new OrderStatus(0,"","",OrderStatusEnum.COMPLETED.toString());
            } else {
                // Order is to be delivered
                orderStatus = new OrderStatus(0, "", "", OrderStatusEnum.PAID.toString());
            }

            model.order.setStatus(orderStatus);

        }catch (Exception e) {
            System.err.println(e.getMessage());
        }

        if (rbttn_Efectivo.isSelected() || (rbttn_Tarjeta.isSelected() && validateTarjeta())) {


            if (rbttn_Boleta.isSelected() || (rbttn_Factura.isSelected() && validateFactura())){


                ordersHolder.getOrderRepository().saveOrder(model.order)
                        .subscribeOn(Schedulers.io())
                        .subscribe(orderResponse -> {
                                    Platform.runLater(() -> {
                                        if (orderResponse.code() != 500) {
                                            if (orderResponse.body() != null) {
                                                System.out.println(orderResponse.body().getId());
                                                printPaymentDocument();

                                                User usuario= usersHolder.getUsersRepository().getCurrentUser();

                                                Order _order = orderResponse.body();
                                                String logMensaje="CREACIÓN - Pedido '"+_order.getId()+" - Cliente: "+_order.getCustomer()+"' - Vendedor "+ usuario.getFullName();

                                                LOGGERAUDIT.info(logMensaje);
                                                setContentPane(SalesPOSController.viewPath);
                                            } else {
                                                AlertDialog.showMessage(dialogContainer, "Error al guardar", "No hay stock");
                                            }
                                        }
                                    });
                                },
                                (e) -> {

                                    Platform.runLater(() -> System.err.println(e.getMessage()));
                                });
            } else{
                AlertDialog.showMessage(dialogContainer, "Datos Incorrectos", "Debe escoger documento de pago y llenar los campos solicitados");
                return;
            }


        }else{
            AlertDialog.showMessage(dialogContainer,"Cuidado","Debe elegir metodo de pago, documento y llenar datos solicitados");
        }


    }


    private void printPaymentDocument() {

        billsHolder.getBillRepository().createBill(rbttn_Boleta.isSelected() ? Bill.DocumentType.BOLETA.toString() : Bill.DocumentType.FACTURA.toString())
                .subscribe(this::fillPaymentDocument);



        if(rbttn_Factura.isSelected()){

            documentPayment.setRecipientBussiness(lbl_NomEmpresa.getText());
            documentPayment.setRecipientAddress(lbl_Direccion.getText());
            documentPayment.setRecipientRuc(lbl_Ruc.getText());
        }

        documentPayment.generateBillPDF();


    }

    private void fillPaymentDocument(Response<Bill> billResponse) {


        if (billResponse.code() != 500) {
            if (billResponse.body() != null) {
                documentPayment = new Bill(rbttn_Boleta.isSelected() ? Bill.DocumentType.BOLETA.toString() : Bill.DocumentType.FACTURA.toString(),
                        billResponse.body().getCodeBoleta(),
                        billResponse.body().getCodeFactura(),
                        billResponse.body().getCodeNotaCredito());

                documentPayment.setTax(model.order.getTotal() * Constants.IGV);
                documentPayment.setOrder(model.order);
                documentPayment.setSubtotal(model.order.getTotal()*(1-Constants.IGV));
                documentPayment.setDate(User.dateFormat.format(new Date()));


            } else {
                System.out.println("hubo un problema con el back");
            }
        }
    }

}
