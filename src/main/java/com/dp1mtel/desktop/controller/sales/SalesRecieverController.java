package com.dp1mtel.desktop.controller.sales;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.controller.BaseController;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SalesRecieverController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/sales/sales_reciever.fxml";
    @FXML
    private JFXButton btnReservar;
    @FXML private StackPane dialogcontainer;
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url,resourceBundle);
        VBox matrix = new VBox(10);
        //HBox fila = new HBox(10);
        //HBox fila2 = new HBox(10);


    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnReservar;
    }


    public void onReservarMouseClicked(MouseEvent mouseEvent) {
        try{
            JFXDialogLayout content = new JFXDialogLayout();
            Pane header = FXMLLoader.load(getClass().getResource(ReservationSuccessController.viewHeaderPath));
            content.setHeading(header);
            Pane pane= FXMLLoader.load(getClass().getResource(ReservationSuccessController.viewBodyPath));
            content.setBody(pane);

            JFXDialog dialog = new JFXDialog(dialogcontainer,content,JFXDialog.DialogTransition.TOP);

            JFXButton aceptar =  new JFXButton("Aceptar");

            aceptar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();

                }
            });



            content.setActions(aceptar);
            dialog.show();
        }catch(Exception ex){
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }

    }


    public void onPagarMouseClicked(MouseEvent mouseEvent) {
        setContentPane(SalesPaymentController.viewPath);

    }
}
