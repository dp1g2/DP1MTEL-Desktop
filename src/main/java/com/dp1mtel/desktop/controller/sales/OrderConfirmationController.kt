package com.dp1mtel.desktop.controller.sales

import com.dp1mtel.desktop.App
import com.dp1mtel.desktop.controller.GenericBaseController
import com.dp1mtel.desktop.model.AlertDialog
import com.dp1mtel.desktop.model.Order
import com.dp1mtel.desktop.model.OrderStatus
import com.dp1mtel.desktop.model.UserAddress
import com.dp1mtel.desktop.repository.ICustomersRepository
import com.dp1mtel.desktop.repository.IOrderRepository
import com.dp1mtel.desktop.util.extensions.toDouble
import com.dp1mtel.desktop.util.extensions.subscribeFx
import com.dp1mtel.desktop.util.extensions.toDate
import com.dp1mtel.desktop.util.extensions.toLocalDate
import com.jfoenix.controls.*
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.control.DateCell
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent
import javafx.scene.layout.StackPane
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import tornadofx.action
import tornadofx.observableList
import java.net.URL
import java.time.LocalDate
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

class OrderConfirmationController :
        GenericBaseController<OrderConfirmationController.Model>(),
        KoinComponent,
        AddCustomerAddressController.AddAddressCallback {
    companion object {
        @JvmField val viewPath = "/com/dp1mtel/desktop/view/sales/order_confirmation.fxml"
    }

    @FXML
    private lateinit var dialogContainer: StackPane
    @FXML
    private lateinit var btnReserve: JFXButton
    @FXML
    private lateinit var btnPayment: JFXButton
    @FXML
    private lateinit var deliveryCheck: JFXCheckBox
    @FXML
    private lateinit var addMessaggeCheckBox: JFXCheckBox
    @FXML
    private lateinit var recipientName: JFXTextField
    @FXML
    private lateinit var recipientDocument: JFXTextField
    @FXML
    private lateinit var messageTextInput: JFXTextArea
    @FXML
    private lateinit var deliveryAddress: JFXComboBox<UserAddress>
    @FXML
    private lateinit var customerInfo: Label
    @FXML
    private lateinit var btnAddAddress: JFXButton
    @FXML private lateinit var orderDeliveryDate: JFXDatePicker
    @FXML private lateinit var userReadyTime: JFXTimePicker
    @FXML private lateinit var userDueTime: JFXTimePicker

    private lateinit var addAddressDialog: JFXDialog

    private val customersRepository: ICustomersRepository by inject()
    private val orderRepository: IOrderRepository by inject()

    private lateinit var model: Model

    override fun initialize(location: URL?, resourceBundle: ResourceBundle?) {
        super.initialize(location, resourceBundle)
        initView()
    }

    private fun initView() {
        deliveryAddress.items = observableList()
        btnAddAddress.action {
            displayAddressCreateDialog()
        }
        btnReserve.action {
            onReserveClicked()
        }
        btnPayment.action {
            if (!validateOrder()) {
                // TODO: alert
                return@action
            }
            saveOrder()
            setContentPane(SalesPaymentController.viewPath, SalesPaymentController.Model(model.order))
        }
        val deliveryCheckSelected = deliveryCheck.selectedProperty()
        recipientName.disableProperty().bind(deliveryCheckSelected.not())
        recipientDocument.disableProperty().bind(deliveryCheckSelected.not())
        deliveryAddress.disableProperty().bind(deliveryCheckSelected.not())

        addMessaggeCheckBox.disableProperty().bind(deliveryCheckSelected.not())
        orderDeliveryDate.disableProperty().bind(deliveryCheckSelected.not())
        userReadyTime.disableProperty().bind(deliveryCheckSelected.not())
        userDueTime.disableProperty().bind(deliveryCheckSelected.not())
        messageTextInput.disableProperty().bind(addMessaggeCheckBox.selectedProperty().not())
        btnAddAddress.disableProperty().bind(deliveryCheckSelected.not())

        val minDate = Date().toLocalDate()
        orderDeliveryDate.setDayCellFactory { picker ->
            object : DateCell() {
                override fun updateItem(item: LocalDate?, empty: Boolean) {
                    super.updateItem(item, empty)
                    if (item?.isBefore(minDate) == true) {
                        isDisable = true
                        style = "-fx-background-color: #ffc0cb;"
                    }
                }
            }
        }
        userReadyTime.setIs24HourView(true)
        userDueTime.setIs24HourView(true)
        userReadyTime.valueProperty().addListener { obs, oldVal, newVal ->
            if ((userDueTime.value != null) && (newVal?.isAfter(userDueTime.value) == true)) {
                // TODO: alert
                println("User ready time should not be after user due time")
            }
        }
        userDueTime.valueProperty().addListener { obs, oldVal, newVal ->
            if ((userReadyTime.value != null) && (newVal?.isBefore(userReadyTime.value) == true)) {
                // TODO: alert
                println("User due time should not be before user ready time")
            }
        }
    }

    private fun saveOrder() {
        if (deliveryCheck.isSelected) {
            model.order.recipientName = recipientName.text
            model.order.recipientDocument = recipientDocument.text
            model.order.deliveryAddress = deliveryAddress.value
            model.order.expectedDeliveryDate = orderDeliveryDate.value.toDate()
            model.order.userReadyTime = userReadyTime.value.toDouble()
            model.order.userDueTime = userDueTime.value.toDouble()
        }

        if (addMessaggeCheckBox.isSelected) {
            model.order.recipientDedication = messageTextInput.text
        }
    }

    private fun validateOrder(): Boolean {
        if (deliveryCheck.isSelected) {
            if (userReadyTime.value == null || userDueTime.value == null || orderDeliveryDate.value == null) {
                AlertDialog.showMessage(dialogContainer, "Cuidado!","Debe seleccionar la fecha, la hora mínima y la hora máxima de entrega")
                return false
            }
            if (userReadyTime.value.toDouble() >= userDueTime.value.toDouble()) {
                AlertDialog.showMessage(dialogContainer, "Cuidado!", "La hora mínima debe ser menor que la hora máxima de entrega")
                println("User ready time should not be after user due time")
                return false
            }
            if (deliveryAddress.value == null) {
                AlertDialog.showMessage(dialogContainer, "Cuidado!", "Debe seleccionar la dirección de destino")
                println("Delivery address should be set")
                return false
            }
        }
        if (addMessaggeCheckBox.isSelected) {
            if (messageTextInput.text.isNullOrEmpty()) {
                AlertDialog.showMessage(dialogContainer, "Cuidado!", "La dedicatoria no puede estar vacía")
                println("Dedication must not be empty")
                return false
            }
        }
        return true
    }

    private fun displayAddressCreateDialog() {
        try {
            val fxmlLoader = FXMLLoader(javaClass.getResource("/com/dp1mtel/desktop/view/sales/add_customer_address.fxml"))
            val layout = fxmlLoader.load() as JFXDialogLayout
            addAddressDialog = JFXDialog(dialogContainer, layout, JFXDialog.DialogTransition.CENTER)
            fxmlLoader.getController<AddCustomerAddressController>().initModel(
                    AddCustomerAddressController.Model(model.order.customer, this)
            )
            addAddressDialog.show()
        } catch (e: Exception) {
            val logger = Logger.getLogger(javaClass.name)
            logger.log(Level.SEVERE, "Failed to create dialog.", e)
            println(e.message)
            e.printStackTrace()
        }
    }

    override fun initModel(model: Model?) {
        this.model = model!!
        customerInfo.text = "${model.order.customer.document}, ${model.order.customer.fullName}"
        customersRepository.getCustomerAddresses(model.order.customer.id).subscribeFx(
                { addresses -> setupAddressList(addresses) },
                { e -> println("Exception: ${e.message}") }
        )
    }

    private fun setupAddressList(addresses: List<UserAddress>) {
        deliveryAddress.items.setAll(addresses)
    }

    override fun getPaneReferenceComponent(): Node? {
        return btnReserve
    }

    fun onReserveClicked() {
        if (!validateOrder()) {
            // TODO: alert
            return
        }
        saveOrder()
        model.order.status = OrderStatus(0, "Reservado", "Reservado", "RESERVED")
        orderRepository.reserverOrder(model.order).subscribeFx(
                { res ->
                    if (!res.isSuccessful) {
                        // TODO: Handle error
                    } else {
                        model.order = res.body()!!
                        println(model.order.reservationCode)
                        showReservationDialog(model.order) {
                            setContentPane(SalesController.viewPath)
                        }
                    }
                },
                { e ->
                    AlertDialog.showMessage(dialogContainer, "Error!", e.localizedMessage)
                    println(e.message)
                }
        )
    }

    private fun updateOrder() {
        model.order.recipientName = recipientName.text
        model.order.recipientDocument = recipientDocument.text
    }

    private fun showReservationDialog(order: Order, onClose: () -> Unit) {
        try {
            val loader = FXMLLoader(javaClass.getResource(ReservationSuccessController.viewPath))
            val content = loader.load<JFXDialogLayout>()
            loader.getController<ReservationSuccessController>().labelCodBank.text = order.reservationCode

            val dialog = JFXDialog(dialogContainer, content, JFXDialog.DialogTransition.TOP)
            val btnAccept = JFXButton("Aceptar")
            btnAccept.onAction = EventHandler {
                dialog.close()
                onClose()
            }
            content.setActions(btnAccept)
            dialog.show()
        } catch (ex: Exception) {
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.message)
            ex.printStackTrace()
        }
    }

    override fun onAddressSave(address: UserAddress) {
        deliveryAddress.items.setAll(deliveryAddress.items + address)
        addAddressDialog.close()
    }

    override fun onAddAddressCancel() {
        addAddressDialog.close()
    }

    fun onBackClicked(mouseEvent: MouseEvent) {
        println("back pressed")
    }

    data class Model(var order: Order)
}