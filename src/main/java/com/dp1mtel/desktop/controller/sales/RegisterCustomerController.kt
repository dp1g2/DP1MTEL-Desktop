package com.dp1mtel.desktop.controller.sales

import com.dp1mtel.desktop.controller.GenericBaseController
import com.dp1mtel.desktop.di.UsersHolder
import com.dp1mtel.desktop.model.Customer
import com.dp1mtel.desktop.model.UserAddress
import com.dp1mtel.desktop.repository.ICustomersRepository
import com.dp1mtel.desktop.util.extensions.subscribeFx
import com.dp1mtel.desktop.util.validation.IdValidator
import com.dp1mtel.desktop.util.validation.NameValidator
import com.dp1mtel.desktop.util.validation.OnlyString20Validator
import com.dp1mtel.desktop.util.validation.PhoneValidator
import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXDialogLayout
import com.jfoenix.controls.JFXTextField
import com.jfoenix.validation.RequiredFieldValidator
import com.squareup.moshi.Moshi
import javafx.fxml.FXML
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.stage.Stage
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.config.plugins.validation.validators.RequiredValidator
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.lang.Double.parseDouble

import java.net.URL
import java.util.ResourceBundle

class RegisterCustomerController :
        GenericBaseController<RegisterCustomerController.Model>(),
        KoinComponent {

    @FXML
    private lateinit var documentField: JFXTextField
    @FXML
    private lateinit var nameField: JFXTextField
    @FXML
    private lateinit var lastNameField: JFXTextField
    @FXML
    private lateinit var phoneField: JFXTextField
    @FXML
    private lateinit var btnSave: JFXButton
    @FXML
    private lateinit var btnCancel: JFXButton
    @FXML
    private lateinit var layout: JFXDialogLayout
    @FXML
    private lateinit var error_label: Label

    private lateinit var nameValidator: NameValidator
    private lateinit var idValidator: IdValidator
    private lateinit var phoneValidator: PhoneValidator
    private lateinit var requiredValidator: RequiredFieldValidator

    private lateinit var model: Model
    private val customerRepository: ICustomersRepository by inject()

    private val LOGGERAUDIT = LogManager.getLogger("FileAuditAppender")
    private val usersHolder = UsersHolder()

    override fun initialize(url: URL?, resourceBundle: ResourceBundle?) {
        btnCancel.setOnAction { model.callback.onCancel() }
        setupFieldValidation()
        btnSave.setOnAction {
            if (validateFields()) {
                saveCustomer()
            }
        }
    }

    private fun setupFieldValidation() {
        requiredValidator = RequiredFieldValidator()
        nameValidator = NameValidator()
        idValidator = IdValidator()
        phoneValidator = PhoneValidator()

        requiredValidator.message = "Este campo es obligatorio"
        nameValidator.message = "Debe introducir nombres válidos"
        idValidator.message = "Debe introducir un DNI válido"
        phoneValidator.message = "Debe introducir un teléfono correcto"
        documentField.setValidators(requiredValidator,idValidator)
        nameField.setValidators(requiredValidator,nameValidator)
        lastNameField.setValidators(requiredValidator,nameValidator)
        phoneField.setValidators(requiredValidator,phoneValidator)

    }

    private fun saveCustomer() {
        nameField.text = nameField.text.trim()
        lastNameField.text = lastNameField.text.trim()
        documentField.text = documentField.text.trim()
        phoneField.text = phoneField.text.trim()

        if (nameField.text == "" || lastNameField.text == "" || documentField.text == "" || phoneField.text == "") {
            error_label.text = "Debe llenar todos los campos"
        } else {
            var numeric = true

            try {
                val num = parseDouble(documentField.text)
            } catch (e: NumberFormatException) {
                numeric = false
            }

            if (numeric) {
                val customer = Customer(
                        firstName = nameField.text,
                        lastName = lastNameField.text,
                        document = documentField.text,
                        phone = phoneField.text
                )
                customerRepository.saveCustomer(customer).subscribeFx(
                        { newCustomer ->
                            model.callback.onCustomerSaved(newCustomer)
                            val usuario = usersHolder.usersRepository.currentUser
                            val logMensaje = "CREAR - Nuevo Cliente '" + customer.id + " " + "' - Usuario " + usuario!!.getFullName()
                            LOGGERAUDIT.info(logMensaje)
                        },
                        { e ->
                            error_label.text = "Ya existe un usuario con ese DNI"
                            println(e.message)
                        }
                )
            } else {
                error_label.text = "El campo DNI debe de ser numerico"
            }

        }


    }

    private fun validateFields(): Boolean {
        if (documentField.validate() && nameField.validate() && lastNameField.validate() && phoneField.validate()){
            return true
        }
        return false
    }

    override fun getPaneReferenceComponent(): Node {
        return btnCancel
    }

    override fun initModel(model: Model?) {
        this.model = model!!
    }

    data class Model(var callback: RegisterCustomerCallback)

    interface RegisterCustomerCallback {
        fun onCustomerSaved(customer: Customer)
        fun onCancel()
    }
}
