package com.dp1mtel.desktop.controller.sales.orderStatus;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.OrdersHolder;
import com.dp1mtel.desktop.model.Order;
import com.dp1mtel.desktop.model.OrderDetail;
import com.dp1mtel.desktop.model.Shop;
import com.dp1mtel.desktop.model.UserAddress;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class OrderStatusIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/sales/orderStatus/index.fxml";
    public static final String windowTitle = "Pedidos";

    @FXML
    private JFXButton btnGenNotaCredito;
    private List<OrderRow> ordersList;

    @FXML
    private JFXTreeTableView<OrderRow> ordersTable;
    @FXML
    private JFXTreeTableColumn<OrderRow, String> indexColumn;
    @FXML
    private JFXTreeTableColumn<OrderRow, String> customerColumn;
    @FXML
    private JFXTreeTableColumn<OrderRow, String> receiverColumn;
    @FXML
    private JFXTreeTableColumn<OrderRow, String> dateColumn;
    @FXML
    private JFXTreeTableColumn<OrderRow, String> stateColumn;
    @FXML
    private JFXTreeTableColumn<OrderRow, Void> actionsColumn;
    private OrdersHolder ordersHolder = new OrdersHolder();
    @FXML
    private JFXTextField searchOrder;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        loadDataStore();
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnGenNotaCredito;
    }

    public void loadDataStore() {
        ordersHolder.getOrderRepository().getOrders().subscribe(this::setupOrderTable);
        ordersTable.refresh();
    }

    public void setupOrderTable(List<Order> orderList) {
        ordersList = orderList.stream().map((order) -> new OrderRow(order)).collect(Collectors.toList());
        ObservableList<OrderRow> ordersList2 = FXCollections.observableArrayList(ordersList);

        //TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        indexColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getId()));
            } else {
                return indexColumn.getComputedValue(param);
            }
        });

        customerColumn.setCellValueFactory((param) -> {
            if (customerColumn.validateValue(param)) {
                return param.getValue().getValue().customerProperty();
            } else {
                return customerColumn.getComputedValue(param);
            }
        });

        receiverColumn.setCellValueFactory((param) -> {
            if (receiverColumn.validateValue(param)) {
                return param.getValue().getValue().receiverProperty();
            } else {
                return receiverColumn.getComputedValue(param);
            }
        });

        dateColumn.setCellValueFactory((param) -> {
            if (dateColumn.validateValue(param)) {
                return param.getValue().getValue().dateProperty();
            } else {
                return dateColumn.getComputedValue(param);
            }
        });

        stateColumn.setCellValueFactory((param) -> {
            if (stateColumn.validateValue(param)) {
                return param.getValue().getValue().stateProperty();
            } else {
                return stateColumn.getComputedValue(param);
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<OrderRow, Void>() {
            private final MaterialDesignIconView editBtn = new MaterialDesignIconView(MaterialDesignIcon.PENCIL);
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final MaterialDesignIconView viewBtn = new MaterialDesignIconView(MaterialDesignIcon.MAGNIFY);
            private final HBox pane = new HBox(editBtn, deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                editBtn.setOnMouseClicked((e) -> {
                    OrderRow orderRow = ordersTable.getTreeItem(getIndex()).getValue();
                    OrderStatusEditController.orderRowSelected = orderRow;

                    setContentPane(OrderStatusEditController.viewPath, OrderStatusEditController.windowTitle);
                });

                deleteBtn.setOnMouseClicked((e) -> {

                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });


        final TreeItem<OrderRow> root = new RecursiveTreeItem<>(ordersList2, RecursiveTreeObject::getChildren);
        ordersTable.setRoot(root);
        ordersTable.setShowRoot(false);
        ordersTable.getColumns().setAll(indexColumn, customerColumn, dateColumn, stateColumn, actionsColumn);

        searchOrder.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                ordersTable.setPredicate(new Predicate<TreeItem<OrderRow>>() {
                    @Override
                    public boolean test(TreeItem<OrderRow> orderRowTreeItem) {
                        Boolean flag = orderRowTreeItem.getValue().getCustomer().toLowerCase().contains(newValue.toLowerCase()) ||
                                orderRowTreeItem.getValue().getState().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });

    }

    public static class OrderRow extends RecursiveTreeObject<OrderRow> {
        //SimpleStringProperty customer = new SimpleStringProperty(ordersList.get().toString());
        private long id;
        private SimpleStringProperty customer = new SimpleStringProperty();
        private SimpleStringProperty receiver = new SimpleStringProperty();
        private SimpleStringProperty seller = new SimpleStringProperty();
        private UserAddress deliveryAddress;
        private double volume;
        private double userReadyTime;
        private double userDueTime;
        private Shop shop;
        private Date date;
        private SimpleStringProperty state = new SimpleStringProperty();
        //private SimpleListProperty<OrderDetail> orderDetails = new SimpleListProperty<>();
        private List<OrderDetail> orderDetails = new ArrayList<>();
        public Order order;

        public OrderRow(Order order) {
            this.order = order;
            setId(order.getId());
            setCustomer(order.getCustomer().getFullName());
            setSeller(order.getSeller().getFullName());
            setDeliveryAddress(order.getDeliveryAddress());
            setVolume(order.getVolume());
            setUserReadyTime(order.getUserReadyTime());
            setUserDueTime(order.getUserDueTime());
            setShop(order.getStore());
            setDate(order.getExpectedDeliveryDate());
            setState(order.getStatus().getName());
            setOrderDetailList(order.getOrderDetails());
        }

        public void setCustomer(String customer) {
            this.customer.set(customer);
        }

        public String getCustomer() {
            return customer.get();
        }

        public SimpleStringProperty customerProperty() {
            return customer;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Date getDate() {
            return date;
        }

        public SimpleStringProperty dateProperty() {
            if (date == null)
                return new SimpleStringProperty("Entregado en el momento");
            else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH) + 1;
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                String s_month = (month < 10) ? "0" + String.valueOf(month) : String.valueOf(month);
                String s_day = (day < 10) ? "0" + String.valueOf(day) : String.valueOf(day);
                return new SimpleStringProperty(s_day + "/" + s_month + "/" + Integer.toString(year));
            }
        }

        public void setReceiver(String receiver) {
            this.receiver.set(receiver);
        }

        public String getReceiver() {
            return receiver.get();
        }

        public SimpleStringProperty receiverProperty() {
            return receiver;
        }

        public void setSeller(String seller) {
            this.seller.set(seller);
        }

        public String getSeller() {
            return seller.get();
        }

        public SimpleStringProperty sellerProperty() {
            return seller;
        }

        public void setDeliveryAddress(UserAddress userAddress) {
            this.order.setDeliveryAddress(userAddress);
        }

        public UserAddress getDeliveryAddress() {
            return this.order.getDeliveryAddress();
        }


        public void setVolume(double volume) {
            this.volume = volume;
        }

        public double getVolume() {
            return volume;
        }

        public void setUserReadyTime(double userReadyTime) {
            this.userReadyTime = userReadyTime;
        }

        public double getUserReadyTime() {
            return userReadyTime;
        }

        public void setUserDueTime(double userDueTime) {
            this.userDueTime = userDueTime;
        }

        public double getUserDueTime() {
            return userDueTime;
        }

        public void setShop(Shop shop) {
            this.shop = shop;
        }

        public Shop getShop() {
            return this.shop;
        }

        public void setState(String state) {
            this.state.set(state);
        }

        public String getState() {
            return state.get();
        }

        public SimpleStringProperty stateProperty() {
            return state;
        }

        /*public void setOrderDetailList(Collection<OrderDetail> orderDetailList) {
            this.orderDetails.set(FXCollections.observableArrayList(orderDetailList));
        }*/
        public void setOrderDetailList(List<OrderDetail> orderDetails) {
            this.orderDetails = orderDetails;
        }

        public List<OrderDetail> getOrderDetailList() {
            return orderDetails;
        }

        //public SimpleListProperty<OrderDetail> orderDetailListProperty(){ return orderDetails;}

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Order getOrder() {
            return order;
        }

        public void setOrder(Order order) {
            this.order = order;
        }
    }
}
