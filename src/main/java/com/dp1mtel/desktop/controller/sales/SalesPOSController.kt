package com.dp1mtel.desktop.controller.sales

import com.dp1mtel.desktop.controller.BaseController
import com.dp1mtel.desktop.di.UsersHolder
import com.dp1mtel.desktop.model.*
import com.dp1mtel.desktop.repository.*
import com.dp1mtel.desktop.util.extensions.subscribeFx
import com.jfoenix.animation.alert.JFXAlertAnimation
import com.jfoenix.controls.*
import com.jfoenix.controls.cells.editors.IntegerTextFieldEditorBuilder
import com.jfoenix.controls.cells.editors.base.GenericEditableTreeTableCell
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.TreeItem
import javafx.scene.layout.StackPane
import javafx.stage.Modality
import javafx.stage.Stage
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import tornadofx.getValue
import tornadofx.observable
import tornadofx.onChange
import tornadofx.setValue
import java.net.URL
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

class SalesPOSController :
        BaseController(),
        KoinComponent,
        RegisterCustomerController.RegisterCustomerCallback {
    companion object {
        @JvmField
        val viewPath = "/com/dp1mtel/desktop/view/sales/sales_pos.fxml"
    }

    /**
     * Properties
     */
    @FXML
    private lateinit var customerDocumentField: JFXTextField
    @FXML
    private lateinit var productSearchField: JFXTextField
    @FXML
    private lateinit var productSearchButton: FontAwesomeIconView
    @FXML
    private lateinit var btnRegisterCustomer: JFXButton
    @FXML
    private lateinit var productTable: JFXTreeTableView<CartItem>
    @FXML
    private lateinit var btnShoppingCart: JFXButton
    @FXML
    private lateinit var codeCol: JFXTreeTableColumn<CartItem, String>
    @FXML
    private lateinit var nameCol: JFXTreeTableColumn<CartItem, String>
    @FXML
    private lateinit var priceCol: JFXTreeTableColumn<CartItem, Double>
    @FXML
    private lateinit var discountCol: JFXTreeTableColumn<CartItem, Double>
    @FXML
    private lateinit var finalPriceCol: JFXTreeTableColumn<CartItem, Double>
    @FXML
    private lateinit var stockCol: JFXTreeTableColumn<CartItem, Int>
    @FXML
    private lateinit var quantityCol: JFXTreeTableColumn<CartItem, Int>
    @FXML
    private lateinit var labelClientName: Label
    @FXML
    private lateinit var orderSubtotal: Label
    @FXML
    private lateinit var orderIgv: Label
    @FXML
    private lateinit var orderTotalAmount: Label
    @FXML
    private lateinit var dialogContainer: StackPane


    private var usersHolder = UsersHolder()
    private val productsRepository: IProductsRepository by inject()
    private val usersRepository: IUsersRepository by inject()

    private val posProductRepository: IPosProductRepository by inject()
    private val posComboRepository: IPosComboRepository by inject()
    private val customersRepository: ICustomersRepository by inject()

    private var cart = Cart(usersRepository)

    private val invalidQtyAlert: JFXAlert<Unit> by lazy {
        val layout = JFXDialogLayout().apply {
            setBody(Label("No es una cantidad válida."))
        }
        buildAlertFromLayout(layout)
    }
    private val emptyCustomerAlert: JFXAlert<Unit> by lazy {
        val layout = JFXDialogLayout().apply {
            setBody(Label("No se ha seleccionado el cliente."))
        }
        buildAlertFromLayout(layout)
    }
    private val emptyCartAlert: JFXAlert<Unit> by lazy {
        val layout = JFXDialogLayout().apply {
            setBody(Label("No se han seleccionado productos."))
        }
        buildAlertFromLayout(layout)
    }
    private lateinit var newCustomerDialog: JFXDialog
    private lateinit var customerPopup: JFXAutoCompletePopup<Customer>

    /**
     * Methods
     */

    override fun initialize(location: URL?, resourceBundle: ResourceBundle?) {
        super.initialize(location, resourceBundle)
        initView()
    }

    private fun initView() {
        btnRegisterCustomer.setOnAction { ev -> loadRegisterCustomerWindow() }

        val user = usersHolder.usersRepository.currentUser
        val store = user!!.store
        posProductRepository.getPosProducts(store.id)
                .subscribeFx(
                        { posProducts ->

                            posComboRepository.getPosCombos(store.id)
                                    .subscribeFx(
                                            { posCombos ->
                                                setupProductsTable(posProducts, posCombos)
                                            },
                                            { e -> println(e.message) })
                        },
                        { e -> println(e.message) }
                )
        btnShoppingCart.onAction = EventHandler {
            generateOrder()
        }
        productSearchField.textProperty().addListener { observable, oldValue, newValue ->
            productTable.setPredicate {
                val comparisonProduct = it.value.name?.apply {}?.equals(newValue) == true
                comparisonProduct
            }
        }

        setupCustomerAutoComplete()
    }

    private fun setupCustomerAutoComplete() {
        customersRepository.getCustomers().subscribeFx(
                { customers ->

                    customerPopup = JFXAutoCompletePopup<Customer>().apply {
                        suggestions.setAll(customers)
                        selectionHandler = EventHandler {
                            updateCurrentCustomer(it.`object`)
                        }
                    }

                    //customerPopup.cellLimit = 20
                    customerDocumentField.textProperty().addListener { observable, oldValue, newValue ->
                        customerPopup.filter { it.document.toLowerCase().contains(newValue.toLowerCase()) }
                        if (customerPopup.filteredSuggestions.isEmpty()) {
                            customerPopup.hide()
                        } else {
                            customerPopup.show(customerDocumentField)
                        }
                    }
                },
                { e -> println(e.message) }
        )

    }


    private fun setupProductsTable(products: List<IPosElement>, combos: List<IPosElement>) {
        val itemsList: List<IPosElement> = products + combos

        val productList = FXCollections.observableList(processPosProducts(itemsList))

        codeCol.setCellValueFactory { param -> param.value.value.codeProperty }
        nameCol.setCellValueFactory { param -> param.value.value.nameProperty }
        priceCol.setCellValueFactory { param -> param.value.value.priceProperty }
        discountCol.setCellValueFactory { param -> param.value.value.discountProperty }
        finalPriceCol.setCellValueFactory { param -> param.value.value.finalPriceProperty }
        stockCol.setCellValueFactory { param -> param.value.value.stockProperty }
        quantityCol.setCellValueFactory { param -> param.value.value.quantityProperty }

        quantityCol.setCellFactory { param -> GenericEditableTreeTableCell(IntegerTextFieldEditorBuilder()) }
        quantityCol.setOnEditCommit { t ->
            val cartItem = t.treeTableView
                    .getTreeItem(t.treeTablePosition.row)
                    .value
            if (t.oldValue != t.newValue && (t.newValue > cartItem.stock || t.newValue < 0)) {
                // Show alert
                invalidQtyAlert.show()
//                t.treeTableView.columns[0].isVisible = false
                t.treeTableView.columns[0].isVisible = true
            } else {
                cartItem.quantity = t.newValue
                updateCart(cartItem!!)
            }
        }

        val root = RecursiveTreeItem(productList) { param -> param.children }
        productTable.root = root
        productTable.isShowRoot = false
        productTable.isEditable = true
        productTable.columns.setAll(codeCol, nameCol, priceCol, discountCol, finalPriceCol, stockCol, quantityCol)

    }

    private fun buildAlertFromLayout(layout: JFXDialogLayout): JFXAlert<Unit> {
        return JFXAlert<Unit>(mainContentPane.scene.window as Stage).apply {
            animation = JFXAlertAnimation.CENTER_ANIMATION
            setContent(layout)
            initModality(Modality.NONE)
        }
    }

    private fun generateOrder() {
        if (validCart()) {
            setContentPane(
                    OrderConfirmationController.viewPath,
                    OrderConfirmationController.Model(cart.toOrder())
            )
        }
    }

    private fun validCart(): Boolean {
        if (cart.customer == null) {
            emptyCustomerAlert.show()
            return false
        }
        if (cart.total == 0.0) {
            emptyCartAlert.show()
            return false
        }
        return true
    }

    private fun updateCart(cartItem: CartItem) {
        cart.cartItems.remove(cartItem)
        cart.cartItems.add(cartItem)
        orderSubtotal.text = "%.2f".format(cart.subtotal)
        orderIgv.text = "%.2f".format(cart.igv)
        orderTotalAmount.text = "%.2f".format(cart.total)
        updateProductStock()
    }

    private fun updateProductStock() {
        // TODO: Dynamically update product stock based on selected quantity

    }

    private fun processPosProducts(posProducts: List<IPosElement>): List<CartItem> {
        return posProducts.map { p -> CartItem(p) }
    }

    private fun loadRegisterCustomerWindow() {
        try {
            val fxmlLoader = FXMLLoader(javaClass.getResource("/com/dp1mtel/desktop/view/sales/register_customer_dialog.fxml"))
            val layout = fxmlLoader.load() as JFXDialogLayout
            newCustomerDialog = JFXDialog(dialogContainer, layout, JFXDialog.DialogTransition.CENTER)
            fxmlLoader.getController<RegisterCustomerController>().initModel(
                    RegisterCustomerController.Model(this)
            )
            newCustomerDialog.show()
        } catch (e: Exception) {
            val logger = Logger.getLogger(javaClass.name)
            logger.log(Level.SEVERE, "Failed to create dialog.", e)
            println(e.message)
            e.printStackTrace()
        }
    }

    override fun getPaneReferenceComponent(): Node? {
        return btnShoppingCart
    }

    override fun onCustomerSaved(customer: Customer) {
        updateCurrentCustomer(customer)
        customerPopup.suggestions.add(customer)
        newCustomerDialog.close()
    }

    private fun updateCurrentCustomer(customer: Customer) {
        this.cart.customer = customer
        labelClientName.text = customer.fullName
        customerDocumentField.text = customer.document
    }

    override fun onCancel() {
        newCustomerDialog.close()
    }

    data class CartItem(val posElement: IPosElement) : RecursiveTreeObject<CartItem>() {

        val nameProperty = SimpleStringProperty(posElement.name)
        var name by nameProperty
        val priceProperty = SimpleObjectProperty<Double>(posElement.price.toDouble())
        var price by priceProperty
        val codeProperty = SimpleStringProperty(posElement.id.toString())
        var code by codeProperty
        val discountProperty = SimpleObjectProperty<Double>(posElement.discount*100)
        var discount by discountProperty
        val quantityProperty = SimpleObjectProperty(0)
        var quantity by quantityProperty
        val finalPriceProperty = SimpleObjectProperty<Double>(0.0)
        var finalPrice by finalPriceProperty
        val stockProperty = SimpleObjectProperty(posElement.stock)
        var stock by stockProperty
        var originalStock = stock

        init {
            quantityProperty.onChange { value ->
                finalPrice = price * (value ?: 1) * (1 - discount/100)
            }
        }

        fun toOrderDetail(): OrderDetail {
            if (posElement.classType == PosCombo::class.java) {
                val combo = posElement.productCombo

                return OrderDetail().let { orderDetail ->
                    orderDetail.combo = combo
                    orderDetail.discount = discount/100
                    orderDetail.quantity = quantity
                    orderDetail.unitPrice = posElement.price
                    orderDetail
                }
            } else {
                val product = posElement.product


                return OrderDetail().let { orderDetail ->
                    orderDetail.product = product
                    orderDetail.discount = discount/100
                    orderDetail.quantity = quantity
                    orderDetail.unitPrice = product.price.toDouble()
                    orderDetail
                }
            }

        }
    }

    data class Cart(
            val usersRepository: IUsersRepository,
            var customer: Customer? = null,
            val cartItems: MutableSet<CartItem> = mutableSetOf()
    ) {
        val subtotal: Double
            get() = (cartItems.sumByDouble { item -> item.finalPrice })/1.18

        val igv: Double
            get() = subtotal * Constants.IGV

        val total: Double
            get() = subtotal + igv

        fun toOrder(): Order {
            return Order(
                    seller = usersRepository.currentUser!!,
                    customer = customer!!,
                    status = OrderStatus(0, "En espera", "Pedido en espera", "TEMP"),
                    orderDetails = cartItems.map { item -> item.toOrderDetail() },
                    store = usersRepository.currentUser!!.shop)
        }
    }
}
