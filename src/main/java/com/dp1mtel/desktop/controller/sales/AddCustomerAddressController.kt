package com.dp1mtel.desktop.controller.sales

import com.dp1mtel.desktop.controller.GenericBaseController
import com.dp1mtel.desktop.model.AlertDialog
import com.dp1mtel.desktop.model.Customer
import com.dp1mtel.desktop.model.UserAddress
import com.dp1mtel.desktop.repository.ICustomersRepository
import com.dp1mtel.desktop.service.DispatchService
import com.dp1mtel.desktop.util.extensions.subscribeFx
import com.dp1mtel.desktop.util.toCoordinate
import com.dp1mtel.desktop.util.toPoint2D
import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXTextField
import com.sothawo.mapjfx.Coordinate
import com.sothawo.mapjfx.MapView
import com.sothawo.mapjfx.Marker
import com.sothawo.mapjfx.event.MapViewEvent
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.input.KeyCode
import javafx.scene.layout.StackPane
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.net.URL
import java.util.*

class AddCustomerAddressController :
        GenericBaseController<AddCustomerAddressController.Model>(),
        KoinComponent {
    @FXML private lateinit var btnCancel: JFXButton
    @FXML private lateinit var btnSave: JFXButton
    @FXML private lateinit var addressNameField: JFXTextField
    @FXML private lateinit var addressField: JFXTextField
    @FXML private lateinit var customerInfo: Label
    @FXML private lateinit var addressMap: MapView
    @FXML private lateinit var btnSearchAddress: FontAwesomeIconView
    @FXML private lateinit var dialogContainer: StackPane

    private var coordinates = Coordinate(-12.0652157, -77.0817718)
    private val currentMarker: Marker = Marker.createProvided(Marker.Provided.RED).setVisible(true)

    private val customersRepository: ICustomersRepository by inject()
    private val dispatchService: DispatchService by inject()
    private lateinit var model: Model

    override fun initialize(location: URL?, resourceBundle: ResourceBundle?) {
        super.initialize(location, resourceBundle)
        btnCancel.onAction = EventHandler { model.callback.onAddAddressCancel() }
        btnSave.onAction = EventHandler {
            if (validateFields()) {
                saveAddress()
            }
        }
        btnSearchAddress.setOnMouseClicked {
            findAddressCoordinates(addressField.text ?: "")
        }
        initMap()
    }

    private fun initMap() {
        addressMap.addEventHandler(MapViewEvent.MAP_CLICKED) {
            println(currentMarker.position)
            addMapMarker(it.coordinate)
        }
        addressMap.initializedProperty().addListener { it, oldVal, newVal ->
            if (newVal) {
                addressMap.center = coordinates
                addressMap.zoom = 16.0
                addressField.setOnKeyPressed { value ->
                    if (value.code == KeyCode.ENTER) {
                        findAddressCoordinates(addressField.text)
                    }
                }
            }
        }
        addressMap.initialize()
    }

    private fun findAddressCoordinates(address: String) {
        dispatchService.geocode(address).subscribeFx { coords ->
            if (coords.isEmpty()) {
                AlertDialog.showMessage(
                        dialogContainer,
                        "Error!",
                        "No se encontró la dirección ingresada. Intente nuevamente")
                currentMarker.position = null
                return@subscribeFx
            }
            println(coords.first())
            addMapMarker(coords.first().toCoordinate())
        }
    }

    private fun addMapMarker(coordinate: Coordinate) {
        if (currentMarker.visible) {
            val needToAddMarker = null == currentMarker.position
            currentMarker.position = coordinate
            if (needToAddMarker) {
                // adding can only be done after coordinate is set
                addressMap.addMarker(currentMarker)
            }
            addressMap.center = coordinate
        }
    }

    private fun validateFields(): Boolean {
        var valid = true
        if (addressField.text.isBlank()) {
            valid = false
        }
        if (currentMarker.position == null) {
            println("Position not selected")
            valid = false
        }
        return valid
    }

    private fun saveAddress() {
        val address = UserAddress(addressField.text, addressNameField.text, currentMarker.position.toPoint2D())
        customersRepository.saveCustomerAddress(model.customer.id, address).subscribeFx(
                { savedAddress ->
                    model.callback.onAddressSave(savedAddress)
                },
                { e ->
                    AlertDialog.showMessage(dialogContainer, "Error!", e.localizedMessage)
                }
        )
    }

    override fun initModel(model: Model?) {
        this.model = model!!
        customerInfo.text = model.customer.toString()
    }

    data class Model(
            val customer: Customer,
            val callback: AddAddressCallback
    )

    interface AddAddressCallback {
        fun onAddressSave(address: UserAddress)
        fun onAddAddressCancel()
    }
}
