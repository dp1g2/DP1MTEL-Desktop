package com.dp1mtel.desktop.controller.sales;

import com.dp1mtel.desktop.controller.BaseController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Deprecated
public class OldSalesPOSController extends BaseController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/sales/old_sales_pos.fxml";
    public static double total = 0.0;  // manejar el monto total.
    private static Pane temporal;


    @FXML private ScrollPane matrix_products;
    @FXML private Pane container;
    @FXML private Label lblMontoTotal;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url,resourceBundle);
        VBox matrix = new VBox(10);
        //HBox fila = new HBox(10);
        //HBox fila2 = new HBox(10);

        try{
            //Solicitar API los productos
            //por ahora hardcodeo
            for (int i = 0; i <3 ; i++) {
                HBox fila = new HBox(10);
                for (int j = 0; j <3 ; j++) {
                    Pane product = FXMLLoader.load(getClass().getResource("/com/dp1mtel/desktop/view/sales/view_producto.fxml"));
                    fila.getChildren().add(product);
                }
                matrix.getChildren().add(fila);

            }
            matrix_products.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
            matrix_products.setContent(matrix);



        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected Node getPaneReferenceComponent() {
        return matrix_products;
    }



    public void onLimpiarMouseClicked(MouseEvent mouseEvent) {
        System.out.println("Limpieza");
    }

    public void onContinuarMouseClicked(MouseEvent mouseEvent) {
        setContentPane(SalesRecieverController.viewPath);
    }
}
