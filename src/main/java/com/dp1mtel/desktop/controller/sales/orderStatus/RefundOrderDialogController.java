package com.dp1mtel.desktop.controller.sales.orderStatus;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.BillsHolder;
import com.dp1mtel.desktop.model.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import retrofit2.Response;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class RefundOrderDialogController extends GenericBaseController<RefundOrderDialogController.Model> {
    public JFXTextField txtCancellationReason;
    public Label customerInfo;
    public JFXButton btnSave;
    public JFXButton btnBack;
    public StackPane container;

    private BillsHolder billsHolder = new BillsHolder();
    private Bill documentPayment;
    private Model model;

    public static class Model {
        private final RefundCallback callback;
        private final Order order;

        public Model(Order order, RefundCallback callback) {
            this.order = order;
            this.callback = callback;
        }

        public Order getOrder() {
            return order;
        }

        public RefundCallback getCallback() {
            return callback;
        }
    }

    public interface RefundCallback {
        void onConfirmRefundOrder(Order order);
        void onRefundCancel();
    }

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        btnBack.setOnAction((e) -> model.callback.onRefundCancel());
        btnSave.setOnAction((e) -> {
            AlertDialog.showConfirmationDialog(
                    container,
                    "Confirmar", "Confirme que desea devolver el pedido",
                    () -> {
                        System.out.println("Confirmar");
                        AlertDialog.showMessage(container, "", "Seleccione donde guardar la nota de credito", () -> {
                            generateCreditNote();
                            model.order.setCancellationReason(txtCancellationReason.getText());
                            model.callback.onConfirmRefundOrder(model.order);
                        });
                    },
                    () -> {
                        System.out.println("Cancelar");
                    });
        });
    }

    private void generateCreditNote() {

        billsHolder.getBillRepository().createBill(Bill.DocumentType.NOTA_CREDITO.toString())
                .subscribe(this::fillPaymentDocument);

        documentPayment.generateBillPDF();


    }

    private void fillPaymentDocument(Response<Bill> billResponse) {

        if (billResponse.code() != 500) {
            if (billResponse.body() != null) {
                documentPayment = new Bill(Bill.DocumentType.NOTA_CREDITO.toString(),
                        billResponse.body().getCodeBoleta(),
                        billResponse.body().getCodeFactura(),
                        billResponse.body().getCodeNotaCredito());

                documentPayment.setTax(model.getOrder().getTotal()*Constants.IGV);
                documentPayment.setOrder(model.order);
                documentPayment.setSubtotal(model.getOrder().getTotal());
                documentPayment.setDate(User.dateFormat.format(new Date()));

            } else {
                System.out.println("hubo un problema con el back");
            }
        }
    }


    @Override
    public void initModel(Model model) {
        this.model = model;
        this.customerInfo.setText(model.order.getCustomer().toString());
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnBack;
    }
}
