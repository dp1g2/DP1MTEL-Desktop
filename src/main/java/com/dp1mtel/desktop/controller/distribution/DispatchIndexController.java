package com.dp1mtel.desktop.controller.distribution;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.simulation.SimulationController;
import com.dp1mtel.desktop.controller.simulation.SimulationResultController;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class DispatchIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/distribution/configDispatch.fxml";

    public void initialize(URL url, ResourceBundle resourceBundle){ super.initialize(url, resourceBundle);}

    @FXML
    private JFXButton btnGenerateRoutes;

    @Override
    protected Node getPaneReferenceComponent() {
        return btnGenerateRoutes;
    }

    public void onCancelarClicked(MouseEvent mouseEvent) {
        setContentPane(DistributionController.viewPath);
    }

    public void onGenerateRoutesClicked(ActionEvent actionEvent) {
        setContentPane(SimulationResultController.viewPath);
    }
}
