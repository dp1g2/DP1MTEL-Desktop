package com.dp1mtel.desktop.controller.distribution;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.simulation.DispatchController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class DistributionController  extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/distribution/index.fxml";

    @FXML private JFXButton btnDispatch;

    public void initialize(URL url, ResourceBundle resourceBundle){ super.initialize(url, resourceBundle);}


    @Override
    protected Node getPaneReferenceComponent() {
        return btnDispatch;
    }

    public void onDispatchClicked(MouseEvent mouseEvent) {
        setContentPane(DispatchController.viewPath, new DispatchController.Model(true));
    }

   
}
