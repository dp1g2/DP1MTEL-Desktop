package com.dp1mtel.desktop.controller.distribution;

import com.dp1mtel.desktop.controller.BaseController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class TrackOrderController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/distribution/seguirPedido.fxml";

    public void initialize(URL url, ResourceBundle resourceBundle){ super.initialize(url, resourceBundle);}

    @FXML
    private JFXButton btnbuscarPedido;


    @Override
    protected Node getPaneReferenceComponent() {
        return btnbuscarPedido;
    }

    public void onVolverClicked(MouseEvent mouseEvent) {
        setContentPane(DistributionController.viewPath);
    }
}
