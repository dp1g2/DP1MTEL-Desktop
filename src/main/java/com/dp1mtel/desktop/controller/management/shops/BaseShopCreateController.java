package com.dp1mtel.desktop.controller.management.shops;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.EmployeeHolder;
import com.dp1mtel.desktop.di.ShopsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.dp1mtel.desktop.util.validation.IdValidator;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

import javax.jws.soap.SOAPBinding;
import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BaseShopCreateController extends GenericBaseController<Shop> {

    public JFXTextField txtDireccion;
    public JFXComboBox<String> cmbDistrito;
    public JFXComboBox<String> cmbEstadoTienda;
    public JFXTextField txtGerente;
    public JFXTextField txtUnidadesReparto;
    public JFXTextField txtCapacidad;
    public JFXTextField searchEmpleado;

    public JFXTreeTableView<User> userTable;
    public JFXTreeTableColumn<User, String> indexColumn;
    public JFXTreeTableColumn<User, String> dniColumn;
    public JFXTreeTableColumn<User, String> empleadoColumn;
    public JFXTreeTableColumn<User, String> cargoColumn;
    public JFXTreeTableColumn<User, CheckBox> accionesColumn;
    public StackPane stackPane;
    public List<User> listContainingAdmin = new ArrayList<>();
    public JFXButton btnSave;

    public StackPane idStackPane;

    public Shop shopModel;
    public ShopsHolder shopHolder = new ShopsHolder();
    public UsersHolder usersHolder = new UsersHolder();
    public ObservableList<User> usersList;


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFieldValidation();
        loadComboBoxDistricts();
        //loadComboBoxStatus();
    }

    public void setupFieldValidation() {
        RequiredFieldValidator requiredValidator = new RequiredFieldValidator();
        requiredValidator.setMessage("Este campo es obligatorio");

        //Address validation
        txtDireccion.setValidators(requiredValidator);
        txtDireccion.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) txtDireccion.validate();
        });

        //N° cars to repart validation
        txtUnidadesReparto.setValidators(requiredValidator);
        txtUnidadesReparto.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) txtUnidadesReparto.validate();
        });
        txtUnidadesReparto.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    txtUnidadesReparto.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        //Capacity of cars to repart validation
        txtCapacidad.setValidators(requiredValidator);
        txtCapacidad.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) txtCapacidad.validate();
        });
        txtCapacidad.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    txtCapacidad.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        cmbEstadoTienda.setItems(FXCollections.observableArrayList(Shop.statusMap.values()));
        //todo: check is pass y confirmpass are equal
    }

    public void loadComboBoxDistricts() {
        shopHolder.getShopsRepository().getDistricts()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (districts) -> Platform.runLater(() -> setupComboBoxDistricts(districts)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    protected void setupComboBoxDistricts(List<Districts> districts) {
         districts.sort((param,param2) -> {
            return param.getName().compareToIgnoreCase(param2.getName());
        });
        for (int i = 0; i < districts.size(); i++) {
            cmbDistrito.getItems().add(districts.get(i).getName());
        }
    }

    public void onSaveMouseClicked(MouseEvent mouseEvent) {
        if (isAllTextFieldsFilled()) {
            if (validateComboBoxes()) {
                saveShop();
                goBack();
            }
        } else {
            /*Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Cuidado");
            alert.setHeaderText("Te faltan llenar campos obligatorios");
            alert.show();*/
            AlertDialog.showMessage(stackPane, "Error al guardar", "Por favor, corregir los errores mostrados.");
        }
    }

    protected boolean isAllTextFieldsFilled() {
        if (txtDireccion.validate() || txtCapacidad.validate() || txtUnidadesReparto.validate())
            return true;
        else {
            AlertDialog.showMessage(stackPane, "Error al guardar", "Por favor, completar los campos requeridos.");
            return false;
        }
    }

    private boolean validateComboBoxes() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        if (cmbEstadoTienda.getSelectionModel().isEmpty()) {
            AlertDialog.showMessage(stackPane, "Error al guardar", "Por favor, seleccionar un estado para tienda.");
            cmbEstadoTienda.requestFocus();
            return false;
        }

        if (cmbDistrito.getSelectionModel().isEmpty()) {
            AlertDialog.showMessage(stackPane, "Error al guardar", "Por favor, seleccionar el distrito de la tienda.");
            cmbDistrito.requestFocus();
            return false;
        }
        return true;
    }

    public void onCancelMouseClicked(MouseEvent mouseEvent) {
        goBack();
    }

    public void setupTableUser(List<User> users) {
        List<User> filteredUsers = users.stream().filter((_users) -> {
            //System.out.println(_users.getShop() == null);
            return _users.getShop() == null;
        }).collect(Collectors.toList());

        usersList = FXCollections.observableArrayList(filteredUsers);

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        dniColumn.setCellValueFactory((param) -> {
            if (dniColumn.validateValue(param)) {
                return param.getValue().getValue().dniProperty();
            } else {
                return dniColumn.getComputedValue(param);
            }
        });

        empleadoColumn.setCellValueFactory((param) -> {
            if (empleadoColumn.validateValue(param)) {
                return param.getValue().getValue().fullNameProperty();
            } else {
                return empleadoColumn.getComputedValue(param);
            }
        });

        cargoColumn.setCellValueFactory((param) -> {
            if (cargoColumn.validateValue(param)) {
                return param.getValue().getValue().getRole().nameProperty();
            } else {
                return cargoColumn.getComputedValue(param);
            }
        });

        accionesColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<User, CheckBox>,
                ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(
                    TreeTableColumn.CellDataFeatures<User, CheckBox> arg0) {

                User user = arg0.getValue().getValue();
                CheckBox checkBox = new JFXCheckBox();
                checkBox.selectedProperty().setValue(user.getSelect());
                checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        user.setSelect(newValue);
                        //validateFields();
                    }
                });
                return new SimpleObjectProperty<CheckBox>(checkBox);
            }
        });

        //validateFields();

        final TreeItem<User> root = new RecursiveTreeItem<>(usersList, RecursiveTreeObject::getChildren);
        userTable.setRoot(root);
        userTable.setShowRoot(false);
        userTable.getColumns().setAll(indexColumn, dniColumn, empleadoColumn, cargoColumn, accionesColumn);

        searchEmpleado.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                userTable.setPredicate(new Predicate<TreeItem<User>>() {
                    @Override
                    public boolean test(TreeItem<User> userTreeItem) {
                        Boolean flag = userTreeItem.getValue().getDni().toLowerCase().contains(newValue.toLowerCase()) ||
                                userTreeItem.getValue().getRole().getName().toLowerCase().contains(newValue.toLowerCase()) ||
                                userTreeItem.getValue().getFullName().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });
    }

    public void setupTableUserEdit(List<User> users, Shop shopAux) {

        List<User> filteredUsers = users.stream().filter((_users) -> {
            //System.out.println(_users.getShop() == null);
            return (_users.getShop() == null || _users.getShop().getId() == shopAux.getId());
        }).filter((_user) -> {
            return (!_user.getDni().equals("admin"));
        }).collect(Collectors.toList());

        usersList = FXCollections.observableArrayList(filteredUsers);

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        dniColumn.setCellValueFactory((param) -> {
            if (dniColumn.validateValue(param)) {
                return param.getValue().getValue().dniProperty();
            } else {
                return dniColumn.getComputedValue(param);
            }
        });

        empleadoColumn.setCellValueFactory((param) -> {
            if (empleadoColumn.validateValue(param)) {
                return param.getValue().getValue().fullNameProperty();
            } else {
                return empleadoColumn.getComputedValue(param);
            }
        });

        cargoColumn.setCellValueFactory((param) -> {
            if (cargoColumn.validateValue(param)) {
                return param.getValue().getValue().getRole().nameProperty();
            } else {
                return cargoColumn.getComputedValue(param);
            }
        });

        accionesColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<User, CheckBox>,
                ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(
                    TreeTableColumn.CellDataFeatures<User, CheckBox> arg0) {

                User user = arg0.getValue().getValue();
                CheckBox checkBox = new JFXCheckBox();
                checkBox.selectedProperty().setValue(user.getSelect());
                checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable,
                                        Boolean oldValue, Boolean newValue) {
                        user.setSelect(newValue);
                        //validateFields();
                    }
                });
                return new SimpleObjectProperty<CheckBox>(checkBox);
            }
        });

        //validateFields();

        final TreeItem<User> root = new RecursiveTreeItem<>(usersList, RecursiveTreeObject::getChildren);
        userTable.setRoot(root);
        userTable.setShowRoot(false);
        userTable.getColumns().setAll(indexColumn, dniColumn, empleadoColumn, cargoColumn, accionesColumn);

        searchEmpleado.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                userTable.setPredicate(new Predicate<TreeItem<User>>() {
                    @Override
                    public boolean test(TreeItem<User> userTreeItem) {
                        Boolean flag = userTreeItem.getValue().getDni().toLowerCase().contains(newValue.toLowerCase()) ||
                                userTreeItem.getValue().getRole().getName().toLowerCase().contains(newValue.toLowerCase()) ||
                                userTreeItem.getValue().getFullName().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });
    }


    /*
    public void validateFields(){
        btnSave.disableProperty().bind(Bindings.createBooleanBinding(
                () ->   txtDireccion.getText().isEmpty() || txtUnidadesReparto.getText().isEmpty() ||
                        txtCapacidad.getText().isEmpty() //|| isNoUsersSelected()
        ));
    }

    private boolean isNoUsersSelected(){
        for (User u: usersList) {
            if(u.getSelect()) return false;
        }
        return true;
    }
    */
    public void saveShop() {
        //Todo: cada controller que herede debe sobreescribir este metodo.
    }

    ;

    public void goBack() {
        setContentPane(ShopIndexController.viewPath);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnSave;
    }
}
