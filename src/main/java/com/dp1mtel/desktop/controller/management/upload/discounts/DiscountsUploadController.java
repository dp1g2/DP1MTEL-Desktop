package com.dp1mtel.desktop.controller.management.upload.discounts;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.management.upload.UploadController;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class DiscountsUploadController extends BaseController{
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/upload/discounts/index.fxml";
    public static final String windowTitle = "Carga masiva de descuentos";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    public void onLoadQuantitiesClicked(MouseEvent mouseEvent) {
    }

    public void onLoadCategoriesClicked(MouseEvent mouseEvent) {
    }

    public void onQuantityClicked(MouseEvent mouseEvent) {
    }

    public void onCategoriesClicked(MouseEvent mouseEvent) {
    }
    public void onBackClicked(MouseEvent mouseEvent) {
        setContentPane(UploadController.viewPath);
    }

}
