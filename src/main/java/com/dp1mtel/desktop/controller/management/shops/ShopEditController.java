package com.dp1mtel.desktop.controller.management.shops;

import com.dp1mtel.desktop.model.Shop;
import com.dp1mtel.desktop.model.User;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ShopEditController extends BaseShopCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/shops/edit.fxml";
    public static final String windowTitle = "Edit Shop";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT = LogManager.getLogger("FileAuditAppender");


    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        try {
            usersHolder.getUsersRepository().getUsers()
                    .subscribeOn(Schedulers.io())
                    .subscribe(_users -> {
                        shopHolder.getShopsRepository().getEmployees(shopModel.getId())
                                .subscribe(_listEmployees -> {
                                    Platform.runLater(() -> setupTableUserEdit(_users, shopModel));
                                    Platform.runLater(() -> updateCheckColumn(_listEmployees));
                                });
                    });
        } catch (Exception e) {
        }
        usersHolder.getUsersRepository().getUsers()
                .subscribeOn(Schedulers.io())
                .subscribe((_users) -> Platform.runLater(() -> {
                            fillListWithAdmin(_users);
                        }),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));

    }

    public void initModel(Shop model) {
        this.shopModel = model;
        txtDireccion.setText(shopModel.getAddress());
        txtUnidadesReparto.setText(shopModel.getNumVehicles());
        txtCapacidad.setText(shopModel.getNumCapacity());
        //cmbEstadoTienda.setValue(shopModel.statusPropertyToString().toString());
        //System.out.println("Estadp de la tienda " + shopModel.getStatus());
        //System.out.println("Estadp de la tienda " + shopModel.statusPropertyToString());
        if (shopModel.getStatus() == true) {
            cmbEstadoTienda.getSelectionModel().select(1);
        } else {
            cmbEstadoTienda.getSelectionModel().select(0);
        }

        //System.out.println("ID de la tienda " + shopModel.getId());
    }

    private void fillListWithAdmin(List<User> repoUsers) {
        listContainingAdmin = repoUsers.stream().filter((_user) -> {
            return (_user.getDni().equals("admin"));
        }).collect(Collectors.toList());
    }

    @Override
    public void loadComboBoxDistricts() {
        shopHolder.getShopsRepository().getDistricts()
                .subscribeOn(Schedulers.io())
                .subscribe((_district) -> Platform.runLater(() -> {
                            setupComboBoxDistricts(_district);
                            setupSelectedValueOnCombox();
                            //updateCheckColumn();
                        }),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    private void setupSelectedValueOnCombox() {
        for (int i = 0; i < cmbDistrito.getItems().size(); i++) {
            if (shopModel.getDistrict().equals(cmbDistrito.getItems().get(i))) {
                cmbDistrito.getSelectionModel().select(i);
                break;
            }
        }
    }

    protected Node getPaneReferenceComponent() {
        return txtDireccion;
    }

    @Override
    public void saveShop() {
        ObservableList<User> usersListAux = FXCollections.observableArrayList();

        shopModel.setAddress(txtDireccion.getText());
        shopModel.setStatusString(cmbEstadoTienda.getValue());
        shopModel.setNumCapacity(txtCapacidad.getText());
        shopModel.setNumVehicles(txtUnidadesReparto.getText());
        shopModel.setDistrict(cmbDistrito.getSelectionModel().getSelectedItem());

        final Boolean[] adminIsThere = {false};
        if (shopModel.getDistrict().equals("Lima")) {
            usersListAux.forEach((user -> {
                if (user.getDni().equals("admin")) {
                    adminIsThere[0] = true;
                }
            }));
            if (!adminIsThere[0]) {
                usersListAux.add(listContainingAdmin.get(0));
            }
        }
        for (User u : usersList) {
            if (u.getSelect()) {
                //shopModel.listemployeeProperty().get().add(u);
                usersListAux.add(u);
            }
        }
        shopModel.setEmployee(usersListAux);

        /*
        System.out.println("Id Tienda: " + shopModel.getId());
        System.out.println("Distrito: " + shopModel.getDistrict());
        System.out.println("Estado: " + shopModel.getStatus());
        System.out.println("Estado del combo box: " + cmbEstadoTienda.getValue());
        System.out.println("Numero de trabajadores: " + shopModel.listemployeeProperty().get().size());
        System.out.println("Número de vehículos: " + shopModel.getNumVehicles());
        System.out.println("Número de capacidad: " + shopModel.getNumCapacity());
        */

        shopHolder.getShopsRepository().updateShopById(shopModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_shop) -> {
                    shopHolder.getShopsRepository().saveEmployees(_shop.getId(), shopModel.getListemployee())
                            .subscribeOn(Schedulers.io())
                            .subscribe((_listEmployees) -> {
                                System.out.println(_listEmployees.size());
//                                _shop.setEmployee(_listEmployees);
                            }, (e) -> {
                                Platform.runLater(() -> System.err.println(e.getMessage()));
                            });
                    User usuario = usersHolder.getUsersRepository().getCurrentUser();
                    usuario.setStore(_shop);
                    String logMensaje = "EDITAR - Tienda '" + shopModel.getId() + " " + shopModel.getDistrict() + "-" + shopModel.getAddress()
                            + "' - Usuario " + usuario.getFullName();
                    LOGGERAUDIT.info(logMensaje);
                    Platform.runLater(() -> goBack());
                }, (e) -> {
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });
    }

    private void updateCheckColumn(List<User> listEmployees) {
        //System.out.println("La cantidad de trabajadores " + listEmployees.size());
        //System.out.println("La cantidad de trabajadores " + usersList);
        for (User u : usersList) {
            for (int i = 0; i < listEmployees.size(); i++) {
                if (listEmployees.get(i).getId() == u.getId())
                    u.setSelect(true);
                //System.out.println("--------" + listEmployees.get(i).getFullName() + " - " + listEmployees.get(i).getSelect() + "----------" + u.getSelect());
            }
        }
    }
}
