package com.dp1mtel.desktop.controller.management.upload.supplies;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.management.upload.UploadController;
import com.dp1mtel.desktop.di.*;
import com.dp1mtel.desktop.model.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.apache.logging.log4j.LogManager;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SupplyUploadController extends BaseController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/upload/supplies/index.fxml";
    public static final String windowTitle = "Carga masiva de insumos";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT = LogManager.getLogger("FileAuditAppender");
    private UsersHolder usersHolder = new UsersHolder();

    public enum SupplyType {CATEGORY, ITEM, PRODUCT, PRODUCT_COMBO}

    private final int INITIAL_ROWS_CSV = 3;
    private List<Category> listCategories;
    private List<Item> listItems;
    private List<Product> listProducts;
    private List<ProductCombo> listCombos;

    private BufferedReader bufferedReader;
    private FileChooser fileChooser = new FileChooser();
    private File file;
    private String line = "";

    private CategoriesHolder categoriesHolder = new CategoriesHolder();
    private ItemsHolder itemsHolder = new ItemsHolder();
    private ProductsHolder productsHolder = new ProductsHolder();
    private ProductComboHolder productComboHolder = new ProductComboHolder();

    @FXML
    private JFXButton btnItems;
    @FXML
    private JFXTextField pathCategories;
    @FXML
    private JFXTextField pathItems;
    @FXML
    private JFXTextField pathProducts;
    @FXML
    private JFXTextField pathCombos;
    @FXML
    private StackPane stackPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnItems;
    }

    public void onCategoriesClicked(MouseEvent mouseEvent) {
        initilizeFileReader(SupplyType.CATEGORY);
    }

    public void onItemsClicked(MouseEvent mouseEvent) {
        initilizeFileReader(SupplyType.ITEM);
    }

    public void onProductsClicked(MouseEvent mouseEvent) {
        initilizeFileReader(SupplyType.PRODUCT);
    }

    public void onComboClicked(MouseEvent mouseEvent) {
        initilizeFileReader(SupplyType.PRODUCT_COMBO);
    }

    public void onBackClicked(MouseEvent mouseEvent) {
        setContentPane(UploadController.viewPath);
    }

    public void onLoadCategoriesClicked(MouseEvent mouseEvent) throws IOException {
        if (!pathCategories.getText().isEmpty()) {
            processFile(SupplyType.CATEGORY);

        } else {
            showErrorMessage("Debe seleccionar un archivo válido para cargar las categorías");
        }
    }

    public void onLoadItemsClicked(MouseEvent mouseEvent) throws IOException {
        if (!pathItems.getText().isEmpty()) {
            processFile(SupplyType.ITEM);

        } else {
            showErrorMessage("Debe seleccionar un archivo válido para cargar los ítems");
        }
    }

    public void onLoadProductsClicked(MouseEvent mouseEvent) throws IOException {
        if (!pathProducts.getText().isEmpty()) {
            processFile(SupplyType.PRODUCT);

        } else {
            showErrorMessage("Debe seleccionar un archivo válido para cargar los productos");
        }
    }

    public void onLoadCombosClicked(MouseEvent mouseEvent) throws IOException {
        if (!pathCombos.getText().isEmpty()) {
            processFile(SupplyType.PRODUCT_COMBO);

        } else {
            showErrorMessage("Debe seleccionar un archivo válido para cargar los combos");
        }
    }

    private void initilizeFileReader(SupplyType supplyType) {
        fileChooser.setTitle("Seleccione el archivo CSV");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        file = fileChooser.showOpenDialog(null);
        try {
            String fileName = file.getAbsolutePath();
            //Verify that the file has a .csv extension
            if (verifyFileExtension(fileName)) {
                switch (supplyType) {
                    case CATEGORY:
                        pathCategories.setText(fileName);
                        break;
                    case ITEM:
                        pathItems.setText(fileName);
                        break;
                    case PRODUCT:
                        pathProducts.setText(fileName);
                        break;
                    case PRODUCT_COMBO:
                        pathCombos.setText(fileName);
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("No se seleccionó archivo...");
        }

    }

    public void processFile(SupplyType supplyType) throws IOException {
        switch (supplyType) {
            case CATEGORY:
                bufferedReader = new BufferedReader(new FileReader(pathCategories.getText()));
                listCategories = new ArrayList<>();
                break;
            case ITEM:
                bufferedReader = new BufferedReader(new FileReader(pathItems.getText()));
                listItems = new ArrayList<>();
                break;
            case PRODUCT:
                bufferedReader = new BufferedReader(new FileReader(pathProducts.getText()));
                listProducts = new ArrayList<>();
                break;
            case PRODUCT_COMBO:
                bufferedReader = new BufferedReader(new FileReader(pathCombos.getText()));
                listCombos = new ArrayList<>();
                break;
        }
        int linesRead = 0;
        while ((line = bufferedReader.readLine()) != null) {
            if (linesRead == 1) {
                if (!validFormatExcelFile(line.split(";"), supplyType)) {
                    closeBufferedReader(bufferedReader);
                    return;
                }
            }
            if (linesRead++ > 1) {
                String[] csvLine = line.split(";");
                readModelAndSaveToDB(csvLine, supplyType);
            }
        }
        saveModelsToDB(supplyType);
    }

    private void readModelAndSaveToDB(String[] line, SupplyType supplyType) {
        switch (supplyType) {
            case CATEGORY:
                Category modelCategory = new Category(line[0], line[1], line[2]);
                listCategories.add(modelCategory);
                break;
            case ITEM:
                Item modelItem = new Item(line[0], line[1], line[2], "true", line[4].equals("SI") ? true : false, Double.parseDouble(line[3]));
                listItems.add(modelItem);
                break;
            case PRODUCT:
                List<ProductHelp> readItemsFromCSV = new ArrayList<>();
                for (int i = 5; i < line.length; i += 2) {
                    if (line[i + 1].equals("") || line[i + 1] == null) line[i + 1] = "1";
                    readItemsFromCSV.add(new ProductHelp(line[i + 1], new Item(line[i])));
                }
                Product modelProduct = new Product(line[0], line[1], line[2], line[3], "true", new Category(line[4]), readItemsFromCSV);
                listProducts.add(modelProduct);
                break;
            case PRODUCT_COMBO:
                List<ProductComboHelp> readProductsFromCSV = new ArrayList<>();
                for (int i = 4; i < line.length; i += 2) {
                    if (line[i + 1].equals("") || line[i + 1] == null) line[i + 1] = "1";
                    readProductsFromCSV.add(new ProductComboHelp(line[i + 1], new Product(line[i], 0)));
                }
                ProductCombo modelProductCombo = new ProductCombo(line[0], line[1], line[2], line[3], "true", readProductsFromCSV);
                listCombos.add(modelProductCombo);
                break;
        }

    }

    private void saveModelsToDB(SupplyType supplyType) {
        switch (supplyType) {
            case CATEGORY:
                categoriesHolder.getCategoriesRepository().saveMassiveCategories(listCategories)
                        .subscribeOn(Schedulers.io())
                        .subscribe((_model) -> {
                                    Platform.runLater(() -> {
                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(), _model.getPermissions(), "categorias");
                                    });
                                    User usuario = usersHolder.getUsersRepository().getCurrentUser();
                                    String logMensaje = "CARGA MASIVA : Categorías de productos" + " - Usuario " + usuario.getFullName();
                                    LOGGERAUDIT.info(logMensaje);
                                },
                                (e) -> {
                                    Platform.runLater(() -> System.err.println(e.getMessage()));
                                });
                break;
            case ITEM:
                itemsHolder.getItemsRepository().saveMassiveItems(listItems).subscribeOn(Schedulers.io())
                        .subscribe((_model) -> {
                                    Platform.runLater(() -> {
                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(), _model.getPermissions(), "items");
                                    });
                                    User usuario = usersHolder.getUsersRepository().getCurrentUser();
                                    String logMensaje = "CARGA MASIVA : Items" + " - Usuario " + usuario.getFullName();
                                    LOGGERAUDIT.info(logMensaje);
                                },
                                (e) -> {
                                    Platform.runLater(() -> System.err.println(e.getMessage()));
                                });
                break;
            case PRODUCT:
                productsHolder.getProductsRepository().saveMassiveProducts(listProducts).subscribeOn(Schedulers.io())
                        .subscribe((_model) -> {
                                    Platform.runLater(() -> {
                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(), _model.getPermissions(), "productos");
                                    });
                                    User usuario = usersHolder.getUsersRepository().getCurrentUser();
                                    String logMensaje = "CARGA MASIVA : Productos" + " - Usuario " + usuario.getFullName();
                                    LOGGERAUDIT.info(logMensaje);
                                },
                                (e) -> {
                                    Platform.runLater(() -> System.err.println(e.getMessage()));
                                });
                break;
            case PRODUCT_COMBO:
                productComboHolder.getProductComboRepository().saveMassiveCombos(listCombos).subscribeOn(Schedulers.io())
                        .subscribe((_model) -> {
                                    Platform.runLater(() -> {
                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(), _model.getPermissions(), "combos");
                                    });
                                    User usuario = usersHolder.getUsersRepository().getCurrentUser();
                                    String logMensaje = "CARGA MASIVA : Combo de productos" + " - Usuario " + usuario.getFullName();
                                    LOGGERAUDIT.info(logMensaje);
                                },
                                (e) -> {
                                    Platform.runLater(() -> System.err.println(e.getMessage()));
                                });
                break;
        }
    }

    private void printMessageAcordingToErrors(String correctlyInserted, String badlyInserted, List<Integer> permissions, String type) {
        int correct = Integer.parseInt(correctlyInserted);
        int incorrect = Integer.parseInt(badlyInserted);
        String shownListOfPositions = "";
        final String[] nestedPositions = {""};
        if (correct > 0 && incorrect == 0) {
            showErrorMessage("Se han insertado correctamente " + correctlyInserted + " " + type);
        } else if (correct >= 0 && incorrect > 0) {
            permissions.forEach((position) -> {
                position += INITIAL_ROWS_CSV;
                nestedPositions[0] += position.toString() + ",";
            });
            shownListOfPositions = nestedPositions[0];
            showErrorMessage("Se han insertado correctamente " + correctlyInserted + type +
                    "No se ha podido cargar " + badlyInserted + " " + type + ". Estas se encuentran en las posiciones " +
                    shownListOfPositions.substring(0, shownListOfPositions.length() - 1));
        } else if (correct == 0 && incorrect == 0) {
            showErrorMessage("No se ha ingresado ningun dato.");
        }
    }

    //Getters by name

    private Observable<Item> getItemByName(String nameQuery) {
        return itemsHolder.getItemsRepository().getItemsByName(nameQuery).map(items -> items.get(0));
    }

    private Observable<Category> getCategoryByName(String nameQuery) {
        return categoriesHolder.getCategoriesRepository().getCategoryByName(nameQuery).map(categories -> categories.get(0));
    }

    //Helpers

    public Boolean verifyFileExtension(String fileName) {
        String fileExtension = ".csv";
        if (!fileName.substring(fileName.length() - 4).equals(fileExtension)) {
            showErrorMessage("El archivo debe tener el formato 'csv' ");
            return false;
        }
        return true;
    }

    private void closeBufferedReader(BufferedReader bufferedReader) {
        //Closes opened file
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validFormatExcelFile(String[] line, SupplyType supplyType) {
        if (line.length == 0) return false;
        switch (supplyType) {
            case CATEGORY:
                if (line.length > 3) {
                    showErrorMessage("El archivo de categorías tiene más de 3 columnas");
                    return false;
                }
                if (!(line[0].toUpperCase().equals("CODIGO")
                        && line[1].toUpperCase().equals("NOMBRE")
                        && line[2].toUpperCase().equals("DESCRIPCION"))) {
                    showErrorMessage("No se tienen las columnas correctas");
                    return false;
                }
                break;
            case ITEM:
                if (line.length > 5) {
                    showErrorMessage("El archivo de ítems tiene más de 5 columnas");
                    return false;
                }
                if (!(line[0].toUpperCase().equals("ID")
                        && line[1].toUpperCase().equals("NOMBRE")
                        && line[2].toUpperCase().equals("DESCRIPCION")
                        && line[3].toUpperCase().equals("VOLUMEN")
                        && line[4].toUpperCase().equals("EXPIRA")
                )) {
                    showErrorMessage("No se tienen las columnas correctas");
                    return false;
                }
                break;
            case PRODUCT:
                if (line.length < 5) {
                    showErrorMessage("El archivo de ítems debe tener como minimo de 7 columnas");
                    return false;
                }
                if (!(line[0].toUpperCase().equals("ID") && line[1].toUpperCase().equals("NOMBRE")
                        && line[2].toUpperCase().equals("DESCRIPCION")
                        && line[3].toUpperCase().equals("PRECIO")
                        && line[4].toUpperCase().equals("CATEGORIA")
                        && line[5].toUpperCase().equals("ITEM"))
                        && line[6].toUpperCase().equals("CANTIDAD ITEM")) {
                    showErrorMessage("No se tienen las columnas correctas");
                    return false;
                }
                break;
            case PRODUCT_COMBO:
                if (line.length < 5) {
                    showErrorMessage("El archivo de ítems debe tener como minimo de 7 columnas");
                    return false;
                }
                if (!(line[0].toUpperCase().equals("ID") && line[1].toUpperCase().equals("NOMBRE")
                        && line[2].toUpperCase().equals("DESCRIPCION")
                        && line[3].toUpperCase().equals("PRECIO")
                        && line[4].toUpperCase().equals("PRODUCTO")
                        && line[5].toUpperCase().equals("CANTIDAD PRODUCTO"))
                        ) {
                    showErrorMessage("No se tienen las columnas correctas");
                    return false;
                }
                break;
        }
        return true;
    }

    private void showErrorMessage(String errorMessage) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Notificación"));
        Text message = new Text(errorMessage);
        message.wrappingWidthProperty().set(345);
        content.setBody(message);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("OK");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        content.setActions(button);
        dialog.show();
    }
}
