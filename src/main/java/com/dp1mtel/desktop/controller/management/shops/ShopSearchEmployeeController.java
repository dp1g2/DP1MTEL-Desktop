package com.dp1mtel.desktop.controller.management.shops;

import com.dp1mtel.desktop.controller.BaseController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class ShopSearchEmployeeController extends BaseController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/shops/searchEmployee.fxml";
    public static final String viewHeaderPath = "/com/dp1mtel/desktop/view/managament/shops/searchHeader.fxml";


    public void initialize(URL url, ResourceBundle resourceBundle){
        super.initialize(url, resourceBundle);


    }

    public void onSeleccionarClicked(MouseEvent mouseEvent) {

        //hay que pasar los datos a la tabla de la vista anterior
    }
}
