package com.dp1mtel.desktop.controller.management;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.management.upload.UploadController;
import com.dp1mtel.desktop.controller.management.shops.ShopIndexController;
import com.dp1mtel.desktop.controller.management.vehicles.VehicleIndexController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class ManagementController extends BaseController {

    @FXML private JFXButton btnManTiendas;
    @FXML private JFXButton btnCargaMasiva;

    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/index.fxml";

    public void initialize(URL url, ResourceBundle resourceBundle){
        super.initialize(url, resourceBundle);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnManTiendas;
    }

    public void onManTiendasMouseClicked(MouseEvent mouseEvent) {
        setContentPane(ShopIndexController.viewPath);
    }

    public void onVehiculosMouseClicked(MouseEvent mouseEvent) {
        setContentPane(VehicleIndexController.viewPath);
    }

    public void onCargaMasivaMouseClicked(MouseEvent mouseEvent) {
        setContentPane(UploadController.viewPath);
    }
}
