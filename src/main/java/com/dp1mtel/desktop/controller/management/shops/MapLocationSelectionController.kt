package com.dp1mtel.desktop.controller.management.shops

import com.dp1mtel.desktop.controller.GenericBaseController
import com.dp1mtel.desktop.model.AlertDialog
import com.dp1mtel.desktop.repository.IUsersRepository
import com.dp1mtel.desktop.service.DispatchService
import com.dp1mtel.desktop.util.extensions.subscribeFx
import com.dp1mtel.desktop.util.toCoordinate
import com.jfoenix.controls.JFXTextField
import com.sothawo.mapjfx.Coordinate
import com.sothawo.mapjfx.MapView
import com.sothawo.mapjfx.Marker
import com.sothawo.mapjfx.event.MapViewEvent
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.event.EventType
import javafx.fxml.FXML
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.layout.StackPane
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import tornadofx.action
import tornadofx.onDoubleClick
import java.awt.event.KeyEvent
import java.net.URL
import java.util.*

class MapLocationSelectionController :
        GenericBaseController<MapLocationSelectionController.Model>(),
        KoinComponent {
    companion object {
        @JvmField val viewPath = "/com/dp1mtel/desktop/view/managament/shops/searchUbication.fxml"
    }

    @FXML private lateinit var resultsMap: MapView
    @FXML private lateinit var btnSave: Button
    @FXML private lateinit var btnCancel: Button
    @FXML private lateinit var storeAddress: JFXTextField
    @FXML private lateinit var btnSearchAddress: FontAwesomeIconView
    @FXML private lateinit var dialogContainer: StackPane

    private val userRepository: IUsersRepository by inject()
    private val dispatchService: DispatchService by inject()
    private var currentUser = userRepository.currentUser!!
    private lateinit var model: Model
    private var startCoords = Coordinate(-12.0652157, -77.0817718)
    private val currentMarker: Marker = Marker.createProvided(Marker.Provided.RED).setVisible(true)

    /**
     * Model
     */
    data class Model(
            val storeAddress: String? = null,
            val callback: MapLocationCallback? = null
    )

    interface MapLocationCallback {
        fun onSelection(address: String, coordinate: Coordinate)
        fun onCancel()
    }

    /**
     * Initialization
     */
    override fun initialize(location: URL?, resourceBundle: ResourceBundle?) {
        super.initialize(location, resourceBundle)
        initView()
        initMap()
     }

    private fun initView() {
        btnSave.action {
            if (storeAddress.text?.isEmpty() == true || currentMarker.position == null) {
                AlertDialog.showMessage(
                        dialogContainer,
                        "Cuidado!",
                        "Debe completar la dirección y la ubicación en el mapa")
                return@action
            }
            model.callback?.onSelection(storeAddress.text, currentMarker.position)
        }
        btnCancel.action { model.callback?.onCancel() }
        btnSearchAddress.onDoubleClick { findAddressCoordinates(storeAddress.text) }
    }

    private fun initMap() {
        resultsMap.addEventHandler(MapViewEvent.MAP_CLICKED) {
            println(currentMarker.position)
            addMapMarker(it.coordinate)
        }
        resultsMap.initialize()
        resultsMap.initializedProperty().addListener { it, oldVal, newVal ->
            if (newVal) {
                resultsMap.center = startCoords
                resultsMap.zoom = 16.0
                storeAddress.setOnKeyPressed { value ->
                    if (value.code == KeyCode.ENTER) {
                        findAddressCoordinates(storeAddress.text)
                    }
                }
            }
        }
    }

    private fun addMapMarker(coordinate: Coordinate) {
        if (currentMarker.visible) {
            val needToAddMarker = null == currentMarker.position
            currentMarker.position = coordinate
            if (needToAddMarker) {
                // adding can only be done after coordinate is set
                resultsMap.addMarker(currentMarker)
            }
            resultsMap.center = coordinate
        }
    }

    private fun findAddressCoordinates(address: String) {
        dispatchService.geocode(address).subscribeFx { coords ->
            if (coords.isEmpty()) {
                AlertDialog.showMessage(
                        dialogContainer,
                        "Error!",
                        "No se encontró la dirección ingresada. Intente nuevamente")
                return@subscribeFx
            }
            println(coords.first())
            addMapMarker(coords.first().toCoordinate())
        }
    }

    override fun initModel(model: Model?) {
        this.model = model!!
        storeAddress.text = this.model.storeAddress
    }

    override fun getPaneReferenceComponent(): Node {
        return resultsMap
    }

}
