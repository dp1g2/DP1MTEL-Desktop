package com.dp1mtel.desktop.controller.management.upload.movements;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.management.upload.UploadController;
import com.dp1mtel.desktop.di.*;
import com.dp1mtel.desktop.model.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class MovementUploadController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/upload/movements/index.fxml";
    public static final String windowTitle = "Carga masiva de movimientos";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;



    @FXML
    private JFXTextField pathIN;
//    @FXML
//    private JFXTextField pathOUT;
    @FXML
    private StackPane stackPane;

    public enum UploadType {IN, OUT}

    private BufferedReader bufferedReader;
    private FileChooser fileChooser = new FileChooser();
    private File file;
    private String line = "";

    private List<InventoryOperation> listIN;
    private List<InventoryOperation> listOUT;

    private List<InventoryOperationType> operationTypes;

    private InventoryOperationsHolder inventoryOperationsHolder = new InventoryOperationsHolder();
    private UsersHolder usersHolder = new UsersHolder();
    public InventoryOperationTypesHolder inventoryOperationTypesHolder = new InventoryOperationTypesHolder();

    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected Node getPaneReferenceComponent() {
        return pathIN;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        loadOperationTypes();
    }

    private void loadOperationTypes() {
        inventoryOperationTypesHolder.getInventoryOperationTypeRepository().getInventoryOperationTypes()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (types) -> Platform.runLater(() -> fillOperationTypeList(types)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    private void fillOperationTypeList(List<InventoryOperationType> types) {
        operationTypes = new ArrayList<>();
        operationTypes.addAll(types);
    }

    public void onBackClicked(MouseEvent mouseEvent) {
        setContentPane(UploadController.viewPath);
    }

//    public void onLoadOUTClicked(MouseEvent mouseEvent) throws IOException, ParseException {
//        if (!pathOUT.getText().isEmpty()) {
//            processFile(UploadType.OUT);
//        } else {
//            showErrorMessage("Debe seleccionar un archivo válido para cargar los movimientos de salida");
//        }
//    }

    public void onLoadINClicked(MouseEvent mouseEvent) throws IOException, ParseException {
        if (!pathIN.getText().isEmpty()) {
            processFile(UploadType.IN);
        } else {
            showErrorMessage("Debe seleccionar un archivo válido para cargar los movimientos de entrada");
        }
    }

//    public void onOUTClicked(MouseEvent mouseEvent) {
//        initiliazeFileReader(UploadType.OUT);
//    }

    public void onINClicked(MouseEvent mouseEvent) {
        initiliazeFileReader(UploadType.IN);
    }


    private void initiliazeFileReader(UploadType uploadType) {
        fileChooser.setTitle("Seleccione el archivo CSV");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        file = fileChooser.showOpenDialog(null);
        try{
            String fileName = file.getAbsolutePath();
            //Verify that the file has a .csv extension
            if (verifyFileExtension(fileName)) {
                switch (uploadType) {
                    case IN:
                        pathIN.setText(fileName);
                        break;
//                case OUT:
//                    pathOUT.setText(fileName);
//                    break;
                }
            }
        }catch (Exception e) {
            System.out.println("No se seleccionó archivo...");
        }

    }
    public void processFile(UploadType uploadType) throws IOException, ParseException {
        switch (uploadType) {
            case IN:
                bufferedReader = new BufferedReader(new FileReader(pathIN.getText()));
                listIN = new ArrayList<>();
                break;
//            case OUT:
//                bufferedReader = new BufferedReader(new FileReader(pathOUT.getText()));
//                listOUT = new ArrayList<>();
//                break;
        }
        int linesRead = 0;
        while ((line = bufferedReader.readLine()) != null) {
            if (linesRead == 1) {
                if (!validFormatExcelFile(line.split(";"), uploadType)) {
                    closeBufferedReader(bufferedReader);
                    return;
                }
            }
            if (linesRead++ > 1) {
                String[] csvLine = line.split(";");
                readModelAndSaveToDB(csvLine, uploadType);
            }
        }
        saveModelsToDB(uploadType);
    }
    private void readModelAndSaveToDB(String[] line, UploadType uploadType) throws ParseException {
        User user = usersHolder.getUsersRepository().getCurrentUser();
        //Todo add shop base on district
        Shop store = new Shop(line[1]);
        switch (uploadType) {
            case IN:
                InventoryOperation operationIN = new InventoryOperation();
                Item itemIN = new Item(line[2]);
                operationIN.setQuantity(Integer.parseInt(line[3]));
                operationIN.setUser(user);
                operationIN.setStore(store);
                operationIN.setItem(itemIN);
                operationIN.setDescription(line[4]);
                operationIN.setDate(new Date());
                operationIN.setLot(new Lot(formatter.parse(line[5])));
                for (int i=0;i<operationTypes.size();i++){
                    if (operationTypes.get(i).getName().equals(line[0])){
                        operationIN.setTypeOperation(operationTypes.get(i));
                        break;
                    }
                }
                listIN.add(operationIN);
                break;
//            case OUT:
//                InventoryOperation operationOUT = new InventoryOperation();
//                Item itemOUT = new Item();
//                itemOUT.setId(line[2]);
//                itemOUT.setQuantity(Integer.parseInt(line[3]));
//                operationOUT.setUser(user);
//                operationOUT.setStore(store);
//                operationOUT.setItem(itemOUT);
//                operationOUT.setDescription(line[4]);
//                operationOUT.setDate(new Date());
//                operationOUT.setLot(new Lot(Long.parseLong(line[5])));
//                for (int i=0;i<operationTypes.size();i++){
//                    if (operationTypes.get(i).getName().equals(line[0])){
//                        operationOUT.setTypeOperation(operationTypes.get(i));
//                        break;
//                    }
//                }
//
//                listOUT.add(operationOUT);
//                break;
        }

    }
    private void saveModelsToDB(UploadType uploadType) {
        switch (uploadType) {
            case IN:
                inventoryOperationsHolder.getInventoryOperationsRepository().saveMassiveOperations(listIN)
                        .subscribeOn(Schedulers.io())
                        .subscribe((_model) -> {
                                    Platform.runLater(() -> {
                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(), _model.getPermissions(), "movimientos de entrada");
                                    });
                                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                    String logMensaje="CARGA MASIVA : Movimientos de almacén"+" - Usuario "+ usuario.getFullName();
                                    LOGGERAUDIT.info(logMensaje);

                                },
                                (e) -> {
                                    Platform.runLater(() -> System.err.println(e.getMessage()));
                                });
                break;
//            case OUT:
//                inventoryOperationsHolder.getInventoryOperationsRepository().saveMassiveOperations(listOUT)
//                        .subscribeOn(Schedulers.io())
//                        .subscribe((_model) -> {
//                                    Platform.runLater(() -> {
//                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(), _model.getPermissions(), "movimientos de salida");
//                                    });
//                                },
//                                (e) -> {
//                                    Platform.runLater(() -> System.err.println(e.getMessage()));
//                                });
//                break;
        }
    }
    private void printMessageAcordingToErrors(String correctlyInserted, String badlyInserted, List<Integer> permissions, String type) {
        int correct = Integer.parseInt(correctlyInserted);
        int incorrect = Integer.parseInt(badlyInserted);
        String shownListOfPositions = "";
        final String[] nestedPositions = {""};
        if (correct > 0 && incorrect == 0) {
            showErrorMessage("Se han insertado correctamente " + correctlyInserted + " " + type);
        } else if (correct >= 0 && incorrect > 0) {
            permissions.forEach((position) -> {
                position += 3;
                nestedPositions[0] += position.toString() + ",";
            });
            shownListOfPositions = nestedPositions[0];
            showErrorMessage("Se han insertado correctamente " + correctlyInserted + " " +type +
                    "No se ha podido cargar " + badlyInserted + " " + type + ". Estas se encuentran en las posiciones " +
                    shownListOfPositions.substring(0, shownListOfPositions.length() - 1));
        } else if (correct == 0 && incorrect == 0) {
            showErrorMessage("No se ha ingresado ningun dato.");
        }
    }

    // Helpers


    public Boolean verifyFileExtension(String fileName) {
        String fileExtension = ".csv";
        if (!fileName.substring(fileName.length() - 4).equals(fileExtension)) {
            showErrorMessage("El archivo debe tener el formato 'csv' ");
            return false;
        }
        return true;
    }

    private void closeBufferedReader(BufferedReader bufferedReader) {
        //Closes opened file
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validFormatExcelFile(String[] line, UploadType uploadType) {
        if (line.length == 0) return false;
        switch (uploadType) {
            case IN:
                if (line.length < 5) {
                    showErrorMessage("El archivo de movimientos de entrada debe tener más de 5 columnas");
                    return false;
                }
                if (!(line[0].toUpperCase().equals("TIPO MOVIMIENTO")
                        && line[1].toUpperCase().equals("TIENDA")
                        && line[2].toUpperCase().equals("ITEM"))
                        && line[3].toUpperCase().equals("CANTIDAD")
                        && line[4].toUpperCase().equals("DESCRIPCION")
                        && line[5].toUpperCase().equals("FECHA EXPIRACION")
                        ) {
                    showErrorMessage("No se tienen las columnas correctas");
                    return false;
                }
                break;
//            case OUT:
//                if (line.length < 5) {
//                    showErrorMessage("El archivo de movimientos de salida debe tener más de 5 columnas");
//                    return false;
//                }
//                if (!(line[0].toUpperCase().equals("TIPO MOVIMIENTO")
//                        && line[1].toUpperCase().equals("TIENDA")
//                        && line[2].toUpperCase().equals("ITEM"))
//                        && line[3].toUpperCase().equals("CANTIDAD")
//                        && line[4].toUpperCase().equals("DESCRIPCION")
//                        && line[5].toUpperCase().equals("ID LOTE")
//                        ) {
//                    showErrorMessage("No se tienen las columnas correctas");
//                    return false;
//                }
//                break;
        }
        return true;
    }

    private void showErrorMessage(String errorMessage) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Notificación"));
        Text message = new Text(errorMessage);
        message.wrappingWidthProperty().set(345);
        content.setBody(message);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("OK");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        content.setActions(button);
        dialog.show();
    }

}
