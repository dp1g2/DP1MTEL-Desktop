package com.dp1mtel.desktop.controller.management.upload.orders;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.management.upload.UploadController;
import com.dp1mtel.desktop.di.OrdersHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.apache.logging.log4j.LogManager;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class OrderUploadController extends BaseController {

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT = LogManager.getLogger("FileAuditAppender");

    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/upload/orders/index.fxml";
    public static final String windowTitle = "Carga masiva de pedidos";

    public enum UploadType {ORDERS}

    private BufferedReader bufferedReader;
    private FileChooser fileChooser = new FileChooser();
    private File file;
    private String line = "";

    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    public UsersHolder usersHolder = new UsersHolder();
    public OrdersHolder ordersHolder = new OrdersHolder();
    private List<Order> listOrders;

    @FXML
    private JFXTextField pathOrders;
    @FXML
    private StackPane stackPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    public void onOrdersClicked(MouseEvent mouseEvent) {
        initiliazeFileReader(UploadType.ORDERS);
    }

    public void onLoadOrdersClicked(MouseEvent mouseEvent) throws IOException, ParseException {
        if (!pathOrders.getText().isEmpty()) {
            processFile(UploadType.ORDERS);
        } else {
            showErrorMessage("Debe seleccionar un archivo válido para cargar los pedidos");
        }
    }

    public void onBackClicked(MouseEvent mouseEvent) {
        setContentPane(UploadController.viewPath);
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return pathOrders;
    }

    private void initiliazeFileReader(UploadType uploadType) {
        fileChooser.setTitle("Seleccione el archivo CSV");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        file = fileChooser.showOpenDialog(null);
        try {
            String fileName = file.getAbsolutePath();
            //Verify that the file has a .csv extension
            if (verifyFileExtension(fileName)) {
                switch (uploadType) {
                    case ORDERS:
                        pathOrders.setText(fileName);
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("No se seleccionó archivo...");
        }

    }

    public void processFile(UploadType uploadType) throws IOException, ParseException {
        switch (uploadType) {
            case ORDERS:
                bufferedReader = new BufferedReader(new FileReader(pathOrders.getText()));
                listOrders = new ArrayList<>();
                break;
        }
        int linesRead = 0;
        while ((line = bufferedReader.readLine()) != null) {
            if (linesRead == 1) {
                if (!validFormatExcelFile(line.split(";"), uploadType)) {
                    closeBufferedReader(bufferedReader);
                    return;
                }
            }
            if (linesRead++ > 1) {
                String[] csvLine = line.split(";");
                readModelAndSaveToDB(csvLine, uploadType);
            }
        }
        saveModelsToDB(uploadType);
    }

    private void readModelAndSaveToDB(String[] line, UploadType uploadType) throws ParseException {
        switch (uploadType) {
            case ORDERS:

                Customer customer = new Customer(0, "firstName", "lastName",
                        "phone", line[0], new ArrayList<>());

                Role randomRole = new Role("name", "descripcion", new ArrayList<>());
                User seller = new User("firstName", "lastName", line[1],
                        "email", new Date(), "Masculino", randomRole);
                String slug = "";
                switch (line[2]) {
                    case "RESERVADO":
                        slug = "RESERVED";
                        break;
                    case "PAGADO":
                        slug = "PAID";
                        break;
                    case "CANCELADO":
                        slug = "CANCELED";
                        break;
                    case "ENVIADO":
                        slug = "SENT";
                        break;
                    case "NO COMPLETADO":
                        slug = "NON_COMPLETED";
                        break;
                    case "COMPLETADO":
                        slug = "COMPLETED";
                        break;
                }

                OrderStatus orderStatus = new OrderStatus(1L, line[2], "description", slug);


                Shop shop = new Shop(0, "address", line[3], "0", "0", true);

                if (line[4].isEmpty()) line[4] = "0"; //Direccion del usuario
                // 5 y 6 son para las coordenadas x & y
                if (line[7].isEmpty()) line[7] = "01/01/1900"; //Fecha de entrega
                if (line[8].isEmpty()) line[8] = "0"; //Hora minima
                if (line[9].isEmpty()) line[9] = "0"; //Hora maxima
                System.out.println(line[5] + " " + line[6]);
                UserAddress userAddress = new UserAddress(line[4], "name",
                        new Point2D.Double(Double.parseDouble(line[5]), Double.parseDouble(line[6])));
                List<OrderDetail> listOrderDetails = new ArrayList<>();

                for (int i = 10; i < line.length; i += 2) {
                    OrderDetail orderDetail = new OrderDetail();
                    if (line[i].substring(0, 3).equals("PRO")) orderDetail.setProduct(new Product(line[i], 0));
                    else if (line[i].substring(0, 3).equals("COM")) orderDetail.setCombo(new ProductCombo(line[i]));
                    orderDetail.setQuantity(Integer.parseInt(line[i + 1]));
                    orderDetail.setUnitPrice(Double.parseDouble("0")); //No sirven
                    orderDetail.setDiscount(Double.parseDouble("0")); //No sirven
                    listOrderDetails.add(orderDetail);
                }

                Order newOrder = new Order(0, seller, customer, orderStatus, shop, listOrderDetails, 0, userAddress,
                        formatter.parse(line[7]), Double.parseDouble(line[8]), Double.parseDouble(line[9]), 0);
                listOrders.add(newOrder);
                break;
        }

    }


    private void saveModelsToDB(UploadType uploadType) {
        switch (uploadType) {
            case ORDERS:
                ordersHolder.getOrderRepository().saveMassiveOrders(listOrders)
                        .subscribeOn(Schedulers.io())
                        .subscribe((_model) -> {
                                    Platform.runLater(() -> {
                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(),
                                                _model.getPermissions(), "pedidos");
                                    });
                                    User usuario = usersHolder.getUsersRepository().getCurrentUser();
                                    String logMensaje = "CARGA MASIVA : Pedidos" + " - Usuario " + usuario.getFullName();
                                    LOGGERAUDIT.info(logMensaje);
                                },
                                (e) -> {
                                    Platform.runLater(() -> System.err.println("THIS IS ERROR" + e.getMessage()));
                                });
                break;
        }
    }

    private void printMessageAcordingToErrors(String correctlyInserted, String badlyInserted, List<Integer> permissions, String type) {
        int correct = Integer.parseInt(correctlyInserted);
        int incorrect = Integer.parseInt(badlyInserted);
        String shownListOfPositions = "";
        final String[] nestedPositions = {""};
        if (correct > 0 && incorrect == 0) {
            showErrorMessage("Se han insertado correctamente " + correctlyInserted + " " + type);
        } else if (correct >= 0 && incorrect > 0) {
            permissions.forEach((position) -> {
                position += 3;
                nestedPositions[0] += position.toString() + ",";
            });
            shownListOfPositions = nestedPositions[0];
            showErrorMessage("Se han insertado correctamente " + correctlyInserted + " " + type +
                    "No se ha podido cargar " + badlyInserted + " " + type + ". Estas se encuentran en las posiciones " +
                    shownListOfPositions.substring(0, shownListOfPositions.length() - 1));
        } else if (correct == 0 && incorrect == 0) {
            showErrorMessage("No se ha ingresado ningun dato.");
        }
    }

    // Helpers


    public Boolean verifyFileExtension(String fileName) {
        String fileExtension = ".csv";
        if (!fileName.substring(fileName.length() - 4).equals(fileExtension)) {
            showErrorMessage("El archivo debe tener el formato 'csv' ");
            return false;
        }
        return true;
    }

    private void closeBufferedReader(BufferedReader bufferedReader) {
        //Closes opened file
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validFormatExcelFile(String[] line, UploadType uploadType) {
        if (line.length == 0) return false;
        switch (uploadType) {
            case ORDERS:
                if (line.length < 7) {
                    showErrorMessage("El archivo de usuarios debe tener más de 6 columnas");
                    return false;
                }
                if (!(line[0].toUpperCase().equals("DNI CLIENTE")
                        && line[1].toUpperCase().equals("DNI VENDEDOR")
                        && line[2].toUpperCase().equals("ESTADO PEDIDO")
                        && line[3].toUpperCase().equals("TIENDA ORIGEN"))) {
                    showErrorMessage("No se tienen las columnas correctas");
                    return false;
                }
                break;
        }
        return true;
    }

    private void showErrorMessage(String errorMessage) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Notificación"));
        Text message = new Text(errorMessage);
        message.wrappingWidthProperty().set(345);
        content.setBody(message);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("OK");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        content.setActions(button);
        dialog.show();
    }


}
