package com.dp1mtel.desktop.controller.management.vehicles;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.controller.management.ManagementController;
import com.dp1mtel.desktop.di.ShopsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Districts;
import com.dp1mtel.desktop.model.Shop;
import com.dp1mtel.desktop.model.User;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class VehicleIndexController extends GenericBaseController<Shop>  {
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/vehicles/vehiclesEdit.fxml";
    public static final String windowTitle = "Edit Vehicle";

    public ShopsHolder shopHolder = new ShopsHolder();
    public JFXComboBox<String> cmbDistrito;
    public JFXTextField txtCantidadAct;
    public JFXRadioButton rbAgregar;
    public JFXRadioButton rbRemover;
    public JFXTextField txtCantidad;
    public Shop shopModel;

    public StackPane stackPane;

    public JFXButton btnAceptar;
    public JFXButton btnCancelar;

    public ObservableList<Shop> shopsList;

    private UsersHolder usersHolder = new UsersHolder();
    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFieldValidation();
        setupRadioButtons();
        try{
            shopHolder.getShopsRepository().getShops()
                    .subscribeOn(Schedulers.io())
                    .subscribe(_stores ->{
                        Platform.runLater(() -> System.err.println("se cargó las tiendas"));
                        Platform.runLater(() -> shopsList = FXCollections.observableArrayList(_stores));
                        Platform.runLater(() -> loadComboBoxDistricts());
                    });

        }catch (Exception e){};
    }

    public void setupFieldValidation(){
        RequiredFieldValidator requiredValidator = new RequiredFieldValidator();
        requiredValidator.setMessage("Este campo es obligatorio");

        //Quantity validation
        txtCantidad.setValidators(requiredValidator);
        txtCantidad.focusedProperty().addListener((observable,oldValue,newValue)->{
            if(!newValue) txtCantidad.validate();
        });
        txtCantidad.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue){
                if(!newValue.matches("\\d*")){
                    txtCantidad.setText(newValue.replaceAll("[^\\d]",""));
                }
            }
        });
    }

    public void setupRadioButtons(){
        ToggleGroup  group = new ToggleGroup();
        rbAgregar.setToggleGroup(group);
        rbRemover.setToggleGroup(group);
        rbAgregar.setSelected(true);
    }

    public void loadComboBoxDistricts() {
        shopHolder.getShopsRepository().getShops()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (_stores) -> Platform.runLater(() -> setupComboBoxDistricts(_stores)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    protected void setupComboBoxDistricts(List<Shop> stores) {
        for (int i = 0 ; i<stores.size();i++){
            cmbDistrito.getItems().add(stores.get(i).getDistrict() + "/" + stores.get(i).getAddress());
        }
        cmbDistrito.setOnAction(e->{
            for (Shop s : shopsList){
                //System.out.println("Edición de vehículos: " + "stores.district" + s.getDistrict() + " cmbDistrito: " + cmbDistrito.getValue());
                String distAddr = new String(s.getDistrict()+"/"+s.getAddress());
                if (distAddr.equals(cmbDistrito.getValue())){
                    txtCantidadAct.setText(s.getNumVehicles());
                    shopModel = s;
                    break;
                }
            }
        });
    }

    public void onSaveMouseClicked(){
        if(isAllTextFieldsFilled()){
            if(validateQuantity()){
                saveUpdateVehicule();
                goBack();
            }
        }else{
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, corregir los errores mostrados.");
        }
    }

    private boolean isAllTextFieldsFilled(){
        if (!cmbDistrito.getSelectionModel().isEmpty() || txtCantidad.validate())
            return true;
        else{
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar el local de la tienda.");
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar una cantidad.");
            return false;
        }
    }

    private boolean validateQuantity() {
        if (cmbDistrito.getSelectionModel().isEmpty()) {
            cmbDistrito.requestFocus();
            return false;
        }

        if (!txtCantidad.validate()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar una cantidad.");
            return false;
        }


        if (rbRemover.isSelected() && (Integer.parseInt(shopModel.getNumVehicles()) < Integer.parseInt(txtCantidad.getText()))){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, corregir la cantidad ingresada.");
            txtCantidad.requestFocus();
            return false;
        }
        return true;
    }


    public void saveUpdateVehicule(){
        if(rbAgregar.isSelected()){
            shopModel.setNumVehicles(Integer.toString(Integer.parseInt(shopModel.getNumVehicles()) + Integer.parseInt(txtCantidad.getText())));
        }else{
            shopModel.setNumVehicles(Integer.toString(Integer.parseInt(shopModel.getNumVehicles()) - Integer.parseInt(txtCantidad.getText())));
        }
        shopHolder.getShopsRepository().updateShopById(shopModel)
                .subscribeOn(Schedulers.io())
                .subscribe(_store -> {
                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                    String logMensaje="MODIFICAR - Cant. vehiculos en '"+shopModel.getDistrict()+"/"+shopModel.getAddress()+"' - Usuario "+ usuario.getFullName();
                    LOGGERAUDIT.info(logMensaje);
                    Platform.runLater(() -> System.out.println("Tienda: " + _store.getId() + "/" + "Núm de vehículos: " + _store.getNumVehicles()));
                });

    }

    public void goBack(){setContentPane(ManagementController.viewPath);}

    @Override
    protected Node getPaneReferenceComponent() {
        return btnAceptar;
    }

    public void onCancelClicked(){goBack();}
}
