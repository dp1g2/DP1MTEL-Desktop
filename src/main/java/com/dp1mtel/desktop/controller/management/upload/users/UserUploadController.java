package com.dp1mtel.desktop.controller.management.upload.users;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.management.upload.UploadController;
import com.dp1mtel.desktop.di.CustomersHolder;
import com.dp1mtel.desktop.di.PermissionsHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.apache.logging.log4j.LogManager;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class UserUploadController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/upload/users/index.fxml";
    public static final String windowTitle = "Carga masiva de usuarios";


    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;

    public enum UploadType {ROLES, USERS}

    @FXML
    private JFXButton btnRoles;
    @FXML
    private JFXTextField pathRoles;
    @FXML
    private JFXTextField pathUsers;
    @FXML
    private StackPane stackPane;

    private BufferedReader bufferedReader;
    private FileChooser fileChooser = new FileChooser();
    private File file;
    private String line = "";

    private List<Permission> listPermissions;
    private List<Customer> listCustomers;
    private List<User> listUsers;

    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    public PermissionsHolder permissionsHolder = new PermissionsHolder();
    public CustomersHolder customersHolder = new CustomersHolder();
    public UsersHolder usersHolder = new UsersHolder();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        permissionsHolder.getPermissionsRepository().getPermissions()
                .subscribeOn(Schedulers.io())
                .subscribe(_permissions -> {
                    Platform.runLater(() -> initiliazePermissionList(_permissions));
                });
    }

    private void initiliazePermissionList(List<Permission> permissions) {
        listPermissions = new ArrayList<>();
        listPermissions = permissions;
        System.out.println("Permissions added successfully");
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return pathUsers;
    }

    public void onLoadUsersClicked(MouseEvent mouseEvent) throws Exception {
        if (!pathUsers.getText().isEmpty()) {
            processFile(UploadType.USERS);

            //String logMensaje="CARGA MASIVA : Usuarios '"+" - Usuario "+ usuario.getFullName();
            //LOGGERAUDIT.info(logMensaje);
        } else {
            showErrorMessage("Debe seleccionar un archivo válido para cargar los usuarios");
        }
    }

    public void onLoadRolesClicked(MouseEvent mouseEvent) throws Exception {
        if (!pathRoles.getText().isEmpty()) {
            processFile(UploadType.ROLES);
        } else {
            showErrorMessage("Debe seleccionar un archivo válido para cargar los clientes");
        }
    }

    public void onUsersClicked(MouseEvent mouseEvent) {
        initiliazeFileReader(UploadType.USERS);
    }

    public void onRolesClicked(MouseEvent mouseEvent) {
        initiliazeFileReader(UploadType.ROLES);
    }

    private void initiliazeFileReader(UploadType uploadType) {
        fileChooser.setTitle("Seleccione el archivo CSV");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        file = fileChooser.showOpenDialog(null);
        try{
            String fileName = file.getAbsolutePath();
            //Verify that the file has a .csv extension
            if (verifyFileExtension(fileName)) {
                switch (uploadType) {
                    case ROLES:
                        pathRoles.setText(fileName);
                        break;
                    case USERS:
                        pathUsers.setText(fileName);
                        break;
                }
            }
        }
        catch (Exception e){
            System.out.println("No se seleccionó archivo...");
        }

    }

    public void processFile(UploadType uploadType) throws IOException, ParseException {
        switch (uploadType) {
            case ROLES:
                bufferedReader = new BufferedReader(new FileReader(pathRoles.getText()));
                listCustomers = new ArrayList<>();
                break;
            case USERS:
                bufferedReader = new BufferedReader(new FileReader(pathUsers.getText()));
                listUsers = new ArrayList<>();
                break;
        }
        int linesRead = 0;
        while ((line = bufferedReader.readLine()) != null) {
            if (linesRead == 1) {
                if (!validFormatExcelFile(line.split(";"), uploadType)) {
                    closeBufferedReader(bufferedReader);
                    return;
                }
            }
            if (linesRead++ > 1) {
                String[] csvLine = line.split(";");
                readModelAndSaveToDB(csvLine, uploadType);
            }
        }
        saveModelsToDB(uploadType);
    }

    private void readModelAndSaveToDB(String[] line, UploadType uploadType) throws ParseException {
        switch (uploadType) {
            case ROLES:
                List<UserAddress> listAddresses = new ArrayList<>();
                listAddresses.add(new UserAddress(line[8], line[8],
                        new Point2D.Double(Double.parseDouble(line[9]), Double.parseDouble(line[10]))));
                Customer modelCustomer =
                        new Customer(0, line[0], line[1], line[11], line[2], listAddresses);
                listCustomers.add(modelCustomer);
                break;
            case USERS:
                User modelUser = new User(new Shop(line[7]),line[0], line[1], line[2], line[3], formatter.parse(line[4]), line[5],
                        new Role(line[6], "descripcion", new ArrayList<>()));
                listUsers.add(modelUser);
                break;
        }

    }


    private void saveModelsToDB(UploadType uploadType) {
        switch (uploadType) {
            case ROLES:
                customersHolder.getCustomersRepository().saveMassiveCustomers(listCustomers).subscribeOn(Schedulers.io())
                        .subscribe((_model) -> {
                                    Platform.runLater(() -> {
                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(), _model.getPermissions(), "clientes");
                                    });
                                },
                                (e) -> {
                                    Platform.runLater(() -> System.err.println(e.getMessage()));
                                });
                break;
            case USERS:
                usersHolder.getUsersRepository().saveMassiveUsers(listUsers).subscribeOn(Schedulers.io())
                        .subscribe((_model) -> {
                                    Platform.runLater(() -> {
                                        printMessageAcordingToErrors(_model.getCorrectlyInserted(), _model.getBadlyInserted(), _model.getPermissions(), "usuarios");
                                    });
                                },
                                (e) -> {
                                    Platform.runLater(() -> System.err.println(e.getMessage()));
                                });
                break;
        }
    }

    private void printMessageAcordingToErrors(String correctlyInserted, String badlyInserted, List<Integer> permissions, String type) {
        int correct = Integer.parseInt(correctlyInserted);
        int incorrect = Integer.parseInt(badlyInserted);
        String shownListOfPositions = "";
        final String[] nestedPositions = {""};
        if (correct > 0 && incorrect == 0) {
            showErrorMessage("Se han insertado correctamente " + correctlyInserted + " " + type);
        } else if (correct >= 0 && incorrect > 0) {
            permissions.forEach((position) -> {
                position += 3;
                nestedPositions[0] += position.toString() + ",";
            });
            shownListOfPositions = nestedPositions[0];
            showErrorMessage("Se han insertado correctamente " + correctlyInserted + " " + type + "." +
                    "No se ha podido cargar " + badlyInserted + " " + type + ". Estas se encuentran en las posiciones " +
                    shownListOfPositions.substring(0, shownListOfPositions.length() - 1));
        } else if (correct == 0 && incorrect == 0) {
            showErrorMessage("No se ha ingresado ningun dato.");
        }
    }

    // Helpers


    public Boolean verifyFileExtension(String fileName) {
        String fileExtension = ".csv";
        if (!fileName.substring(fileName.length() - 4).equals(fileExtension)) {
            showErrorMessage("El archivo debe tener el formato 'csv' ");
            return false;
        }
        return true;
    }

    private void closeBufferedReader(BufferedReader bufferedReader) {
        //Closes opened file
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validFormatExcelFile(String[] line, UploadType uploadType) {
        if (line.length == 0) return false;
        switch (uploadType) {
            case ROLES:
                if (line.length < 10) {
                    showErrorMessage("El archivo de roles debe tener más de 10 columnas");
                    return false;
                }
                if (!(line[0].toUpperCase().equals("NOMBRES")
                        && line[1].toUpperCase().equals("APELLIDOS")
                        && line[2].toUpperCase().equals("DNI")
                        && line[3].toUpperCase().equals("EMAIL")
                        && line[4].toUpperCase().equals("FECHA NACIMIENTO")
                        && line[5].toUpperCase().equals("SEXO")
                        && line[6].toUpperCase().equals("ROL")
                        && line[7].toUpperCase().equals("TIENDA")
                        && line[8].toUpperCase().equals("DIRECCION")
                        && line[9].toUpperCase().equals("LATITUD")
                        && line[10].toUpperCase().equals("LONGITUD")
                        && line[11].toUpperCase().equals("TELEFONO")
                )) {
                    showErrorMessage("No se tienen las columnas correctas");
                    return false;
                }
                break;
            case USERS:
                if (line.length < 7) {
                    showErrorMessage("El archivo de usuarios debe tener más de 6 columnas");
                    return false;
                }
                if (!(line[0].toUpperCase().equals("NOMBRES") && line[1].toUpperCase().equals("APELLIDOS")
                        && line[2].toUpperCase().equals("DNI")
                        && line[3].toUpperCase().equals("EMAIL")
                        && line[4].toUpperCase().equals("FECHA NACIMIENTO")
                        && line[5].toUpperCase().equals("SEXO")
                        && line[6].toUpperCase().equals("ROL"))) {
                    showErrorMessage("No se tienen las columnas correctas");
                    return false;
                }
                break;
        }
        return true;
    }

    private void showErrorMessage(String errorMessage) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("Notificación"));
        Text message = new Text(errorMessage);
        message.wrappingWidthProperty().set(345);
        content.setBody(message);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("OK");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        content.setActions(button);
        dialog.show();
    }

    public void onBackClicked(MouseEvent mouseEvent) {
        setContentPane(UploadController.viewPath);
    }
}
