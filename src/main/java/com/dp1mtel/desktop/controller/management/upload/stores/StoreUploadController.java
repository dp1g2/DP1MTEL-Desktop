package com.dp1mtel.desktop.controller.management.upload.stores;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.management.upload.UploadController;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class StoreUploadController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/upload/stores/index.fxml";
    public static final String windowTitle = "Carga masiva de tiendas";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }


    public void onLoadStoresClicked(MouseEvent mouseEvent) {
    }

    public void onStoresClicked(MouseEvent mouseEvent) {
    }


    public void onBackClicked(MouseEvent mouseEvent) {
        setContentPane(UploadController.viewPath);
    }
}
