package com.dp1mtel.desktop.controller.management.upload;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.management.upload.discounts.DiscountsUploadController;
import com.dp1mtel.desktop.controller.management.upload.movements.MovementUploadController;
import com.dp1mtel.desktop.controller.management.upload.orders.OrderUploadController;
import com.dp1mtel.desktop.controller.management.upload.stores.StoreUploadController;
import com.dp1mtel.desktop.controller.management.upload.supplies.SupplyUploadController;
import com.dp1mtel.desktop.controller.management.upload.users.UserUploadController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class UploadController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/upload/index.fxml";

    @FXML
    private JFXButton btnSupplies;

    public void initialize(URL url, ResourceBundle resourceBundle){
        super.initialize(url, resourceBundle);
    }
    @Override
    protected Node getPaneReferenceComponent() {
        return btnSupplies;
    }

    public void onSuppliesClicked(MouseEvent mouseEvent) {
        setContentPane(SupplyUploadController.viewPath);
    }

    public void onUsersClicked(MouseEvent mouseEvent) {
        setContentPane(UserUploadController.viewPath);
    }

    public void onStoresClicked(MouseEvent mouseEvent) {
        setContentPane(StoreUploadController.viewPath);
    }

    public void onDiscountsClicked(MouseEvent mouseEvent) {
        setContentPane(DiscountsUploadController.viewPath);
    }

    public void onOrdersClicked(MouseEvent mouseEvent) {
        setContentPane(OrderUploadController.viewPath);
    }

    public void onMovementsClicked(MouseEvent mouseEvent) {
        setContentPane(MovementUploadController.viewPath);
    }
}
