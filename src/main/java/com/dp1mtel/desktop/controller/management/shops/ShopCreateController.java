package com.dp1mtel.desktop.controller.management.shops;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.model.Shop;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.GeoUtilsKt;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.sothawo.mapjfx.Coordinate;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShopCreateController
        extends BaseShopCreateController
        implements MapLocationSelectionController.MapLocationCallback {
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/shops/create.fxml";

    @FXML private JFXButton btnAgregarEmpleado;
    @FXML private StackPane dialogContainer;
    @FXML private JFXButton btnObtUbicacion;
    private JFXDialog mapDialog;


    private Coordinate storeCoordinates;


    public void initialize(URL url, ResourceBundle resourceBundle){
        super.initialize(url, resourceBundle);
        try{
            usersHolder.getUsersRepository().getUsers()
                    .subscribeOn(Schedulers.io())
                    .subscribe(_users -> {
                        Platform.runLater(() -> setupTableUser(_users));
                    });
        }catch (Exception e){
            System.err.println("ERROR EN CREAR TIENDA TIENDA" + e.getMessage());
        }
        //btnSave.disableProperty().setValue(true);
    }

    public void saveShop(){
        shopModel = new Shop(txtDireccion.getText(),
                            cmbDistrito.getValue(),
                            cmbEstadoTienda.getValue(),
                            "0",
                            txtUnidadesReparto.getText(),
                            txtCapacidad.getText(),
                            new ArrayList<>());
        shopModel.setCoordinates(GeoUtilsKt.toPoint2D(storeCoordinates));

        for (User u : usersList){
            if(u.getSelect()){
                shopModel.listemployeeProperty().get().add(u);

            }
        }

        //Shop shop = new Shop("","San Miguel","Av. La Marina 123","0","0",true, new ArrayList<>());
        // para que trabaje en un hilo por separado, no en el principal.
        shopHolder.getShopsRepository().saveShop(shopModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_shop) -> {
                    //Platform.runLater(() -> System.err.println(_shop.getAddress()));

                    shopHolder.getShopsRepository().saveEmployees(_shop.getId(),shopModel.getListemployee())
                            .subscribeOn(Schedulers.io())
                            .subscribe((listEmployees)->{
                                System.out.println(listEmployees.size());
                                //Platform.runLater(() -> System.err.println(listEmployees.size()));
                                //Platform.runLater(() -> goBack());
                            }, (e)->{
                                Platform.runLater(()-> System.err.println(e.getMessage()));
                            });

                    User usuario= usersHolder.getUsersRepository().getCurrentUser();

                    Platform.runLater(() -> goBack());
                }, (e) -> {
                    Platform.runLater(()-> System.err.println(e.getMessage()));
                });
    }

    public void onAgregarEmpleadoMouseClicked(MouseEvent mouseEvent) {

        // ver como invocar como un modal y pasar la data que se almacenaria en la tabla.


        try{
            JFXDialogLayout content = new JFXDialogLayout();
            Pane header = FXMLLoader.load(getClass().getResource(ShopSearchEmployeeController.viewHeaderPath));
            content.setHeading(header);
            Pane pane= FXMLLoader.load(getClass().getResource(ShopSearchEmployeeController.viewPath));
            content.setBody(pane);

            JFXDialog dialog = new JFXDialog(dialogContainer,content,JFXDialog.DialogTransition.TOP);

            JFXButton aceptar =  new JFXButton("Aceptar");
            JFXButton cancelar =  new JFXButton("Cancelar");

            aceptar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();

                }
            });

            cancelar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();

                }
            });

            content.setActions(cancelar, aceptar);
            dialog.show();
        }catch(Exception ex){
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void onOpenModalMapMouseClicked(MouseEvent mouseEvent){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource(MapLocationSelectionController.viewPath));
            JFXDialogLayout pane = loader.load();
            ((MapLocationSelectionController) loader.getController()).initModel(
                    new MapLocationSelectionController.Model(txtDireccion.getText(), this)
            );
            mapDialog = new JFXDialog(dialogContainer, pane, JFXDialog.DialogTransition.TOP);
            mapDialog.show();

        }catch(Exception e){
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onSelection(@NotNull String address, @NotNull Coordinate coordinate) {
        txtDireccion.setText(address);
        storeCoordinates = coordinate;
        mapDialog.close();
    }

    @Override
    public void onCancel() {
        mapDialog.close();
    }

    @Override
    protected boolean isAllTextFieldsFilled() {
        boolean valid = super.isAllTextFieldsFilled();
        if (!valid) {
            return valid;
        }
        valid = storeCoordinates != null;
        return valid;
    }
}
