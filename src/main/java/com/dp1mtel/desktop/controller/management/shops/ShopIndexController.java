package com.dp1mtel.desktop.controller.management.shops;

import com.dp1mtel.desktop.controller.BaseController;


import com.dp1mtel.desktop.di.ShopsHolder;

import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.Employee;
import com.dp1mtel.desktop.model.Shop;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class ShopIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/managament/shops/indexShops.fxml";
    private String windowTitle = "editar shop view";

    @FXML private JFXTreeTableView<Shop> shopTable;
    @FXML private JFXTreeTableColumn<Shop,String> indexColumn;
    @FXML private JFXTreeTableColumn<Shop,String> addressColumn;
    @FXML private JFXTreeTableColumn<Shop,String> districtColumn;
    @FXML private JFXTreeTableColumn<Shop,String> nEmployeesColumn;
    @FXML private JFXTreeTableColumn<Shop,String> nVehiclesColumn;
    @FXML private JFXTreeTableColumn<Shop,String> statusColumn;
    @FXML private JFXTreeTableColumn<Shop,Void> actionsColumn;

    @FXML private JFXButton newStoreBtn;
    @FXML private JFXTextField searchTienda;
    @FXML private FontAwesomeIconView searchBtn;

    private ShopsHolder shopsHolder = new ShopsHolder();

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        super.initialize(url, resourceBundle);
        loadStoreData();
    }

    private void loadStoreData(){
        shopsHolder.getShopsRepository().getShops().subscribe(this::setupShopTable);
        shopTable.refresh();
    }

    private void setupShopTable(List<Shop> shops) {
        // lista que permite mantener un handler de eventos

        for (Shop shop: shops) {
            shopsHolder.getShopsRepository().getEmployees(shop.getId())
                    .subscribe(_employees->{
                        shop.nemployeesProperty().set(Integer.toString(_employees.size()));
                        //shop.setEmployee(_employees);
            });
        }

        ObservableList<Shop> shopList = FXCollections.observableArrayList(shops);
        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

//        managerColumn.setCellValueFactory((param) -> {
//            if (managerColumn.validateValue(param)) {
//                return param.getValue().getValue().managerProperty();
//            } else {
//                return managerColumn.getComputedValue(param);
//            }
//        });

        addressColumn.setCellValueFactory((param) -> {
            if (addressColumn.validateValue(param)) {
                return param.getValue().getValue().addressProperty();
            } else {
                return addressColumn.getComputedValue(param);
            }
        });

        districtColumn.setCellValueFactory((param) -> {
            if (districtColumn.validateValue(param)) {
                return param.getValue().getValue().districtProperty();
            } else {
                return districtColumn.getComputedValue(param);
            }
        });

        nEmployeesColumn.setCellValueFactory((param) -> {
            if (nEmployeesColumn.validateValue(param)) {
                return param.getValue().getValue().nemployeesProperty();
            } else {
                return nEmployeesColumn.getComputedValue(param);
            }
        });

        nVehiclesColumn.setCellValueFactory((param) -> {
            if (nVehiclesColumn.validateValue(param)) {
                return param.getValue().getValue().numVehiclesProperty();
            } else {
                return nVehiclesColumn.getComputedValue(param);
            }
        });

        statusColumn.setCellValueFactory((param) -> {
            if (statusColumn.validateValue(param)) {
                return param.getValue().getValue().statusPropertyToString();
            } else {
                return statusColumn.getComputedValue(param);
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<Shop, Void>() {
            private final MaterialDesignIconView editBtn = new MaterialDesignIconView(MaterialDesignIcon.PENCIL);
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(editBtn, deleteBtn);
            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                editBtn.setOnMouseClicked((e) -> {
                    Shop shop = shopTable.getTreeItem(getIndex()).getValue();
                    setContentPane(ShopEditController.viewPath,ShopEditController.windowTitle, shop);
                });

                deleteBtn.setOnMouseClicked((e) -> {
                    Shop shop = shopTable.getTreeItem(getIndex()).getValue();
                    // TODO: Display delete userModel dialog
                    System.out.println("Delete: " + shop.getDistrict());

                    shopsHolder.getShopsRepository().deleteShop(shop.getId())
                            .subscribeOn(Schedulers.io())
                            .subscribe(
                                    System.err::println,
                                  (ex) -> System.err.println(ex.getMessage()));
                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                    String logMensaje="ELIMINAR - Tienda '"+shop.getId()+" "+shop.getDistrict()+"-"+shop.getAddress()
                            +"' - Usuario "+ usuario.getFullName();
                    LOGGERAUDIT.info(logMensaje);

              });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<Shop> root = new RecursiveTreeItem<>(shopList, RecursiveTreeObject::getChildren);
        shopTable.setRoot(root);
        shopTable.setShowRoot(false);
        shopTable.getColumns().setAll(indexColumn, addressColumn, districtColumn,
                nEmployeesColumn, nVehiclesColumn, statusColumn, actionsColumn);

        searchTienda.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                shopTable.setPredicate(new Predicate<TreeItem<Shop>>() {
                    @Override
                    public boolean test(TreeItem<Shop> shopTreeItem) {
                        Boolean flag = shopTreeItem.getValue().getAddress().toLowerCase().contains(newValue.toLowerCase())||
                                shopTreeItem.getValue().getDistrict().toLowerCase().contains(newValue.toLowerCase())||
                                shopTreeItem.getValue().statusPropertyToString().get().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return newStoreBtn;
    }

    public void onCreateClicked(MouseEvent mouseEvent) {setContentPane(ShopCreateController.viewPath);}
}
