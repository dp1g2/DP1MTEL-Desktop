package com.dp1mtel.desktop.controller.security.roles;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.Permission;
import com.dp1mtel.desktop.model.Role;
import com.dp1mtel.desktop.model.User;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class RoleEditController extends BaseRoleCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/security/roles/edit.fxml";
    public static final String windowTitle = "Editar Rol";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location,resourceBundle);

        try{
            permissionsHolder.getPermissionsRepository().getPermissions()
                    .subscribeOn(Schedulers.io())
                    .subscribe(_permissions ->{
                        Platform.runLater(() -> setupTablePermissions(_permissions));
                        Platform.runLater(()-> updateCheckColumn());
                    });

        }catch(Exception e){
            System.err.println("ERROR EN CREAR ROL" + e.getMessage());
        }

    }

    public void initModel(Role model) {
        this.roleModel = model;
        System.out.println("ID: " + model.getId());

        nameField.setText(roleModel.getName());
        descriptionField.setText(roleModel.getDescription());

        //para este caso debo actualizar los datos del permission table con lo del model
    }

    private void updateCheckColumn() {

        //permissionsList viene del servidor
        //pModel es un permiso que pertenece al modelo Role que acabo de escoger para editar
        for (Permission p: permissionsList) {
            int i = 0;
            for (Permission pModel: roleModel.getPermissions()) {
                if(pModel.getId()==p.getId())
                    p.setSelect(true);
            }
        }

    }

//    public void validateFields(){
//        saveBtn.disableProperty().bind(Bindings.createBooleanBinding(
//                () -> nameField.getText().isEmpty() || descriptionField.getText().isEmpty() || isNoPermissionSelected()
//        ));
//
//    }

    public void saveRole(){

        roleModel.setName(nameField.getText());
        roleModel.setDescription(descriptionField.getText());

        roleModel.setPermissions(new ArrayList<Permission>());

        System.out.println("LA LISTA TIENE LONGITUD DE ");
        System.out.println(roleModel.permissionsProperty().get().size());

        for (Permission p : permissionsList){

            if(p.getSelect()){
                System.out.println("Permiso seleccionado : "+ p.getName() );
                roleModel.getPermissions().add(p);
            }
        }



        System.out.println("LA LISTA TIENE LONGITUD FINAL DE ");
        System.out.println(roleModel.permissionsProperty().get().size());

        rolesHolder.getRolesRepository().updateRoleById(roleModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_role) ->{
                    Platform.runLater(() ->
                            System.err.println(_role.getName()));

                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                    String logMensaje="EDICIÓN - Rol '"+roleModel.getId()+" "+roleModel.getName()+"' - Usuario "+ usuario.getFullName();
                    LOGGERAUDIT.info(logMensaje);
                    Platform.runLater(()-> goBack());
                }, (e)-> {
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });
    }
}
