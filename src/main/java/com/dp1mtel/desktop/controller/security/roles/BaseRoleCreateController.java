package com.dp1mtel.desktop.controller.security.roles;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.PermissionsHolder;
import com.dp1mtel.desktop.di.RolesHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Permission;
import com.dp1mtel.desktop.model.Role;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.validation.RequiredFieldValidator;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BaseRoleCreateController extends GenericBaseController<Role>  {

    public JFXButton saveBtn;
    public JFXButton cancelBtn;
    public JFXTextField nameField;
    public JFXTextArea descriptionField;
    public JFXTreeTableView<Permission> permissionTable;

    public JFXTreeTableColumn<Permission,String> indexColumn;
    public JFXTreeTableColumn<Permission,String> nameColumn;
    public JFXTreeTableColumn<Permission,String> descriptionColumn;
    public JFXTreeTableColumn<Permission,CheckBox> checkColumn;

    public Role roleModel;
    public RolesHolder rolesHolder = new RolesHolder();
    public PermissionsHolder permissionsHolder = new PermissionsHolder();
    public ObservableList<Permission> permissionsList;

    public StackPane stackPane;

    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFieldValidation();

    }

    public void setupFieldValidation(){
        RequiredFieldValidator requiredValidator = new RequiredFieldValidator();
        requiredValidator.setMessage("Este campo es obligatorio");

        nameField.setValidators(requiredValidator);
        nameField.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) nameField.validate();});
        descriptionField.setValidators(requiredValidator);
        descriptionField.focusedProperty().addListener((observable, oldValue, newValue) -> {if (!newValue) descriptionField.validate();});
    }

    public void onSaveClicked(MouseEvent mouseEvent) {
        if (validateFields())
            saveRole();
        else
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, corregir los errores mostrados.");
    }

    public void setupTablePermissions(List<Permission> permissions){
        permissionsList = FXCollections.observableArrayList(permissions);

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        nameColumn.setCellValueFactory((param) -> {
            if(nameColumn.validateValue(param)){
                return param.getValue().getValue().nameProperty();
            }else{
                return nameColumn.getComputedValue(param);
            }

        });

        descriptionColumn.setCellValueFactory((param)->{
            if(descriptionColumn.validateValue(param)){
                return param.getValue().getValue().descriptionProperty();
            }else{
                return descriptionColumn.getComputedValue(param);
            }
        });

        checkColumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Permission, CheckBox>,
                ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call (
                    TreeTableColumn.CellDataFeatures<Permission,CheckBox> arg0){

                Permission permission = arg0.getValue().getValue();
                CheckBox checkBox = new JFXCheckBox();
                checkBox.selectedProperty().setValue(permission.getSelect());
                checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable,
                                        Boolean oldValue, Boolean newValue) {
                        permission.setSelect(newValue);
                        //validateFields();
                    }
                });
                return new SimpleObjectProperty<CheckBox>(checkBox);
            }
        });

        final TreeItem<Permission> root = new RecursiveTreeItem<>(permissionsList, RecursiveTreeObject::getChildren);
        permissionTable.setRoot(root);
        permissionTable.setShowRoot(false);
        permissionTable.getColumns().setAll(indexColumn,nameColumn,descriptionColumn,checkColumn);
    }

    private boolean isNoPermissionSelected(){
        for (Permission p: permissionsList) {
            if(p.getSelect()) return false;
        }
        return true;
    }

    public boolean validateFields(){
        if(nameField.getText().isEmpty() && descriptionField.getText().isEmpty() && isNoPermissionSelected()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar el nombre del Rol.");
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar la descripción del Rol.");
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, seleccionar los permisos.");
            return false;
        }
        if (nameField.getText().isEmpty() && descriptionField.getText().isEmpty()) {
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar el nombre del Rol.");
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar la descripción del Rol.");
            return false;
        }
        if (descriptionField.getText().isEmpty() && isNoPermissionSelected()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar la descripción del Rol.");
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, seleccionar los permisos.");
            return false;
        }
        if (nameField.getText().isEmpty() && isNoPermissionSelected()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar el nombre del Rol.");
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, seleccionar los permisos.");
            return false;
        }
        if (nameField.getText().isEmpty()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar el nombre del Rol.");
            return false;
        }
        if (descriptionField.getText().isEmpty()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar la descripción del Rol.");
            return false;
        }
        if(isNoPermissionSelected()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, seleccionar los permisos.");
            return false;
        }
        return true;


        /*saveBtn.disableProperty().bind(Bindings.createBooleanBinding(
                () -> nameField.getText().isEmpty() || descriptionField.getText().isEmpty() || isNoPermissionSelected()
        ));*/
    }


    public void saveRole(){
        //Todo: cada controller que herede debe sobreescribir este metodo.
    }

    public void goBack() { setContentPane(RoleIndexController.viewPath); }

    @Override
    protected Node getPaneReferenceComponent() { return saveBtn; }

    public void onCancelClicked(MouseEvent mouseEvent) { goBack(); }
}


