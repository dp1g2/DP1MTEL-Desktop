package com.dp1mtel.desktop.controller.security;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.security.logs.LogsIndexController;
import com.dp1mtel.desktop.controller.security.roles.RoleIndexController;
import com.dp1mtel.desktop.controller.security.users.UserIndexController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class SecurityController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/security/index.fxml";

    public void goSecurity(){ setContentPane(SecurityController.viewPath); }

    @FXML
    private JFXButton btnUsuarios;

    public void initialize(URL url, ResourceBundle resourceBundle){ super.initialize(url, resourceBundle);}

    @Override
    protected Node getPaneReferenceComponent() {
        return btnUsuarios;
    }

    public void onUsuariosClicked(MouseEvent mouseEvent) {
        setContentPane(UserIndexController.viewPath);
    }

    public void onRolesClicked(MouseEvent mouseEvent) {
        setContentPane(RoleIndexController.viewPath);
    }

    public void onLogsClicked(MouseEvent mouseEvent) {
        setContentPane(LogsIndexController.viewPath);
    }
}
