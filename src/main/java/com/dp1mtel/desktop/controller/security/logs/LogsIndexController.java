package com.dp1mtel.desktop.controller.security.logs;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.reports.ReportsController;
import com.dp1mtel.desktop.controller.security.SecurityController;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.InventoryOperation;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class LogsIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/security/log/index.fxml";

    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXDatePicker startDate;
    @FXML
    private JFXDatePicker endDate;
    @FXML
    private StackPane dialogContainer;

    private FileReader fileReader = null;

    @Override
    protected Node getPaneReferenceComponent() {
        return btnCancelar;
    }

    public void onCancelClicked(MouseEvent mouseEvent) {
        setContentPane(SecurityController.viewPath);
    }

    public void onExportarClicked(MouseEvent mouseEvent) {
        if (validateFields()) {
            generarReporteInventario();
        } else return;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        JFXDatePicker maxDate = new JFXDatePicker();
        maxDate.setValue(DateExtensions.toLocalDate(new Date())); // colocar la fecha de hoy como el minimo

        final Callback<DatePicker, DateCell> dayCellFactory;

        dayCellFactory = (final DatePicker datePicker) -> new DateCell() {
            @Override
            public void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                if (item.isAfter(maxDate.getValue())) {
                    setDisable(true);
                    setVisible(false);
                } else {
                    setVisible(true);
                    setDisable(false);
                }
            }

        };

        startDate.setDayCellFactory(dayCellFactory);
        endDate.setDayCellFactory(dayCellFactory);
    }

    public void generarReporteInventario() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Guardar reporte");
        File savedFile = directoryChooser.showDialog(null);
        if (savedFile != null) {
            try {
                imprimirReporte(savedFile);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            return;
        }
    }

    private void imprimirReporte(File savedFile) throws IOException, ParseException {

        LocalDate start = startDate.getValue().minusDays(1);
        LocalDate finish = endDate.getValue().plusDays(1);

        File archivoTxt = new File(savedFile.getAbsolutePath() + File.separator + "Audit_Logs" + ".txt");
        FileWriter fw = new FileWriter(archivoTxt);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter pw = new PrintWriter(bw);

        fileReader = new FileReader("mtel_audit.log");
        BufferedReader reader = new BufferedReader(fileReader);

        String linea;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        pw.write("Reporte de logs ");
        pw.append("\r\n");
        while ((linea = reader.readLine()) != null) {
            String sSubCadena = linea.substring(0, 10);
            Date date = formatter.parse(sSubCadena);
            LocalDate dateL = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if ((dateL.isAfter(start)) && dateL.isBefore(finish)) {
                pw.append("\n");
                pw.println(linea);
            } else {
                break;
            }
        }
        pw.close();
        bw.close();
        AlertDialog.showMessage(dialogContainer,"Operación exitosa","Se ha guardado el log en la dirección indicada");
    }

    public boolean validateFields() {

        LocalDate start = startDate.getValue();
        LocalDate finish = endDate.getValue();

        if (start== null) {
            AlertDialog.showMessage(dialogContainer, "Error", "Debe introducir una fecha de inicio");
            return false;
        }

        if (finish== null) {
            AlertDialog.showMessage(dialogContainer, "Error", "Debe introducir una fecha de fin");
            return false;
        }

        if (finish.isAfter(DateExtensions.toLocalDate(new Date()))) {
            endDate.setValue(null);
            AlertDialog.showMessage(dialogContainer, "Error", "La fecha es incorrecta");
            return false;
        }

        if (start.isAfter(finish)) {
            endDate.setValue(null);
            AlertDialog.showMessage(dialogContainer, "Error", "La fecha es incorrecta");
            return false;
        }

        return true;
    }

}

