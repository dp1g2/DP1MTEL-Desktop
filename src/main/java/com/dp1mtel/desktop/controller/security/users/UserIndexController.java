package com.dp1mtel.desktop.controller.security.users;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.Role;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class UserIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/security/users/index.fxml";
    public static final String windowTitle = "Usuarios";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @FXML private JFXButton newUserBtn;
    @FXML private JFXTreeTableView<User> userTable;
    @FXML private JFXTreeTableColumn<User, String> indexColumn;
    @FXML private JFXTreeTableColumn<User, String> nameColumn;
    @FXML private JFXTreeTableColumn<User, String> emailColumn;
    @FXML private JFXTreeTableColumn<User, String> idColumn;
    @FXML private JFXTreeTableColumn<User, String> rolesColumn;
    @FXML private JFXTreeTableColumn<User, Void> actionsColumn;
    @FXML private JFXTreeTableColumn<User, String> stateColumn;
    @FXML private JFXTextField searchBox;


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);

        // Setup user table when user list is updated
        loadUsers();


    }

    private void loadUsers(){

        usersHolder.getUsersRepository().getUsers()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (users) -> Platform.runLater(() -> setupUserTable(users)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));


    }

    private void setupUserTable(List<User> users) {
        ObservableList<User> userList = FXCollections.observableArrayList(users);

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().fullNameProperty();
            } else {

                return nameColumn.getComputedValue(param);
            }
        });

//      nameColumn.setCellValueFactory(new CellPropertyFactory<User, String>("fullName"));

        emailColumn.setCellValueFactory((param) -> {
            if (emailColumn.validateValue(param)) {
                return param.getValue().getValue().emailProperty();
            } else {
                return emailColumn.getComputedValue(param);
            }
        });

        idColumn.setCellValueFactory((param)->{
            if (idColumn.validateValue(param)) {
                return param.getValue().getValue().dniProperty();
            } else {
                return idColumn.getComputedValue(param);
            }
        });


        stateColumn.setCellValueFactory((param)->{
            if (stateColumn.validateValue(param)) {
                return new SimpleStringProperty(param.getValue().getValue().isActive() ? "Activo" : "Inactivo");
            } else {
                return stateColumn.getComputedValue(param);
            }
        });

        rolesColumn.setCellValueFactory((param) -> {
            try{
                if (rolesColumn.validateValue(param)) {

                    SimpleObjectProperty <Role> roleProp = param.getValue().getValue().roleProperty();
                    return new SimpleStringProperty(roleProp.get().getName());

                } else {
                    return rolesColumn.getComputedValue(param);
                }
            }catch (Exception e){
                System.err.print(e.getMessage());
            }

            return null;
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<User, Void>() {
            private final MaterialDesignIconView editBtn = new MaterialDesignIconView(MaterialDesignIcon.PENCIL);
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(editBtn, deleteBtn);

            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                editBtn.setOnMouseClicked((e) -> {
                    User user = userTable.getTreeItem(getIndex()).getValue();
                    setContentPane(UserEditController.viewPath, UserEditController.windowTitle, user);
                });

                deleteBtn.setOnMouseClicked((e) -> {
                    User user = userTable.getTreeItem(getIndex()).getValue();
                    // TODO: Display delete userModel dialog
                    System.out.println("Delete: " + user.getFullName());

                    usersHolder.getUsersRepository().deleteUser(user.getId())
                            .subscribeOn(Schedulers.io())
                            .subscribe((_response) -> {
                                Platform.runLater(() -> {
                                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                    String logMensaje="EDICIÓN - Usuario '"+user.getId()+" "+user.getFullName()+"' - Usuario "+ usuario.getFullName();
                                    LOGGERAUDIT.info(logMensaje);
                                    //no es capaz de llamar a esta funcion
                                    System.out.println("Refrescar tabla");
                                });


                            });
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<User> root = new RecursiveTreeItem<>(userList, RecursiveTreeObject::getChildren);
        userTable.setRoot(root);
        userTable.setShowRoot(false);
        userTable.getColumns().setAll(indexColumn, idColumn ,nameColumn, emailColumn, rolesColumn, stateColumn, actionsColumn);

        searchBox.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                userTable.setPredicate(new Predicate<TreeItem<User>>() {
                    @Override
                    public boolean test(TreeItem<User> userTreeItem) {
                        //se busca por nombre, dni, rol
                        Boolean flag = userTreeItem.getValue().getFullName().toLowerCase().contains(newValue.toLowerCase())||
                                userTreeItem.getValue().getDni().toLowerCase().contains(newValue.toLowerCase())||
                                userTreeItem.getValue().getRole().getName().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return newUserBtn;
    }

    public void onCreateUserClicked(MouseEvent mouseEvent) {
        setContentPane(UserCreateController.viewPath);
    }
}
