package com.dp1mtel.desktop.controller.security.roles;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.RolesHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.Role;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import com.squareup.moshi.JsonWriter;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
    import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class RoleIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/security/roles/index.fxml";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @FXML private JFXTreeTableView<Role> roleTable;
    @FXML private JFXTreeTableColumn<Role, String> indexColumn;
    @FXML private JFXTreeTableColumn<Role, String> nameColumn;
    @FXML private JFXTreeTableColumn<Role, String> descripColumn;
    @FXML private JFXTreeTableColumn<Role, Void> actionsColumn;

    @FXML private JFXTextField searchBox;
    @FXML private FontAwesomeIconView searchBtn;
    @FXML private JFXButton createBtn;

    private RolesHolder rolesHolder = new RolesHolder();

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        loadRoleData();
    }

    private void loadRoleData(){
        rolesHolder.getRolesRepository().getRoles().subscribe(this::setupRoleTable);
        roleTable.refresh();
    }

    private void setupRoleTable(List<Role> roles) {
        ObservableList<Role> roleList = FXCollections.observableArrayList(roles);
        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        nameColumn.setCellValueFactory((param) -> {
            if (nameColumn.validateValue(param)) {
                return param.getValue().getValue().nameProperty();
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        descripColumn.setCellValueFactory((param) -> {
            if (descripColumn.validateValue(param)) {
                return param.getValue().getValue().descriptionProperty();
            } else {
                return descripColumn.getComputedValue(param);
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<Role, Void>() {
            private final MaterialDesignIconView editBtn = new MaterialDesignIconView(MaterialDesignIcon.PENCIL);
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(editBtn, deleteBtn);
            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);

                editBtn.setOnMouseClicked((e) -> {
                    Role role = roleTable.getTreeItem(getIndex()).getValue();
                    setContentPane(RoleEditController.viewPath, RoleEditController.windowTitle, role);
                });

                deleteBtn.setOnMouseClicked((event) -> {
                    Role role = roleTable.getTreeItem(getIndex()).getValue();
                    // TODO: Display delete userModel dialog
                    System.out.println("Delete: " + role.getName());

                    rolesHolder.getRolesRepository().deleteRole(role.getId())
                            .subscribeOn(Schedulers.io())
                            .subscribe((_response_user) -> {
                                Platform.runLater(() -> {
                                    //no es capaz de llamar a esta funcion
                                   loadRoleData();
                                });




                            });
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<Role> root = new RecursiveTreeItem<>(roleList, RecursiveTreeObject::getChildren);
        roleTable.setRoot(root);
        roleTable.setShowRoot(false);
        roleTable.getColumns().setAll(indexColumn, nameColumn, descripColumn, actionsColumn);

        searchBox.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                roleTable.setPredicate(new Predicate<TreeItem<Role>>() {
                    @Override
                    public boolean test(TreeItem<Role> roleTreeItem) {
                        //se busca por nombre del  role
                        Boolean flag = roleTreeItem.getValue().getName().toLowerCase().contains(newValue.toLowerCase())||
                                roleTreeItem.getValue().getDescription().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return createBtn;
    }

    public void onCreateClicked(MouseEvent mouseEvent) {
        setContentPane(RoleCreateController.viewPath);
    }
}
