package com.dp1mtel.desktop.controller.security.users;

import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.Role;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.util.ResourceBundle;

public class UserCreateController extends BaseUserCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/security/users/create.fxml";

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();



    @Override
    public void saveUser(){
        userModel = new User(
                firstNameField.getText(),
                lastNameField.getText(),
                idField.getText(),
                emailField.getText(),
                DateExtensions.toDate(birthdayField.getValue()),
                genderField.getValue(),
                passField.getText(),
                roleField.getValue(),
                new Boolean(true), null, "");

        userHolder.getUsersRepository().saveUser(userModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_user) -> {
                    Platform.runLater(() ->
                            System.err.println(_user.getFullName()));
                    User usuario= usersHolder.getUsersRepository().getCurrentUser();
                    String logMensaje="CREACIÓN - Nuevo Usuario '"+userModel.getId()+" "+userModel.getFullName()+"' - Usuario "+ usuario.getFullName();
                    LOGGERAUDIT.info(logMensaje);
                    Platform.runLater(() -> goBack());

                }, (e) -> {
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });



    }
}
