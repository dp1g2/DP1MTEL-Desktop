package com.dp1mtel.desktop.controller.security.users;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.RolesHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Role;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.dp1mtel.desktop.util.validation.EmailValidator;
import com.dp1mtel.desktop.util.validation.IdValidator;
import com.jfoenix.controls.*;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

public class BaseUserCreateController extends GenericBaseController<User> {

    public JFXButton saveBtn;
    public JFXButton cancelBtn;
    public JFXTextField firstNameField;
    public JFXTextField lastNameField;
    public JFXTextField idField;
    public JFXTextField emailField;
    public JFXDatePicker birthdayField;
    public JFXComboBox<String> genderField;
    public JFXPasswordField passField;
    public JFXPasswordField confirmPassField;
    public JFXComboBox<Role> roleField;

    protected User userModel;

    protected UsersHolder userHolder = new UsersHolder();
    protected RolesHolder rolesHolder = new RolesHolder();

    public StackPane stackPane;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFieldValidation();
        loadComboBoxRoles();
    }


    public void loadComboBoxRoles() {
        rolesHolder.getRolesRepository().getRoles()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (roles) -> Platform.runLater(() -> setupComboBoxRoles(roles)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }


    public void setupFieldValidation() {
        RequiredFieldValidator requiredValidatorName = new RequiredFieldValidator();
        requiredValidatorName.setMessage("Este campo es obligatorio");
        RequiredFieldValidator requiredValidatorLastN = new RequiredFieldValidator();
        requiredValidatorLastN.setMessage("Este campo es obligatorio");
        RequiredFieldValidator requiredValidatorPW = new RequiredFieldValidator();
        requiredValidatorPW.setMessage("Este campo es obligatorio");
        RequiredFieldValidator requiredValidatorPWC = new RequiredFieldValidator();
        requiredValidatorPWC.setMessage("Este campo es obligatorio");
        IdValidator idValidator = new IdValidator();
        idValidator.setMessage("Este campo debe ser un DNI válido");
        EmailValidator emailValidator = new EmailValidator();
        emailValidator.setMessage("Este campo debe ser un email válido");

        // Name validation
        firstNameField.setValidators(requiredValidatorName);
        firstNameField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) firstNameField.validate();
        });
        firstNameField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(!newValue.matches("[A-Za-z ]")){
                    firstNameField.setText(newValue.replaceAll("[^A-Za-z ]",""));
                }
            }
        });

        lastNameField.setValidators(requiredValidatorLastN);
        lastNameField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) lastNameField.validate();
        });
        lastNameField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(!newValue.matches("[A-Za-z ]")){
                    lastNameField.setText(newValue.replaceAll("[^A-Za-z ]",""));
                }
            }
        });
        // Id validation
        idField.setValidators(idValidator);
        idField.focusedProperty().addListener(((observable, oldValue, newValue) -> {
            if (!newValue) idField.validate();
        }));
        idField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue){
                if(!newValue.matches("\\d*")){
                    idField.setText(newValue.replaceAll("[^\\d]",""));
                }
            }
        });

        // Email validation
        emailField.setValidators(emailValidator);
        emailField.focusedProperty().addListener((obs, old, newVal) -> {
            if (!newVal) emailField.validate();
        });

        // Id validation
        idField.focusedProperty().addListener(((observable, oldValue, newValue) -> {
            if (!newValue) idField.validate();
        }));

        //genderField.setItems(FXCollections.observableArrayList(User.genderMap.values()));
        // Gender Validation
        Collection<String> list = new ArrayList<String>();
        list.add(User.genderMap.get(User.Gender.MALE));
        list.add(User.genderMap.get(User.Gender.FEMALE));
        genderField.setItems(FXCollections.observableArrayList(list));

        passField.setValidators(requiredValidatorPW);

        passField.focusedProperty().addListener(((observable, oldValue, newValue) -> {
            if (!newValue) passField.validate();
        }));


        confirmPassField.setValidators(requiredValidatorPWC);
        confirmPassField.focusedProperty().addListener(((observable, oldValue, newValue) -> {
            if (!newValue) confirmPassField.validate();
        }));

        //todo: check is pass y confirmpass are equal

        //birthday Validation
        DatePicker maxDate = new DatePicker();
        maxDate.setValue(LocalDate.now());
        final Callback<DatePicker,DateCell> dayCellFactory;

        dayCellFactory = (final DatePicker jfxDatePicker1)->new DateCell(){
            @Override
            public void updateItem(LocalDate item, boolean empty){
                if(item.isAfter(maxDate.getValue())){
                    setDisable(true);
                }
            }
        };
        birthdayField.setDayCellFactory(dayCellFactory);
    }

    protected void setupComboBoxRoles(List<Role> roles) {
        System.out.println("Setup combo roles");
        for (Role role : roles) {
            roleField.getItems().add(role);
        }

    }

    @Override
    protected Node getPaneReferenceComponent() {
        return saveBtn;
    }

    public void onSaveClicked(MouseEvent mouseEvent) {
        //todo: validar que los campos esten llenos.

        if (isAllTextFieldsFilled()) {
            if (validateComboBoxes()) {
                if(validateBirthdayField()) {
                    if (validatePassword())
                        saveUser();
                }
            }
        } else {
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, corregir los errores mostrados.");
        }
    }

    private boolean validateComboBoxes() {
        if(genderField.getSelectionModel().isEmpty()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, especifique un género.");

            genderField.requestFocus();
            return false;
        }

        if(roleField.getSelectionModel().isEmpty()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, especifique un Rol.");

            roleField.requestFocus();

            return false;
        }
        return true;
    }

    private boolean validateBirthdayField(){
        if(birthdayField.getValue() == null){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar la fecha de nacimiento.");
            birthdayField.requestFocus();
            return false;
        }else if(!validateBirthday()){
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, contratar a mayores de edad.");
            birthdayField.requestFocus();
            return false;
        }else
            return true;
    }

    private boolean validateBirthday(){
        LocalDate dateMax = LocalDate.of(LocalDate.now().getYear()-18,LocalDate.now().getMonth(),LocalDate.now().getDayOfMonth());
        if (birthdayField.getValue().isBefore(dateMax))
            return true;
        else
            return false;
    }

    private boolean validatePassword(){
        if (passField.getText().equals(confirmPassField.getText()))
            return true;
        else {
            AlertDialog.showMessage(stackPane, "Error al guardar","Por favor, ingresar las mismas contraseñas.");
            return false;
        }
    }

    private boolean isAllTextFieldsFilled() {

        return firstNameField.validate()||
                lastNameField.validate()||
                emailField.validate()||
                passField.validate()||
                confirmPassField.validate()||
                idField.validate();
    }

    public void saveUser() {System.err.println("Aqui en saveUser");}

    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();
    }

    protected void goBack() {
        setContentPane(UserIndexController.viewPath);
    }
}
