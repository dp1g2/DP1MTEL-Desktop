package com.dp1mtel.desktop.controller.security.users;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;

import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserEditController extends BaseUserCreateController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/security/users/edit.fxml";
    public static final String windowTitle = "Editar Usuario";


    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();


    @Override
    public void loadComboBoxRoles() {
        rolesHolder.getRolesRepository().getRoles()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (roles) -> Platform.runLater(() -> {
                            setupComboBoxRoles(roles);
                            setupSelectedValueOnCombox();
                        }),

                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }


    @Override
    public void initModel(User model) {
        this.userModel = model;
        firstNameField.setText(userModel.getFirstName());
        lastNameField.setText(userModel.getLastName());
        idField.setText(userModel.getDni());
        emailField.setText(userModel.getEmail());

        try {
            birthdayField.setValue(DateExtensions.toLocalDate( userModel.getDob()) );

        } catch (ParseException e) {
            Logger.getLogger(App.TAG).log(Level.WARNING, "Invalid date of birth for userModel: " + userModel.getFullName());
            birthdayField.setValue(DateExtensions.toLocalDate(new Date()));
        }

        genderField.setValue(userModel.getGender());

//        passField.setText(userModel.getPassword().replaceAll(".", "*"));
////        confirmPassField.setText(userModel.getPassword().replaceAll(".", "*"));

        passField.setVisible(false);
        confirmPassField.setVisible(false);
    }

    private void setupSelectedValueOnCombox(){
        System.out.println("Setup selected value roles");

    //    roleField.setValue(userModel.getRole());
        for (int i = 0; i <roleField.getItems().size() ; i++) {
            if(userModel.getRole().getId()==roleField.getItems().get(i).getId()){
                roleField.getSelectionModel().select(i);
                break;
            }
        }
    }

    protected Node getPaneReferenceComponent() {
        return idField;
    }

    @Override
    public void saveUser(){

        userModel.setFirstName(firstNameField.getText());
        userModel.setLastName(lastNameField.getText());
        userModel.setEmail(emailField.getText());
        userModel.setDni(idField.getText());
        userModel.setPassword(passField.getText());
        userModel.setRole(roleField.getValue());


        System.out.println("ID: " + userModel.getId());

        userHolder.getUsersRepository().updateUserById(userModel)
                .subscribeOn(Schedulers.io())
                .subscribe((_user) -> {
                    Platform.runLater(() -> {
                        System.err.println(_user.getLastName());
                        User usuario= usersHolder.getUsersRepository().getCurrentUser();
                        String logMensaje="EDICIÓN - Usuario '"+userModel.getId()+" "+userModel.getFullName()+"' - Usuario "+ usuario.getFullName();
                        LOGGERAUDIT.info(logMensaje);
                        goBack();
                    });
                }, (e) -> {
                    Platform.runLater(() -> System.err.println(e.getMessage()));
                });


    }
}
