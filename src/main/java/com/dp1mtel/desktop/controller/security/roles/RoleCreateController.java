    package com.dp1mtel.desktop.controller.security.roles;

    import com.dp1mtel.desktop.controller.BaseController;
    import com.dp1mtel.desktop.di.PermissionsHolder;
    import com.dp1mtel.desktop.di.RolesHolder;
    import com.dp1mtel.desktop.di.UsersHolder;
    import com.dp1mtel.desktop.model.Permission;
    import com.dp1mtel.desktop.model.Role;
    import com.dp1mtel.desktop.model.User;
    import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
    import com.jfoenix.controls.*;
    import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
    import com.jfoenix.validation.RequiredFieldValidator;
    import io.reactivex.schedulers.Schedulers;
    import javafx.application.Platform;
    import javafx.beans.binding.Bindings;
    import javafx.beans.property.SimpleObjectProperty;
    import javafx.beans.value.ChangeListener;
    import javafx.beans.value.ObservableValue;
    import javafx.collections.FXCollections;
    import javafx.collections.ObservableList;
    import javafx.fxml.FXML;
    import javafx.scene.Node;
    import javafx.scene.control.*;
    import javafx.scene.input.MouseEvent;
    import javafx.util.Callback;
    import org.apache.logging.log4j.LogManager;

    import java.net.URL;
    import java.util.ArrayList;
    import java.util.List;
    import java.util.ResourceBundle;

    public class RoleCreateController extends BaseRoleCreateController {
        public static final String viewPath = "/com/dp1mtel/desktop/view/security/roles/create.fxml";

        private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
        private UsersHolder usersHolder = new UsersHolder();

    //    @FXML private JFXButton saveBtn;
    //    @FXML private JFXButton cancelBtn;
    //    @FXML private JFXTextField nameField;
    //    @FXML private JFXTextArea descriptionField;
    //
    //    @FXML private JFXTreeTableView<Permission> permissionTable;
    //    @FXML private JFXTreeTableColumn<Permission,String> indexColumn;
    //    @FXML private JFXTreeTableColumn<Permission,String> nameColumn;
    //    @FXML private JFXTreeTableColumn<Permission,String> descriptionColumn;
    //    @FXML private JFXTreeTableColumn<Permission,CheckBox> checkColumn;


        public void initialize (URL location, ResourceBundle resourceBundle){
            super.initialize(location,resourceBundle);
            try{
                permissionsHolder.getPermissionsRepository().getPermissions()
                        .subscribeOn(Schedulers.io())
                        .subscribe(_permissions ->{

                            Platform.runLater(() -> setupTablePermissions(_permissions));
                        });

            }catch(Exception e){
                System.err.println("ERROR EN CREAR ROL" + e.getMessage());
            }
            //saveBtn.disableProperty().setValue(true);
        }

        public void saveRole() {
            roleModel = new Role(nameField.getText(), descriptionField.getText(), new ArrayList<>());
            for (Permission p : permissionsList){
                if(p.getSelect()){
                    roleModel.permissionsProperty().get().add(p);
                }
            }

            rolesHolder.getRolesRepository().saveRole(roleModel)
                    .subscribeOn(Schedulers.io())
                    .subscribe((_role) -> {
                        Platform.runLater(() ->
                                    System.err.println(_role.getName()));
                        User usuario= usersHolder.getUsersRepository().getCurrentUser();
                        String logMensaje="ELIMINAR - Rol '"+roleModel.getId()+" "+roleModel.getName()+"' - Usuario "+ usuario.getFullName();
                        LOGGERAUDIT.info(logMensaje);
                        Platform.runLater(()-> goBack());

                    }, (e) -> {
                        Platform.runLater(() -> System.err.println(e.getMessage()));
                    });
        }
    }
