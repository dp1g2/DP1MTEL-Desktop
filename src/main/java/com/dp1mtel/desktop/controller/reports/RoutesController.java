package com.dp1mtel.desktop.controller.reports;

import com.dp1mtel.desktop.controller.BaseController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class RoutesController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/reports/report_Routes.fxml";



    public void initialize(URL url, ResourceBundle resourceBundle){ super.initialize(url, resourceBundle);}

    @FXML
    private JFXButton btnRutas;


    @Override
    protected Node getPaneReferenceComponent() {
        return btnRutas;
    }


    public void onCancelClicked(MouseEvent mouseEvent) {
        setContentPane(ReportsController.viewPath);
    }


    public void onReportGenerateClicked(MouseEvent mouseEvent) {
    }
}
