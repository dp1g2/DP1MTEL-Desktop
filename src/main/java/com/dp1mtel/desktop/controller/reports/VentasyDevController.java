package com.dp1mtel.desktop.controller.reports;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.OrdersHolder;
import com.dp1mtel.desktop.di.ShopsHolder;
import com.dp1mtel.desktop.model.*;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class VentasyDevController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/reports/report_SalesDev.fxml";

    List<Order> orderList;

    private OrdersHolder ordersHolder= new OrdersHolder();

    private ShopsHolder storeHolder= new ShopsHolder();
    @FXML private JFXComboBox<Shop> storeField;

    private String estadoReturned= "RETURNED";
    private String estadoCompleted = "COMPLETED";



    private void loadStoreCbx() {
        System.out.println("Entro al load");
        storeHolder.getShopsRepository().getShops()
                .subscribeOn(Schedulers.io())
                .subscribe((_stores)->Platform.runLater(()->setUpCbx(_stores)),
                        (e)->Platform.runLater(()->System.err.println(e.getLocalizedMessage())));
    }

    private void setUpCbx(List<Shop> stores) {
        System.out.println("LA DIRECCION DEL PRIMER ELEMENTO ES " +  stores.get(0).getAddress());
//        storeField.setItems(FXCollections.observableArrayList(stores));
        storeField.getItems().setAll(stores);

    }

    @Override

    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        System.out.println("Entro al initialiar");
        loadStoreCbx();

        JFXDatePicker maxDate = new JFXDatePicker();
        maxDate.setValue(DateExtensions.toLocalDate(new Date())); // colocar la fecha de hoy como el minimo

        final Callback<DatePicker, DateCell> dayCellFactory;

        dayCellFactory = (final DatePicker datePicker) -> new DateCell() {
            @Override
            public void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);

                if (item.isAfter(maxDate.getValue())) {
                    setDisable(true);
                    setVisible(false);

                } else {
                    setVisible(true);
                    setDisable(false);
                }
            }


        };
        startDate.setDayCellFactory(dayCellFactory);
        endDate.setDayCellFactory(dayCellFactory);
    }

    @FXML
    private JFXDatePicker startDate;
    @FXML private JFXDatePicker endDate;



    @FXML private StackPane dialogContainer;

    public boolean setupValidation(){



        LocalDate start =startDate.getValue();
        LocalDate finish =endDate.getValue();

        if(finish.isAfter(DateExtensions.toLocalDate(new Date ())) ){
            endDate.setValue(null);
            AlertDialog.showMessage(dialogContainer,"Error","La fecha es incorrecta");
            return false;
        }

        if(start.isAfter(finish) ){
            startDate.setValue(null);
            AlertDialog.showMessage(dialogContainer,"Error","La fecha es incorrecta");
            return false;
        }

        return true;
    }

    @FXML
    private JFXButton btnSales;


    @Override
    protected Node getPaneReferenceComponent() {
        return btnSales;
    }


    public void onCancelClicked(MouseEvent mouseEvent) {
        setContentPane(ReportsController.viewPath);
    }


    public void onReportGenerateClicked(MouseEvent mouseEvent) throws IOException {
        loadOrder(storeField.getSelectionModel().getSelectedItem().getId());
        if(setupValidation())
            generarReporteOrdenes(orderList);
    }

    public void loadOrder(Long id){

        ordersHolder.getOrderRepository().getOrdersByStore(id).subscribe(_order -> {orderList=_order;});

    }

    public void generarReporteOrdenes(List<Order> orderList){

        // date : fecha de la orden
        // startDate
        // endDate
        // date >= startDate && date <=endDate
//
//        System.out.println("INICIO: "+startDate.getValue().toString());
//        System.out.println("FIN: "+endDate.getValue().toString());

        List <Order> listaSemi= orderList.stream().filter(order->{
            try{
                LocalDate date=DateExtensions.toLocalDate(order.getCreatedAt());


                return(!startDate.getValue().isAfter(date) &&
                        !endDate.getValue().isBefore(date));

            }catch(Exception e){

                return false;
            }
        }).collect(Collectors.toList());

        System.out.println("La lista filtro por fechas tiene elementos:"+ listaSemi.size());

        List <Order> listaFinal= listaSemi.stream().filter(order->{
            try{
                System.out.println(order.getStatus().getSlug());
                return(order.getStatus().getSlug().equals(estadoCompleted) ||
                        order.getStatus().getSlug().equals(estadoReturned)) ;

            }catch(Exception e){

                return false;
            }
        }).collect(Collectors.toList());
        System.out.println("La lista filtro por fechas tiene elementos:"+ listaFinal.size());

        Stage savedStage = new Stage();
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Guardar reporte");

        File savedFile = directoryChooser.showDialog(savedStage);

        if (savedFile != null) {
            try {
                System.out.println( savedFile.getAbsolutePath());
                saveFileRoutine(savedFile, listaFinal);
            }
            catch(IOException e) {
                e.printStackTrace();

                return;
            }
            //actionStatus.setText("File saved: " + savedFile.toString());
        }
        else {
            //actionStatus.setText("File save cancelled.");
        }

    }

    public void saveFileRoutine(File savedFile, List<Order> listaFinal)throws IOException {

        File archivosXLS = new File(savedFile.getAbsolutePath() + File.separator + "ReporteDevoluciones" + ".xlsx");
        if (archivosXLS.exists()) archivosXLS.delete();

        archivosXLS.createNewFile();
        XSSFWorkbook workbook = new XSSFWorkbook();
        workbook.createSheet("hoja1");
        XSSFSheet hoja = workbook.getSheetAt(0);



        int indiceMove = -1;

        // Cabeceras
        String[] titulos = {"FECHA", "ID_PEDIDO",  "CLIENTE", "VENDEDOR", "MONTO", "MOTIVO"};
        Row encabezados = hoja.createRow(0);

        // Creamos el encabezado
        for (int i = 1; i <= titulos.length; i++) {
            Cell celda = encabezados.createCell(i);
            celda.setCellValue(titulos[i - 1]);
        }

        // Data
        System.out.println(listaFinal.size());
        //XSSFWorkbook wb = null;
        //CellStyle cellStyle = wb.createCellStyle();
        //CreationHelper createHelper = wb.getCreationHelper();
        //createHelper.createDataFormat().getFormat("m/d/yyyy");
        CellStyle cellStyle = workbook.createCellStyle();
        CreationHelper createHelper = workbook.getCreationHelper();
        cellStyle.setDataFormat(
                createHelper.createDataFormat().getFormat("dd/mm/yyyy"));

        for (int i = 1; i <= listaFinal.size(); i++) {

            Row fila = hoja.createRow(i);


            Cell cell= fila.createCell(1);
            try {
                cell.setCellValue(DateUtil.getExcelDate(listaFinal.get(indiceMove+i).getCreatedAt()));
                cell.setCellStyle(cellStyle);

            } catch (Exception e) {
                e.printStackTrace();
            }



            cell = fila.createCell(2);
            cell.setCellValue(listaFinal.get(indiceMove+i).getId());

            cell = fila.createCell(3);
            cell.setCellValue(listaFinal.get(indiceMove+i).getCustomer().getFullName());

            cell = fila.createCell(4);
            cell.setCellValue(listaFinal.get(indiceMove+i).getSeller().getFullName());

            cell = fila.createCell(5);
            cell.setCellValue(listaFinal.get(indiceMove+i).getTotal());

            cell = fila.createCell(6);
            cell.setCellValue(listaFinal.get(indiceMove+i).getCancellationReason());


        }
        FileOutputStream archivo = new FileOutputStream(archivosXLS);
        workbook.write(archivo);
        archivo.close();
        AlertDialog.showMessage(dialogContainer,"Operación exitosa","El reporte ha sido generado en la ruta especificada");



    }

}
