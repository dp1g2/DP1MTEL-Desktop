package com.dp1mtel.desktop.controller.reports;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.ItemInventoryHolder;
import com.dp1mtel.desktop.di.ShopsHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Shop;
import com.dp1mtel.desktop.reports.ItemInventory;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class InventoryController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/reports/report_Inventory.fxml";

    private ShopsHolder storeHolder = new ShopsHolder();
    @FXML
    private JFXComboBox<Shop> storeField;
    private ItemInventoryHolder itemInventoryHolder = new ItemInventoryHolder();
    @FXML
    private JFXButton btnInventory;

    @FXML
    private StackPane dialogContainer;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        loadStoreCbx();
    }

    private void loadStoreCbx() {
        storeHolder.getShopsRepository().getShops()
                .subscribeOn(Schedulers.io())
                .subscribe((_stores) -> Platform.runLater(() -> setUpCbx(_stores)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    private void setUpCbx(List<Shop> stores) {
        storeField.setItems(FXCollections.observableArrayList(stores));
    }

    @Override
    protected Node getPaneReferenceComponent() {
        return btnInventory;
    }

    public void OnCancelClicked(MouseEvent mouseEvent) {
        setContentPane(ReportsController.viewPath);
    }

    public void onReportGenerateClicked(MouseEvent mouseEvent) throws IOException {
        if (setupValidation()) {
            loadItemInventory(storeField.getSelectionModel().getSelectedItem().getId());
        }
    }

    public boolean setupValidation() {

        if (storeField.getSelectionModel().getSelectedItem() == null) {
            AlertDialog.showMessage(dialogContainer, "Error", "Debe seleccionar una tienda");
            return false;
        }
        return true;
    }

    private void loadItemInventory(Long id) {
        itemInventoryHolder.getItemsRepository().getItems(id)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (_items) -> Platform.runLater(() -> fillListWithElements(_items)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    private void fillListWithElements(List<ItemInventory> items) {
        if (items.size()==0){
            AlertDialog.showMessage(dialogContainer,"Error", "No hay stock de inventario para dicha tienda");
            return;
        }
        try {
            generarReporteInventario(items);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generarReporteInventario(List<ItemInventory> itemList) throws IOException {

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Guardar reporte");

        File savedFile = directoryChooser.showDialog(null);

        if (savedFile != null) {
            saveFileRoutine(savedFile, itemList);
        }

    }

    public void saveFileRoutine(File savedFile, List<ItemInventory> itemList) throws IOException {

        File archivosXLS = new File(savedFile.getAbsolutePath() + File.separator + "ReporteInventarios" + ".xlsx");
        if (archivosXLS.exists()) archivosXLS.delete();

        archivosXLS.createNewFile();
        XSSFWorkbook workbook = new XSSFWorkbook();
        workbook.createSheet("hoja1");
        XSSFSheet hoja = workbook.getSheetAt(0);

        int indiceItem = -1;

        // Cabeceras
        String[] titulos = {"COD PRODUCTO", "NOMBRE", "DESCRIPCIÓN", "CANTIDAD"};
        Row encabezados = hoja.createRow(0);

        // Creamos el encabezado
        for (int i = 1; i <= titulos.length; i++) {
            Cell celda = encabezados.createCell(i);
            celda.setCellValue(titulos[i - 1]);
        }

        // Data
        System.out.println(itemList.size());

        for (int i = 1; i <= itemList.size(); i++) {

            Row fila = hoja.createRow(i);

            Cell cell = fila.createCell(1);
            cell.setCellValue(itemList.get(indiceItem + i).getItem().getId());

            cell = fila.createCell(2);
            cell.setCellValue(itemList.get(indiceItem + i).getItem().getName());

            cell = fila.createCell(3);
            cell.setCellValue(itemList.get(indiceItem + i).getItem().getDescription());

            cell = fila.createCell(4);
            cell.setCellValue(itemList.get(indiceItem + i).getStock());

        }

        FileOutputStream archivo = new FileOutputStream(archivosXLS);
        workbook.write(archivo);
        archivo.close();

        AlertDialog.showMessage(dialogContainer, "Operación exitosa", "El reporte ha sido generado en la ruta especificada");

    }
}
