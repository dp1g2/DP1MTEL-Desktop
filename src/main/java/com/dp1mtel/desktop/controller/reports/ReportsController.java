package com.dp1mtel.desktop.controller.reports;

import com.dp1mtel.desktop.controller.BaseController;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class ReportsController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/reports/index.fxml";

    public void initialize(URL url, ResourceBundle resourceBundle){ super.initialize(url, resourceBundle);}

    @FXML
    private JFXButton btnVentas;
    @Override
    protected Node getPaneReferenceComponent() {
        return btnVentas;
    }

    public void onVentasClicked(MouseEvent mouseEvent) {
        setContentPane(VentasyDevController.viewPath);
    }

    public void onInventarioClicked(MouseEvent mouseEvent) {
        setContentPane(InventoryController.viewPath);
    }

    public void onPedidosClicked(MouseEvent mouseEvent) {
        setContentPane(OrdersController.viewPath);
    }



    public void onKardexClicked(MouseEvent mouseEvent)  {
        setContentPane(KardexController.viewPath);
    }
}
