package com.dp1mtel.desktop.controller;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.controller.distribution.DistributionController;
import com.dp1mtel.desktop.controller.management.ManagementController;
import com.dp1mtel.desktop.controller.discounts.DiscountsIndexController;
import com.dp1mtel.desktop.controller.reports.ReportsController;
import com.dp1mtel.desktop.controller.sales.SalesController;
import com.dp1mtel.desktop.controller.security.SecurityController;
import com.dp1mtel.desktop.controller.simulation.SimulationController;
import com.dp1mtel.desktop.controller.storage.StorageController;
import com.dp1mtel.desktop.controller.stores.StoresController;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.Permission;
import com.dp1mtel.desktop.model.User;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class AppController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/home.fxml";
    public static final String windowTitle = "MargaritaTEL";

    @FXML
    private JFXDrawer menuDrawer;
    @FXML
    private AnchorPane mainContentPane;
    @FXML
    private Label currentUserName;
    @FXML
    private Label currentUserRole;
    private VBox menu;

    @FXML private Label addressField;
    @FXML private Label districtField;

    private HamburgerSlideCloseTransition menuToggleTransition;
    private UsersHolder usersHolder = new UsersHolder();
    private User currentUser;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            menu = FXMLLoader.load(getClass().getResource("/com/dp1mtel/desktop/view/menu.fxml"));
        } catch (IOException e) {
            Logger.getLogger(App.TAG).log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
        }


        currentUser = usersHolder.getUsersRepository().getCurrentUser();
        currentUserName.setText(currentUser.getFullName());
        currentUserRole.setText(currentUser.getRole().getName());


        addressField.setText(currentUser.getShop().getAddress());
        districtField.setText(currentUser.getShop().getDistrict());

        menuDrawer.setSidePane(menu);
        menuDrawer.open();
        setupMenuItems(menu.getChildren());
    }

    private void setupMenuItems(List<Node> menuItems) {
        Set<String> userPermissions = currentUser.getRole().getPermissions().stream()
                .map(Permission::getName)
                .collect(Collectors.toSet());

        List<Node> newItems = new ArrayList<>(menuItems);
        for (Node node : newItems) {
            if (node.getAccessibleText() != null) {
                boolean visible = setMenuItemVisibility(node, userPermissions);
                if (!visible) {
                    menuItems.remove(node);
                } else {
                    setMenuItemAction(node);
                }
            }
        }
    }

    private boolean setMenuItemVisibility(Node menuItem, Set<String> userPermissions) {
        String permissionToFind;
        switch (menuItem.getAccessibleText()) {
            case "sales": {
                permissionToFind = "Ventas";
                break;
            }
            case "storage": {
                permissionToFind = "Almacen";
                break;
            }
            case "prices": {
                permissionToFind = "Precios y Descuentos";
                break;
            }
            case "stores": {
                permissionToFind = "Locales";
                break;
            }
            case "management": {
                permissionToFind = "Administracion";
                break;
            }
            case "reports": {
                permissionToFind = "Reportes";
                break;
            }
            /*
            case "users": {
                handler = () -> setContentPane(UserIndexController.viewPath);
                break;
            }
            case "roles": {
                handler = () -> setContentPane(RoleIndexController.viewPath);
                break;
            }
            */
            case "security": {
                permissionToFind = "Seguridad";
                break;
            }
            case "simulation": {
                permissionToFind = "Distribucion";
                break;
            }
            case "distribution": {
                permissionToFind = "Distribucion";
                break;
            }
            default:
                permissionToFind = null;
        }
        return permissionToFind != null && (permissionToFind.isEmpty() || userPermissions.contains(permissionToFind));
    }

    private void setMenuItemAction(Node menuItem) {
        Runnable handler;
        switch (menuItem.getAccessibleText()) {
            case "sales": {
                handler = () -> setContentPane(SalesController.viewPath);
                break;
            }
            case "storage": {
                handler = () -> setContentPane(StorageController.viewPath);
                break;
            }
            case "prices": {
                handler = () -> setContentPane(DiscountsIndexController.viewPath);
                break;
            }
            case "stores": {
                handler = () -> setContentPane(StoresController.viewPath);
                break;
            }
            case "management": {
                // TODO: Update with corresponding viewPath
                handler = () -> setContentPane(ManagementController.viewPath);
                break;
            }
            case "reports": {
                handler = () -> setContentPane(ReportsController.viewPath);
                break;
            }
            /*
            case "users": {
                handler = () -> setContentPane(UserIndexController.viewPath);
                break;
            }
            case "roles": {
                handler = () -> setContentPane(RoleIndexController.viewPath);
                break;
            }
            */
            case "security": {
                handler = () -> setContentPane(SecurityController.viewPath);
                break;
            }
            case "simulation": {
                handler = () -> setContentPane(SimulationController.viewPath);
                break;
            }
            case "distribution": {
                handler = () -> setContentPane(DistributionController.viewPath);
                break;
            }
            default:
                handler = () -> {};
        }

        menuItem.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> {
            //menuDrawer.close();
            handler.run();
        });
    }

    @Override
    public Pane getMainContentPane() {
        return mainContentPane;
    }

    public void logout() throws IOException {
        currentUserName.getScene().getWindow().hide();
        Parent root = FXMLLoader.load(getClass().getResource(LoginController.viewPath));
        Stage stage = new Stage();
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(root));
        stage.show();
    }
}
