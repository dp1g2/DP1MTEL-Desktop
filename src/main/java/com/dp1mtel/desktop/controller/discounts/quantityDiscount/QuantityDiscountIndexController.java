package com.dp1mtel.desktop.controller.discounts.quantityDiscount;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.QuantityDiscountHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.QuantityDiscount;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;

import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;

import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;



public class QuantityDiscountIndexController extends BaseController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/discounts/quantityDiscount/index.fxml";


    public static final String windowTitle = "Combo de productos";

    @FXML private FontAwesomeIconView searchBtn;
    @FXML private JFXButton btnNewDiscount;
    @FXML private JFXTreeTableView<QuantityDiscount> quantityDiscountTable;
    @FXML private JFXTreeTableColumn<QuantityDiscount, String> indexColumn;
    @FXML private JFXTreeTableColumn<QuantityDiscount, String> idColumn;
    @FXML private JFXTreeTableColumn<QuantityDiscount, String> nameColumn;
    @FXML private JFXTreeTableColumn<QuantityDiscount, String> startColumn;
    @FXML private JFXTreeTableColumn<QuantityDiscount, String> endColumn;
    @FXML private JFXTreeTableColumn<QuantityDiscount, String> productColumn;
    @FXML private JFXTreeTableColumn<QuantityDiscount, Void> actionsColumn;
    @FXML private StackPane dialogContainer;
    @FXML private JFXTextField searchField;


    private QuantityDiscountHolder quantityDiscountHolder = new QuantityDiscountHolder();

    @Override
    protected Node getPaneReferenceComponent() {
        return btnNewDiscount;
    }

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        super.initialize(url,resourceBundle);
        loadQuantityDiscount();
    }

    private void loadQuantityDiscount(){
        quantityDiscountHolder.getQuantityDiscountRepository().getQuantityDiscounts()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (quantityDiscounts) -> Platform.runLater(() -> setupQuantityDiscountTable(quantityDiscounts)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }

    private void setupQuantityDiscountTable(List<QuantityDiscount> quantityDiscounts){
        ObservableList<QuantityDiscount> quantityDiscountList = FXCollections.observableArrayList(quantityDiscounts);

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);


        idColumn.setCellValueFactory((param) -> {
            if (indexColumn.validateValue(param)) {
                return new SimpleStringProperty (param.getValue().getValue().getProductCombo().getId());
            } else {
                return indexColumn.getComputedValue(param);
            }
        });


        nameColumn.setCellValueFactory((param)->{
            if (nameColumn.validateValue(param)){
                return new SimpleStringProperty(param.getValue().getValue().getName()) ;
            } else {
                return nameColumn.getComputedValue(param);
            }
        });

        startColumn.setCellValueFactory((param)->{
            if (startColumn.validateValue(param)){
                try{

                    Date date =  dateFormat.parse(param.getValue().getValue().getStartDate());
                    return new SimpleStringProperty (User.dateFormat.format(date));

                }catch(Exception ex){
                    System.out.println(ex.getMessage());
                    return new SimpleStringProperty(User.dateFormat.format(new Date()));
                }

            } else {
                return startColumn.getComputedValue(param);
            }
        });

        endColumn.setCellValueFactory((param)->{
            if (endColumn.validateValue(param)){
                try{

                    Date date =  dateFormat.parse(param.getValue().getValue().getEndDate());
                    return new SimpleStringProperty (User.dateFormat.format(date));

                }catch(Exception ex){
                    System.out.println(ex.getMessage());
                    return new SimpleStringProperty(User.dateFormat.format(new Date()));
                }

            } else {
                return endColumn.getComputedValue(param);
            }
        });

        productColumn.setCellValueFactory((param)->{
            if (productColumn.validateValue(param)){
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getProductCombo().getName()) );
            } else {
                return productColumn.getComputedValue(param);
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<QuantityDiscount, Void>() {
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox(deleteBtn);
            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);
                pane.setAlignment(Pos.CENTER);


                deleteBtn.setOnMouseClicked((e) -> {
                    QuantityDiscount cat = quantityDiscountTable.getTreeItem(getIndex()).getValue();
                    // TODO: Display delete userModel dialog
                    System.out.println("Delete: " + cat.getName());

                    quantityDiscountHolder.getQuantityDiscountRepository().deleteQuantityDiscount(cat)
                            .subscribeOn(Schedulers.io())
                            .subscribe(
                                    (_res) -> Platform.runLater(() -> {
                                        if (_res.code() == 200) {
                                            loadQuantityDiscount();
                                            User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                            String logMensaje="ELIMINAR - Quantity Discount '"+cat.getId()+" "+cat.getName()
                                                    +"' - Usuario "+ usuario.getFullName();

                                            LOGGERAUDIT.info(logMensaje);
                                            AlertDialog.showMessage(dialogContainer,
                                                    "Operación exitosa", "Se ha borrado correctamente el descuento por cantidad");
                                        } else if (_res.code() == 400) {
                                            AlertDialog.showMessage(dialogContainer,
                                                    "Error al eliminar", "Verifique que el descuento no está en uso");
                                        }
                                    }),
                                    (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage())));

                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });

        final TreeItem<QuantityDiscount> root = new RecursiveTreeItem<>(quantityDiscountList, RecursiveTreeObject::getChildren);
        quantityDiscountTable.setRoot(root);
        quantityDiscountTable.setShowRoot(false);

        quantityDiscountTable.getColumns().setAll(indexColumn, nameColumn, productColumn, startColumn, endColumn,actionsColumn);


        searchField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                quantityDiscountTable.setPredicate(new Predicate<TreeItem<QuantityDiscount>>() {
                    @Override
                    public boolean test(TreeItem<QuantityDiscount> quantityDiscountTreeItem) {
                        Boolean flag = quantityDiscountTreeItem.getValue().getName().toLowerCase().contains(newValue.toLowerCase())||
                                quantityDiscountTreeItem.getValue().getProductCombo().getName().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });


    }


    public void onCreateDiscountClicked(MouseEvent mouseEvent) {
        setContentPane(QuantityDiscountCreateController.viewPath);
    }
}
