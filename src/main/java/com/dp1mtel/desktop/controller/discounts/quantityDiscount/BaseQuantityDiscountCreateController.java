package com.dp1mtel.desktop.controller.discounts.quantityDiscount;

import com.dp1mtel.desktop.App;
import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.ProductsHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Product;
import com.dp1mtel.desktop.model.QuantityDiscount;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.DecimalFormatter;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.dp1mtel.desktop.util.validation.DateValidator;
import com.jfoenix.controls.*;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseQuantityDiscountCreateController extends GenericBaseController<QuantityDiscount> {

    public JFXTextField nameField;
    public JFXTextField basePriceField;
    public JFXTextField realPriceField;
    public JFXButton selectProductBtn;
    public JFXTextField productField;
    public JFXComboBox<Integer> firstNumField;
    public JFXComboBox<Integer> secondNumField;
    public JFXDatePicker startField;
    public JFXDatePicker endField;
    public StackPane stackPane;
    public JFXTextField startTextField;
    public JFXTextField endTextField;
    public JFXTextField auxField;
    public JFXTextField codField;
    public JFXTextField comboNameField;

    public static ObservableList<Product> productList ;
    public static Product selectedProduct;

    private ProductsHolder productsHolder = new ProductsHolder();

    private boolean areDatesValid = false;
    private String chooseProduct = "Elija un producto";

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
        setupFields();

        productField.setText(chooseProduct);
        loadProductList();
    }

    private void loadProductList() {

        productsHolder.getProductsRepository().getProducts()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        (_products) -> Platform.runLater(() -> productList = FXCollections.observableArrayList(_products)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage())));
    }


    private void setupFields() {

        RequiredFieldValidator requiredFieldValidator = new RequiredFieldValidator();
        requiredFieldValidator.setMessage("Campo obligatorio");

        nameField.setValidators(requiredFieldValidator);
        productField.setValidators(requiredFieldValidator);


        codField.setValidators(requiredFieldValidator);
        comboNameField.setValidators(requiredFieldValidator);




        DateValidator dateValidator = new DateValidator();
        DateValidator dateValidator2 = new DateValidator();
        dateValidator.setMessage("Formato Fecha Correcta");
        dateValidator2.setMessage("Formato Fecha Correcta");

        startField.setDayCellFactory(dateValidator.getDayCellFactory());
        endField.setDayCellFactory(dateValidator.getDayCellFactory());

        startField.getEditor().disableProperty().set(true);
        endField.getEditor().disableProperty().set(true);
        comboNameField.setEditable(false);


        startField.getEditor().setVisible(false);
        endField.getEditor().setVisible(false);


        startTextField.setValidators(requiredFieldValidator,dateValidator);
        endTextField.setValidators(requiredFieldValidator,dateValidator2);


        startField.getEditor().textProperty().addListener((observable, oldValue, newValue) ->{

            try{
                LocalDate newDate = DateExtensions.toLocalDate(User.dateFormat.parse(newValue));
                if(endField.getValue()!=null && endField.getValue().isBefore(newDate)) {
                    AlertDialog.showMessage(stackPane,"Cuidado", "Revisa el rango de fechas");
                    areDatesValid=false;
                }else{

                    areDatesValid=true;
                    startTextField.setText( newValue );
                }
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        });

        endField.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            try{

                LocalDate newDate = DateExtensions.toLocalDate(User.dateFormat.parse(newValue));
                if(startField.getValue()!=null && startField.getValue().isAfter(newDate)) {
                    AlertDialog.showMessage(stackPane,"Cuidado", "Revisa el rango de fechas");
                    areDatesValid=false;
                }else{

                    areDatesValid=true;
                    endTextField.setText(newValue);
                }
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        });


        startTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            try{

                if(startTextField.getText()!=null && startTextField.getText().length()==10){

                    LocalDate newDate = DateExtensions.toLocalDate(User.dateFormat.parse(newValue));
                    LocalDate endDate =  DateExtensions.toLocalDate(User.dateFormat.parse(endTextField.getText()));

                    if( endTextField.getText()!=null && newDate.isAfter(endDate) ){
                        AlertDialog.showMessage(stackPane,"Cuidado", "Revisa el rango de fechas");
                        areDatesValid=false;
                    }else {
                        areDatesValid=true;
                    }
                }else if(startTextField.getText()!=null && startTextField.getText().length()>10){
                    AlertDialog.showMessage(stackPane, "Error", "Formato de fecha inválido");
                    areDatesValid=false;

                }
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }

        });


        endTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            try{


                if(endTextField.getText() != null && endTextField.getText().length()==10){

                    LocalDate newDate = DateExtensions.toLocalDate(User.dateFormat.parse(newValue));
                    LocalDate startDate = DateExtensions.toLocalDate(User.dateFormat.parse(startTextField.getText()));


                    if( startTextField.getText()!=null && newDate.isBefore(startDate) ){
                        AlertDialog.showMessage(stackPane,"Cuidado", "Revisa el rango de fechas");
                        areDatesValid=false;
                    }else {
                        areDatesValid=true;
                    }
                }else if(endTextField.getText()!=null && endTextField.getText().length()>10){
                    AlertDialog.showMessage(stackPane, "Error", "Formato de fecha inválido");
                    areDatesValid=false;

                }

            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }

        });
        //set value on combo box
        ObservableList<Integer> firstList = FXCollections.observableArrayList();
        ObservableList<Integer> secondList = FXCollections.observableArrayList();

        firstList.addAll(2,3);
        secondList.addAll(1,2);

        firstNumField.setItems(firstList);
        secondNumField.setItems(secondList);

        nameField.setEditable(false);
//        nameField.setDisable(true);
        nameField.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) nameField.validate();
        });


        startTextField.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) startTextField.validate();

        });

        endTextField.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) endTextField.validate();
        });


        productField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(firstNumField.getValue() != null && secondNumField.getValue()!= null){
                nameField.setText( "Desc. "+ firstNumField.getValue() + "x" +secondNumField.getValue()+" " + productField.getText());
                comboNameField.setText(firstNumField.getValue() + "x" +secondNumField.getValue()+" en " + productField.getText());
                realPriceField.setText(DecimalFormatter.formats(Double.parseDouble(  selectedProduct.getPrice())  * secondNumField.getValue()/firstNumField.getValue()));
            }
        });



        firstNumField.setOnAction(event -> {
            System.out.println("Entre al primero action");
            if(productField.getText().compareTo(chooseProduct)!=0 && secondNumField.getValue()!= null){
                nameField.setText( "Desc. "+ firstNumField.getValue() + "x" +secondNumField.getValue()+" " + productField.getText());
                comboNameField.setText(firstNumField.getValue() + "x" +secondNumField.getValue()+" en " + productField.getText());
                realPriceField.setText(DecimalFormatter.formats(Double.parseDouble(  selectedProduct.getPrice())  * secondNumField.getValue()/firstNumField.getValue()));
            }

            if(firstNumField.getValue()==2) secondNumField.getItems().remove(1);
            if(firstNumField.getValue()==3) if(secondNumField.getItems().size() ==1) secondNumField.getItems().add(2);

        });

        secondNumField.setOnAction(event -> {
            System.out.println("Entre al second action");
            if(productField.getText().compareTo(chooseProduct)!=0 && firstNumField.getValue()!= null){
                nameField.setText( "Desc. "+ firstNumField.getValue() + "x" +secondNumField.getValue()+" " + productField.getText());
                comboNameField.setText(firstNumField.getValue() + "x" +secondNumField.getValue()+" en " + productField.getText());
                realPriceField.setText(DecimalFormatter.formats(Double.parseDouble(  selectedProduct.getPrice())  * secondNumField.getValue()/firstNumField.getValue()));
            }

            if(secondNumField.getValue()==2) firstNumField.getItems().remove(0);
            if(secondNumField.getValue()==1) if(firstNumField.getItems().size()==1) firstNumField.getItems().add(0,2);


        });


    }


    public void onSelectProductClicked(MouseEvent mouseEvent) {
        try {
            JFXDialogLayout content = new JFXDialogLayout();

            Pane pane = FXMLLoader.load(getClass().getResource(SearchProductButtonController.viewPath));
            content.setBody(pane);
            JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.TOP);

            JFXButton add = new JFXButton("Elegir");
            JFXButton cancel = new JFXButton("Cancelar");

            add.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    productField.setText(selectedProduct.getName());
                    basePriceField.setText(selectedProduct.getPrice());

                    System.out.print("Nombre producto: " + selectedProduct.getName());
                    dialog.close();
                }
            });

            cancel.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    dialog.close();
                }
            });

            content.setActions(add,cancel);

            dialog.show();
        } catch (Exception ex) {
            Logger.getLogger(App.TAG).log(Level.SEVERE, "Could not load view file. " + ex.getMessage());
            ex.printStackTrace();
        }


    }

    public void goBack(){
        setContentPane(QuantityDiscountIndexController.viewPath);
    }




    public boolean validateFields (){

        if(productField.getText().equals(chooseProduct)){

            AlertDialog.showMessage(stackPane,"Error","Debe seleccionar un producto");
            return false;
        }


        if (startField.getValue() == null || endField.getValue() == null) {
            AlertDialog.showMessage(stackPane, "Error", "Revise los campos incorrectos o vacios");
            return false;
        }


        if(nameField.validate() && startTextField.validate() && endTextField.validate()
                && firstNumField.getValue() != secondNumField.getValue()
                && codField.validate() && comboNameField.validate() && areDatesValid) return true;



        AlertDialog.showMessage(stackPane,"Error","Revise los campos incorrectos o vacios");

        return false;
    }


}
