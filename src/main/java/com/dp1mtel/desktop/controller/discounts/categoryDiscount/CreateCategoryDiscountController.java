package com.dp1mtel.desktop.controller.discounts.categoryDiscount;

import com.dp1mtel.desktop.di.CategoryDiscountHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.CategoryDiscount;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;

import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;

public class CreateCategoryDiscountController extends BaseCreateCategoryDiscountController{
    public static final String viewPath = "/com/dp1mtel/desktop/view/discounts/categoryDiscount/create.fxml";

    private CategoryDiscountHolder categoryDiscountHolder = new CategoryDiscountHolder();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


    private static final Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();


    @Override
    protected Node getPaneReferenceComponent() {
        return saveBtn;
    }

    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();
    }

    public void saveDiscount(){


        CategoryDiscount categoryDiscount = new CategoryDiscount(new Long(0),
                nameField.getText() ,
                Double.parseDouble(percField.getText())/100,
                dateFormat.format(DateExtensions.toDate(startField.getValue())),
                dateFormat.format(DateExtensions.toDate( endField.getValue())),
                categoryField.getSelectionModel().getSelectedItem(),
                true
        );

        categoryDiscountHolder.getCategoryDiscountsRepository().saveCategoryDiscount(categoryDiscount)
                .subscribeOn(Schedulers.io())
                .subscribe(_response_categoryDiscount -> {
                    Platform.runLater(() -> {
                        if (_response_categoryDiscount.code() != 500) {
                            if (_response_categoryDiscount.body() != null) {

                                System.err.println(_response_categoryDiscount.body().getName());

                                User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                CategoryDiscount _cat = _response_categoryDiscount.body();

                                String logMensaje="CREACIÓN - Descuento por Categoria '"+_cat.getId()+" "+_cat.getName()+"' - Usuario "+ usuario.getFullName();

                                LOGGERAUDIT.info(logMensaje);
//                                    setupFieldValidation();
                                goBack();
                            }
                            else {
                                //Show error message that the field is already used;
                                AlertDialog.showMessage(dialogContainer,"Error al guardar","Las fechas ingresadas se cruzan con descuentos ya existentes");
                            }
                        }
                    });

                }, (e) -> {
                    Platform.runLater(() -> System.err.println(e.getLocalizedMessage()));
                });
    }



}
