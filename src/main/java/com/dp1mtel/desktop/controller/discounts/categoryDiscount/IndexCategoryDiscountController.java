package com.dp1mtel.desktop.controller.discounts.categoryDiscount;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.di.CategoryDiscountHolder;
import com.dp1mtel.desktop.di.UsersHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.CategoryDiscount;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.TreeTableColumnExtensions;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class IndexCategoryDiscountController extends BaseController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/discounts/categoryDiscount/index.fxml";
    public JFXTextField searchDisc;


    @FXML
    private JFXTreeTableView <CategoryDiscount> discountTable;
    @FXML private JFXTreeTableColumn <CategoryDiscount, String> indexColumn;
    @FXML private JFXTreeTableColumn <CategoryDiscount,String> nameColumn;
    @FXML private JFXTreeTableColumn <CategoryDiscount,String> categoryColumn;
    @FXML private JFXTreeTableColumn <CategoryDiscount,String> startDateColumn;
    @FXML private JFXTreeTableColumn <CategoryDiscount,String> endDateColumn;
    @FXML private JFXTreeTableColumn <CategoryDiscount,String> discountColumn;
    @FXML private JFXTreeTableColumn <CategoryDiscount,Void> actionsColumn;

    @FXML private StackPane dialogContainer;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


    private CategoryDiscountHolder categoryDiscountHolder = new CategoryDiscountHolder();

    private static final org.apache.logging.log4j.Logger LOGGERAUDIT= LogManager.getLogger("FileAuditAppender") ;
    private UsersHolder usersHolder = new UsersHolder();

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);

        loadTable();
    }

    private void loadTable() {
        categoryDiscountHolder.getCategoryDiscountsRepository().getCategoryDiscounts().
                subscribeOn(Schedulers.io())
                .subscribe(
                        (_discounts) -> Platform.runLater( () -> setupTable(_discounts)),
                        (e) -> Platform.runLater(() -> System.err.println(e.getLocalizedMessage()))
                );
    }

    protected void setupTable(List<CategoryDiscount> discounts) {

        ObservableList<CategoryDiscount> discountsList = FXCollections.observableArrayList(discounts);

        TreeTableColumnExtensions.makeIndexColumn(indexColumn);

        nameColumn.setCellValueFactory((param)->{
            if (nameColumn.validateValue(param)) {
                return new SimpleStringProperty(param.getValue().getValue().getName());
            } else {
                return nameColumn.getComputedValue(param);
            }
        });


        categoryColumn.setCellValueFactory((param)->{
            if (categoryColumn.validateValue(param)) {
                return new SimpleStringProperty( param.getValue().getValue().getCategory().getName() );
            } else {
                return categoryColumn.getComputedValue(param);
            }
        });

        discountColumn.setCellValueFactory((param)->{
            if (discountColumn.validateValue(param)){
                return new SimpleStringProperty(String.valueOf(param.getValue().getValue().getDiscount()*100) );
            } else {
                return discountColumn.getComputedValue(param);
            }
        });

        startDateColumn.setCellValueFactory((param)->{
            if (startDateColumn.validateValue(param)) {
                try{
                    Date startDate = dateFormat.parse(param.getValue().getValue().getStart());
                    return new SimpleStringProperty( User.dateFormat.format(startDate));
                }catch (Exception ex){
                    return new SimpleStringProperty(User.dateFormat.format(new Date()));
                }
            } else {
                return startDateColumn.getComputedValue(param);
            }
        });

        endDateColumn.setCellValueFactory((param)->{
            if (endDateColumn.validateValue(param)) {
                try{
                    Date endDate = dateFormat.parse(param.getValue().getValue().getEnd());
                    return new SimpleStringProperty( User.dateFormat.format(endDate));
                }catch (Exception ex){
                    return new SimpleStringProperty(User.dateFormat.format(new Date()));
                }
            } else {
                return endDateColumn.getComputedValue(param);
            }
        });

        actionsColumn.setCellFactory((col) -> new TreeTableCell<CategoryDiscount, Void>() {
            private final MaterialDesignIconView deleteBtn = new MaterialDesignIconView(MaterialDesignIcon.DELETE);
            private final HBox pane = new HBox( deleteBtn);
            {
                pane.getStyleClass().add("action-cell");
                pane.setSpacing(25);    
                pane.setAlignment(Pos.CENTER);


                deleteBtn.setOnMouseClicked((e) -> {
                    CategoryDiscount cat = discountTable.getTreeItem(getIndex()).getValue();
                    // TODO: Display delete userModel dialog
                    System.out.println("Delete: " + cat.getName());

                    categoryDiscountHolder.getCategoryDiscountsRepository().deleteCategoryDiscount(cat)
                            .subscribeOn(Schedulers.io())
                            .subscribe(
                                    (_res) -> Platform.runLater(() -> {
                                        if (_res.code() == 200) {
                                            loadTable();
                                            User usuario= usersHolder.getUsersRepository().getCurrentUser();
                                            String logMensaje="ELIMINAR - Category Discount '"+cat.getId()+" "+cat.getName()
                                                    +"' - Usuario "+ usuario.getFullName();
                                            LOGGERAUDIT.info(logMensaje);

                                            AlertDialog.showMessage(dialogContainer,
                                                    "Operación exitosa", "Se ha borrado correctamente el descuento por categoria");
                                        } else if (_res.code() == 400) {
                                            AlertDialog.showMessage(dialogContainer,

                                                    "Error al eliminar",  "Hubo un error. Contacte al administrador");
                                        }
                                    }),
                                    (ex) -> Platform.runLater(() -> System.err.println(ex.getMessage())));

                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                setGraphic(empty ? null : pane);
            }
        });


        final TreeItem<CategoryDiscount> root = new RecursiveTreeItem<>(discountsList , RecursiveTreeObject::getChildren);

        discountTable.setRoot(root);
        discountTable.setShowRoot(false);
        discountTable.getColumns().setAll(indexColumn,nameColumn,categoryColumn,discountColumn, startDateColumn,endDateColumn,actionsColumn);

        searchDisc.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                discountTable.setPredicate(new Predicate<TreeItem<CategoryDiscount>>() {
                    @Override
                    public boolean test(TreeItem<CategoryDiscount> categoryDiscountTreeItem) {
                        Boolean flag = categoryDiscountTreeItem.getValue().getId().toString().toLowerCase().contains(newValue.toLowerCase())||
                                categoryDiscountTreeItem.getValue().getName().toLowerCase().contains(newValue.toLowerCase())||
                                categoryDiscountTreeItem.getValue().getCategory().getName().toLowerCase().contains(newValue.toLowerCase());
                        return flag;
                    }
                });
            }
        });

    }

    @Override
    protected Node getPaneReferenceComponent() {
        return discountTable;
    }


    public void onAddDiscountClicked(MouseEvent mouseEvent) {

        setContentPane(CreateCategoryDiscountController.viewPath);
    }
}
