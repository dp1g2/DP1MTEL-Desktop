package com.dp1mtel.desktop.controller.discounts.categoryDiscount;

import com.dp1mtel.desktop.controller.GenericBaseController;
import com.dp1mtel.desktop.di.CategoriesHolder;
import com.dp1mtel.desktop.model.AlertDialog;
import com.dp1mtel.desktop.model.Category;
import com.dp1mtel.desktop.model.CategoryDiscount;
import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.dp1mtel.desktop.util.validation.MyDoubleValidator;
import com.jfoenix.controls.*;
import com.jfoenix.validation.RequiredFieldValidator;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;



public class BaseCreateCategoryDiscountController  extends GenericBaseController<CategoryDiscount> {

    public AnchorPane container;
    public StackPane dialogContainer;
    public JFXButton saveBtn;
    public JFXButton cancelBtn;
    public JFXDatePicker startField;
    public JFXDatePicker endField;
    public JFXTextField percField;
    public JFXTextField nameField;
    public JFXComboBox<Category> categoryField;

    private CategoriesHolder categoriesHolder= new CategoriesHolder();
    private Boolean areDatesValid = false;

    private double maxDiscountValue = 70.0;
    private double minDiscountValue = 1.0;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);


//        container.getStylesheets().add("/css/snackbar.css");
        loadCategories();


    }


    private void loadCategories() {
        categoriesHolder.getCategoriesRepository().getCategories()
                .subscribeOn(Schedulers.io())
                .subscribe( (_categories) ->
                                Platform.runLater(()-> setupFields(_categories)),
                        (e) -> Platform.runLater( () -> System.err.println(e.getLocalizedMessage()))

                );
    }

    private void setupFields(List<Category> categories) {
        //Combo Categoria
        categoryField.setItems(FXCollections.observableArrayList(categories));

        // la fecha final no puede ser menor a la fecha inicial
        // la fecha inicial no puede menor a la fecha de hoy.

        JFXDatePicker minDate = new JFXDatePicker();
        minDate.setValue(DateExtensions.toLocalDate(new Date())); // colocar la fecha de hoy como el minimo

        final Callback<DatePicker, DateCell> dayCellFactory;

        dayCellFactory = (final DatePicker datePicker) -> new DateCell(){
            @Override
            public void updateItem(LocalDate item, boolean empty){
                super.updateItem(item,empty);

                if(item.isBefore(minDate.getValue())){
                    setDisable(true);
                    setVisible(false);

                }else{
                    setVisible(true);
                    setDisable(false);
                }
            }

        };
        startField.setDayCellFactory(dayCellFactory);
        endField.setDayCellFactory(dayCellFactory);

        startField.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if(endField.getValue()!=null && endField.getValue().isBefore(newValue)) {
                    AlertDialog.showMessage(dialogContainer,"Error", "Verifique el rango de fechas");
                    areDatesValid=false;
                }else{
                    areDatesValid=true;
                }
            }
        });

        startField.getEditor().textProperty().addListener((observable , oldValue, newValue) -> {
            try{
                if(newValue.length()==10){
                    LocalDate newDate = DateExtensions.toLocalDate(User.dateFormat.parse(newValue));
                    System.out.println("Valor actual de END FIELD " + endField.getValue());
                    System.out.println("Valor actual de START FIELD " + startField.getValue());

                    if(endField.getValue()!=null && endField.getValue().isBefore(newDate)) {
//                        startField.getEditor().textProperty().setValue("");
                        AlertDialog.showMessage(dialogContainer,"Error", "Verifique el rango de fechas");
                        areDatesValid=false;
                    }else{
                        areDatesValid=true;
                    }

                }
            }catch(ParseException e){
                AlertDialog.showMessage(dialogContainer,"Error", "Formato de Fecha Inicial Incorrecto");

            }
        });

        endField.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {

                if(startField.getValue()!=null && startField.getValue().isAfter(newValue)) {
                    AlertDialog.showMessage(dialogContainer,"Error", "Verifique el rango de fechas");
                    areDatesValid=false;
                }else{
                    areDatesValid=true;
                }
            }
        });

        endField.getEditor().textProperty().addListener((observable , oldValue, newValue) -> {
//            JFXSnackbar snackbar = new JFXSnackbar(container);
            try{
                if(newValue.length()==10){
                    LocalDate newDate = DateExtensions.toLocalDate(User.dateFormat.parse(newValue));

                    if(startField.getValue()!=null && newDate.isBefore(startField.getValue())){
//                        endField.getEditor().textProperty().setValue("");
                        AlertDialog.showMessage(dialogContainer,"Error", "Verifique el rango de fechas");
//                        snackbar.show("Verifique el rango de fechas",20000);
                        areDatesValid=false;
                    }else{
                        areDatesValid=true;
                    }
                }
            }catch(ParseException e){
//                System.err.println(e.getLocalizedMessage());
                AlertDialog.showMessage(dialogContainer,"Error", "Formato de Fecha Final Incorrecto");
            }
        });

        RequiredFieldValidator requiredFieldValidator = new RequiredFieldValidator();
        requiredFieldValidator.setMessage("Campo obligatorio");

//

        MyDoubleValidator doubleValidator = new MyDoubleValidator();
        doubleValidator.setMessage("Valor inválido");

        percField.setValidators(requiredFieldValidator,doubleValidator);
        percField.focusedProperty().addListener((obs, old, newVal) -> {
            if(!newVal) percField.validate();
        });

        nameField.setValidators(requiredFieldValidator);
        nameField.focusedProperty().addListener((obs, old, newVal) ->{
            if(!newVal) nameField.validate();
        });
    }



    public void onSaveClicked(MouseEvent mouseEvent) {
        if(validateFields()){
            saveDiscount();
        }
    }

    private boolean validateFields() {
//        System.out.println(nameField.validate());
//        System.out.println(percField.validate());
//        System.out.println((categoryField.getSelectionModel().getSelectedItem() != null));
//        System.out.println((percField.getText() != null));
//        System.out.println(areDatesValid);
//        System.out.println((startField.getValue() != null));
//        System.out.println(endField.getValue() != null);
        if (nameField.validate() &&
                percField.validate() &&
                categoryField.getSelectionModel().getSelectedItem() != null &&
                percField.getText()!=null &&
                areDatesValid &&
                startField.getValue()!=null &&
                endField.getValue()!=null) {

            Double discount = Double.parseDouble(percField.getText());

            if(discount>maxDiscountValue || discount<minDiscountValue){
                AlertDialog.showMessage(dialogContainer, "Error", "El descuento no puede ser mayor a "+maxDiscountValue +"% ni menor a "+minDiscountValue+"%");
                return false;
            }

            if (startField.getValue().isAfter(endField.getValue())){
                AlertDialog.showMessage(dialogContainer, "Error", "Revisa el rango de fechas");
                return false;
            }

            return true;
        }

        AlertDialog.showMessage(dialogContainer, "Error", "Existen campos incorrectos o vacios");
        return false;
    }


    public void saveDiscount(){

    }

    public  void goBack(){
        setContentPane(IndexCategoryDiscountController.viewPath);
    }


}
