package com.dp1mtel.desktop.controller.discounts.quantityDiscount;


import com.dp1mtel.desktop.di.QuantityDiscountHolder;
import com.dp1mtel.desktop.model.*;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;


import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;


public class QuantityDiscountCreateController extends BaseQuantityDiscountCreateController {

    public static final String viewPath = "/com/dp1mtel/desktop/view/discounts/quantityDiscount/create.fxml";

    private QuantityDiscountHolder quantityDiscountHolder = new QuantityDiscountHolder();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);
    }

    public void onCancelClicked(MouseEvent mouseEvent) {
        goBack();

    }

    public void onSaveClicked(MouseEvent mouseEvent) {

        if (validateFields()){
            saveDiscount();
        }

    }

    private void saveDiscount() {
        QuantityDiscount quantityDiscount = new QuantityDiscount();

        quantityDiscount.setId(new Long(0));
        quantityDiscount.setName(nameField.getText());
        quantityDiscount.setStartDate(startTextField.getText());
        quantityDiscount.setEndDate(endTextField.getText());
        try{

            Date startDate = User.dateFormat.parse(startTextField.getText());
            Date endDate = User.dateFormat.parse(endTextField.getText());

            quantityDiscount.setStartDate(dateFormat.format(startDate));
            quantityDiscount.setEndDate(dateFormat.format(endDate));


        }catch (Exception ex){

            System.out.println("Error al parsear las fechas");
        }

//        quantityDiscount.setDiscount( secondNumField.getValue()/firstNumField.getValue() * 1.0);

        List<ProductComboHelp> help = new ArrayList<>();

        ProductComboHelp pch = new ProductComboHelp();
        pch.setQuantity(String.valueOf(firstNumField.getValue()) );

        if (selectedProduct == null || selectedProduct.getName().isEmpty()){
            AlertDialog.showMessage(stackPane,"Error","Debe seleccionar un producto");
            return;
        }

        pch.setProduct(selectedProduct);

        help.add(pch);

        ProductCombo productCombo = new ProductCombo();
        productCombo.setActive("true");
        productCombo.setDescription("Oferta de "+ selectedProduct.getName()  + ": Lleva " + firstNumField.getValue() + " paga " + secondNumField.getValue());
        productCombo.setId(auxField.getText() + codField.getText());
        productCombo.setName(comboNameField.getText());

        Double price = Double.parseDouble( selectedProduct.getPrice());
        productCombo.setPrice(realPriceField.getText());

        //agregar formateador decimal.
        productCombo.setListProducts(help);

        quantityDiscount.setProductCombo(productCombo);

        System.out.println("Llegue al repository");

        quantityDiscountHolder.getQuantityDiscountRepository().saveQuantityDiscount(quantityDiscount)
                .subscribeOn(Schedulers.io())
                .subscribe((_response_item) -> {
                            Platform.runLater(() -> {
                                if (_response_item.code() != 500) {
                                    if (_response_item.body() != null) {
                                        System.err.println(_response_item.body().getName());
                                        validateFields();
                                        System.out.println("Entre aqui");
                                        goBack();
                                    }
                                } else {
                                    //Show error message that the field is already used;
                                    AlertDialog.showMessage(stackPane,"Error al guardar","Contacte al Administrador");
                                }
                            });
                        },
                        (e) -> {
                            Platform.runLater(() -> System.err.println(e.getMessage()));
                        });

    }


    @Override
    protected Node getPaneReferenceComponent() {
        return nameField;
    }
}
