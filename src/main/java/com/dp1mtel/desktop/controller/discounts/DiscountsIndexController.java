package com.dp1mtel.desktop.controller.discounts;

import com.dp1mtel.desktop.controller.BaseController;
import com.dp1mtel.desktop.controller.discounts.categoryDiscount.IndexCategoryDiscountController;
import com.dp1mtel.desktop.controller.discounts.quantityDiscount.QuantityDiscountIndexController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class DiscountsIndexController extends BaseController {
    public static final String viewPath = "/com/dp1mtel/desktop/view/discounts/index.fxml";


    @FXML JFXButton categoryBtn;
    @FXML JFXButton quantityBtn;
    public void initialize(URL location, ResourceBundle resourceBundle) {
        super.initialize(location, resourceBundle);


    }

    @Override
    protected Node getPaneReferenceComponent() {
        return categoryBtn;
    }

    public void onQuantityClicked(MouseEvent mouseEvent) {
        setContentPane(QuantityDiscountIndexController.viewPath);
    }

    public void onCategoryClicked(MouseEvent mouseEvent) {
        setContentPane(IndexCategoryDiscountController.viewPath);

    }
}
