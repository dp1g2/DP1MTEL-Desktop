package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IRolesRepository
import com.dp1mtel.desktop.repository.RolesRepository
import com.dp1mtel.desktop.service.RolesService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val rolesModule = applicationContext {
   // bean { MockRolesRepository(get()) as IRolesRepository }
    bean { RolesRepository(get()) as IRolesRepository }
    bean { createRolesService(get()) }
}

fun createRolesService(retrofit: Retrofit): RolesService = retrofit.create(RolesService::class.java)

class RolesHolder : KoinComponent {
    val rolesRepository: IRolesRepository by inject()
}