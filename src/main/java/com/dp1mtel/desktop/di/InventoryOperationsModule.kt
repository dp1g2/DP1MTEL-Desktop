package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.InventoryOperationsRepository
import com.dp1mtel.desktop.repository.IInventoryOperationsRepository
import com.dp1mtel.desktop.service.InventoryOperationsService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val inventoryOperationsModule = applicationContext {
    bean { InventoryOperationsRepository(get()) as IInventoryOperationsRepository }
    bean { createInventoryOperationsService(get()) }
}

fun createInventoryOperationsService(retrofit: Retrofit):
        InventoryOperationsService = retrofit.create(InventoryOperationsService::class.java)

class InventoryOperationsHolder : KoinComponent {
    val inventoryOperationsRepository: IInventoryOperationsRepository by inject()
}
