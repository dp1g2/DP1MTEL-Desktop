package com.dp1mtel.desktop.di


import com.dp1mtel.desktop.repository.ILotsRepository
import com.dp1mtel.desktop.repository.LotsRepository
import com.dp1mtel.desktop.service.LotsService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit


val lotsModule = applicationContext {
    bean { LotsRepository(get()) as ILotsRepository }
    bean { createLotsService(get()) }
}

fun createLotsService(retrofit: Retrofit): LotsService = retrofit.create(LotsService::class.java)

class LotsHolder : KoinComponent {
    val lotsRepository: ILotsRepository by inject()
}
