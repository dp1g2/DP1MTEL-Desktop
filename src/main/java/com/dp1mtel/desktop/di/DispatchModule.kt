package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.DispatchRepository
import com.dp1mtel.desktop.repository.IDispatchRepository
import com.dp1mtel.desktop.service.DispatchService
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit

val dispatchModule = applicationContext {
    bean { DispatchRepository(get()) as IDispatchRepository }
    bean { createDispatchService(get()) }
}

fun createDispatchService(retrofit: Retrofit): DispatchService =
        retrofit.create(DispatchService::class.java)

