package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IQuantityDiscountRepository
import com.dp1mtel.desktop.repository.QuantityDiscountRepository
import com.dp1mtel.desktop.service.QuantityDiscountService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val quantityDiscountModule = applicationContext {

    bean { QuantityDiscountRepository(get()) as IQuantityDiscountRepository}
    bean { createQuantityDiscountService(get()) }
}

fun createQuantityDiscountService(retrofit: Retrofit): QuantityDiscountService= retrofit.create(QuantityDiscountService::class.java)

class QuantityDiscountHolder: KoinComponent {
    val quantityDiscountRepository: IQuantityDiscountRepository by inject()
}