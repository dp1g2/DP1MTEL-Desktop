package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IPosComboRepository
import com.dp1mtel.desktop.repository.PosComboRepository
import com.dp1mtel.desktop.service.PosComboService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val posComboModule = applicationContext {
    bean { PosComboRepository(get()) as IPosComboRepository }
    bean { createPosCombosService(get()) }
}

fun createPosCombosService(retrofit: Retrofit): PosComboService = retrofit.create(PosComboService::class.java)

class PosCombosHolder : KoinComponent {
    val posComboRepository: IPosComboRepository by inject()
}