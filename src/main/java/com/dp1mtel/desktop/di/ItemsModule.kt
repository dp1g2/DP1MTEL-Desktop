package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IItemsRepository
import com.dp1mtel.desktop.repository.ItemsRepository
import com.dp1mtel.desktop.service.ItemsService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val itemsModule = applicationContext {
    bean { ItemsRepository(get()) as IItemsRepository }
    bean { createItemsService(get()) }
}

fun createItemsService(retrofit: Retrofit): ItemsService = retrofit.create(ItemsService::class.java)

class ItemsHolder : KoinComponent {
    val itemsRepository: IItemsRepository by inject()
}
