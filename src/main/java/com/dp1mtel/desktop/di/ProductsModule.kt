package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.ProductsRepository
import com.dp1mtel.desktop.repository.IProductsRepository
import com.dp1mtel.desktop.service.ProductsService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val productsModule = applicationContext {
    bean { ProductsRepository(get()) as IProductsRepository }
    bean { createProductsService(get()) }
}

fun createProductsService(retrofit: Retrofit): ProductsService = retrofit.create(ProductsService::class.java)

class ProductsHolder : KoinComponent {
    val productsRepository: IProductsRepository by inject()
}
