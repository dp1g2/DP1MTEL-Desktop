package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.CategoriesRepository
import com.dp1mtel.desktop.repository.ICategoriesRepository
import com.dp1mtel.desktop.service.CategoriesService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val categoriesModule = applicationContext {
    bean { CategoriesRepository(get()) as ICategoriesRepository }
    bean { createCategoriesService(get()) }
}

fun createCategoriesService(retrofit: Retrofit): CategoriesService = retrofit.create(CategoriesService::class.java)

class CategoriesHolder : KoinComponent {
    val categoriesRepository: ICategoriesRepository by inject()
}
