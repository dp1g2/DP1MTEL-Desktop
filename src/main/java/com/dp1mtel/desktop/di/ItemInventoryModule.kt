package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IItemInventoryRepository
import com.dp1mtel.desktop.repository.ItemInventoryRepository
import com.dp1mtel.desktop.service.ItemInventoryService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val itemInvetoryModule = applicationContext {
    bean { ItemInventoryRepository(get()) as IItemInventoryRepository }
    bean { createItemInventoryService(get()) }
}

fun createItemInventoryService(retrofit: Retrofit): ItemInventoryService = retrofit.create(ItemInventoryService::class.java)

class ItemInventoryHolder : KoinComponent {
    val itemsRepository: IItemInventoryRepository by inject()
}