package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.CategoryDiscountsRepository
import com.dp1mtel.desktop.repository.ICategoryDiscountsRepository
import com.dp1mtel.desktop.service.CategoryDiscountsService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val categoryDiscountModule = applicationContext {
    bean { CategoryDiscountsRepository (get()) as ICategoryDiscountsRepository}
    bean { createCategoryDiscountsService( get() )}

}
    fun createCategoryDiscountsService(retrofit: Retrofit): CategoryDiscountsService = retrofit.create(CategoryDiscountsService::class.java)

    class CategoryDiscountHolder : KoinComponent {
        val categoryDiscountsRepository: ICategoryDiscountsRepository by inject()
    }




