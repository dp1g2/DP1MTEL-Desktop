package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IPermissionsRepository
import com.dp1mtel.desktop.repository.PermissionsRepository
import com.dp1mtel.desktop.repository.mock.MockPermissionsRepository
import com.dp1mtel.desktop.service.PermissionsService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val permissionsModule = applicationContext {
    //bean { MockPermissionsRepository() as IPermissionsRepository }

   bean { PermissionsRepository(get()) as IPermissionsRepository }
    bean { createPermissionsService(get()) }
}

fun createPermissionsService(retrofit: Retrofit): PermissionsService = retrofit.create(PermissionsService::class.java)

class PermissionsHolder : KoinComponent {
    val permissionsRepository: IPermissionsRepository by inject()
}