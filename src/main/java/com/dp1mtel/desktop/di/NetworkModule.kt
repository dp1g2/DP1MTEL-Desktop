package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.model.*
import com.dp1mtel.desktop.reports.ItemInventory
import com.dp1mtel.desktop.service.BaseApi
import com.dp1mtel.desktop.service.adapters.*
import com.dp1mtel.desktop.util.Point
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

val networkModule = applicationContext {
    bean { provideRetrofit(get(), get()) }
    bean { provideMoshi() }
    bean { provideHttpClient() }
}

fun provideHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
            .readTimeout(300, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
//            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
}

fun provideMoshi(): Moshi {

    //Todo: check date
    val moshi = Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
            .add(UserAdapter())
            .add(RoleAdapter())
            .add(MassiveStorageResponseAdapter())
            .add(CategoryAdapter())
            .add(ItemAdapter())
            .add(PermissionAdapter())
            .add(ProductAdapter())
            .add(ProductHelpAdapter())
            .add(ProductComboAdapter())
            .add(ProductComboHelpAdapter())
            .add(ShopAdapter())
            .add(ProductAdapter())
            .add(EmployeeAdapter())
//            .add(RouteAdapter())
            .add(Point2DAdapter())
            .add(InventoryOperationAdapter())
            .add(InventoryOperationTypesAdapter())
            .add(LotAdapter())
            .add(DistrictsAdapter())
//            .add(DistrictAdapter())
//            .add(AddressAdapter())
            .add(CustomerAdapter())
            .add(OrderDetailAdapter())
//            .add(OrderStatusAdapter())
            .add(OrderAdapter())
            .add(PosProductAdapter())
            .add(PosComboAdapter())
            .add(KotlinJsonAdapterFactory())
            .add(CategoryDiscountAdapter())
            .add(ItemInventoryAdapter())
            .add(QuantityDiscountAdapter())
            //.add(OrderStatusAdapter())
            .build()
    // Adapters created this way are cached by Moshi

    moshi.adapter(ProductHelp::class.java)
    moshi.adapter(ProductComboHelp::class.java)
    moshi.adapter(MassiveStorageResponse::class.java)
    moshi.adapter(UserAddress::class.java)
    moshi.adapter(District::class.java)
    moshi.adapter(Route::class.java)
    moshi.adapter(Point::class.java)
//    moshi.adapter(Order::class.java)
    moshi.listAdapter<User>()
    moshi.listAdapter<Role>()
    moshi.listAdapter<Permission>()
    moshi.listAdapter<Shop>()
    moshi.listAdapter<Employee>()
    moshi.listAdapter<Category>()
    moshi.listAdapter<Route>()
    moshi.listAdapter<Item>()
    moshi.listAdapter<Product>()
    moshi.listAdapter<ProductHelp>()
    moshi.listAdapter<InventoryOperationType>()
    moshi.listAdapter<InventoryOperation>()
    moshi.listAdapter<Districts>()
    moshi.listAdapter<OrderDetail>()
    moshi.listAdapter<ProductComboHelp>()
    moshi.listAdapter<Lot>()
    //moshi.listAdapter<OrderStatus>()
    moshi.listAdapter<UserAddress>()
    moshi.listAdapter<District>()
    moshi.listAdapter<PosProduct>()
    moshi.listAdapter<PosCombo>()
    moshi.listAdapter<CategoryDiscount>()
    moshi.listAdapter<ItemInventory>()
    moshi.listAdapter<Order>()

    return moshi
}

fun provideRetrofit(moshi: Moshi, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BaseApi.BASE_URL)
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
