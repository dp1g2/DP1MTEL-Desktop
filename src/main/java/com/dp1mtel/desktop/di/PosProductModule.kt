package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IPosProductRepository
import com.dp1mtel.desktop.repository.PosProductRepository
import com.dp1mtel.desktop.service.PosProductService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val posProductModule = applicationContext {
    bean { PosProductRepository(get()) as IPosProductRepository }
    bean { createPosProductService(get()) }
}

fun createPosProductService(retrofit: Retrofit): PosProductService = retrofit.create(PosProductService::class.java)

class PosProductsHolder : KoinComponent {
    val posProRepository: IPosProductRepository by inject()
}