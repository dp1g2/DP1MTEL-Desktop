package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.CustomersRepository
import com.dp1mtel.desktop.repository.ICustomersRepository
import com.dp1mtel.desktop.service.CustomersService
import com.dp1mtel.desktop.service.UsersService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val customersModule = applicationContext {
    bean { CustomersRepository(get()) as ICustomersRepository }
    bean { createCustomersService(get()) }
}

fun createCustomersService(retrofit: Retrofit): CustomersService = retrofit.create(CustomersService::class.java)

class CustomersHolder : KoinComponent {
    val customersRepository: ICustomersRepository by inject()
}
