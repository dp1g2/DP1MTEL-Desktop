package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IOrderStatusRepository
import com.dp1mtel.desktop.repository.OrderStatusRepository
import com.dp1mtel.desktop.service.OrderStatusService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val orderStatusModule = applicationContext {
    bean { OrderStatusRepository(get()) as IOrderStatusRepository }
    bean { createOrderStatusService(get()) }
}

fun createOrderStatusService(retrofit: Retrofit) : OrderStatusService = retrofit.create(OrderStatusService::class.java)

class OrderStatusHolder : KoinComponent {
    val orderStatusRepository: IOrderStatusRepository by inject()
}