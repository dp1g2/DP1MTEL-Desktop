package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.DistrictRepository
import com.dp1mtel.desktop.repository.IDistrictRepository
import com.dp1mtel.desktop.service.DistrictService
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit

val districtsModule = applicationContext {
    bean { DistrictRepository(get()) as IDistrictRepository }
    bean { createDistrictService(get()) }
}

fun createDistrictService(retrofit: Retrofit): DistrictService = retrofit.create(DistrictService::class.java)
