package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IShopsRepository
import com.dp1mtel.desktop.repository.ShopsRepository
import com.dp1mtel.desktop.repository.mock.MockShopsRepository
import com.dp1mtel.desktop.service.ShopsService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val shopsModule = applicationContext {
    //bean { MockShopsRepository(get()) as IShopsRepository }
    bean { ShopsRepository(get()) as IShopsRepository }
    bean { createShopsService(get()) }
}

fun createShopsService(retrofit: Retrofit): ShopsService = retrofit.create(ShopsService::class.java)

class ShopsHolder : KoinComponent {
    val shopsRepository: IShopsRepository by inject()
}

