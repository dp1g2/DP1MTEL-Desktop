package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IProductComboRepository
import com.dp1mtel.desktop.repository.ProductComboRepository
import com.dp1mtel.desktop.service.ProductComboService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val productComboModule = applicationContext {
    // bean { MockRolesRepository(get()) as IRolesRepository }
    bean { ProductComboRepository(get()) as IProductComboRepository}
    bean { createProductComboService(get()) }
}

fun createProductComboService(retrofit: Retrofit): ProductComboService = retrofit.create(ProductComboService::class.java)

class ProductComboHolder : KoinComponent {
    val productComboRepository: IProductComboRepository by inject()
}