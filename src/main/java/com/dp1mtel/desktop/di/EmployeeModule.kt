package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.EmployeeRepository
import com.dp1mtel.desktop.repository.IEmployeeRepository
import com.dp1mtel.desktop.service.EmployeeService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val employeeModule = applicationContext {
    // bean { MockRolesRepository(get()) as IRolesRepository }
    bean { EmployeeRepository(get()) as IEmployeeRepository}
    bean { createEmployeeService(get()) }
}

fun createEmployeeService(retrofit: Retrofit): EmployeeService = retrofit.create(EmployeeService::class.java)

class EmployeeHolder : KoinComponent{
    val employeeRepository: IEmployeeRepository by inject()
}
