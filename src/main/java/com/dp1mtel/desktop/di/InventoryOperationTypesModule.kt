package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IInventoryOperationTypeRepository
import com.dp1mtel.desktop.repository.InventoryOperationTypeRepository
import com.dp1mtel.desktop.service.InventoryOperationTypeService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val inventoryOperationTypesModule = applicationContext {
    bean { InventoryOperationTypeRepository(get()) as IInventoryOperationTypeRepository }
    bean { createInventoryOperationTypesService(get()) }
}

fun createInventoryOperationTypesService(retrofit: Retrofit):
        InventoryOperationTypeService = retrofit.create(InventoryOperationTypeService::class.java)

class InventoryOperationTypesHolder : KoinComponent {
    val InventoryOperationTypeRepository: IInventoryOperationTypeRepository by inject()
}
