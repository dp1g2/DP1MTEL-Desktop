package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IOrderRepository
import com.dp1mtel.desktop.repository.OrderRepository
import com.dp1mtel.desktop.service.OrderService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val orderModule = applicationContext {
    bean { OrderRepository(get()) as IOrderRepository }
    bean { createOrdersService(get()) }
}

fun createOrdersService(retrofit: Retrofit): OrderService = retrofit.create(OrderService::class.java)

class OrdersHolder : KoinComponent {
    val orderRepository : IOrderRepository by inject()
}