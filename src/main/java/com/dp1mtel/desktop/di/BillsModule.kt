package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.BillRepository
import com.dp1mtel.desktop.repository.IBillRepository
import com.dp1mtel.desktop.service.BillService
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit

val billsModule = applicationContext {
    bean { BillRepository(get()) as IBillRepository }
    bean { createBillService(get()) }
}

fun createBillService(retrofit: Retrofit): BillService = retrofit.create(BillService::class.java)

class BillsHolder : KoinComponent {
    val billRepository: IBillRepository by inject()
}

