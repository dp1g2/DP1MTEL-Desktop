package com.dp1mtel.desktop.di

import org.koin.standalone.StandAloneContext.startKoin

fun start() {
    startKoin(listOf(
            networkModule,
            usersModule,
            rolesModule,
            permissionsModule,
            productComboModule,
            employeeModule,
            shopsModule,
            categoriesModule,
            permissionsModule,
            dispatchModule,
            itemsModule,
            productsModule,
            inventoryOperationsModule,
            inventoryOperationTypesModule,
            lotsModule,
            customersModule,
            districtsModule,
            categoryDiscountModule,
            shopsModule,
            posProductModule,
            posComboModule,
            itemInvetoryModule,
            orderModule,
            quantityDiscountModule,
            //orderStatusModule
            billsModule
    ))
}