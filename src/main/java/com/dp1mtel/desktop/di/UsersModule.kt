package com.dp1mtel.desktop.di

import com.dp1mtel.desktop.repository.IUsersRepository
import com.dp1mtel.desktop.repository.UsersRepository
import com.dp1mtel.desktop.repository.mock.MockUsersRepository
import com.dp1mtel.desktop.service.UsersService
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Retrofit



val usersModule : Module = applicationContext {
    // Only for mock data (demo purposes)
    //bean { MockUsersRepository(get()) as IUsersRepository }

    bean { UsersRepository(get()) as IUsersRepository }
    bean { createUsersService(get()) }
}

fun createUsersService(retrofit: Retrofit): UsersService = retrofit.create(UsersService::class.java)

class UsersHolder : KoinComponent {
    val usersRepository: IUsersRepository by inject()
}