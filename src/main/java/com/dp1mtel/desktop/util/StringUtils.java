package com.dp1mtel.desktop.util;

import java.util.Arrays;
import java.util.stream.Collectors;

public class StringUtils {
    public static String capitalize(String src) {
        if (src == null) {
            return null;
        }
        // Basic split
        return Arrays.stream(src.split(" "))
                .map((s) -> {
                    String c = s.charAt(0) + "";
                    return c.toUpperCase() + s.substring(1);
                })
                .reduce("", (old, val) -> old + val);
    }
}
