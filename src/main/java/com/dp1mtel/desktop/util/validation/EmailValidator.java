package com.dp1mtel.desktop.util.validation;

import com.jfoenix.validation.base.ValidatorBase;
import javafx.scene.control.TextInputControl;

public class EmailValidator extends ValidatorBase {

    @Override
    protected void eval() {
        TextInputControl textField = (TextInputControl) srcControl.get();
        hasErrors.set(!org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(textField.getText()));
    }
}
