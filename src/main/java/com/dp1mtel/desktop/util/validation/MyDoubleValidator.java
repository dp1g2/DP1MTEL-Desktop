package com.dp1mtel.desktop.util.validation;

import com.jfoenix.validation.base.ValidatorBase;
import javafx.scene.control.TextInputControl;
import org.apache.commons.validator.routines.RegexValidator;

public class MyDoubleValidator extends ValidatorBase {

    private RegexValidator regexValidator = new RegexValidator("^\\s*(?=.*[1-9])\\d*(?:\\.\\d{1,2})?\\s*$");

    @Override
    protected void eval() {
        String text = ((TextInputControl) srcControl.get()).getText();
        hasErrors.set(!regexValidator.isValid(text));
    }
}
