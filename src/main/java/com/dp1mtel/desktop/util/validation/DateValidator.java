package com.dp1mtel.desktop.util.validation;

import com.dp1mtel.desktop.model.User;
import com.dp1mtel.desktop.util.extensions.DateExtensions;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.validation.base.ValidatorBase;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextInputControl;
import javafx.util.Callback;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;

public class DateValidator  extends ValidatorBase {

    final Callback<DatePicker, DateCell> dayCellFactory;
    JFXDatePicker minDate = new JFXDatePicker();


    //verifica que la fecha tenga un formato correcto
    //verifica que la fecha no sea menor a la de hoy.
    //nada mas.
    @Override
    protected void eval() {
        String text = ((TextInputControl) srcControl.get()).getText();

        try{

            if(text.length()==10){
                LocalDate newDate = DateExtensions.toLocalDate(User.dateFormat.parse(text));

                if (newDate.isBefore(DateExtensions.toLocalDate(new Date()))){
                    hasErrors.set(true);
                    return;
                }

                System.out.println("Fecha Ingresada: " + text);
                hasErrors.set(false);
            }
        }catch(ParseException e){
            hasErrors.set(true);
        }





    }


    public DateValidator (){
        minDate.setValue(DateExtensions.toLocalDate(new Date()));
        dayCellFactory = (final DatePicker datePicker) -> new DateCell(){
            @Override
            public void updateItem(LocalDate item, boolean empty){
                super.updateItem(item,empty);

                if(item.isBefore(minDate.getValue())){
                    setDisable(true);
                    setVisible(false);

                }else{
                    setVisible(true);
                    setDisable(false);
                }
            }

        };
    }

    public Callback<DatePicker, DateCell> getDayCellFactory() {
        return dayCellFactory;
    }
}
