package com.dp1mtel.desktop.util.validation;

import com.jfoenix.validation.base.ValidatorBase;
import javafx.scene.control.TextInputControl;
import org.apache.commons.validator.routines.RegexValidator;

public class OnlyString20Validator extends ValidatorBase {
    private RegexValidator regexValidator = new RegexValidator("^.{1,20}$");

    @Override
    protected void eval() {
        String text = ((TextInputControl) srcControl.get()).getText();
        hasErrors.set(!regexValidator.isValid(text));
    }

}
