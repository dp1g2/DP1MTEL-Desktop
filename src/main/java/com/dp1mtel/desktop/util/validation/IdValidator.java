package com.dp1mtel.desktop.util.validation;

import com.jfoenix.validation.NumberValidator;
import com.jfoenix.validation.base.ValidatorBase;
import javafx.scene.control.TextInputControl;
import org.apache.commons.validator.routines.RegexValidator;

public class IdValidator extends ValidatorBase {
    // Assumes DNI
    private RegexValidator regexValidator = new RegexValidator("^\\d{8}");

    @Override
    protected void eval() {
        String text = ((TextInputControl) srcControl.get()).getText();
        hasErrors.set(!regexValidator.isValid(text));
    }
}
