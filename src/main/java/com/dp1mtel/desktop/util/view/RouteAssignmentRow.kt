package com.dp1mtel.desktop.util.view

import com.dp1mtel.desktop.model.Route
import com.dp1mtel.desktop.model.User
import com.jfoenix.controls.JFXComboBox
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.paint.Color
import javafx.scene.shape.Line

class RouteAssignmentRow : HBox() {
    val routeColorElement: Line
        get() = this.children.first() as Line

    val routeNameElement: Label
        get() = this.children[1] as Label

    val routeDriverSelectElement: JFXComboBox<User>
        get() = this.children.last() as JFXComboBox<User>

    var routeColor: Color
        get() = routeColorElement.stroke as Color
        set(color) {
            routeColorElement.stroke = color
        }

    var routeName: String
        get() = routeNameElement.text
        set(name) {
            routeNameElement.text = name
        }

    var routeDriver: User?
        get() = routeDriverSelectElement.value
        set(driver) {
            routeDriverSelectElement.value = driver
        }

    lateinit var route: Route
}