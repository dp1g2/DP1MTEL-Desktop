package com.dp1mtel.desktop.util;

import java.text.DecimalFormat;

public class DecimalFormatter {

    public static String formats(Double valor) {
        DecimalFormat formatter = new DecimalFormat("#.##");
        String newValue = formatter.format(valor);
        return newValue.replace(',', '.');
    }
}

