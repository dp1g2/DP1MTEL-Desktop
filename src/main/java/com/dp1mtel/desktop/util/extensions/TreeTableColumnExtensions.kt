@file:JvmName("TreeTableColumnExtensions")

package com.dp1mtel.desktop.util.extensions

import com.jfoenix.controls.JFXTreeTableColumn
import javafx.scene.control.TreeTableCell
import javafx.util.Callback

fun <T, R> JFXTreeTableColumn<T, R>.makeIndexColumn() {
    this.cellFactory = Callback { col -> object : TreeTableCell<T, R>() {
        override fun updateIndex(i: Int) {
            super.updateIndex(i)
            if (isEmpty || i < 0) {
                text = null
            } else {
                text = (i + 1).toString()
            }
        }
    }}
}