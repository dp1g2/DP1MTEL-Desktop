@file:JvmName("DateExtensions")

package com.dp1mtel.desktop.util.extensions

import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.util.Date

fun Date.toLocalDate(): LocalDate = toLocalDate(ZoneId.systemDefault())

fun Date.toLocalDate(zoneId: ZoneId): LocalDate = this.toInstant().atZone(zoneId).toLocalDate()

fun LocalDate.toDate(): Date = toDate(ZoneId.systemDefault())

fun LocalDate.toDate(zoneId: ZoneId): Date = Date.from(this.atStartOfDay(zoneId).toInstant())

fun LocalTime.toDouble(): Double {
    val hour = this.hour.toDouble()
    val minute = this.minute / 60.0
    return hour + minute
}

fun LocalTime?.toDouble(): Double? {
    if (this == null) {
        return null
    }
    return this.toDouble()
}

fun Double.toLocalTime(): LocalTime {
    val hour = this.toInt()
    val minute = (this - hour) * 60
    return LocalTime.of(hour, minute.toInt())
}

fun Double?.toLocalTime(): LocalTime? {
    if (this == null) {
        return null
    }
    return this.toLocalTime()
}

var Date.localTime: LocalTime
    get() {
        return LocalTime.of(hours, minutes)
    }
    set(value) {
        hours = value.hour
        minutes = value.minute
    }