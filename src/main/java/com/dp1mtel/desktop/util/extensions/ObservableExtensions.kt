package com.dp1mtel.desktop.util.extensions

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javafx.application.Platform

inline fun <T> Observable<T>.subscribeFx(crossinline onNext: (e: T) -> Unit,
                                         crossinline onError: (e: Throwable) -> Unit): Disposable {
    return this.subscribeOn(Schedulers.io()).subscribe(
            { p -> runOnMainThread { onNext(p) }},
            { e -> runOnMainThread { onError(e) }}
    )
}

inline fun <T> Observable<T>.subscribeFx(crossinline onNext: (e: T) -> Unit): Disposable {
    return this.subscribeOn(Schedulers.io()).subscribe { p -> runOnMainThread { onNext(p) } }
}

inline fun runOnMainThread(crossinline block: () -> Unit) {
    Platform.runLater { block() }
}
