package com.dp1mtel.desktop.util

import com.sothawo.mapjfx.Coordinate
import java.awt.geom.Point2D
import kotlin.math.PI
import kotlin.math.cos

val EARTH_RADIUS = 6378 // km

fun Coordinate.addKilometers(dx: Double, dy: Double): Coordinate {
    val newLat = latitude + (dy / EARTH_RADIUS) * 180 / PI
    val newLong = longitude + (dx / EARTH_RADIUS) * 180 / PI / cos(latitude * PI / 180)
    return Coordinate(newLat, newLong)
}

fun Coordinate.toPoint2D(): Point2D = Point2D.Double(latitude, longitude)

fun Point2D.toCoordinate(): Coordinate = Coordinate(x, y)