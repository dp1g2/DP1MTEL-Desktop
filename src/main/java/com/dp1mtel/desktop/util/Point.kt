package com.dp1mtel.desktop.util

import java.awt.geom.Point2D

class Point(val id: Long, x: kotlin.Double, y: kotlin.Double) : Point2D.Double(x, y)