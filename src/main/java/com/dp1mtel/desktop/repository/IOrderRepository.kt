package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Order
import io.reactivex.Observable
import retrofit2.Response


interface IOrderRepository {
    fun saveOrder(order: Order):Observable<Response<Order>>
    fun getOrders(): Observable<List<Order>>
    fun saveMassiveOrders(listOrders: List<Order>): Observable<MassiveStorageResponse>
    fun getOrdersByStore(storeId: Long): Observable<List<Order>>
    fun reserverOrder(order: Order): Observable<Response<Order>>
    fun updateOrder(order: Order): Observable<Response<Order>>
    fun refundOrder(orderId: Long, order: Order): Observable<Response<Order>>
    fun payOrder(order: Order, reservationCode: String): Observable<Response<Order>>
}