package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.District
import io.reactivex.Observable

interface IDistrictRepository {
    fun getAllDistricts(): Observable<List<District>>
}