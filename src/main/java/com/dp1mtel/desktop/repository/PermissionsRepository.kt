package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Permission
import com.dp1mtel.desktop.service.PermissionsService
import io.reactivex.Observable

class PermissionsRepository(val permissionsService: PermissionsService) : IPermissionsRepository {
    override fun getPermissions(): Observable<List<Permission>> {
        return permissionsService.getPermissions()
    }
}