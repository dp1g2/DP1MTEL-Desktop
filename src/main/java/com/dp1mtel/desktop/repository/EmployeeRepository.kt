package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Employee
import com.dp1mtel.desktop.service.EmployeeService
import io.reactivex.Observable

class EmployeeRepository (val employeeService: EmployeeService): IEmployeeRepository {
    override fun getEmployee(): Observable<List<Employee>> {
        return employeeService.getEmployee();
    }

    override fun saveEmployee(employee: Employee): Observable<Employee> {
        return employeeService.saveEmployee(employee);
    }

    override fun getEmployeeById(id: Long): Observable<Employee> {
        return employeeService.getEmployeeById(id);
    }

    override fun deleteEmployee(id: Long): Observable<Any> {
        return employeeService.deleteEmployee(id);
    }
}