package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Customer
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.UserAddress
import io.reactivex.Observable

interface ICustomersRepository {
    fun getCustomers(): Observable<List<Customer>>
    fun getCustomerById(id: Long): Observable<Customer>
    fun saveCustomer(customer: Customer): Observable<Customer>
    fun getCustomerAddresses(id: Long): Observable<List<UserAddress>>
    fun saveMassiveCustomers(listCustomers: List<Customer>): Observable<MassiveStorageResponse>
    fun saveCustomerAddress(customerId: Long, address: UserAddress): Observable<UserAddress>
}