package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Dispatch
import com.dp1mtel.desktop.model.DispatchRequest
import com.dp1mtel.desktop.model.Route
import io.reactivex.Observable

interface IDispatchRepository {
    fun dispatchDemo(): Observable<List<Route>>
    fun dispatch(orders: DispatchRequest): Observable<List<Route>>
    fun saveDispatch(dispatch: Dispatch): Observable<Dispatch>
    fun getDispatchedRoutes(storeId: Long): Observable<List<Route>>
}
