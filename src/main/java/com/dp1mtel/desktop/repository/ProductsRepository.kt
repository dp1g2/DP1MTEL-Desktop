package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Product
import com.dp1mtel.desktop.service.ProductsService
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class ProductsRepository (val productsService: ProductsService) : IProductsRepository {
    override fun uploadImage(image: MultipartBody.Part, id: RequestBody): Observable<Response<Void>> {
        return productsService.uploadImage(image,id)
    }


    override fun saveMassiveProducts(listProducts: List<Product>): Observable<MassiveStorageResponse> {
        return productsService.saveMassiveProducts(listProducts)
    }

    //nombre,descripcion,price,active,productCategory,idItem,Cantidad
    override fun updateProductById(product: Product): Observable<Product> {
        return productsService.updateProductById(product.id, product)
    }

    override fun getProducts(): Observable<List<Product>> {
        return productsService.getProducts()
    }

    override fun saveProduct(product: Product): Observable<Response<Product>> {
        return productsService.saveProduct(product)
    }

    override fun getProductById(id: String): Observable<Product> {
        return productsService.getProductById(id)
    }

    override fun deleteProduct(id: String): Observable<Response<Void>> {
        return productsService.deleteProduct(id)
    }
}