package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.service.ItemsService
import io.reactivex.Observable
import retrofit2.Response

class ItemsRepository(val itemsService: ItemsService) : IItemsRepository {
    override fun disableItem(item: Item): Observable<Response<Void>> {
        return itemsService.disableItem(item.id,item)
    }
    override fun saveMassiveItems(listItems: List<Item>): Observable<MassiveStorageResponse> {
        return itemsService.saveMassiveItems(listItems);
    }

    override fun getItems(): Observable<List<Item>> {
        return itemsService.getItems()
    }

    override fun getItemsByName(name:String): Observable<List<Item>> {
        return itemsService.getItemsByName(name)
    }
    override fun saveItem(item: Item): Observable<Response<Item>> {
        return itemsService.saveItem(item)
    }

    override fun getItemById(id: String): Observable<Item> {
        return itemsService.getItemById(id)
    }

    override fun deleteItem(id: String): Observable<Response<Void>> {
        return itemsService.deleteItem(id)
    }
    override fun updateItemById(item: Item): Observable<Item> {
      return itemsService.updateItemById(item.id,item)
    }
}