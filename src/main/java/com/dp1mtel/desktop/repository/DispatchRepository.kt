package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Dispatch
import com.dp1mtel.desktop.model.DispatchRequest
import com.dp1mtel.desktop.model.Route
import com.dp1mtel.desktop.service.DispatchService
import io.reactivex.Observable

class DispatchRepository(val dispatchService: DispatchService) : IDispatchRepository {
    override fun dispatch(orders: DispatchRequest): Observable<List<Route>> {
        return dispatchService.dispatch(orders)
    }

    override fun dispatchDemo(): Observable<List<Route>> {
        return dispatchService.dispatchDemo()
    }

    override fun saveDispatch(dispatch: Dispatch): Observable<Dispatch> {
        return dispatchService.saveDispatch(dispatch)
    }

    override fun getDispatchedRoutes(storeId: Long): Observable<List<Route>> {
        return dispatchService.getDispatchedRoutes(storeId)
    }
}
