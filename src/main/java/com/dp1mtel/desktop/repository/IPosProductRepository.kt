package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.PosProduct
import io.reactivex.Observable


interface IPosProductRepository {
    fun getPosProducts(storeId:Long): Observable<List<PosProduct>>
}