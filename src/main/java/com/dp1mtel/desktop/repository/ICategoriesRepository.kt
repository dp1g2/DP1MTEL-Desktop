package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.MassiveStorageResponse
import io.reactivex.Observable
import retrofit2.Response

interface ICategoriesRepository {
    fun getCategories(): Observable<List<Category>>
    fun getCategoryByName(name:String) :  Observable<List<Category>>
    fun saveCategory(category: Category): Observable<Response<Category>>
    fun saveMassiveCategories(listCategories: List<Category>): Observable<MassiveStorageResponse>
    fun getCategoryById(id: String): Observable<Category>
    fun deleteCategory(id: String): Observable<Response<Void>>
    fun disableCategory(category: Category): Observable<Response<Void>>
    fun updateCategoryById(category: Category): Observable<Category>
}