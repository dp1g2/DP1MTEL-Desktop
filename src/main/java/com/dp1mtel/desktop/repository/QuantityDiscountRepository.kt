package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.QuantityDiscount
import com.dp1mtel.desktop.service.QuantityDiscountService
import io.reactivex.Observable
import retrofit2.Response


class QuantityDiscountRepository(val quantityDiscountService: QuantityDiscountService) : IQuantityDiscountRepository{
    override fun deleteQuantityDiscount(quantityDiscount: QuantityDiscount): Observable<Response<Void>> {
        return quantityDiscountService.deleteQuantityDiscount(quantityDiscount.id)
    }

    override fun getQuantityDiscounts(): Observable<List<QuantityDiscount>> {
        return quantityDiscountService.getQuantityDiscounts();
    }

    override fun saveQuantityDiscount(quantityDiscount: QuantityDiscount): Observable<Response<QuantityDiscount>> {
        return quantityDiscountService.saveQuantityDiscount(quantityDiscount)
    }
}