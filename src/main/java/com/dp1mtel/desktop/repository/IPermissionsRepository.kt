package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Permission
import io.reactivex.Observable

interface IPermissionsRepository {
    fun getPermissions(): Observable<List<Permission>>
}

