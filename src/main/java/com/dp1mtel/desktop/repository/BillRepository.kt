package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Bill
import com.dp1mtel.desktop.service.BillService
import io.reactivex.Observable
import retrofit2.Response

class BillRepository(val billService: BillService):IBillRepository {

    override fun createBill(type: String): Observable<Response<Bill>> {
        return billService.createBill(type)
    }
}