package com.dp1mtel.desktop.repository


import com.dp1mtel.desktop.model.InventoryOperation
import com.dp1mtel.desktop.model.MassiveStorageResponse
import io.reactivex.Observable

interface IInventoryOperationsRepository {
    fun getInventoryOperations(id: Long): Observable<List<InventoryOperation>>

    fun saveInventoryOperation(inventoryOperation: InventoryOperation): Observable<InventoryOperation>

    fun saveMassiveOperations(listOperations: List<InventoryOperation>): Observable<MassiveStorageResponse>

    fun getInventoryOperationById(id: Long): Observable<InventoryOperation>

    fun deleteInventoryOperation(id: Long): Observable<Any>

    fun updateInventoryOperationById(inventoryOperation: InventoryOperation): Observable<InventoryOperation>
}