package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Employee
import io.reactivex.Observable

interface IEmployeeRepository {
    fun getEmployee():Observable<List<Employee>>
    fun saveEmployee(employee: Employee): Observable<Employee>
    fun getEmployeeById(id: Long): Observable<Employee>
    fun deleteEmployee(id: Long): Observable<Any>
}