package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.service.CategoriesService
import io.reactivex.Observable
import retrofit2.Response

class CategoriesRepository(val categoriesService: CategoriesService) :ICategoriesRepository {
    override fun disableCategory(category: Category): Observable<Response<Void>> {
        return categoriesService.disableItem(category.id,category)
    }

    override fun saveMassiveCategories(listCategories: List<Category>): Observable<MassiveStorageResponse> {
            return categoriesService.saveMassiveCategories(listCategories)
    }

    override fun getCategoryByName(name: String): Observable<List<Category>> {
        return categoriesService.getCategoryByName(name)
    }

    override fun updateCategoryById(category:Category): Observable<Category> {
        return categoriesService.updateCategoryById(category.id, category)
    }

    override fun getCategories(): Observable<List<Category>> {
        return categoriesService.getCategories()
    }

    override fun saveCategory(category: Category): Observable<Response<Category>> {
        return categoriesService.saveCategory(category)
    }

    override fun getCategoryById(id: String): Observable<Category> {
       return categoriesService.getCategoryById(id)
    }

    override fun deleteCategory(id: String): Observable<Response<Void>> {
        return categoriesService.deleteCategory(id)
    }
}