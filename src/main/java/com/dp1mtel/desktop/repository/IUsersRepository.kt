package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.User
import io.reactivex.Observable

interface IUsersRepository {
    var currentUser: User?

    fun getUsers(): Observable<List<User>>

    fun saveUser(user: User): Observable<User>

    fun deleteUser(id: Long): Observable<Any>

    fun loginUser(user: String, password: String): Observable<retrofit2.Response<User>>

    fun saveMassiveUsers(listUsers: List<User>): Observable<MassiveStorageResponse>

    fun updateUserById(user : User): Observable<User>
}