package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Role
import io.reactivex.Observable
import retrofit2.Response

interface IRolesRepository {
    fun getRoles(): Observable<List<Role>>
    fun saveRole(role: Role): Observable<Role>
    fun getRoleById(id: Long): Observable<Role>
    fun deleteRole(id: Long): Observable<Response<Any>>
    fun saveMassiveRoles(listRoles: List<Role>): Observable<MassiveStorageResponse>
    fun updateRoleById(role:Role): Observable<Role>
}