package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.PosCombo
import io.reactivex.Observable

interface IPosComboRepository {
    fun getPosCombos(storeId:Long): Observable<List<PosCombo>>
}