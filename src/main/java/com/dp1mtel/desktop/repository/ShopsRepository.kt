package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Districts
import com.dp1mtel.desktop.model.Employee
import com.dp1mtel.desktop.model.Shop
import com.dp1mtel.desktop.model.User
import com.dp1mtel.desktop.service.ShopsService
import io.reactivex.Observable
import retrofit2.Response

class ShopsRepository(val shopsService: ShopsService) : IShopsRepository{
    override fun getShopById(id: Long): Observable<Shop> {
        return shopsService.getShopById(id)
    }

    override fun deleteShop(id: Long): Observable<Response<Any>>{
        return shopsService.deleteShop(id)
    }

    override fun saveShop(shop: Shop): Observable<Shop> {
        return shopsService.saveShop(shop)
    }

    override fun getShops(): Observable<List<Shop>> {
        return shopsService.getShops()
    }

    override fun getEmployees(id: Long): Observable<List<User>> {
        return shopsService.getEmployees(id)
    }

    override fun getDistricts(): Observable<List<Districts>> {
        return shopsService.getDistricts()
    }

    override fun saveEmployees(id: Long, users: List<User>): Observable<List<User>> {
        return shopsService.saveEmployees(id,users.toTypedArray())
    }

    override fun updateShopById(shop : Shop): Observable<Shop> {
        return shopsService.updateShopById(shop.id, shop)
    }
}