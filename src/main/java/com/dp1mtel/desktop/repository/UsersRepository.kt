package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.User
import com.dp1mtel.desktop.service.UsersService
import io.reactivex.Observable
import retrofit2.Response

class UsersRepository(val usersService: UsersService) : IUsersRepository {
    override fun saveMassiveUsers(listUsers: List<User>): Observable<MassiveStorageResponse> {
        return usersService.saveMassiveUsers(listUsers)
    }

    override var currentUser: User? = null

    override fun updateUserById(user : User): Observable<User> {
        return usersService.updateUserById(user.id, user)
    }

    override fun saveUser(user: User): Observable<User> {
        return usersService.saveUser(user)
    }

    override fun deleteUser(id: Long): Observable<Any> {
        return usersService.deleteUser(id)
    }

    override fun getUsers(): Observable<List<User>> {
        return usersService.getUsers()
    }

    override fun loginUser(user: String, password: String): Observable<Response<User>> {
        return usersService.loginUser(user,password)
    }


}