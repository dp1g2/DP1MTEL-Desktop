package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Lot
import com.dp1mtel.desktop.service.LotsService
import io.reactivex.Observable


class LotsRepository(val lotsService: LotsService) : ILotsRepository {
    override fun getLots(): Observable<List<Lot>> {
        return lotsService.getLots()
    }
}