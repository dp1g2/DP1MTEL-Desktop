package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.InventoryOperationType
import com.dp1mtel.desktop.service.InventoryOperationTypeService
import io.reactivex.Observable
//



class InventoryOperationTypeRepository(val inventoryOperationTypeService: InventoryOperationTypeService) :IInventoryOperationTypeRepository {
    override fun updateInventoryOperationTypeById(inventoryOperationType:InventoryOperationType): Observable<InventoryOperationType> {
        return inventoryOperationTypeService.updateInventoryOperationTypeById(inventoryOperationType.id, inventoryOperationType)
    }

    override fun getInventoryOperationTypes(): Observable<List<InventoryOperationType>> {
        return inventoryOperationTypeService.getInventoryOperationTypes()
    }

    override fun saveInventoryOperationType(inventoryOperationType: InventoryOperationType): Observable<InventoryOperationType> {
        return inventoryOperationTypeService.saveInventoryOperationType(inventoryOperationType)
    }

    override fun getInventoryOperationTypeById(id: Long): Observable<InventoryOperationType> {
        return inventoryOperationTypeService.getInventoryOperationTypeById(id)
    }

    override fun deleteInventoryOperationType(id: Long): Observable<Any> {
        return inventoryOperationTypeService.deleteInventoryOperationType(id)
    }


}