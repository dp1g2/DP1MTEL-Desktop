package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.CategoryDiscount
import com.dp1mtel.desktop.service.CategoryDiscountsService
import io.reactivex.Observable
import retrofit2.Response

class CategoryDiscountsRepository (val categoryDiscountService: CategoryDiscountsService) : ICategoryDiscountsRepository{
    override fun deleteCategoryDiscount(categoryDiscount: CategoryDiscount): Observable<Response<Void>> {
        return categoryDiscountService.deleteCategoryDiscount(categoryDiscount.id)
    }

    override fun getCategoryDiscounts(): Observable<List<CategoryDiscount>> {
        return categoryDiscountService.getCategoryDiscounts()
    }

    override fun saveCategoryDiscount(categoryDiscount: CategoryDiscount): Observable<Response<CategoryDiscount>> {
        return categoryDiscountService.saveCategoryDiscounts(categoryDiscount)
    }

    override fun updateCategoryDiscount(categoryDiscount: CategoryDiscount): Observable<Response<CategoryDiscount>> {
        return categoryDiscountService.updateCategoryDiscounts(categoryDiscount.id,categoryDiscount)
    }



}