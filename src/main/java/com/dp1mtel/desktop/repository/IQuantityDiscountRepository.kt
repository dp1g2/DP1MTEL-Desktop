package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.QuantityDiscount
import io.reactivex.Observable
import retrofit2.Response


interface IQuantityDiscountRepository {

    fun getQuantityDiscounts(): Observable<List<QuantityDiscount>>

    fun saveQuantityDiscount(quantityDiscount: QuantityDiscount) : Observable<Response<QuantityDiscount>>

    fun deleteQuantityDiscount(quantityDiscount: QuantityDiscount) : Observable<Response <Void>>
}