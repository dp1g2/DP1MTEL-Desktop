package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.ProductCombo
import com.dp1mtel.desktop.service.ProductComboService
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class ProductComboRepository (val productComboService: ProductComboService): IProductComboRepository{
    override fun uploadImage(image: MultipartBody.Part, id: RequestBody): Observable<Response<Void>> {
        return productComboService.uploadImage(image,id)
    }

    override fun saveMassiveCombos(listCombos: List<ProductCombo>): Observable<MassiveStorageResponse> {
        return productComboService.saveMassiveCombos(listCombos)
    }

    override fun updateProductComboById(productCombo: ProductCombo): Observable<ProductCombo> {
        return productComboService.updateProductComboById(productCombo.id, productCombo)
    }
    override fun getProductCombo(): Observable<List<ProductCombo>> {
        return productComboService.getProductCombo()
    }

    override fun saveProductCombo(productCombo: ProductCombo): Observable<Response<ProductCombo>> {
        return productComboService.saveProductCombo(productCombo)
    }

    override fun getProductComboById(id: String): Observable<ProductCombo> {
        return productComboService.getProductComboById(id)
    }

    override fun deleteProductCombo(id: String): Observable<Response<Void>> {
        return productComboService.deleteProductCombo(id)
    }
}