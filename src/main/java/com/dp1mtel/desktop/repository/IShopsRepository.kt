package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Districts
import com.dp1mtel.desktop.model.Employee
import com.dp1mtel.desktop.model.Shop
import com.dp1mtel.desktop.model.User
import io.reactivex.Observable
import retrofit2.Response

interface IShopsRepository {
    fun getShops(): Observable<List<Shop>>
    fun saveShop(shop: Shop): Observable<Shop>
    fun getShopById(id:Long): Observable<Shop>
    fun deleteShop(id : Long ): Observable<Response<Any>>
    fun getEmployees(id: Long): Observable<List<User>>
    fun getDistricts(): Observable<List<Districts>>
    fun saveEmployees(id: Long, users:List<User>): Observable<List<User>>
    fun updateShopById(shop: Shop): Observable<Shop>
}