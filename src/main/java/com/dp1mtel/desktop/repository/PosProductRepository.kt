package com.dp1mtel.desktop.repository;

import com.dp1mtel.desktop.model.PosProduct
import com.dp1mtel.desktop.service.PosProductService
import io.reactivex.Observable

class PosProductRepository(val posProductService : PosProductService) : IPosProductRepository{

    override fun getPosProducts(storeId:Long): Observable<List<PosProduct>> {
        return posProductService.getPosProducts(storeId)
    }


}
