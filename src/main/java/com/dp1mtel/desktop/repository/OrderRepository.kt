package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Order
import com.dp1mtel.desktop.service.OrderService
import io.reactivex.Observable
import retrofit2.Response

class OrderRepository(val orderService: OrderService) : IOrderRepository {
    override fun saveMassiveOrders(listOrders: List<Order>): Observable<MassiveStorageResponse> {
        return orderService.saveMassiveOrders(listOrders)
    }

    override fun getOrders(): Observable<List<Order>> {
        return orderService.getOrders()
    }

    override fun getOrdersByStore(storeId: Long): Observable<List<Order>> {
        return orderService.getOrdersByStore(storeId)
    }

    override fun saveOrder(order: Order): Observable<Response<Order>> {
        return orderService.createOrder(order)
    }

    override fun reserverOrder(order: Order): Observable<Response<Order>> {
        return orderService.reserveOrder(order)
    }

    override fun updateOrder(order:Order):Observable<Response<Order>>{
        return orderService.updateOrder(order.id,order);
    }

    override fun refundOrder(orderId: Long, order: Order): Observable<Response<Order>> {
        return orderService.refundOrder(orderId, order)
    }

    override fun payOrder(order: Order, reservationCode: String): Observable<Response<Order>> {
        return orderService.payOrder(order, reservationCode)
    }
}