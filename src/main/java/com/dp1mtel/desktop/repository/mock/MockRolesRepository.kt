package com.dp1mtel.desktop.repository.mock

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Role
import com.dp1mtel.desktop.repository.IPermissionsRepository
import com.dp1mtel.desktop.repository.IRolesRepository
import io.reactivex.Observable
import retrofit2.Response

class MockRolesRepository(val permissionsRepository: IPermissionsRepository): IRolesRepository {
    override fun saveMassiveRoles(listRoles: List<Role>): Observable<MassiveStorageResponse> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteRole(id: Long): Observable<Response<Any>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateRoleById(role: Role): Observable<Role> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveRole(role: Role): Observable<Role> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRoleById(id: Long): Observable<Role> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    private val _roles = mutableListOf<Role>()

    override fun getRoles(): Observable<List<Role>> {
        if (_roles.isEmpty()) {
            permissionsRepository.getPermissions().blockingSubscribe { permissions ->
                for (i in 0..5) {
                    _roles.add(
                            Role("Rol $i", "Descripcion $i", permissions)
                    )
                }
            }
        }
        return Observable.just(_roles)
    }
}