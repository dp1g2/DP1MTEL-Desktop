package com.dp1mtel.desktop.repository.mock

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.User
import com.dp1mtel.desktop.repository.IRolesRepository
import com.dp1mtel.desktop.repository.IUsersRepository
import com.dp1mtel.desktop.service.RolesService
import io.reactivex.Observable
import retrofit2.Response
import java.util.*

class MockUsersRepository(val rolesRepository: IRolesRepository) : IUsersRepository {
    override fun saveMassiveUsers(listUsers: List<User>): Observable<MassiveStorageResponse> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override var currentUser: User? = null

    override fun updateUserById(user: User): Observable<User> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveUser(user: User): Observable<User> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteUser(id: Long): Observable<Any> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    val _users = mutableListOf<User>()

    override fun getUsers(): Observable<List<User>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loginUser(user:String, password:String): Observable<Response <User> >{
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}