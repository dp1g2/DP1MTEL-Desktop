package com.dp1mtel.desktop.repository.mock

import com.dp1mtel.desktop.model.Districts
import com.dp1mtel.desktop.model.Employee
import com.dp1mtel.desktop.model.Shop
import com.dp1mtel.desktop.model.User
import com.dp1mtel.desktop.repository.IPermissionsRepository
import com.dp1mtel.desktop.repository.IShopsRepository
import io.reactivex.Observable
import retrofit2.Response

class MockShopsRepository(val permissionsRepository: IPermissionsRepository): IShopsRepository {
    override fun getShopById(id: Long): Observable<Shop> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteShop(id: Long): Observable<Response<Any>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveShop(shop: Shop): Observable<Shop> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getShops(): Observable<List<Shop>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getEmployees(id: Long): Observable<List<User>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getDistricts(): Observable<List<Districts>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveEmployees(id: Long, users: List<User>): Observable<List<User>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateShopById(shop: Shop): Observable<Shop> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}