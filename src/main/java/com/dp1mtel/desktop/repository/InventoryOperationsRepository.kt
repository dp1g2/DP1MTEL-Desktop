package com.dp1mtel.desktop.repository


import com.dp1mtel.desktop.model.InventoryOperation
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.service.InventoryOperationsService
import io.reactivex.Observable


class InventoryOperationsRepository (val inventoryOperationsService: InventoryOperationsService) :IInventoryOperationsRepository {
    override fun saveMassiveOperations(listOperations: List<InventoryOperation>): Observable<MassiveStorageResponse> {
        return inventoryOperationsService.saveMassiveOperations(listOperations)
    }

    override fun getInventoryOperations(id: Long): Observable<List<InventoryOperation>> {
       return inventoryOperationsService.getInventoryOperations(id)
    }

    override fun saveInventoryOperation(inventoryOperation: InventoryOperation): Observable<InventoryOperation> {
       return inventoryOperationsService.saveInventoryOperation(inventoryOperation )
    }

    override fun getInventoryOperationById(id: Long): Observable<InventoryOperation> {
       return inventoryOperationsService.getInventoryOperationById(id)
    }

    override fun deleteInventoryOperation(id: Long): Observable<Any> {
       return inventoryOperationsService.deleteInventoryOperation(id)
    }

    override fun updateInventoryOperationById(inventoryOperation: InventoryOperation): Observable<InventoryOperation> {
       return inventoryOperationsService.updateInventoryOperationById(inventoryOperation.id, inventoryOperation)
    }


}