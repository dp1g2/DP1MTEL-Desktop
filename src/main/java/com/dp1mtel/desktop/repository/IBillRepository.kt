package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Bill
import io.reactivex.Observable
import retrofit2.Response

interface IBillRepository {

    fun createBill(type:String): Observable<Response<Bill>>

}