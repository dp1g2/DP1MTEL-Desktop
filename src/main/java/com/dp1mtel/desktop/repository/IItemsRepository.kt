package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Category
import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.model.MassiveStorageResponse
import io.reactivex.Observable
import retrofit2.Response

interface IItemsRepository {
    fun getItems(): Observable<List<Item>>
    fun getItemsByName(name:String) : Observable<List<Item>>
    fun saveItem(item: Item): Observable<Response<Item>>

    fun saveMassiveItems(listItems: List<Item>): Observable<MassiveStorageResponse>
    fun getItemById(id: String): Observable<Item>
    fun deleteItem(id: String): Observable<Response<Void>>
    fun disableItem(item: Item) : Observable<Response<Void>>
    fun updateItemById(item: Item): Observable<Item>
}