package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Customer
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.UserAddress
import com.dp1mtel.desktop.service.CustomersService
import io.reactivex.Observable

class CustomersRepository(val customersService: CustomersService) : ICustomersRepository {
    override fun saveMassiveCustomers(listCustomers: List<Customer>): Observable<MassiveStorageResponse> {
        return customersService.saveMassiveCustomers(listCustomers)
    }

    override fun getCustomers(): Observable<List<Customer>> {
        return customersService.getCustomers()
    }

    override fun getCustomerById(id: Long): Observable<Customer> {
        return customersService.getCustomerById(id)
    }

    override fun saveCustomer(customer: Customer): Observable<Customer> {
        return customersService.saveCustomer(customer)
    }

    override fun getCustomerAddresses(id: Long): Observable<List<UserAddress>> {
        return customersService.getCustomerAddresses(id)
    }

    override fun saveCustomerAddress(customerId: Long, address: UserAddress): Observable<UserAddress> {
        return customersService.saveCustomerAddress(customerId, address)
    }
}