package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.District
import com.dp1mtel.desktop.service.DistrictService
import io.reactivex.Observable

class DistrictRepository(val districtService: DistrictService) : IDistrictRepository {
    override fun getAllDistricts(): Observable<List<District>> {
        return districtService.getAllDistricts()
    }
}