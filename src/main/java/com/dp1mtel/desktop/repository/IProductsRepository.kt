package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Item
import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Product
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.bouncycastle.ocsp.Req
import retrofit2.Response

interface IProductsRepository {
    fun getProducts(): Observable<List<Product>>
    fun saveProduct(product: Product): Observable<Response<Product>>
    fun uploadImage(image: MultipartBody.Part, id: RequestBody): Observable<Response<Void>>
    fun saveMassiveProducts(listProducts: List<Product>): Observable<MassiveStorageResponse>
    fun getProductById(id: String): Observable<Product>
    fun deleteProduct(id: String): Observable<Response<Void>>
    fun updateProductById(product: Product): Observable<Product>
}