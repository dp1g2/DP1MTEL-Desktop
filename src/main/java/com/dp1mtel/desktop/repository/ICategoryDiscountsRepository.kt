package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.CategoryDiscount
import io.reactivex.Observable
import retrofit2.Response

interface ICategoryDiscountsRepository {
    fun getCategoryDiscounts(): Observable<List<CategoryDiscount>>
    fun saveCategoryDiscount(categoryDiscount: CategoryDiscount) : Observable<Response<CategoryDiscount>>
    fun updateCategoryDiscount(categoryDiscount: CategoryDiscount) : Observable<Response<CategoryDiscount>>
    fun deleteCategoryDiscount(categoryDiscount: CategoryDiscount) : Observable<Response<Void>>
}