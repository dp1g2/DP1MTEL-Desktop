package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.OrderStatus
import com.dp1mtel.desktop.service.OrderStatusService
import io.reactivex.Observable

class OrderStatusRepository(val orderStatusService : OrderStatusService) : IOrderStatusRepository {
    override fun getAllOrderStatus(): Observable<List<OrderStatus>> {
        return orderStatusService.getAllOrderStatus()
    }
}