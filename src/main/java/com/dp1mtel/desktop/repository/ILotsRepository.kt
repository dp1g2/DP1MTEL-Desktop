package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.Lot
import io.reactivex.Observable


interface ILotsRepository {
    fun getLots(): Observable<List<Lot>>
}