package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.OrderStatus
import io.reactivex.Observable

interface IOrderStatusRepository {
    fun getAllOrderStatus():Observable<List<OrderStatus>>
}