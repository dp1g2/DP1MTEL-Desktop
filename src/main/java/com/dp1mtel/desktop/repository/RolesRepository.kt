package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.Role
import com.dp1mtel.desktop.service.RolesService
import io.reactivex.Observable
import retrofit2.Response

class RolesRepository(val rolesService: RolesService) : IRolesRepository {
    override fun saveMassiveRoles(listRoles: List<Role>): Observable<MassiveStorageResponse> {
        return rolesService.saveMassiveRoles(listRoles)
    }

    override fun updateRoleById(role: Role): Observable<Role> {
        return rolesService.updateRoleById(role.id,role)
    }

    override fun getRoles(): Observable<List<Role>> {
        return rolesService.getRoles()
    }

    override fun saveRole(role: Role): Observable<Role> {
        return rolesService.saveRole(role)
    }

    override fun getRoleById(id: Long): Observable<Role> {
        return rolesService.getRoleById(id)
    }

    override fun deleteRole(id: Long): Observable<Response<Any>> {
        return rolesService.deleteRole(id)
    }
}