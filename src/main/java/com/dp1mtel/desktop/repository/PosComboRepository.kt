package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.PosCombo
import com.dp1mtel.desktop.service.PosComboService
import io.reactivex.Observable

class PosComboRepository(val posComboService: PosComboService):IPosComboRepository {
    override fun getPosCombos(storeId: Long): Observable<List<PosCombo>> {
        return posComboService.getPosCombos(storeId)
    }


}