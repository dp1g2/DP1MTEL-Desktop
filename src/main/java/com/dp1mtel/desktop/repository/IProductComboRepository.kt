package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.MassiveStorageResponse
import com.dp1mtel.desktop.model.ProductCombo
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

interface IProductComboRepository {
    fun getProductCombo(): Observable<List<ProductCombo>>
    fun saveProductCombo(productCombo: ProductCombo): Observable<Response<ProductCombo>>
    fun uploadImage(image: MultipartBody.Part, id: RequestBody): Observable<Response<Void>>
    fun saveMassiveCombos(listCombos: List<ProductCombo>): Observable<MassiveStorageResponse>
    fun getProductComboById(id: String): Observable<ProductCombo>
    fun deleteProductCombo(id: String): Observable<Response<Void>>
    fun updateProductComboById(product: ProductCombo): Observable<ProductCombo>
}