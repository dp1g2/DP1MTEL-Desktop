package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.model.InventoryOperationType
import io.reactivex.Observable

interface IInventoryOperationTypeRepository {
    fun getInventoryOperationTypes(): Observable<List<InventoryOperationType>>

    fun saveInventoryOperationType(inventoryOperationType: InventoryOperationType): Observable<InventoryOperationType>

    fun getInventoryOperationTypeById(id: Long): Observable<InventoryOperationType>
    fun deleteInventoryOperationType(id: Long): Observable<Any>
    fun updateInventoryOperationTypeById(inventoryOperationType: InventoryOperationType): Observable<InventoryOperationType>
}