package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.reports.ItemInventory
import com.dp1mtel.desktop.service.ItemInventoryService
import io.reactivex.Observable

class ItemInventoryRepository(val itemsService: ItemInventoryService) : IItemInventoryRepository {
    override fun getItems(id:Long): Observable<List<ItemInventory>> {
        return itemsService.getItemInventory(id)
    }


}
