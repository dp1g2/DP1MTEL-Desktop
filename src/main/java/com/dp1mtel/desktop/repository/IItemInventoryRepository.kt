package com.dp1mtel.desktop.repository

import com.dp1mtel.desktop.reports.ItemInventory
import io.reactivex.Observable


interface IItemInventoryRepository {
    fun getItems(id:Long): Observable<List<ItemInventory>>
}